package cz.cuni.mff.d3s.been.hostruntime.model.events;

import java.io.Serializable;


/**
 * Run task starter event for
 * {@link cz.cuni.mff.d3s.been.hostruntime.server.daemons.EventListener}.
 */
public class InvokeTaskAndBenchmarkStarter implements Event, Serializable {
}
