package cz.cuni.mff.d3s.been.hostruntime.server.task.taskprocess;

import de.flapdoodle.embed.process.collections.Collections;
import de.flapdoodle.embed.process.config.ISupportConfig;
import de.flapdoodle.embed.process.config.process.ProcessConfig;
import de.flapdoodle.embed.process.distribution.Platform;
import de.flapdoodle.embed.process.io.IStreamProcessor;
import de.flapdoodle.embed.process.io.Slf4jLevel;
import de.flapdoodle.embed.process.io.Slf4jStreamProcessor;
import de.flapdoodle.embed.process.runtime.ProcessControl;
import de.flapdoodle.embed.process.runtime.Processes;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.Watchdog;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NotifyingExecuteWatchdog extends ExecuteWatchdog {

    public static final Logger LOG = LoggerFactory.getLogger(NotifyingExecuteWatchdog.class);

    private final TimeoutNotifier timeoutNotifier;

    private Process process;

    /**
     * Creates a new watchdog with a given timeout.
     *
     * @param timeout the timeout for the process in milliseconds. It must be
     *                greater than 0 or 'INFINITE_TIMEOUT'
     */
    public NotifyingExecuteWatchdog(long timeout, TimeoutNotifier timeoutNotifier) {
        super(timeout);
        this.timeoutNotifier = timeoutNotifier;
    }

    @Override
    public synchronized void start(Process processToMonitor) {
        this.process = processToMonitor;
        super.start(processToMonitor);
    }

    @Override
    public synchronized void timeoutOccured(Watchdog w) {
        timeoutNotifier.notifyTimeout();
        super.timeoutOccured(w);
    }

    @Override
    public synchronized void destroyProcess() {
        Platform platform = Platform.detect();
        Long pid = Processes.processId(process);
        super.destroyProcess();
        if (pid != null) {
            tryWaitForProcessTerminate(platform, pid);
            if (Processes.isProcessRunning(platform, pid)) {
                LOG.info("Process [" + pid + "] is still alive. Trying to force kill it.");
                tryToTerminateProcess(platform, pid);
            }
        } else {
            LOG.warn("Unable to detect process ID (PID). Process may be already eneded or obtaining PID is not supported on this platform.");
        }
    }

    private void tryToTerminateProcess(Platform platform, Long pid) {
        try {
            if (pid != null) {
                if (platform.isUnixLike()) {
                    unixKill(pid, 9);
                } else if (platform == Platform.Windows) {
                    windowsKill(pid);
                }
            }
        } catch (Exception e) {
            // this was the last resort .. just ignore it .. user will have to solve it manuallu :(
        }
    }

    private void tryWaitForProcessTerminate(Platform platform, Long pid) {
        try {
            for (int i = 0; i < 20; i++) {
                if (Processes.isProcessRunning(platform, pid)) {
                    LOG.info("waiting for process [" + pid + "] to terminate.");
                    Thread.sleep(1000);
                } else {
                    break;
                }
            }
            // Give some time to terminate gracefully
        } catch (InterruptedException e) {
            // ignore
        }
    }

    private boolean windowsKill(long pid) {
        ISupportConfig support = createSupportConfig(pid);
        IStreamProcessor output = new Slf4jStreamProcessor(LoggerFactory.getLogger("process [" + pid + "]"), Slf4jLevel.INFO);
        Platform platform = Platform.detect();

        return Processes.tryKillProcess(support, platform, output, pid);
    }


    private boolean unixKill(long pid, int signal) {
        ISupportConfig support = createSupportConfig(pid);
        IStreamProcessor output = new Slf4jStreamProcessor(LoggerFactory.getLogger("process [" + pid + "]"), Slf4jLevel.INFO);
        Platform platform = Platform.detect();

        if (platform.isUnixLike()) {
            return ProcessControl.executeCommandLine(support, "[kill process]",
                    new ProcessConfig(Collections.newArrayList("kill", "-" + signal, "" + pid), output));
        }
        return false;
    }

    /**
     * @param pid PID of process for which this {@link ISupportConfig} is intended
     * @deprecated in time of writing this code, there was de.flapdoodle.embed.process library in version 2.0.1
     * available in the maven repository. Once the version is updated, we can switch to
     * de.flapdoodle.embed.process.config.SupportConfig.generic() instead of creating it manually.
     */
    @Deprecated
    private ISupportConfig createSupportConfig(final Long pid) {
        return new ISupportConfig() {
            @Override
            public String getName() {
                return "PID: " + pid;
            }

            @Override
            public String getSupportUrl() {
                return ":)";
            }

            @Override
            public String messageOnException(Class<?> context, Exception exception) {
                return "We really tried out to kill the process '" + pid + "' ... But we failed :( Punish us !!!!";
            }
        };
    }
}
