package cz.cuni.mff.d3s.been.hostruntime.server.services;

import cz.cuni.mff.d3s.been.api.cluster.ClusterUuidGenerator;
import cz.cuni.mff.d3s.been.hostruntime.HostRuntimeStructures;
import cz.cuni.mff.d3s.been.hostruntime.client.Benchmarks;
import cz.cuni.mff.d3s.been.hostruntime.client.Tasks;
import cz.cuni.mff.d3s.been.model.benchmark.*;
import cz.cuni.mff.d3s.been.model.bpk.BpkId;
import cz.cuni.mff.d3s.been.model.node.NodeInfo;
import cz.cuni.mff.d3s.been.model.task.Task;
import cz.cuni.mff.d3s.been.model.task.TaskState;
import cz.cuni.mff.d3s.been.model.td.TaskDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static cz.cuni.mff.d3s.been.model.benchmark.BenchmarkState.*;

@Service
@Profile("hr-server")
public class BenchmarkService {
    private static final Logger LOG = LoggerFactory.getLogger(BenchmarkService.class);

    private static final Map<TaskState, PlanItemState> remapTaskStates = new HashMap<>();

    static {
        remapTaskStates.put(TaskState.REQUESTED, PlanItemState.RUNNING);
        remapTaskStates.put(TaskState.NOT_ENOUGH_RESOURCES, PlanItemState.RUNNING);
        remapTaskStates.put(TaskState.STARTING, PlanItemState.RUNNING);
        remapTaskStates.put(TaskState.RUNNING, PlanItemState.RUNNING);
        remapTaskStates.put(TaskState.FINISHED, PlanItemState.FINISHED);
        remapTaskStates.put(TaskState.FAILED, PlanItemState.FAILED);
        remapTaskStates.put(TaskState.TIMEOUTED, PlanItemState.FAILED);
        remapTaskStates.put(TaskState.KILLED, PlanItemState.FAILED);
    }

    @Autowired
    private Benchmarks benchmarks;

    @Autowired
    private TaskService taskService;

    @Autowired
    private Tasks tasks;

    @Autowired
    private HostRuntimeStructures structures;

    @Autowired
    private NodeInfo nodeInfo;

    @Autowired
    private ClusterUuidGenerator uuidGenerator;


    public void createNewBenchmark(String benchmarkGeneratorTaskUuid, String benchmarkUuid, BpkId bpkId) {
        Benchmark benchmark = new Benchmark();
        benchmark.setStart(new Date());
        benchmark.setGeneratorTaskUuid(benchmarkGeneratorTaskUuid);
        benchmark.setBenchmarkUuid(benchmarkUuid);
        benchmark.setBpkId(bpkId);
        benchmark.setBenchmarkState(BEING_GENERATED);

        structures.benchmarksMap().put(benchmark.getBenchmarkUuid(), benchmark);
    }

    public void prepareBenchmarkToRun(String benchmarkId, BenchmarkPlan benchmarkPlan) {
        Benchmark benchmark = benchmarks.getBenchmarkByUuid(benchmarkId);
        benchmark.setBenchmarkPlan(benchmarkPlan);
        benchmark.setBenchmarkState(REQUESTED);
        benchmark.setBenchmarkName(benchmarkPlan.getName());

        structures.benchmarksMap().put(benchmarkId, benchmark);
    }

    public void killBenchmark(String benchmarkUuid) {
        structures.startBenchmarksLock().lock();
        try {
            Benchmark benchmark = benchmarks.getBenchmarkByUuid(benchmarkUuid);
            if (benchmark.getBenchmarkPlan() != null) {
                List<TaskPlanItem> runningTaskPlans = benchmark.getBenchmarkPlan().runningTaskPlans();
                List<TaskContextPlanItem> runningTaskContextPlans = benchmark.getBenchmarkPlan().runningTaskContextPlans();
                for (TaskPlanItem item : runningTaskPlans) {
                    if (item.getTaskUuid() != null) {
                        taskService.killTask(item.getTaskUuid());
                    }
                    item.setState(PlanItemState.FAILED);
                }
                for (TaskContextPlanItem item : runningTaskContextPlans) {
                    item.setState(PlanItemState.FAILED);
                }
            }
            benchmark.setEnd(new Date());
            benchmark.setBenchmarkState(KILLED);
            structures.benchmarksMap().put(benchmark.getBenchmarkUuid(), benchmark);
        } finally {
            structures.startBenchmarksLock().unlock();
        }
    }

    public void startPreparedBenchmarks() {
        LOG.debug("Trying to start benchmark restarter on node [" + nodeInfo.getUuid() + "]");
        if (structures.startBenchmarksLock().tryLock()) {
            try {
                for (Benchmark benchmark : benchmarks.listByStates(REQUESTED, RUNNING)) {
                    processBenchmark(benchmark);
                    structures.benchmarksMap().put(benchmark.getBenchmarkUuid(), benchmark);
                }
            } finally {
                structures.startBenchmarksLock().unlock();
            }
        } else {
            LOG.debug("Benchmarks starter was not started on node [" + nodeInfo.getUuid() + "]. Couldn't acquire lock which " +
                    "means that another starter is in progress");
        }
    }

    private void processBenchmark(Benchmark benchmark) {
        if (benchmark.getBenchmarkState() == REQUESTED) {
            benchmark.setBenchmarkState(RUNNING);
        }
        BenchmarkPlan benchmarkPlan = benchmark.getBenchmarkPlan();
        List<TaskContextPlanItem> contextPlanItems = benchmarkPlan.getTaskContextPlanItems();

        if (contextPlanItems.isEmpty()) {
            benchmark.setBenchmarkState(BenchmarkState.FINISHED);
            benchmark.setEnd(new Date());
            return;
        }

        updateTaskStates(benchmark); // just update task state
        updateContextStates(benchmark); // update task contexts
        updateBenchmarkState(benchmark);
        if (benchmark.getBenchmarkState() == FINISHED || benchmark.getBenchmarkState() == FAILED) {
            return;
        }

        runNextPartIfPossible(benchmark);
    }

    private void runNextPartIfPossible(Benchmark benchmark) {
        BenchmarkPlan benchmarkPlan = benchmark.getBenchmarkPlan();
        List<TaskContextPlanItem> contextPlanItems = benchmarkPlan.getTaskContextPlanItems();
        boolean allFinishedOrFailed = contextPlanItems.stream()
                .filter(t -> t.getState() == PlanItemState.FAILED || t.getState() == PlanItemState.FINISHED)
                .count() == contextPlanItems.size();

        if (allFinishedOrFailed) {
            if (benchmark.getBenchmarkPlan().getFinalizerTaskPlanItem() != null) {
                if (benchmark.getBenchmarkPlan().getFinalizerTaskPlanItem().getState() == PlanItemState.RUNNING
                        || benchmark.getBenchmarkPlan().getFinalizerTaskPlanItem().getState() == PlanItemState.FINISHED
                        || benchmark.getBenchmarkPlan().getFinalizerTaskPlanItem().getState() == PlanItemState.FAILED) {
                    return;
                    //has been run already
                }
                // ok I can run benchmark finalizer
                TaskDescriptor taskDescriptor = benchmark.getBenchmarkPlan().getFinalizerTaskPlanItem().getTaskDescriptor();
                BpkId bpkId = benchmark.getBpkId();
                String taskContextUuid = "BENCHMARK-FINALIZER";
                String finalizerTaskId = taskService.createTask(bpkId, taskDescriptor, taskContextUuid, benchmark.getBenchmarkUuid());
                benchmark.getBenchmarkPlan().getFinalizerTaskPlanItem().setTaskContextUuid(taskContextUuid);
                benchmark.getBenchmarkPlan().getFinalizerTaskPlanItem().setTaskUuid(finalizerTaskId);
                benchmark.getBenchmarkPlan().getFinalizerTaskPlanItem().setState(PlanItemState.RUNNING);
                return;
            } else {
                // nothing to run
                return;
            }
        }

        TaskContextPlanItem lastRunning = null;
        for (TaskContextPlanItem contextPlanItem : contextPlanItems) {
            if (contextPlanItem.getState() == PlanItemState.RUNNING) {
                lastRunning = contextPlanItem;
                break;
            }
        }

        if (lastRunning != null) {
            boolean finished = lastRunning.getTasksPlanItems().stream().filter(t -> t.getState() == PlanItemState.FINISHED).count() == lastRunning.getTasksPlanItems().size();
            boolean hasFinalizer = lastRunning.getFinalizerTaskPlanItem() != null;

            if (finished && hasFinalizer) {
                if (lastRunning.getFinalizerTaskPlanItem().getState() == PlanItemState.RUNNING) {
                    // finalizer still running
                    return;
                } else if (lastRunning.getFinalizerTaskPlanItem().getState() == PlanItemState.PLANNED) {
                    // ok I can run context finalizer
                    TaskDescriptor taskDescriptor = lastRunning.getFinalizerTaskPlanItem().getTaskDescriptor();
                    BpkId bpkId = benchmark.getBpkId();
                    String taskContextUuid = lastRunning.getTaskContextUuid();
                    String finalizerTaskId = taskService.createTask(bpkId, taskDescriptor, taskContextUuid, benchmark.getBenchmarkUuid());
                    lastRunning.getFinalizerTaskPlanItem().setTaskContextUuid(taskContextUuid);
                    lastRunning.getFinalizerTaskPlanItem().setTaskUuid(finalizerTaskId);
                    lastRunning.getFinalizerTaskPlanItem().setState(PlanItemState.RUNNING);
                    return;
                }
            } else {
                // still some tasks running
                return;
            }
        }

        if (lastRunning != null) {
            return;
        }

        TaskContextPlanItem nextContextPlan = null;
        for (TaskContextPlanItem contextPlanItem : contextPlanItems) {
            if (contextPlanItem.getState() == PlanItemState.PLANNED) {
                nextContextPlan = contextPlanItem;
                break;
            }
        }

        if (nextContextPlan != null) {
            nextContextPlan.setState(PlanItemState.RUNNING);
            String taskContextUuid = uuidGenerator.generate();
            nextContextPlan.setTaskContextUuid(taskContextUuid);
            for (TaskPlanItem taskPlan : nextContextPlan.getTasksPlanItems()) {
                taskPlan.setState(PlanItemState.RUNNING);
                TaskDescriptor taskDescriptor = taskPlan.getTaskDescriptor();
                BpkId bpkId = benchmark.getBpkId();
                String taskId = taskService.createTask(bpkId, taskDescriptor, taskContextUuid, benchmark.getBenchmarkUuid());
                taskPlan.setTaskContextUuid(taskContextUuid);
                taskPlan.setTaskUuid(taskId);
            }
        }
    }

    private void updateBenchmarkState(Benchmark benchmark) {
        BenchmarkPlan benchmarkPlan = benchmark.getBenchmarkPlan();
        if (benchmark.getBenchmarkPlan().getFinalizerTaskPlanItem() != null) {
            final PlanItemState finalizerState = benchmark.getBenchmarkPlan().getFinalizerTaskPlanItem().getState();
            if (finalizerState == PlanItemState.FINISHED) {
                benchmark.setBenchmarkState(FINISHED);
                benchmark.setEnd(new Date());
            } else if (finalizerState == PlanItemState.FAILED) {
                benchmark.setBenchmarkState(FAILED);
                benchmark.setEnd(new Date());
            }
        }

        List<TaskContextPlanItem> contextPlanItems = benchmarkPlan.getTaskContextPlanItems();
        boolean finishedOrFailed = contextPlanItems.stream()
                .filter(t -> t.getState() == PlanItemState.FAILED || t.getState() == PlanItemState.FINISHED)
                .count() == contextPlanItems.size();

        if (finishedOrFailed) {
            if (benchmark.getBenchmarkPlan().getFinalizerTaskPlanItem() == null) {
                benchmark.setBenchmarkState(BenchmarkState.FINISHED);
                benchmark.setEnd(new Date());
            } else {
                if (benchmark.getBenchmarkPlan().getFinalizerTaskPlanItem().getState() == PlanItemState.FAILED
                        || benchmark.getBenchmarkPlan().getFinalizerTaskPlanItem().getState() == PlanItemState.FINISHED) {
                    benchmark.setBenchmarkState(BenchmarkState.FINISHED);
                    benchmark.setEnd(new Date());
                }
            }
        }
    }

    private void updateContextStates(Benchmark benchmark) {
        BenchmarkPlan benchmarkPlan = benchmark.getBenchmarkPlan();
        List<TaskContextPlanItem> contextPlanItems = benchmarkPlan.getTaskContextPlanItems();

        for (TaskContextPlanItem plan : contextPlanItems) {
            if (plan.getState() != PlanItemState.RUNNING) {
                // no need to process, already finished
                continue;
            }

            boolean failed = plan.getTasksPlanItems().stream().filter(t -> t.getState() == PlanItemState.FAILED).count() > 0;
            boolean finished = plan.getTasksPlanItems().stream().filter(t -> t.getState() == PlanItemState.FINISHED).count() == plan.getTasksPlanItems().size();

            if (failed) {
                for (TaskPlanItem taskPlanItem : plan.getTasksPlanItems()) {
                    taskService.killTask(taskPlanItem.getTaskUuid());
                }
                plan.setState(PlanItemState.FAILED);
            } else if (finished && plan.getFinalizerTaskPlanItem() == null) {
                plan.setState(PlanItemState.FINISHED);
            } else if (finished && plan.getFinalizerTaskPlanItem() != null) {
                boolean finalizerFailed = plan.getFinalizerTaskPlanItem().getState() == PlanItemState.FAILED;
                boolean finalizerFinished = plan.getFinalizerTaskPlanItem().getState() == PlanItemState.FINISHED;
                if (finalizerFailed) {
                    plan.setState(PlanItemState.FAILED);
                } else if (finalizerFinished) {
                    plan.setState(PlanItemState.FINISHED);
                } else {
                    // fianlizerFailed == false && fianlizerFinished == false => finalizer still running
                }
            } else {
                // failed == false && finished == false => task still running
            }
        }
    }

    void updateTaskStates(Benchmark benchmark) {
        BenchmarkPlan benchmarkPlan = benchmark.getBenchmarkPlan();
        List<TaskContextPlanItem> contextPlanItems = benchmarkPlan.getTaskContextPlanItems();

        for (TaskContextPlanItem contextPlanItem : contextPlanItems) {
            List<TaskPlanItem> taskPlanItems = contextPlanItem.getTasksPlanItems();
            for (TaskPlanItem taskPlanItem : taskPlanItems) {
                if (taskPlanItem.getState() == PlanItemState.RUNNING) {
                    Task task = tasks.getTaskByUuid(taskPlanItem.getTaskUuid());
                    taskPlanItem.setState(remapTaskStates.get(task.getTaskState()));
                }
            }
            TaskPlanItem finalizerPlanItem = contextPlanItem.getFinalizerTaskPlanItem();
            if (finalizerPlanItem != null && finalizerPlanItem.getState() == PlanItemState.RUNNING) {
                Task finalizer = tasks.getTaskByUuid(finalizerPlanItem.getTaskUuid());
                finalizerPlanItem.setState(remapTaskStates.get(finalizer.getTaskState()));
            }
        }
        TaskPlanItem benchmarkFinalizerPlanItem = benchmarkPlan.getFinalizerTaskPlanItem();
        if (benchmarkFinalizerPlanItem != null && benchmarkFinalizerPlanItem.getState() == PlanItemState.RUNNING) {
            Task finalizer = tasks.getTaskByUuid(benchmarkFinalizerPlanItem.getTaskUuid());
            benchmarkFinalizerPlanItem.setState(remapTaskStates.get(finalizer.getTaskState()));
        }
    }
}