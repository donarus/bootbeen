package cz.cuni.mff.d3s.been.hostruntime.server.task.taskprocess;

import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.ExecuteException;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class TaskProcessExecutionHandler extends DefaultExecuteResultHandler {

    private static ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

    private TaskProcessEventHandler handler;
    private TaskProcessCleaner cleaner;
    private ScheduledFuture updater;
    private volatile boolean killedByUser;
    private volatile boolean timeouted;

    public TaskProcessExecutionHandler(TaskProcessEventHandler handler, TaskProcessCleaner cleaner) {
        this.handler = handler;
        this.cleaner = cleaner;
        this.updater = scheduleUpdater();
    }

    private ScheduledFuture<?> scheduleUpdater() {
        return executor.scheduleAtFixedRate(() -> {
            handler.onTaskRunning();
        }, 0, 15, TimeUnit.SECONDS);
    }

    @Override
    public void onProcessComplete(int exitValue) {
        super.onProcessComplete(exitValue);
        try {
            updater.cancel(true);
            handler.onTaskFinished(exitValue);
        } finally {
            cleaner.clean();
        }
    }

    @Override
    public void onProcessFailed(ExecuteException e) {
        super.onProcessFailed(e);
        try {
            updater.cancel(true);
            if (killedByUser) {
                handler.onTaskKilled();
            } else if (timeouted) {
                handler.onTaskTimeouted();
            } else {
                handler.onTaskFailed(e.getExitValue());
            }
        } finally {
            cleaner.clean();
        }
    }

    public void setKilledByUserFlag() {
        this.killedByUser = true;
    }

    public void setTimeouted() {
        this.timeouted = true;
    }

    public interface TaskProcessEventHandler {
        void onTaskRunning();

        void onTaskStarted();

        void onTaskFinished(int exitValue);

        void onTaskFailed(int exitValue);

        void onTaskTimeouted();

        void onTaskKilled();
    }

    public interface TaskProcessCleaner {
        void clean();
    }

}
