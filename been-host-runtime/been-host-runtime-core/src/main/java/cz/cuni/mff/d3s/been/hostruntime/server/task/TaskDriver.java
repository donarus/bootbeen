package cz.cuni.mff.d3s.been.hostruntime.server.task;

import cz.cuni.mff.d3s.been.api.clients.HostRuntimeClient;
import cz.cuni.mff.d3s.been.api.clients.SoftwareRepositoryClient;
import cz.cuni.mff.d3s.been.api.clients.SoftwareRepositoryException;
import cz.cuni.mff.d3s.been.hostruntime.server.services.TaskService;
import cz.cuni.mff.d3s.been.hostruntime.server.task.cmdline.CmdLineBuilderException;
import cz.cuni.mff.d3s.been.hostruntime.server.task.cmdline.CmdLineBuilderFactory;
import cz.cuni.mff.d3s.been.hostruntime.server.task.cmdline.TaskCommandLine;
import cz.cuni.mff.d3s.been.hostruntime.server.task.cmdline.UnknownTaskDescriptorTypeException;
import cz.cuni.mff.d3s.been.hostruntime.server.task.taskprocess.TaskProcess;
import cz.cuni.mff.d3s.been.hostruntime.server.task.taskprocess.TaskProcessException;
import cz.cuni.mff.d3s.been.hostruntime.server.task.taskprocess.TaskProcessExecutionHandler;
import cz.cuni.mff.d3s.been.model.benchmark.BenchmarkPlan;
import cz.cuni.mff.d3s.been.model.bpk.Bpk;
import cz.cuni.mff.d3s.been.model.node.NodeInfo;
import cz.cuni.mff.d3s.been.model.task.Task;
import cz.cuni.mff.d3s.been.model.task.TaskState;
import cz.cuni.mff.d3s.been.model.td.TaskDescriptor;
import cz.cuni.mff.d3s.been.model.td.TaskType;
import cz.cuni.mff.d3s.been.taskapi.messaging.MessagingServer;
import cz.cuni.mff.d3s.been.utils.BeenProperties;
import cz.cuni.mff.d3s.been.utils.bpk.BpkException;
import cz.cuni.mff.d3s.been.utils.bpk.BpkLoader;
import cz.cuni.mff.d3s.been.utils.json.JsonException;
import cz.cuni.mff.d3s.been.utils.json.JsonUtils;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.util.*;

import static cz.cuni.mff.d3s.been.hostruntime.server.task.taskprocess.TaskProcessExecutionHandler.TaskProcessCleaner;
import static cz.cuni.mff.d3s.been.hostruntime.server.task.taskprocess.TaskProcessExecutionHandler.TaskProcessEventHandler;
import static cz.cuni.mff.d3s.been.taskapi.TaskApiConstants.SystemEnv.*;
import static java.util.concurrent.TimeUnit.SECONDS;

@Service
@Profile("hr-server")
public class TaskDriver {

    private static final Logger LOG = LoggerFactory.getLogger(TaskDriver.class);

    private static final String BEEN_TASKS_HOME_PROPKEY = "been.tasks.home";

    private static final String DEFAULT_TASKS_HOME_DIRNAME = "tasks";

    @Autowired
    private SoftwareRepositoryClient softwareRepositoryClient;

    @Autowired
    private NodeInfo nodeInfo;

    @Autowired
    private HostRuntimeClient hostRuntimeClient;

    @Autowired
    private TaskService taskService;

    @Autowired
    private BeenProperties beenProperties;

    @Autowired
    private SharedVariables sharedVariables;

    @Autowired
    private TaskMessageServerRequestHandlers requestHandlers;

    private File tasksDirectory;

    private final Map<String, TaskProcess> localTaskProcesses = Collections.synchronizedMap(new HashMap<>());

    @PostConstruct
    private void setup() {
        this.tasksDirectory = new File(beenProperties.getProperty(BEEN_TASKS_HOME_PROPKEY, new File(DEFAULT_TASKS_HOME_DIRNAME).getAbsolutePath()));
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            for (TaskProcess taskProcess : localTaskProcesses.values()) {
                taskProcess.kill();
            }
            LOG.info("Shutting down 0MQ messaging context");
        }));
    }

    public void runTask(Task task) throws TaskDriverException {
        List<Cleaner> cleaners = new ArrayList<>();
        try {
            task.setTaskStartTime(new Date());
            runTask(task, cleaners);
        } catch (TaskDriverException e) {
            task.setTaskState(TaskState.FAILED);
            throw e;
        } finally {

            task.setTaskEndTime(new Date());
            for (Cleaner cleaner : cleaners) {
                try {
                    cleaner.doCleanup();
                } catch (Exception e) {
                    LOG.warn("Cleaner after task execution unexpectedly failed.", e);
                }
            }
        }
    }

    public void killTaskOnThisNode(Task task) {
        TaskProcess taskProcess = localTaskProcesses.get(task.getTaskUuid());
        if (taskProcess != null) {
            taskProcess.kill();
        } else {
            LOG.warn("Task [" + task.getTaskUuid() + "] can't be killed on node [" + nodeInfo.getUuid() + "] because it seems that it is not running here.");
        }
    }

    private void runTask(Task task, List<Cleaner> cleaners) throws TaskDriverException {
        File taskDirectory = createTaskDirectory(task.getTaskUuid());
        File extractedBpkDirectory = createDirectory(taskDirectory, "extractedDir");
        File taskWorkingDirectory = createDirectory(taskDirectory, "workingDir");
        File taskTempDirectory = createDirectory(taskDirectory, "tempDir");

        downloadEndExtractTaskBpk(task, extractedBpkDirectory);
        Bpk bpkDescriptor = loadTaskBpk(extractedBpkDirectory);
        TaskDescriptor taskDescriptor = task.getTaskDescriptor();

        File generatedBenchmarkPlanLocation = new File(taskDirectory, "benchmarkPlan.json");
        File bpkDescriptorLocation = writeToFile(bpkDescriptor, new File(taskDirectory, "bpkDescriptor.json"));
        File currentTaskDescriptorLocation = writeToFile(taskDescriptor, new File(taskDirectory, "currentTaskDescriptor.json"));
        String taskId = task.getTaskUuid();
        String taskContextId = task.getTaskContextUuid();
        String benchmarkId = task.getBenchmarkUuid();

        MessagingServer messagingServer = new MessagingServer(taskId, taskContextId, benchmarkId)
                .addRequestHandler(requestHandlers.storeLogRequestHandler())
                .addRequestHandler(requestHandlers.storeResultRequestHandler())
                .addRequestHandler(requestHandlers.setSharedPropRequestHandler())
                .addRequestHandler(requestHandlers.checkpointReachedRequestHandler())
                .addRequestHandler(requestHandlers.latchCountDownRequestHandler())
                .addRequestForResponseHandler(requestHandlers.getSharedPropRequestHandler())
                .addRequestForResponseHandler(requestHandlers.waitForCheckpointRequestHandler())
                .addRequestForResponseHandler(requestHandlers.waitForLatchRequestHandler())
                .addRequestForResponseHandler(requestHandlers.getStoredResultsRequestHandler())
                .addRequestForResponseHandler(requestHandlers.getActualBenchmarkPlanRequestHandler())
                .addRequestForResponseHandler(requestHandlers.generateUuidRequestHandler())
                .addRequestForResponseHandler(requestHandlers.generateTimestampRequestHandler())
                .start();
        cleaners.add(() -> messagingServer.terminate());


        Map<String, String> envir = new HashMap<>();
        envir.put(COMM_SERVER_SENDER_ADDR, messagingServer.getSenderAddress());
        envir.put(COMM_SERVER_RECEIVER_ADDR, messagingServer.getReceiverAddress());
        envir.put(EXTRACTED_BPK_DIR, extractedBpkDirectory.getAbsolutePath());
        envir.put(TASK_WORKING_DIR, taskWorkingDirectory.getAbsolutePath());
        envir.put(TASK_TEMP_DIR, taskTempDirectory.getAbsolutePath());
        envir.put(GENERATED_BENCHMARK_PLAN_LOCATION, generatedBenchmarkPlanLocation.getAbsolutePath());
        envir.put(BPK_DESCRIPTOR_LOCATION, bpkDescriptorLocation.getAbsolutePath());
        envir.put(CURRENT_TASK_DESCRIPTOR_LOCATION, currentTaskDescriptorLocation.getAbsolutePath());
        envir.put(TASK_ID, taskId);
        envir.put(TASK_CONTEXT_ID, taskContextId);
        envir.put(BENCHMARK_ID, benchmarkId);
        envir.put(TASK_LOG_LEVEL, task.getTaskDescriptor().getLogLevel().name());

        task.setTaskDirectory(taskDirectory);
        task.setTaskStartTime(new Date());
        task.setNodeUuid(nodeInfo.getUuid());

        TaskProcess taskProcess = new TaskProcess();
        taskProcess.setWorkingDirectory(taskWorkingDirectory);
        taskProcess.setEnvironmentProperties(envir);
        taskProcess.setTimeoutInMillis(SECONDS.toMillis(taskDescriptor.getTimeoutSeconds()));
        taskProcess.setTaskCommandLine(createCmdLine(taskDescriptor, extractedBpkDirectory, taskTempDirectory));
        OutputStream stdOutStream = createLogFileOutputStream(new File(taskDirectory, "stdout.log"));
        cleaners.add(() -> stdOutStream.close());
        OutputStream stdErrStream = createLogFileOutputStream(new File(taskDirectory, "stderr.log"));
        cleaners.add(() -> stdErrStream.close());
        taskProcess.setStreamHandler(new PumpStreamHandler(stdOutStream, stdErrStream));
        TaskProcessEventHandler handler = new TaskProcessEventHandlerImpl(task);
        TaskProcessCleaner taskProcessCleaner = createTaskProcessCleaner(task);
        taskProcess.setTaskProcessExecutionHandler(new TaskProcessExecutionHandler(handler, taskProcessCleaner));

        try {
            localTaskProcesses.put(task.getTaskUuid(), taskProcess);
            int exitValue = startTask(taskProcess);
            task.setTaskExitCode(exitValue);
            if (exitValue == 0 && TaskType.BENCHMARK_GENERATOR == task.getTaskType()) {
                postprocessBenchmarkTask(task, generatedBenchmarkPlanLocation);
            }
            if (exitValue == 0 && Boolean.valueOf(beenProperties.getProperty("been.tasks.deleteonsuccess", "true"))) {
                FileUtils.deleteQuietly(extractedBpkDirectory);
                FileUtils.deleteQuietly(taskWorkingDirectory);
                FileUtils.deleteQuietly(taskTempDirectory);
            }
            if (exitValue != 0) {
                taskService.killTaskInContext(taskContextId);
            }
        } finally {
            localTaskProcesses.remove(task.getTaskUuid());
            sharedVariables.deleteAll(taskContextId);
        }
    }

    private void postprocessBenchmarkTask(Task task, File benchmarkPlanLocation) throws TaskDriverException {
        try {
            BenchmarkPlan benchmarkPlan = JsonUtils.read(benchmarkPlanLocation, BenchmarkPlan.class);
            benchmarkPlan.setBenchmarkUuid(task.getBenchmarkUuid());
            hostRuntimeClient.runBenchmark(benchmarkPlan);
        } catch (JsonException e) {
            throw new TaskDriverException("Unable to deserialize generated benchmark plan", e);
        }
    }

    private Bpk loadTaskBpk(File extractedBpkDirectory) throws TaskDriverException {
        try {
            return BpkLoader.load(extractedBpkDirectory);
        } catch (BpkException e) {
            throw new TaskDriverException(e);
        }
    }

    private void downloadEndExtractTaskBpk(Task task, File extractedBpkDirectory) throws TaskDriverException {
        try {
            File bpkFile = softwareRepositoryClient.get(task.getBpkId());
            ZipFile zipFile = new ZipFile(bpkFile);
            zipFile.extractAll(extractedBpkDirectory.getAbsolutePath());
        } catch (ZipException | SoftwareRepositoryException e) {
            throw new TaskDriverException(e);
        }
    }

    private TaskProcessCleaner createTaskProcessCleaner(Task task) {
        return () -> localTaskProcesses.remove(task.getTaskUuid());
    }

    private int startTask(TaskProcess taskProcess) throws TaskDriverException {
        try {
            return taskProcess.start();
        } catch (TaskProcessException e) {
            throw new TaskDriverException("Failed to start task process", e);
        }
    }

    private TaskCommandLine createCmdLine(TaskDescriptor taskDescriptor, File extractedTask, File tempDirectory) throws TaskDriverException {
        try {
            return CmdLineBuilderFactory.create(taskDescriptor, extractedTask, tempDirectory).build();
        } catch (UnknownTaskDescriptorTypeException e) {
            throw new TaskDriverException("Unknown type of task descriptor", e);
        } catch (CmdLineBuilderException e) {
            throw new TaskDriverException("Failed to create task command line", e);
        }
    }

    private OutputStream createLogFileOutputStream(File logFile) throws TaskDriverException {
        try {
            return new LogFileOutputStream(logFile);
        } catch (FileNotFoundException e) {
            throw new TaskDriverException("Failed to create log output stream to " + logFile.getAbsolutePath(), e);
        }
    }

    private File createDirectory(File parent, String dirName) throws TaskDriverException {
        File subdir = new File(parent, dirName);
        if (!subdir.mkdir()) {
            throw new TaskDriverException("Failed to create directory '" + subdir.getAbsolutePath() + "'.");
        }
        return subdir;
    }

    private <T> File writeToFile(T bpkDescriptor, File bpkDescriptorLocation) throws TaskDriverException {
        try {
            JsonUtils.writeToFile(bpkDescriptor, bpkDescriptorLocation);
        } catch (JsonException e) {
            throw new TaskDriverException(e);
        }
        return bpkDescriptorLocation;
    }

    private File createTaskDirectory(String taskUuid) {
        for (int i = 0; true; i++) {
            File taskDir = new File(tasksDirectory, taskUuid + "__" + i);
            if (!taskDir.exists()) {
                taskDir.mkdirs();
                return taskDir;
            }
        }
    }

    private interface Cleaner {
        void doCleanup() throws Exception;
    }
}
