package cz.cuni.mff.d3s.been.hostruntime.server.task.cmdline;

/**
 * Command line builder interface.
 *
 * @author Tadeas Palusga
 */
public interface CmdLineBuilder {

    /**
     * Builds command line.
     *
     * @return task's command line
     * @throws CmdLineBuilderException when command line cannot be generated.
     */
    TaskCommandLine build() throws CmdLineBuilderException;

}
