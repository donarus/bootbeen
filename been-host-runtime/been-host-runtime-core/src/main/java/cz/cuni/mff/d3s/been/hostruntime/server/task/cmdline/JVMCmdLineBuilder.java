package cz.cuni.mff.d3s.been.hostruntime.server.task.cmdline;

import cz.cuni.mff.d3s.been.model.td.JavaTaskDescriptor;
import cz.cuni.mff.d3s.been.model.td.TaskJavaConfig;
import cz.cuni.mff.d3s.been.model.td.TaskJavaDebugConfig;
import cz.cuni.mff.d3s.been.model.td.TaskJavaDebugMode;
import org.apache.commons.exec.CommandLine;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static cz.cuni.mff.d3s.been.model.td.TaskJavaDebugMode.CONNECT;
import static cz.cuni.mff.d3s.been.model.td.TaskJavaDebugMode.LISTEN;

/**
 * Command line builder for JVM based tasks.
 */
class JVMCmdLineBuilder implements CmdLineBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(JVMCmdLineBuilder.class);

    private static final String TASK_RUNNER_CLASS = "cz.cuni.mff.d3s.been.taskapi.TaskRunner";

    private static final String JAVA_DEBUG_ARG_TEMPLATE = "-agentlib:jdwp=transport=dt_socket,server=%s,address=%s,suspend=%s";

    private final JavaTaskDescriptor taskDescriptor;

    private final File taskDir;

    private final File tempDir;

    /**
     * @param taskDescriptor associated TaskDescriptor
     */
    public JVMCmdLineBuilder(JavaTaskDescriptor taskDescriptor, File taskDir, File tempDir) {
        this.taskDescriptor = taskDescriptor;
        this.taskDir = taskDir;
        this.tempDir = tempDir;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TaskCommandLine build() throws CmdLineBuilderException {
        TaskCommandLine cmdLine = new TaskCommandLine("java");

        addClassPath(cmdLine);
        addJavaOptsFromTaskDescriptor(cmdLine);
        addDebugParameters(cmdLine);
        addTempPath(cmdLine);
        addMainClass(cmdLine);
        addArgsFromTaskDescriptor(cmdLine);

        return cmdLine;
    }

    private void addTempPath(TaskCommandLine cmdLine) {
        cmdLine.addArguments("-Djava.io.tmpdir=" + tempDir.getAbsolutePath());
    }

    private void addMainClass(TaskCommandLine cmdLine) {
        TaskJavaConfig javaConfig = taskDescriptor.getJavaConfig();
        cmdLine.addArgument(TASK_RUNNER_CLASS);
        cmdLine.addArgument(javaConfig.getMainClass());
    }

    /**
     * Generates classpath value argument. Joins all relative paths of files
     * specified in {@link TaskJavaConfig#getDependencies()} defined in
     * {@link JavaTaskDescriptor#getJavaConfig()} to single string.
     * (Path join is platform independent - it means that ':' is used as the path
     * delimiter on Microsoft OS and ':' on UNIX/Linux OS)
     *
     * @param cmdLine command line to which the generated argument should be added
     */
    private void addClassPath(TaskCommandLine cmdLine) {
        TaskJavaConfig cfg = taskDescriptor.getJavaConfig();
        List<String> dependenciesPaths = cfg.getDependencies().stream().map(d -> d.getPathInBpk()).collect(Collectors.toList());
        List<String> cpEntries = dependenciesPaths.stream().map(f -> new File(taskDir, f).getAbsolutePath()).collect(Collectors.toList());
        cmdLine.addArgument("-cp").addArgument(String.join(File.pathSeparator, cpEntries));
    }


    /**
     * Searches for java options in task descriptor and appends these options to
     * given {@link CommandLine}
     *
     * @param cmdLine command line to which the options should be appended
     */
    void addJavaOptsFromTaskDescriptor(TaskCommandLine cmdLine) {
        list(taskDescriptor.getJavaConfig().getJavaOptions()).forEach(cmdLine::addArgument);
    }

    /**
     * Searches for debug options in task descriptor. If debug is defined and
     * debug mode is [{@link TaskJavaDebugMode#CONNECT}
     * or {@link TaskJavaDebugMode#LISTEN} ] then
     * the debug argument is generated from defined template (see
     * {@link JVMCmdLineBuilder#JAVA_DEBUG_ARG_TEMPLATE} for detailed informations
     * about debug argument.)
     *
     * @param cmdLine Task's command line
     */
    private void addDebugParameters(TaskCommandLine cmdLine) throws CmdLineBuilderException {
        TaskJavaDebugConfig dbg = taskDescriptor.getJavaConfig().getJavaDebugConfig();
        if (dbg == null) {
            return;
        }

        TaskJavaDebugMode mode = dbg.getMode();
        String host = dbg.getHost();
        int port = dbg.getPort();
        boolean suspendOnStart = dbg.isSuspendOnStart();

        if (mode == CONNECT) {
            cmdLine.addArgument(createDebugParam(false, host + ":" + port, suspendOnStart));
        } else if (mode == LISTEN) {
            if (port == 0) {
                port = detectRandomPort();
            }
            cmdLine.addArgument(createDebugParam(true, "" + port, suspendOnStart));
            LOG.info("Debugged process is listening on port {}", port);
        } else {
            // debug mode set as null, skipping
        }
    }

    /**
     * Searches for program arguments in task descriptor and appends these
     * arguments to given {@link CommandLine}
     *
     * @param cmdLine command line to which the generated argument should be added
     */
    private void addArgsFromTaskDescriptor(TaskCommandLine cmdLine) {
        if (taskDescriptor.getCommandLineArguments() == null) {
            return;
        }
        for (String arg : taskDescriptor.getCommandLineArguments()) {
            cmdLine.addArgument(arg);
        }
    }

    /**
     * Detects <b>random available</b> port on localhost.
     *
     * @return detected port
     */
    private int detectRandomPort() throws CmdLineBuilderException {
        try (ServerSocket ss = new ServerSocket(0)) {
            return ss.getLocalPort();
        } catch (IOException e) {
            throw new CmdLineBuilderException("Cannot detect random port on localhost for debugging", e);
        }
    }

    /**
     * Creates transport debug parameter. For detailed parameter description see
     * {@link JVMCmdLineBuilder#JAVA_DEBUG_ARG_TEMPLATE}
     *
     * @param server  listen for a debugger application to attach
     * @param address transport address for the connection
     * @param suspend if <i>true</i> ... suspend policy = SUSPEND_ALL. If <i>false</i>
     *                ... suspend policy = SUSPEND_NONE
     * @return created parameter
     */
    private String createDebugParam(boolean server, String address, boolean suspend) {
        return String.format(JAVA_DEBUG_ARG_TEMPLATE, (server ? "y" : "n"), address, (suspend ? "y" : "n"));
    }

    private <T> List<T> union(List<T> list1, List<T> list2) {
        List<T> union = new ArrayList<>();
        if (list1 != null) union.addAll(list1);
        if (list2 != null) union.addAll(list2);
        return union;
    }

    private <T> List<T> union(T value, List<T> list2) {
        List<T> union = new ArrayList<>();
        if (value != null) union.add(value);
        if (list2 != null) union.addAll(list2);
        return union;
    }


    private <T> List<T> list(List<T> list) {
        return Optional.ofNullable(list).orElseGet(Collections::emptyList);
    }

}
