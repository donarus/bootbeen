package cz.cuni.mff.d3s.been.hostruntime.server.task.cmdline;

import cz.cuni.mff.d3s.been.model.td.NativeTaskDescriptor;
import org.apache.commons.exec.CommandLine;

import java.io.File;
import java.util.List;

/**
 * Command line builder for native tasks.
 *
 * @author Tadeas Palusga
 */
class NativeCmdLineBuilder implements CmdLineBuilder {

    /**
     * underlying task descriptor from which the command line should be built
     */
    private final NativeTaskDescriptor taskDescriptor;

    /**
     * task home directory
     */
    private final File taskDir;

    /**
     * @param taskDescriptor     associated TaskDescriptor
     * @param taskDir            Task's working directory
     */
    public NativeCmdLineBuilder(NativeTaskDescriptor taskDescriptor, File taskDir) {
        this.taskDescriptor = taskDescriptor;
        this.taskDir = taskDir;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TaskCommandLine build() {
        String executableFileName = taskDescriptor.getExecutableFileName();
        File executable = taskDir.toPath().resolve(executableFileName).toFile();
        executable.setExecutable(true);
        TaskCommandLine cmdLine = new TaskCommandLine(executable);
        addArgsFromTaskDescriptor(cmdLine);
        return cmdLine;
    }

    /**
     * Searches for program arguments in task descriptor and appends these
     * arguments to given {@link CommandLine}
     *
     * @param cmdLine command line to which the generated argument should be added
     */
    private void addArgsFromTaskDescriptor(CommandLine cmdLine) {
        List<String> commandLineArguments = taskDescriptor.getCommandLineArguments();
        boolean hasArguments = commandLineArguments != null;
        if (hasArguments) {
            for (String cmdArg : commandLineArguments) {
                cmdLine.addArgument(cmdArg);
            }
        }
    }

}
