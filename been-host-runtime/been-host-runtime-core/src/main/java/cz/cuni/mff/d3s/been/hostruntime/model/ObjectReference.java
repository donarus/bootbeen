package cz.cuni.mff.d3s.been.hostruntime.model;

/**
 * This class server as simple holder for reference on objects
 * when some return value is needed from lambda expression.
 * <p>
 * This holder is not thread safe/
 *
 * @param <T> type of stored object
 */
public final class ObjectReference<T> {
    private T value;

    public ObjectReference(T defaultValue) {
        this.value = defaultValue;
    }

    public void set(T value) {
        this.value = value;
    }

    public T get() {
        return value;
    }
}
