package cz.cuni.mff.d3s.been.hostruntime.model.events;

import java.io.Serializable;

/**
 * Kill task event for
 * {@link cz.cuni.mff.d3s.been.hostruntime.server.daemons.EventListener}.
 */
public class KillTaskEvent implements Event, Serializable {
    private String taskUuid;

    public KillTaskEvent(String taskUuid) {
        this.taskUuid = taskUuid;
    }

    public String getTaskUuid() {
        return taskUuid;
    }
}
