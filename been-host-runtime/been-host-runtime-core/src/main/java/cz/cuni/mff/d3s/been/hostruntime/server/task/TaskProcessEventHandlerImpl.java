package cz.cuni.mff.d3s.been.hostruntime.server.task;

import cz.cuni.mff.d3s.been.hostruntime.server.task.taskprocess.TaskProcessExecutionHandler;
import cz.cuni.mff.d3s.been.model.task.Task;
import cz.cuni.mff.d3s.been.model.task.TaskState;

public class TaskProcessEventHandlerImpl implements TaskProcessExecutionHandler.TaskProcessEventHandler {

    private final Task task;

    public TaskProcessEventHandlerImpl(Task task) {
        this.task = task;
    }

    @Override
    public void onTaskRunning() {
        task.setTaskState(TaskState.RUNNING);
    }

    @Override
    public void onTaskStarted() {
        task.setTaskState(TaskState.RUNNING);
    }

    @Override
    public void onTaskFinished(int exitCode) {
        task.setTaskState(TaskState.FINISHED);
    }

    @Override
    public void onTaskFailed(int exitCode) {
        task.setTaskState(TaskState.FAILED);
    }

    @Override
    public void onTaskTimeouted() {
        task.setTaskState(TaskState.TIMEOUTED);
    }

    @Override
    public void onTaskKilled() {
        task.setTaskState(TaskState.KILLED);
    }
}
