package cz.cuni.mff.d3s.been.hostruntime.model.events;

import cz.cuni.mff.d3s.been.model.benchmark.BenchmarkPlan;
import cz.cuni.mff.d3s.been.model.bpk.BpkId;

import java.io.Serializable;

/**
 * Run benchmark event for
 * {@link cz.cuni.mff.d3s.been.hostruntime.server.daemons.EventListener}.
 */
public class RunBenchmarkEvent implements Event, Serializable {
    private BenchmarkPlan benchmarkPlan;
    private String benchmarkId;

    public RunBenchmarkEvent(String benchmarkId, BenchmarkPlan benchmarkPlan) {
        this.benchmarkId = benchmarkId;
        this.benchmarkPlan = benchmarkPlan;
    }

    public BenchmarkPlan getBenchmarkPlan() {
        return benchmarkPlan;
    }


    public String getBenchmarkId() {
        return benchmarkId;
    }
}
