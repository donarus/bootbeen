package cz.cuni.mff.d3s.been.hostruntime.client;

import cz.cuni.mff.d3s.been.hostruntime.HostRuntimeStructures;
import cz.cuni.mff.d3s.been.model.benchmark.Benchmark;
import cz.cuni.mff.d3s.been.model.benchmark.BenchmarkState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service("HostRuntimeBenchmarks")
public class Benchmarks {

    @Autowired
    private HostRuntimeStructures structures;

    public List<Benchmark> list(boolean includePlan) {
        if (includePlan) {
            return new ArrayList<>(structures.benchmarksMap().values());
        } else {
            return structures.benchmarksMap().map(Benchmark::lightVersion);
        }
    }

    public List<Benchmark> listByStates(BenchmarkState... states) {
        return listByStates(Arrays.asList(states));
    }

    public List<Benchmark> listByStates(List<BenchmarkState> states) {
        return structures.benchmarksMap().filter((key, value) -> states.contains(value.getBenchmarkState()));
    }

    public Benchmark getBenchmarkByUuid(String benchmarkUuid) {
        return structures.benchmarksMap().get(benchmarkUuid);
    }
}


