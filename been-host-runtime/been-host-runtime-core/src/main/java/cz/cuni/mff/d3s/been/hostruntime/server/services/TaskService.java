package cz.cuni.mff.d3s.been.hostruntime.server.services;

import cz.cuni.mff.d3s.been.api.cluster.ClusterExecutorException;
import cz.cuni.mff.d3s.been.api.cluster.ClusterExecutorService;
import cz.cuni.mff.d3s.been.api.cluster.ClusterUuidGenerator;
import cz.cuni.mff.d3s.been.hostruntime.HostRuntimeStructures;
import cz.cuni.mff.d3s.been.hostruntime.client.Tasks;
import cz.cuni.mff.d3s.been.hostruntime.server.task.TaskDriver;
import cz.cuni.mff.d3s.been.model.bpk.BpkId;
import cz.cuni.mff.d3s.been.model.node.NodeInfo;
import cz.cuni.mff.d3s.been.model.task.Task;
import cz.cuni.mff.d3s.been.model.task.TaskState;
import cz.cuni.mff.d3s.been.model.td.TaskDescriptor;
import cz.cuni.mff.d3s.been.model.td.TaskType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;

import static cz.cuni.mff.d3s.been.model.task.TaskState.*;
import static cz.cuni.mff.d3s.been.springsupport.SpringContextHolder.bean;

@Service
@Profile("hr-server")
public class TaskService {

    private static final Logger LOG = LoggerFactory.getLogger(TaskService.class);

    @Autowired
    private NodeInfo nodeInfo;

    @Autowired
    private ClusterUuidGenerator uuidGenerator;

    @Autowired
    private ClusterExecutorService clusterExecutorService;

    @Autowired
    private HostRuntimeStructures structures;

    @Autowired
    private SuitableNodesFinder suitableNodesFinder;

    @Autowired
    private Tasks beenTasks;

    @Autowired
    private BenchmarkService benchmarksService;

    public String createTask(BpkId bpkId, TaskDescriptor taskDescriptor, String taskContextUuid, String benchmarkUuid) {
        String taskUuid = uuidGenerator.generate();
        if ((taskDescriptor.getType() != TaskType.BENCHMARK_GENERATOR) && (taskContextUuid == null || benchmarkUuid == null)) {
            LOG.error("Only BENCHMARK_GENERATOR task is allowed to be run without benchmarkId and contextId.");
            return null;
        }

        if (TaskType.BENCHMARK_GENERATOR == taskDescriptor.getType()) {
            benchmarkUuid = uuidGenerator.generate();
            benchmarksService.createNewBenchmark(taskUuid, benchmarkUuid, bpkId);
        }

        Task task = new Task();
        task.setBpkId(bpkId);
        task.setTaskDescriptor(taskDescriptor);
        task.setTaskType(taskDescriptor.getType());
        task.setTaskState(REQUESTED);
        task.setTaskUuid(taskUuid);
        task.setTaskContextUuid(taskContextUuid);
        task.setBenchmarkUuid(benchmarkUuid);

        LOG.info("New task [" + task.getTaskUuid() + "] has been created.");

        // following is sage operation, task does not exist in map so can't be being processed
        structures.tasksMap().put(task.getTaskUuid(), task);

        return task.getTaskUuid();
    }

    public void startPreparedTasks() {
        LOG.debug("Trying to start task restarter on node [" + nodeInfo.getUuid() + "]");
        if (structures.startTasksLock().tryLock()) {
            try {
                for (Task task : beenTasks.listByStates(REQUESTED, NOT_ENOUGH_RESOURCES)) {
                    task = TaskProxyCreator.createTaskProxy(task, structures.tasksMap());
                    runTaskOnSuitableNode(task);
                }
            } finally {
                structures.startTasksLock().unlock();
            }
        } else {
            LOG.debug("Task starter was not started on node [" + nodeInfo.getUuid() + "]. Couldn't acquire lock which " +
                    "means that another starter is in progress");
        }
    }

    private void runTaskOnSuitableNode(Task task) {
        LOG.debug("Searching for suitable nodes, where task [" + task.getTaskUuid() + "] can be started.");
        List<NodeInfo> suitableNodes = suitableNodesFinder.findSuitableNodes(task);
        LOG.debug(suitableNodes.size() + " suitable nodes found.");

        boolean started = false;
        for (NodeInfo suitableNode : suitableNodes) {
            task.setNodeUuid(suitableNode.getUuid());
            started = runTaskOnNode(task, suitableNode);
            if (started) {
                // task has been started, return immediately
                // and for god sake don't modify task anymore
                // because it is running on different node !
                return;
            }
        }

        if (suitableNodes.isEmpty()) {
            LOG.info("No suitable node found for task [" + task.getTaskUuid() + "].");
        }

        if (!started) {
            LOG.info("Task [" + task.getTaskUuid() + "] was not accepted by any node. Giving up for now.");
            task.setTaskState(NOT_ENOUGH_RESOURCES);
        }
    }

    private boolean runTaskOnNode(Task taskProxy, NodeInfo suitableNode) {
        boolean started = false;
        LOG.info("Trying to start task [" + taskProxy.getTaskUuid() + "] at node [" + suitableNode.getUuid() + "].");
        try {
            Task task = structures.tasksMap().get(taskProxy.getTaskUuid());
            started = clusterExecutorService.submitOnNode(() -> {
                final Task proxy = TaskProxyCreator.createTaskProxy(task, bean(HostRuntimeStructures.class).tasksMap());
                return bean(TaskService.class).runTaskOnThisNode(proxy);
            }, suitableNode.getUuid());
        } catch (ClusterExecutorException e) {
            LOG.warn("Failed to start task [" + taskProxy.getTaskUuid() + "] at node [" + suitableNode.getUuid() + "].", e);
        }

        if (started) {
            LOG.info("Task [" + taskProxy.getTaskUuid() + "] has been started on node [" + suitableNode.getUuid() + "].");
        }

        return started;
    }

    private synchronized boolean runTaskOnThisNode(Task task) {
        if (!suitableNodesFinder.canRunOnThisNode(task)) {
            return false;
        }
        task.setTaskState(STARTING);
        task.setNodeUuid(nodeInfo.getUuid());
        new Thread(new RunningTaskWrapper(task)).start();
        return true;
    }

    public void killTask(String taskUuid) {
        Task task = beenTasks.getTaskByUuid(taskUuid);
        killTask(task);
    }

    public void killTask(Task task) {
        if (task == null) {
            LOG.warn("Can't kill task, task not found");
            return;
        } else if (task.getNodeUuid() == null) {
            LOG.warn("Can't kill task, node not found");
            return;
        }
        try {
            clusterExecutorService.executeOnNode(() -> bean(TaskDriver.class).killTaskOnThisNode(task), task.getNodeUuid());
        } catch (ClusterExecutorException e) {
            LOG.error("Unable to kill task", e);
        }
    }

    public void killTaskInContext(String taskContextId) {
        for (Task task : beenTasks.listByTaskContextUuid(taskContextId)) {
            if (task.getTaskState() == TaskState.RUNNING || task.getTaskState() == TaskState.STARTING || task.getTaskState() == TaskState.REQUESTED) {
                killTask(task.getTaskUuid());
            }
        }
    }
}
