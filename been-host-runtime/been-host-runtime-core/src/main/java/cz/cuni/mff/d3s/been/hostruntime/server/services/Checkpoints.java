package cz.cuni.mff.d3s.been.hostruntime.server.services;

import cz.cuni.mff.d3s.been.api.cluster.data.CountDownLatches;
import cz.cuni.mff.d3s.been.api.cluster.data.Locks;
import cz.cuni.mff.d3s.been.api.cluster.data.Maps;
import cz.cuni.mff.d3s.been.api.cluster.data.MultiMaps;
import cz.cuni.mff.d3s.been.api.cluster.data.structures.ClusterCountDownLatch;
import cz.cuni.mff.d3s.been.api.cluster.data.structures.ClusterLock;
import cz.cuni.mff.d3s.been.api.cluster.data.structures.ClusterMap;
import cz.cuni.mff.d3s.been.api.cluster.data.structures.ClusterMultiMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;

@Service
public class Checkpoints {

    private static final Logger LOG = LoggerFactory.getLogger(Checkpoints.class);

    @Autowired
    private Maps maps;

    @Autowired
    private MultiMaps multiMaps;

    @Autowired
    private Locks locks;

    @Autowired
    private CountDownLatches countDownLatches;

    private ClusterMultiMap<String, String> reachedCheckpoints;

    private ClusterMap<String, Integer> latchDecrements;

    @PostConstruct
    private void setUp() {
        reachedCheckpoints = multiMaps.getMap("been::hc_internal::checkpoints::reachedCheckpoints");
        latchDecrements = maps.getMap("been::hc_internal::checkpoints::latchDecrements");
    }

    public void waitForCheckpoint(String taskContextId, String checkpointName) {
        String key = createKey(taskContextId, checkpointName);
        ClusterLock lock = locks.getLock(key);
        lock.lock();
        try {
            if (reachedCheckpoints.get(taskContextId).contains(checkpointName)) {
                return;
            }
            Condition condition = lock.newCondition();
            condition.awaitUninterruptibly();
        } finally {
            lock.unlock();
        }
    }

    public void checkpointReached(String taskContextId, String checkpointName) {
        String key = createKey(taskContextId, checkpointName);
        ClusterLock lock = locks.getLock(key);
        lock.lock();
        try {
            reachedCheckpoints.put(taskContextId, checkpointName);
            Condition condition = lock.newCondition();
            condition.signalAll();
        } finally {
            lock.unlock();
        }
    }

    public void waitForLatch(String taskContextId, String latchName, int initialCount) {
        String key = createKey(taskContextId, latchName);
        latchDecrements.lock(key);
        ClusterCountDownLatch latch = countDownLatches.getCountDownLatch(key);
        try {
            Integer decrements = latchDecrements.getOrDefault(key, 0);
            latch.trySetCount(Math.max(0, initialCount - decrements));
        } finally {
            latchDecrements.unlock(key);
        }

        try {
            latch.await(Long.MAX_VALUE, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            LOG.warn("Interrupted while waiting for latch", e);
        } finally {
            latchDecrements.remove(key);
        }
    }

    public void latchCountDown(String taskContextId, String latchName) {
        String key = createKey(taskContextId, latchName);
        latchDecrements.lock(key);
        try {
            ClusterCountDownLatch latch = countDownLatches.getCountDownLatch(key);
            if (latch.getCount() == 0) {
                latchDecrements.compute(key, (s, count) -> count == null ? 1 : count + 1);
            } else {
                latch.countDown();
            }
        } finally {
            latchDecrements.unlock(key);
        }
    }

    private String createKey(String taskContextId, String checkpointName) {
        return taskContextId + "_" + checkpointName;
    }
}
