package cz.cuni.mff.d3s.been.hostruntime.model.exceptions;

public class NotTheOwnerException extends Exception {
    public NotTheOwnerException(String message) {
        super(message);
    }
}
