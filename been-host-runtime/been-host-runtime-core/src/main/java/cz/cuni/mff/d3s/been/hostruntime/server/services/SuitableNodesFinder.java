package cz.cuni.mff.d3s.been.hostruntime.server.services;

import cz.cuni.mff.d3s.been.api.common.BeenNodes;
import cz.cuni.mff.d3s.been.api.common.BeenServices;
import cz.cuni.mff.d3s.been.hostruntime.client.Tasks;
import cz.cuni.mff.d3s.been.hostruntime.model.HostRuntimeServiceInfo;
import cz.cuni.mff.d3s.been.model.node.NodeInfo;
import cz.cuni.mff.d3s.been.model.task.Task;
import cz.cuni.mff.d3s.been.model.task.TaskState;
import cz.cuni.mff.d3s.been.model.td.TaskExclusivity;
import cz.cuni.mff.d3s.been.model.td.TaskRuntimeRequirement;
import cz.cuni.mff.d3s.been.utils.runreq.RuntimeRequirementChecker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static cz.cuni.mff.d3s.been.hostruntime.Constants.HR_SERVICE_NAME;
import static cz.cuni.mff.d3s.been.hostruntime.Constants.HR_SERVICE_VENDOR;
import static cz.cuni.mff.d3s.been.model.td.TaskExclusivity.*;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

/**
 * Searches for nodes passig requirements and task exclusivity condition.
 */
@Service
@Profile("hr-server")
class SuitableNodesFinder {

    private static final Logger LOG = LoggerFactory.getLogger(SuitableNodesFinder.class);

    @Autowired
    private BeenNodes beenNodes;

    @Autowired
    private BeenServices beenServices;

    @Autowired
    private Tasks beenTasks;

    @Autowired
    private NodeInfo nodeInfo;

    boolean canRunOnThisNode(Task task) {
        List<NodeInfo> suitableNodes = new ArrayList<>();
        suitableNodes.add(nodeInfo);
        List<TaskRuntimeRequirement> filter = task.getTaskDescriptor().getRuntimeRequirements();
        suitableNodes = filterByRequirements(suitableNodes, filter);
        suitableNodes = filterByExclusivity(suitableNodes, task);
        return suitableNodes.size() == 1;
    }

    List<NodeInfo> findSuitableNodes(Task task) {
        List<HostRuntimeServiceInfo> runtimes = beenServices.listByNameAndVendor(HR_SERVICE_NAME, HR_SERVICE_VENDOR);

        List<TaskRuntimeRequirement> filter = task.getTaskDescriptor().getRuntimeRequirements();
        List<NodeInfo> suitableNodes = getRuntimeNodes(runtimes);
        suitableNodes = filterByRequirements(suitableNodes, filter);
        suitableNodes = filterByExclusivity(suitableNodes, task);

        return suitableNodes;
    }

    private List<NodeInfo> filterByExclusivity(List<NodeInfo> suitableNodes, Task task) {
        TaskExclusivity taskExclusivity = exclusivityOf(task);
        List<NodeInfoWithTaksCount> nodesPassingExclusivity = new ArrayList<>();
        for (NodeInfo nodeInfo : suitableNodes) {
            List<Task> nodeTasks = runningOrStartingTasksOnNode(nodeInfo);
            if (taskExclusivity == TaskExclusivity.EXCLUSIVE && !nodeTasks.isEmpty()) {
                LOG.info("Task [{}] can't be started on node [{}] because task is EXCLUSIVE and there is already some task running.", task.getTaskUuid(), nodeInfo.getUuid());
                continue;
            }
            if (containsExclusiveTask(nodeTasks)) {
                LOG.info("Task [{}] can't be started on node [{}] because there is already running some another EXCLUSIVE taks.", task.getTaskUuid(), nodeInfo.getUuid());
                continue;
            }
            if (containsContextExclusiveTask(nodeTasks, task.getTaskContextUuid())) {
                LOG.info("Task [{}] can't be started on node [{}] because there is already running some another TASK_EXCLUSIVE taks within same task context [{}].", task.getTaskUuid(), nodeInfo.getUuid(), task.getTaskContextUuid());
                continue;
            }
            nodesPassingExclusivity.add(new NodeInfoWithTaksCount(nodeInfo, nodeTasks.size()));
        }
        return nodesPassingExclusivity.stream().sorted(comparing(NodeInfoWithTaksCount::getTaskCount)).map(n -> n.getNodeInfo()).collect(toList());
    }

    private List<Task> runningOrStartingTasksOnNode(NodeInfo nodeInfo) {
        return beenTasks.listByNodeUuid(nodeInfo.getUuid()).stream()
                .filter(n -> Arrays.asList(TaskState.RUNNING, TaskState.STARTING).contains(n.getTaskState()))
                .collect(toList());
    }

    private List<NodeInfo> filterByRequirements(List<NodeInfo> suitableNodes, List<TaskRuntimeRequirement> filter) {
        return suitableNodes.stream()
                .filter(n -> new RuntimeRequirementChecker(n.getSystemInfo()).check(filter))
                .collect(toList());
    }

    private List<NodeInfo> getRuntimeNodes(List<HostRuntimeServiceInfo> runtimes) {
        return beenNodes.listByUuids(runtimes.stream().map(HostRuntimeServiceInfo::getNodeUUID).collect(toList()));
    }

    private boolean containsExclusiveTask(List<Task> nodeTasks) {
        return nodeTasks.stream()
                .filter(t -> exclusivityOf(t) == EXCLUSIVE)
                .count() > 0;
    }

    private boolean containsContextExclusiveTask(List<Task> nodeTasks, String taskContextUuid) {
        return nodeTasks.stream()
                .filter(t -> exclusivityOf(t) == CONTEXT_EXCLUSIVE)
                .filter(t -> t.getTaskContextUuid() != null)
                .filter(t -> t.getTaskContextUuid().equals(taskContextUuid))
                .count() > 0;
    }

    private final TaskExclusivity exclusivityOf(Task task) {
        TaskExclusivity excl = task.getTaskDescriptor().getExclusivity();
        return excl == null ? NON_EXCLUSIVE : excl;
    }

    private static final class NodeInfoWithTaksCount {
        private final NodeInfo nodeInfo;
        private final Integer taskCount;

        public NodeInfoWithTaksCount(NodeInfo nodeInfo, Integer taskCount) {
            this.nodeInfo = nodeInfo;
            this.taskCount = taskCount;
        }

        public NodeInfo getNodeInfo() {
            return nodeInfo;
        }

        public Integer getTaskCount() {
            return taskCount;
        }
    }
}
