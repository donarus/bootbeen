package cz.cuni.mff.d3s.been.hostruntime.model.events;

import java.io.Serializable;

/**
 * Kill task event for
 * {@link cz.cuni.mff.d3s.been.hostruntime.server.daemons.EventListener}.
 */
public class KillBenchmarkEvent implements Event, Serializable {
    private String benchmarkUuid;

    public KillBenchmarkEvent(String benchmarkUuid) {
        this.benchmarkUuid = benchmarkUuid;
    }

    public String getBenchmarkUuid() {
        return benchmarkUuid;
    }
}
