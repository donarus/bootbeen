package cz.cuni.mff.d3s.been.hostruntime.server.services;

import cz.cuni.mff.d3s.been.api.cluster.ClusterUuidGenerator;
import cz.cuni.mff.d3s.been.api.common.BeenServices;
import cz.cuni.mff.d3s.been.hostruntime.model.HostRuntimeServiceInfo;
import cz.cuni.mff.d3s.been.model.node.NodeInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service("hostRuntimeServiceInfoRegistrator")
@Profile("hr-server")
class ServiceInfoRegistrator {

    @Autowired
    private BeenServices beenServices;

    @Autowired
    private NodeInfo nodeInfo;

    @Autowired
    private ClusterUuidGenerator uuidGenerator;

    @PostConstruct
    void registerInfo() {
        HostRuntimeServiceInfo serviceInfo = new HostRuntimeServiceInfo(nodeInfo.getUuid(), uuidGenerator.generate());
        beenServices.register(serviceInfo);
    }

}
