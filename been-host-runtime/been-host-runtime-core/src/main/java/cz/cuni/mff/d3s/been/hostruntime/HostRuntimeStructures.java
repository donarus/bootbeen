package cz.cuni.mff.d3s.been.hostruntime;

import cz.cuni.mff.d3s.been.api.cluster.data.Locks;
import cz.cuni.mff.d3s.been.api.cluster.data.Maps;
import cz.cuni.mff.d3s.been.api.cluster.data.Queues;
import cz.cuni.mff.d3s.been.api.cluster.data.structures.ClusterLock;
import cz.cuni.mff.d3s.been.api.cluster.data.structures.ClusterMap;
import cz.cuni.mff.d3s.been.api.cluster.data.structures.ClusterQueue;
import cz.cuni.mff.d3s.been.hostruntime.model.events.Event;
import cz.cuni.mff.d3s.been.hostruntime.model.events.KillTaskEvent;
import cz.cuni.mff.d3s.been.hostruntime.model.events.RunTaskEvent;
import cz.cuni.mff.d3s.been.model.benchmark.Benchmark;
import cz.cuni.mff.d3s.been.model.task.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

import static cz.cuni.mff.d3s.been.hostruntime.Constants.*;

/**
 * This service serves ase holder for cluster objects
 * used by this implementation of host runtime.
 *
 * @author Tadeas Palusga
 */
@Service
public class HostRuntimeStructures {

    @Autowired
    private Queues queues;

    @Autowired
    private Maps maps;

    @Autowired
    private Locks locks;

    private ClusterQueue<Event> eventQueue;

    private ClusterMap<String, Task> taskMap;

    private ClusterMap<String, Benchmark> benchmarksMap;

    private ClusterMap<String, String> contextSharedVariables;

    private ClusterLock startTasksLock;

    private ClusterLock startBenchmarksLock;

    @PostConstruct
    private void setUp() {
        this.eventQueue = queues.getQueue(EVENT_QUEUE);
        this.taskMap = maps.getMap(TASKS_MAP);
        this.benchmarksMap = maps.getMap(BENCHMARKS_MAP);
        this.contextSharedVariables = maps.getMap(TASK_CONTEXT_SHARED_VARIABLES_MAP);
        this.startTasksLock = locks.getLock(START_TASKS_LOCK);
        this.startBenchmarksLock = locks.getLock(START_BENCHMARKS_LOCK);
    }

    /**
     * Returns cluster distributed queue used for storing host runtime client
     * events such as {@link KillTaskEvent}
     * and {@link RunTaskEvent}
     *
     * @return
     */
    public ClusterQueue<Event> eventQueue() {
        return eventQueue;
    }

    /**
     * Returns cluster distributed map used for storing tasks.
     * Key corresponds to {@link Task#taskUuid}.
     *
     * @return
     */
    public ClusterMap<String, Task> tasksMap() {
        return taskMap;
    }

    /**
     * Returns cluster distributed map used for storing benchmarks.
     * Key corresponds to {@link Benchmark#benchmarkUuid}.
     *
     * @return
     */
    public ClusterMap<String, Benchmark> benchmarksMap() {
        return benchmarksMap;
    }

    /**
     * Returns cluster distributed map used for storing variables which can be shared among running tasks in the same
     * task context.
     * Key corresponds to {@link Task#taskContextUuid}-$name, where $name stands for name of the shared variable.
     *
     * @return
     */
    public ClusterMap<String, String> contextSharedVariables() {
        return contextSharedVariables;
    }

    /**
     * Returns cluster distributed lock used for cluster-wide locking task starter when starting tasks
     *
     * @return
     */
    public ClusterLock startTasksLock() {
        return startTasksLock;
    }

        /**
     * Returns cluster distributed lock used for cluster-wide locking task starter when starting tasks
     *
     * @return
     */
    public ClusterLock startBenchmarksLock() {
        return startBenchmarksLock;
    }
}
