package cz.cuni.mff.d3s.been.hostruntime.server.task.cmdline;

import org.apache.commons.exec.CommandLine;

import java.io.File;

/**
 * 
 * Task's command line.
 * 
 * @author Tadeas Palusga
 */
public class TaskCommandLine extends CommandLine {

	/**
	 * Creates new TaskCommandLine for a task.
	 * 
	 * @param executable
	 *          target executable of the task
	 */
	public TaskCommandLine(File executable) {
		super(executable);
	}

	/**
	 * Creates new TaskCommandLine for a task.
	 * 
	 * @param executable
	 *          target executable of the task
	 */
	public TaskCommandLine(String executable) {
		super(executable);
	}

}
