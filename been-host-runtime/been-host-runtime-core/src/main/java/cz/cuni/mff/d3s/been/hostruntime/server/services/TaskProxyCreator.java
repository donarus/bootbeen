package cz.cuni.mff.d3s.been.hostruntime.server.services;

import cz.cuni.mff.d3s.been.model.task.Task;
import org.aopalliance.intercept.MethodInterceptor;
import org.springframework.aop.framework.ProxyFactory;

import java.util.Map;

public class TaskProxyCreator {
    public static Task createTaskProxy(Task task, Map<String, Task> tasksMap) {
        ProxyFactory proxyFactory = new ProxyFactory(task);
        proxyFactory.setProxyTargetClass(true); // we are proxying Class and not interface
        proxyFactory.addAdvice((MethodInterceptor) invocation -> {
            // It might be better to use AspectJ, because it supports field interceptors
            Object result = invocation.proceed();
            if (invocation.getMethod().getName().startsWith("set")) {
                tasksMap.put(task.getTaskUuid(), task);
            }
            return result;
        });
        return (Task) proxyFactory.getProxy();
    }
}
