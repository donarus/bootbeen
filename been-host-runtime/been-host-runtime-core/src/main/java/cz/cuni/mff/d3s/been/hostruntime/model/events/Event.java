package cz.cuni.mff.d3s.been.hostruntime.model.events;

import java.io.Serializable;

/**
 * Interface for events from {@link cz.cuni.mff.d3s.been.hostruntime.client.HostRuntimeClientImpl} to
 * {@link cz.cuni.mff.d3s.been.hostruntime.server.daemons.EventListener}.
 */
public interface Event extends Serializable {

}
