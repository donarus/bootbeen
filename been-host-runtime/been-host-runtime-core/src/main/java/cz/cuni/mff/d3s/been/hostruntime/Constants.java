package cz.cuni.mff.d3s.been.hostruntime;

public final class Constants {
    public static final String HR_SERVICE_NAME = "host runtime";
    public static final String HR_SERVICE_VENDOR = "cz.cuni.mff.d3s.been";
    public static final String HR_SERVICE_VERSION = "SHR 1.0";
    public static final String HR_SERVICE_DESCRIPTION = "Simple host runtime.";

    public static final String EVENT_QUEUE = "been::host_runtime::event_queue";
    public static final String TASKS_MAP = "been::host_runtime::tasks_map";
    public static final String BENCHMARKS_MAP = "been::host_runtime::benchmarks_map";
    public static final String TASK_CONTEXT_SHARED_VARIABLES_MAP = "been::host_runtime::task_context_shared_variables_map";
    public static final String START_TASKS_LOCK = "been::host_runtime::start_tasks_lock";
    public static final String START_BENCHMARKS_LOCK = "been::host_runtime::start_benchmarks_lock";

    public static final String TASK_RESTARTER_SCHEDULER = "been::host_runtime::task_restarter_scheduler";
    public static final String RUNTIME_CLEANER_SCHEDULER = "been::host_runtime::runtime_cleaner_scheduler";
}
