package cz.cuni.mff.d3s.been.hostruntime.server.daemons;

import cz.cuni.mff.d3s.been.hostruntime.HostRuntimeStructures;
import cz.cuni.mff.d3s.been.hostruntime.model.events.*;
import cz.cuni.mff.d3s.been.hostruntime.server.services.BenchmarkService;
import cz.cuni.mff.d3s.been.hostruntime.server.services.TaskService;
import cz.cuni.mff.d3s.been.utils.jvm.JVM;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Service
@Profile("hr-server")
public class EventListener implements Runnable {
    private static final Logger LOG = LoggerFactory.getLogger(EventListener.class);

    @Autowired
    private HostRuntimeStructures structures;

    @Autowired
    private BenchmarkService benchmarkService;

    @Autowired
    private TaskService taskService;

    private void processEvent(Event event) {
        if (event instanceof RunTaskEvent) {
            RunTaskEvent runTaskEvent = (RunTaskEvent) event;
            taskService.createTask(runTaskEvent.getBpkId(), runTaskEvent.getTaskDescriptor(), runTaskEvent.getTaskContextId(), runTaskEvent.getBenchmarkId());
        } else if (event instanceof KillTaskEvent) {
            KillTaskEvent killTaskEvent = (KillTaskEvent) event;
            taskService.killTask(killTaskEvent.getTaskUuid());
        } else if (event instanceof RunBenchmarkEvent) {
            RunBenchmarkEvent runBenchmarkEvent = (RunBenchmarkEvent) event;
            benchmarkService.prepareBenchmarkToRun(runBenchmarkEvent.getBenchmarkId(), runBenchmarkEvent.getBenchmarkPlan());
        } else if (event instanceof KillTaskContextEvent) {
            KillTaskContextEvent killTaskContextEvent = (KillTaskContextEvent) event;
            taskService.killTaskInContext(killTaskContextEvent.getTaskContextUuid());
        } else if (event instanceof KillBenchmarkEvent) {
            KillBenchmarkEvent killBenchmarkEvent = (KillBenchmarkEvent) event;
            benchmarkService.killBenchmark(killBenchmarkEvent.getBenchmarkUuid());
        } else if (event instanceof InvokeTaskAndBenchmarkStarter) {
            taskService.startPreparedTasks();
            benchmarkService.startPreparedBenchmarks();
        } else {
            LOG.warn("Unknown task event type '{}', ignoring", event.getClass().getName());
        }
    }

    private ExecutorService executor = Executors.newCachedThreadPool();

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                final Event event = structures.eventQueue().poll(Long.MAX_VALUE, TimeUnit.DAYS);
                if (event != null) {
                    executor.submit(() -> {
                        try {
                            processEvent(event);
                        } catch (Exception e) {
                            LOG.error("Unexpected exception occured during processing event", e);
                        }
                    });
                }
            } catch (InterruptedException e) {
                if (JVM.isShuttingDown()) {
                    LOG.info("JVM is shutting down, stopping " + EventListener.class.getSimpleName());
                    return;
                } else {
                    LOG.error("Caught InterruptedException while listening for task events", e);
                }
            } catch (Exception e) {
                 LOG.error("Unexpected exception occured during polling from event queue event", e);
            }
        }
    }

    @PostConstruct
    public void start() {
        new Thread(this).start();
    }
}
