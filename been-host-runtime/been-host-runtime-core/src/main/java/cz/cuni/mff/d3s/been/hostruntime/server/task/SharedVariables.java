package cz.cuni.mff.d3s.been.hostruntime.server.task;

import cz.cuni.mff.d3s.been.hostruntime.HostRuntimeStructures;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SharedVariables {

    @Autowired
    private HostRuntimeStructures hostRuntimeStructures;

    public void setVariable(String taskContext, String variableName, String variableValue) {
        String key = taskContext + "-" + variableName;
        hostRuntimeStructures.contextSharedVariables().put(key, variableValue);
    }

    public String getVariable(String taskContext, String variableName) {
        String key = taskContext + "-" + variableName;
        return hostRuntimeStructures.contextSharedVariables().get(key);
    }

    public void deleteAll(String taskContextId) {
        hostRuntimeStructures.contextSharedVariables().removeAll((key, value) -> key.startsWith(taskContextId));
    }

}
