package cz.cuni.mff.d3s.been.hostruntime.server.services;

import cz.cuni.mff.d3s.been.hostruntime.server.task.TaskDriver;
import cz.cuni.mff.d3s.been.hostruntime.server.task.TaskDriverException;
import cz.cuni.mff.d3s.been.model.node.NodeInfo;
import cz.cuni.mff.d3s.been.model.task.Task;
import cz.cuni.mff.d3s.been.springsupport.SpringContextHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class RunningTaskWrapper implements Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(RunningTaskWrapper.class);

    private final Task task;

    private final TaskDriver taskDriver;

    private final NodeInfo nodeInfo;

    RunningTaskWrapper(Task task) {
        this.task = task;
        this.taskDriver = SpringContextHolder.applicationContext().getBean(TaskDriver.class);
        this.nodeInfo = SpringContextHolder.applicationContext().getBean(NodeInfo.class);
    }

    @Override
    public void run() {
        task.setNodeUuid(nodeInfo.getUuid());
        try {
            LOG.debug("Requesting task driver to run task");
            taskDriver.runTask(task);
            LOG.debug("Task driver finished its work for task [" + task.getTaskUuid() + "] at node [" + nodeInfo.getUuid() + "].");
        } catch (TaskDriverException e) {
            LOG.error("Task driver failed for task [" + task.getTaskUuid() + "] at node [" + nodeInfo.getUuid() + "].", e);
        }
    }

}
