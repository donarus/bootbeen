package cz.cuni.mff.d3s.been.hostruntime.model.events;

import java.io.Serializable;

/**
 * Kill task event for
 * {@link cz.cuni.mff.d3s.been.hostruntime.server.daemons.EventListener}.
 */
public class KillTaskContextEvent implements Event, Serializable {
    private String taskContextUuid;

    public KillTaskContextEvent(String taskContextUuid) {
        this.taskContextUuid = taskContextUuid;
    }

    public String getTaskContextUuid() {
        return taskContextUuid;
    }
}
