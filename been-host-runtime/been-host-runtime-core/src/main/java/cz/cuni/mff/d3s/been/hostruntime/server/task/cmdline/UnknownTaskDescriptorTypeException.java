package cz.cuni.mff.d3s.been.hostruntime.server.task.cmdline;

public class UnknownTaskDescriptorTypeException extends Exception {
    public UnknownTaskDescriptorTypeException(String message) {
        super(message);
    }
}
