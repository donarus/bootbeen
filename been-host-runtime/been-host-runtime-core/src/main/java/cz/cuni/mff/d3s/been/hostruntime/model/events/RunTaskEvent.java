package cz.cuni.mff.d3s.been.hostruntime.model.events;

import cz.cuni.mff.d3s.been.model.bpk.BpkId;
import cz.cuni.mff.d3s.been.model.td.TaskDescriptor;

import java.io.Serializable;

/**
 * Run task event for
 * {@link cz.cuni.mff.d3s.been.hostruntime.server.daemons.EventListener}.
 */
public class RunTaskEvent implements Event, Serializable {
    private TaskDescriptor taskDescriptor;
    private BpkId bpkId;
    private String taskContextId;
    private String benchmarkId;

    public RunTaskEvent(BpkId bpkId, TaskDescriptor taskDescriptor, String taskContextId, String benchmarkId) {
        this.bpkId = bpkId;
        this.taskDescriptor = taskDescriptor;
        this.taskContextId = taskContextId;
        this.benchmarkId = benchmarkId;
    }

    public TaskDescriptor getTaskDescriptor() {
        return taskDescriptor;
    }

    public BpkId getBpkId() {
        return bpkId;
    }

    public String getTaskContextId() {
        return taskContextId;
    }

    public String getBenchmarkId() {
        return benchmarkId;
    }
}
