package cz.cuni.mff.d3s.been.hostruntime.model;

import cz.cuni.mff.d3s.been.model.service.ServiceInfo;

import java.io.Serializable;

import static cz.cuni.mff.d3s.been.hostruntime.Constants.*;

public class HostRuntimeServiceInfo extends ServiceInfo implements Serializable {

    public HostRuntimeServiceInfo(String nodeUuid, String uuid) {
        super(HR_SERVICE_NAME, HR_SERVICE_VENDOR, HR_SERVICE_VERSION, nodeUuid, uuid);
    }

    @Override
    public String getDescription() {
        return HR_SERVICE_DESCRIPTION;
    }

}
