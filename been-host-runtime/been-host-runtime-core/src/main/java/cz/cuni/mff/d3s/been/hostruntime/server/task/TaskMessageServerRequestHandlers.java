package cz.cuni.mff.d3s.been.hostruntime.server.task;

import cz.cuni.mff.d3s.been.api.clients.LogStorageClient;
import cz.cuni.mff.d3s.been.api.clients.ResultsStorageClient;
import cz.cuni.mff.d3s.been.api.cluster.ClusterTimestampGenerator;
import cz.cuni.mff.d3s.been.api.cluster.ClusterUuidGenerator;
import cz.cuni.mff.d3s.been.hostruntime.client.Benchmarks;
import cz.cuni.mff.d3s.been.hostruntime.server.services.Checkpoints;
import cz.cuni.mff.d3s.been.taskapi.messaging.ServerRequestForResponseHandler;
import cz.cuni.mff.d3s.been.taskapi.messaging.ServerRequestHandler;
import cz.cuni.mff.d3s.been.taskapi.messaging.service.log.StoreLogRequest;
import cz.cuni.mff.d3s.been.taskapi.messaging.service.misc.GetActualBenchmarkPlanRequest;
import cz.cuni.mff.d3s.been.taskapi.messaging.service.misc.GetActualBenchmarkPlanResponse;
import cz.cuni.mff.d3s.been.taskapi.messaging.service.result.GetStoredResultResponse;
import cz.cuni.mff.d3s.been.taskapi.messaging.service.result.GetStoredResultsRequest;
import cz.cuni.mff.d3s.been.taskapi.messaging.service.result.StoreResultRequest;
import cz.cuni.mff.d3s.been.taskapi.messaging.service.shared.GetSharedPropRequest;
import cz.cuni.mff.d3s.been.taskapi.messaging.service.shared.GetSharedPropResponse;
import cz.cuni.mff.d3s.been.taskapi.messaging.service.shared.SetSharedPropRequest;
import cz.cuni.mff.d3s.been.taskapi.messaging.service.sync.*;
import cz.cuni.mff.d3s.been.taskapi.messaging.service.timestamp.GenerateTimestampRequest;
import cz.cuni.mff.d3s.been.taskapi.messaging.service.timestamp.GenerateTimestampResponse;
import cz.cuni.mff.d3s.been.taskapi.messaging.service.uuid.GenerateUuidRequest;
import cz.cuni.mff.d3s.been.taskapi.messaging.service.uuid.GenerateUuidResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TaskMessageServerRequestHandlers {

    private static final Logger LOG = LoggerFactory.getLogger(TaskMessageServerRequestHandlers.class);

    @Autowired
    private ClusterUuidGenerator uuidGenerator;

    @Autowired
    private ClusterTimestampGenerator timestampGenerator;

    @Autowired
    private LogStorageClient logClient;

    @Autowired
    private ResultsStorageClient resultsStorageClient;

    @Autowired
    private SharedVariables sharedVariables;

    @Autowired
    private Checkpoints checkpoints;

    @Autowired
    private Benchmarks benchmarks;

    public ServerRequestHandler<StoreLogRequest> storeLogRequestHandler() {
        return new ServerRequestHandler<StoreLogRequest>() {
            @Override
            protected void handleRequest(String taskId, String taskContextId, String benchmarkId, StoreLogRequest request) {
                logClient.log(taskId, request.getLogMessage());
            }

            @Override
            public Class<StoreLogRequest> getRequestClass() {
                return StoreLogRequest.class;
            }
        };
    }

    public ServerRequestHandler<StoreResultRequest> storeResultRequestHandler() {
        return new ServerRequestHandler<StoreResultRequest>() {
            @Override
            protected void handleRequest(String taskId, String taskContextId, String benchmarkId, StoreResultRequest request) {
                resultsStorageClient.storeResult(request.getResult());
            }

            @Override
            public Class<StoreResultRequest> getRequestClass() {
                return StoreResultRequest.class;
            }
        };
    }

    public ServerRequestHandler<SetSharedPropRequest> setSharedPropRequestHandler() {
        return new ServerRequestHandler<SetSharedPropRequest>() {
            @Override
            protected void handleRequest(String taskId, String taskContextId, String benchmarkId, SetSharedPropRequest request) {
                sharedVariables.setVariable(taskContextId, request.getKey(), request.getValue());
            }

            @Override
            public Class<SetSharedPropRequest> getRequestClass() {
                return SetSharedPropRequest.class;
            }
        };
    }

    public ServerRequestHandler<CheckpointReachedRequest> checkpointReachedRequestHandler() {
        return new ServerRequestHandler<CheckpointReachedRequest>() {
            @Override
            protected void handleRequest(String taskId, String taskContextId, String benchmarkId, CheckpointReachedRequest request) {
                LOG.debug("Task [{}] is informing others that context [{}] checkpoint [{}] has been reached", taskId, taskContextId, request.getCheckpointName());
                checkpoints.checkpointReached(taskContextId, request.getCheckpointName());
            }

            @Override
            public Class<CheckpointReachedRequest> getRequestClass() {
                return CheckpointReachedRequest.class;
            }
        };
    }

    public ServerRequestHandler<LatchCountDownRequest> latchCountDownRequestHandler() {
        return new ServerRequestHandler<LatchCountDownRequest>() {
            @Override
            protected void handleRequest(String taskId, String taskContextId, String benchmarkId, LatchCountDownRequest request) {
                LOG.debug("Task [{}] is informing master that it decreased latch [{}] in context [{}]", taskId, request.getLatchName(), taskContextId);
                checkpoints.latchCountDown(taskContextId, request.getLatchName());
            }

            @Override
            public Class<LatchCountDownRequest> getRequestClass() {
                return LatchCountDownRequest.class;
            }
        };
    }

    public ServerRequestForResponseHandler<WaitForCheckpointRequest> waitForCheckpointRequestHandler() {
        return new ServerRequestForResponseHandler<WaitForCheckpointRequest>() {
            @Override
            protected WaitForCheckpointResponse handleRequest(String taskId, String taskContextId, String benchmarkId, WaitForCheckpointRequest request) {
                LOG.debug("Task [{}] in context [{}] is waiting for checkpoint [{}]", taskId, taskContextId, request.getCheckpointName());
                checkpoints.waitForCheckpoint(taskContextId, request.getCheckpointName());
                LOG.debug("Task [{}] in context [{}] will receive checkpoint [{}]", taskId, taskContextId, request.getCheckpointName());
                return new WaitForCheckpointResponse();
            }

            @Override
            public Class<WaitForCheckpointRequest> getRequestClass() {
                return WaitForCheckpointRequest.class;
            }
        };
    }

    public ServerRequestForResponseHandler<WaitForLatchRequest> waitForLatchRequestHandler() {
        return new ServerRequestForResponseHandler<WaitForLatchRequest>() {
            @Override
            protected WaitForLatchResponse handleRequest(String taskId, String taskContextId, String benchmarkId, WaitForLatchRequest request) {
                LOG.debug("Task [{}] in context [{}] is waiting for latch [{}]", taskId, taskContextId, request.getLatchName());
                checkpoints.waitForLatch(taskContextId, request.getLatchName(), request.getInitialCount());
                LOG.debug("Task [{}] in context [{}] will receive latch [{}]", taskId, taskContextId, request.getLatchName());
                return new WaitForLatchResponse();
            }

            @Override
            public Class<WaitForLatchRequest> getRequestClass() {
                return WaitForLatchRequest.class;
            }
        };
    }

    public ServerRequestForResponseHandler<GetSharedPropRequest> getSharedPropRequestHandler() {
        return new ServerRequestForResponseHandler<GetSharedPropRequest>() {
            @Override
            protected GetSharedPropResponse handleRequest(String taskId, String taskContextId, String benchmarkId, GetSharedPropRequest request) {
                String variableName = request.getKey();
                return new GetSharedPropResponse(sharedVariables.getVariable(taskContextId, variableName));
            }

            @Override
            public Class<GetSharedPropRequest> getRequestClass() {
                return GetSharedPropRequest.class;
            }
        };
    }

    public ServerRequestForResponseHandler<GetStoredResultsRequest> getStoredResultsRequestHandler() {
        return new ServerRequestForResponseHandler<GetStoredResultsRequest>() {
            @Override
            protected GetStoredResultResponse handleRequest(String taskId, String taskContextId, String benchmarkId, GetStoredResultsRequest request) {
                String forTaskId = request.getTaskId();
                return new GetStoredResultResponse(resultsStorageClient.getResultsForTask(forTaskId));
            }

            @Override
            public Class<GetStoredResultsRequest> getRequestClass() {
                return GetStoredResultsRequest.class;
            }
        };
    }

    public ServerRequestForResponseHandler<GetActualBenchmarkPlanRequest> getActualBenchmarkPlanRequestHandler() {
        return new ServerRequestForResponseHandler<GetActualBenchmarkPlanRequest>() {
            @Override
            protected GetActualBenchmarkPlanResponse handleRequest(String taskId, String taskContextId, String benchmarkId, GetActualBenchmarkPlanRequest request) {
                return new GetActualBenchmarkPlanResponse(benchmarks.getBenchmarkByUuid(benchmarkId).getBenchmarkPlan());
            }

            @Override
            public Class<GetActualBenchmarkPlanRequest> getRequestClass() {
                return GetActualBenchmarkPlanRequest.class;
            }
        };
    }

    public ServerRequestForResponseHandler<GenerateUuidRequest> generateUuidRequestHandler() {
        return new ServerRequestForResponseHandler<GenerateUuidRequest>() {
            @Override
            protected GenerateUuidResponse handleRequest(String taskId, String taskContextId, String benchmarkId, GenerateUuidRequest request) {
                return new GenerateUuidResponse(uuidGenerator.generate());
            }

            @Override
            public Class<GenerateUuidRequest> getRequestClass() {
                return GenerateUuidRequest.class;
            }
        };
    }

    public ServerRequestForResponseHandler<GenerateTimestampRequest> generateTimestampRequestHandler() {
        return new ServerRequestForResponseHandler<GenerateTimestampRequest>() {
            @Override
            protected GenerateTimestampResponse handleRequest(String taskId, String taskContextId, String benchmarkId, GenerateTimestampRequest request) {
                return new GenerateTimestampResponse(timestampGenerator.generate());
            }

            @Override
            public Class<GenerateTimestampRequest> getRequestClass() {
                return GenerateTimestampRequest.class;
            }
        };
    }

}
