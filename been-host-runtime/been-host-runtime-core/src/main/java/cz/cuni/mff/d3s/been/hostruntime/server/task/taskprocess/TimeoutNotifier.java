package cz.cuni.mff.d3s.been.hostruntime.server.task.taskprocess;

public interface TimeoutNotifier {
    void notifyTimeout();
}
