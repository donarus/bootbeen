package cz.cuni.mff.d3s.been.hostruntime.server.task;

public class TaskDriverException extends Exception {
    public TaskDriverException(String message, Throwable cause) {
        super(message, cause);
    }

    public TaskDriverException(String message) {
        super(message);
    }

    public TaskDriverException(Throwable cause) {
        super(cause);
    }

}
