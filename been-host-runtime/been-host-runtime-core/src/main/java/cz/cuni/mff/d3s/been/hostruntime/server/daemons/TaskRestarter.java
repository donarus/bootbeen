package cz.cuni.mff.d3s.been.hostruntime.server.daemons;

import cz.cuni.mff.d3s.been.hostruntime.HostRuntimeStructures;
import cz.cuni.mff.d3s.been.hostruntime.model.events.InvokeTaskAndBenchmarkStarter;
import cz.cuni.mff.d3s.been.api.cluster.ClusterExecutorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

import static cz.cuni.mff.d3s.been.hostruntime.Constants.TASK_RESTARTER_SCHEDULER;
import static cz.cuni.mff.d3s.been.springsupport.SpringContextHolder.applicationContext;

@Service
@Profile("hr-server")
public class TaskRestarter {

    @Autowired
    private ClusterExecutorService clusterSchedulerExecutorService;

    @PostConstruct
    public void start() {
        clusterSchedulerExecutorService.scheduleAnywhereOnCluster(
                TASK_RESTARTER_SCHEDULER, () ->
                        applicationContext().getBean(HostRuntimeStructures.class).eventQueue().offer(new InvokeTaskAndBenchmarkStarter()),
                30, 30);
    }
}
