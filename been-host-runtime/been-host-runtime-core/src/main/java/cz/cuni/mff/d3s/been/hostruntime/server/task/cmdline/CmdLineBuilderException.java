package cz.cuni.mff.d3s.been.hostruntime.server.task.cmdline;

public class CmdLineBuilderException extends Exception {
    public CmdLineBuilderException(String message, Throwable cause) {
        super(message, cause);
    }

    public CmdLineBuilderException(String message) {
        super(message);
    }
}
