package cz.cuni.mff.d3s.been.hostruntime.server.task.cmdline;

import cz.cuni.mff.d3s.been.model.td.JavaTaskDescriptor;
import cz.cuni.mff.d3s.been.model.td.NativeTaskDescriptor;
import cz.cuni.mff.d3s.been.model.td.TaskDescriptor;

import java.io.File;

/**
 * Utility class for creating command line builder for tasks based on runtime
 * type.
 */
public final class CmdLineBuilderFactory {

    /**
     * Selects and creates correct {@link CmdLineBuilder} implementation based on
     * task descriptor type.<br>
     *
     * @param taskDescriptor associated TaskDescriptor
     * @param taskDir        task home directory
     * @param tempDir        task temp dir
     * @return correct {@link CmdLineBuilder} implementation
     * @throws UnknownTaskDescriptorTypeException if {@link CmdLineBuilder} implementation for given type of task descriptor
     */
    public static CmdLineBuilder create(TaskDescriptor taskDescriptor, File taskDir, File tempDir) throws UnknownTaskDescriptorTypeException {
        if (taskDescriptor instanceof JavaTaskDescriptor) {
            return new JVMCmdLineBuilder((JavaTaskDescriptor) taskDescriptor, taskDir, tempDir);
        }

        if (taskDescriptor instanceof NativeTaskDescriptor) {
            return new NativeCmdLineBuilder((NativeTaskDescriptor) taskDescriptor, taskDir);
        }

        // FIXME possibly add support for different types of task descriptors
        throw new UnknownTaskDescriptorTypeException("Cannot create command line builder for unknown type of task descriptor: " + taskDescriptor.getClass());
    }

}
