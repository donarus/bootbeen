package cz.cuni.mff.d3s.been.hostruntime.server.daemons;


import cz.cuni.mff.d3s.been.api.cluster.ClusterExecutorService;
import cz.cuni.mff.d3s.been.api.common.BeenNodes;
import cz.cuni.mff.d3s.been.api.common.BeenServices;
import cz.cuni.mff.d3s.been.hostruntime.Constants;
import cz.cuni.mff.d3s.been.hostruntime.HostRuntimeStructures;
import cz.cuni.mff.d3s.been.hostruntime.client.Tasks;
import cz.cuni.mff.d3s.been.hostruntime.model.HostRuntimeServiceInfo;
import cz.cuni.mff.d3s.been.model.node.NodeInfo;
import cz.cuni.mff.d3s.been.model.task.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

import static cz.cuni.mff.d3s.been.hostruntime.Constants.HR_SERVICE_NAME;
import static cz.cuni.mff.d3s.been.hostruntime.Constants.HR_SERVICE_VENDOR;
import static cz.cuni.mff.d3s.been.model.task.TaskState.*;
import static cz.cuni.mff.d3s.been.springsupport.SpringContextHolder.bean;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

@Service
@Profile("hr-server")
public class RuntimeCleaner {
    private static final Logger LOG = LoggerFactory.getLogger(RuntimeCleaner.class);

    @Autowired
    private ClusterExecutorService clusterExecutorService;

    @Autowired
    private BeenNodes beenNodes;

    @Autowired
    private BeenServices beenServices;

    @Autowired
    private Tasks tasks;

    @Autowired
    private NodeInfo nodeInfo;

    @Autowired
    private HostRuntimeStructures structures;

    private void run() {
        LOG.debug("Running runtime cleaner on node [" + nodeInfo.getUuid() + "]");
        List<String> liveNodeUuids = getRuntimeNodes(getHostRuntimes()).stream()
                .map(n -> n.getUuid()).collect(toList());
        List<Task> deadTasks = tasks.list().stream()
                .filter(t -> !liveNodeUuids.contains(t.getNodeUuid()))
                .filter(t -> asList(RUNNING, STARTING).contains(t.getTaskState()))
                .collect(toList());
        for (Task deadTask : deadTasks) {
            deadTask.setTaskState(FAILED);
            // fixme .. possible improvement - add one sentence reason why task failed
            structures.tasksMap().put(deadTask.getTaskUuid(), deadTask);
        }
        // todo fixme clean benchmarks
    }

    private List<HostRuntimeServiceInfo> getHostRuntimes() {
        return beenServices.listByNameAndVendor(HR_SERVICE_NAME, HR_SERVICE_VENDOR);
    }

    private List<NodeInfo> getRuntimeNodes(List<HostRuntimeServiceInfo> runtimes) {
        return beenNodes.listByUuids(runtimes.stream().map(HostRuntimeServiceInfo::getNodeUUID).collect(toList()));
    }

    @PostConstruct
    public void start() {
        beenNodes.addNodeRemovedListener((NodeInfo nodeInfo) -> beenServices.removeByNodeUuid(nodeInfo.getUuid()));

        clusterExecutorService.scheduleAnywhereOnCluster(Constants.RUNTIME_CLEANER_SCHEDULER,
                () -> bean(RuntimeCleaner.class).run(), 10, 60);
    }
}
