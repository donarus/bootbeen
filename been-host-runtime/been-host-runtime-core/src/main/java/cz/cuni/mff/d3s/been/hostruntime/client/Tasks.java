package cz.cuni.mff.d3s.been.hostruntime.client;

import cz.cuni.mff.d3s.been.hostruntime.HostRuntimeStructures;
import cz.cuni.mff.d3s.been.model.task.Task;
import cz.cuni.mff.d3s.been.model.task.TaskState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;


/**
 * Helper service allowing us to list running/finished/failed/requested
 * tasks all over the cluster by different criteria.
 */
@Service("HostRuntimeTasks")
public class Tasks {

    @Autowired
    private HostRuntimeStructures structures;

    public List<Task> list() {
        return new ArrayList<>(structures.tasksMap().values());
    }

    public Task getTaskByUuid(String taskUuid) {
        return structures.tasksMap().get(taskUuid);
    }

    public List<Task> listByUuids(Collection<String> taskUuids) {
        return structures.tasksMap().filter((key, value) -> taskUuids.contains(key));
    }

    public List<Task> listByTaskContextUuid(String taskContextUuid) {
        if (taskContextUuid == null) {
            return Collections.emptyList();
        }
        return structures.tasksMap().filter((key, value) -> taskContextUuid.equals(value.getTaskContextUuid()));
    }

    public List<Task> listByNodeUuid(String nodeUuid) {
        return structures.tasksMap().filter((key, value) -> nodeUuid.equals(value.getNodeUuid()));
    }

    public List<Task> listByState(TaskState state) {
        return structures.tasksMap().filter((key, value) -> state.equals(value.getTaskState()));
    }

    public List<Task> listByStates(List<TaskState> states) {
        return structures.tasksMap().filter((key, value) -> states.contains(value.getTaskState()));
    }

    public List<Task> listByStates(TaskState... states) {
        return listByStates(Arrays.asList(states));
    }
}
