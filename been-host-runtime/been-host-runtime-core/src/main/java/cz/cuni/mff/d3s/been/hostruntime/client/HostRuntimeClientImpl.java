package cz.cuni.mff.d3s.been.hostruntime.client;

import cz.cuni.mff.d3s.been.api.clients.HostRuntimeClient;
import cz.cuni.mff.d3s.been.hostruntime.HostRuntimeStructures;
import cz.cuni.mff.d3s.been.hostruntime.model.events.*;
import cz.cuni.mff.d3s.been.model.benchmark.Benchmark;
import cz.cuni.mff.d3s.been.model.benchmark.BenchmarkPlan;
import cz.cuni.mff.d3s.been.model.bpk.BpkId;
import cz.cuni.mff.d3s.been.model.task.Task;
import cz.cuni.mff.d3s.been.model.task.TaskState;
import cz.cuni.mff.d3s.been.model.td.TaskDescriptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Comparator.comparing;

/**
 * Run and kill operations are transformed to events, which are sent ti the cluster using
 * distributed event queue. These events are then processed by
 * {@link cz.cuni.mff.d3s.been.hostruntime.server.daemons.EventListener}.
 * All other operations are read only and are processed directly by this client.
 */
@Service
public class HostRuntimeClientImpl implements HostRuntimeClient {

    @Autowired
    private HostRuntimeStructures structures;

    @Autowired
    private Tasks tasks;

    @Autowired
    private Benchmarks benchmarks;

    @Override
    public void runTask(BpkId bpkId, TaskDescriptor taskDescriptor) {
        structures.eventQueue().offer(new RunTaskEvent(bpkId, taskDescriptor, null, null));
    }

    @Override
    public void runBenchmark(BenchmarkPlan benchmarkPlan) {
        structures.eventQueue().offer(new RunBenchmarkEvent(benchmarkPlan.getBenchmarkUuid(), benchmarkPlan));
    }

    @Override
    public void killTask(Task task) {
        killTask(task.getTaskUuid());
    }

    @Override
    public void killTask(String taskUuid) {
        structures.eventQueue().offer(new KillTaskEvent(taskUuid));
    }

    @Override
    public List<Task> listTasks() {
        List<Task> tasksList = tasks.list();
        tasksList.sort(comparing(Task::getTaskStartTime));
        return tasksList;
    }

    @Override
    public List<Task> listTasksByNodeUuid(String nodeUuid) {
        List<Task> tasksList = tasks.listByNodeUuid(nodeUuid);
        tasksList.sort(comparing(Task::getTaskStartTime));
        return tasksList;
    }

    @Override
    public Task getTaskByUuid(String taskUuid) {
        return tasks.getTaskByUuid(taskUuid);
    }

    @Override
    public Benchmark getBenchmarkByUuid(String benchmarkUuid) {
        return benchmarks.getBenchmarkByUuid(benchmarkUuid);
    }

    @Override
    public List<Benchmark> listBenchmarks() {
        return listBenchmarks(true);
    }

    @Override
    public List<Benchmark> listBenchmarks(boolean includePlan) {
        List<Benchmark> benchmarksList = benchmarks.list(includePlan);
        benchmarksList.sort(comparing(Benchmark::getStart));
        return benchmarksList;
    }

    @Override
    public void killBenchmark(String benchmarkUuid) {
        structures.eventQueue().offer(new KillBenchmarkEvent(benchmarkUuid));
    }

    @Override
    public void killTaskContext(String taskContextId) {
        structures.eventQueue().offer(new KillTaskContextEvent(taskContextId));
    }

}
