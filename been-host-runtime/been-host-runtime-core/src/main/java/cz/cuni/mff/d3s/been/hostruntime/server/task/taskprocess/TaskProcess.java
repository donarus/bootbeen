package cz.cuni.mff.d3s.been.hostruntime.server.task.taskprocess;

import cz.cuni.mff.d3s.been.hostruntime.server.task.cmdline.TaskCommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteStreamHandler;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.Executor;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public final class TaskProcess implements AutoCloseable {

    private File workingDirectory;
    private Map<String, String> environmentProperties;
    private long timeoutInMillis;
    private TaskCommandLine taskCommandLine;
    private ExecuteStreamHandler streamHandler;
    private TaskProcessExecutionHandler taskProcessExecutionHandler;

    private Executor executor;
    private boolean userInvokedProcessKill;

    public int start() throws TaskProcessException {
        if (timeoutInMillis  <= 0) {
            timeoutInMillis = ExecuteWatchdog.INFINITE_TIMEOUT;
        }
        NotifyingExecuteWatchdog watchDog = new NotifyingExecuteWatchdog(timeoutInMillis, () -> taskProcessExecutionHandler.setTimeouted());

        executor = new DefaultExecutor();
        executor.setWorkingDirectory(workingDirectory);
        executor.setWatchdog(watchDog);
        executor.setStreamHandler(streamHandler);

        try {
            executor.execute(taskCommandLine, environmentProperties, taskProcessExecutionHandler);
            taskProcessExecutionHandler.waitFor();
            return taskProcessExecutionHandler.getExitValue();
        } catch (IOException e) {
            throw new TaskProcessException("Execution of subprocess failed", e);
        } catch (InterruptedException e) {
            throw new TaskProcessException("Interrupted while waiting for task process to finish", e);
        }
    }

    public void kill() {
        userInvokedProcessKill = true;
        taskProcessExecutionHandler.setKilledByUserFlag();
        executor.getWatchdog().destroyProcess();
    }

    @Override
    public void close() throws Exception {
        if (!this.userInvokedProcessKill) {
            executor.getWatchdog().destroyProcess();
        }

        streamHandler.stop();
    }

    public void setWorkingDirectory(File workingDirectory) {
        this.workingDirectory = workingDirectory;
    }

    public void setEnvironmentProperties(Map<String, String> environmentProperties) {
        this.environmentProperties = environmentProperties;
    }

    public void setTimeoutInMillis(long timeoutInMillis) {
        this.timeoutInMillis = timeoutInMillis;
    }

    public void setTaskCommandLine(TaskCommandLine taskCommandLine) {
        this.taskCommandLine = taskCommandLine;
    }

    public void setStreamHandler(ExecuteStreamHandler streamHandler) {
        this.streamHandler = streamHandler;
    }

    public void setTaskProcessExecutionHandler(TaskProcessExecutionHandler taskProcessExecutionHandler) {
        this.taskProcessExecutionHandler = taskProcessExecutionHandler;
    }
}
