package cz.cuni.mff.d3s.been.hostruntime.server.task;

import java.io.*;

public class LogFileOutputStream extends OutputStream {

    private final OutputStream logFileStream;

    public LogFileOutputStream(File logFile) throws FileNotFoundException {
        super();
        logFileStream = new FileOutputStream(logFile);
    }

    @Override
    public void write(int b) throws IOException {
        logFileStream.write(b);
        //System.out.write(b);
    }

    @Override
    public void close() throws IOException {
        super.close();
        logFileStream.close();
    }
}
