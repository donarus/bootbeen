package cz.cuni.mff.d3s.been.taskapi.maven;


import cz.cuni.mff.d3s.been.taskapi.SuperTask;
import cz.cuni.mff.d3s.been.taskapi.TaskException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.cli.MavenCli;
import org.codehaus.plexus.classworlds.ClassWorld;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import static org.apache.maven.cli.MavenCli.LOCAL_REPO_PROPERTY;
import static org.apache.maven.cli.MavenCli.MULTIMODULE_PROJECT_DIRECTORY;

public class MavenSupport {
    private static final Logger LOG = LoggerFactory.getLogger(MavenSupport.class);

    private final SuperTask task;
    private boolean useSystemRepo = false;

    private MavenSupport(SuperTask forTask) {
        this.task = forTask;
    }

    public static MavenSupport forTask(SuperTask task) {
        return new MavenSupport(task);
    }

    public List<File> downloadDependencies(String groupId, String artifactId, String version) throws Exception {
        String originalMultimoduleProjectDirectory = System.getProperty(MULTIMODULE_PROJECT_DIRECTORY);
        String originalLocalRepoProperty = System.getProperty(LOCAL_REPO_PROPERTY);
        try {
            File parent = mkdir(task.getTaskTempDir(), task.generateUuid());
            File projectDir = mkdir(parent, "project");

            downloadPom(groupId, artifactId, version, projectDir);

            System.setProperty(MULTIMODULE_PROJECT_DIRECTORY, projectDir.getAbsolutePath());

            if (!useSystemRepo) {
                File repo = mkdir(parent, "mvn_repo");
                System.setProperty(LOCAL_REPO_PROPERTY, repo.getAbsolutePath());
            }

            File libs = mkdir(task.getTaskTempDir(), "libs");
            ClassWorld myClassWorld = new ClassWorld("plexus.core", getClass().getClassLoader());

            String artifact = groupId + ":" + artifactId + ":" + version;
            new MavenCli(myClassWorld).doMain(new String[]{
                    "dependency:copy",
                    "-Dartifact=" + artifact,
                    "-DoutputDirectory=" + libs.getAbsolutePath()
            }, projectDir.getAbsolutePath(), System.out, System.err);

            new MavenCli(myClassWorld).doMain(new String[]{
                    "dependency:copy-dependencies",
                    "-DoutputDirectory=" + libs.getAbsolutePath(),
                    "-DincludeScope=runtime"
            }, projectDir.getAbsolutePath(), System.out, System.err);

            return Arrays.asList(libs.listFiles());
        } finally {
            if (originalMultimoduleProjectDirectory != null) {
                System.setProperty(MULTIMODULE_PROJECT_DIRECTORY, originalMultimoduleProjectDirectory);
            } else {
                System.clearProperty(MULTIMODULE_PROJECT_DIRECTORY);
            }

            if (originalLocalRepoProperty != null) {
                System.setProperty(LOCAL_REPO_PROPERTY, originalLocalRepoProperty);
            } else {
                System.clearProperty(LOCAL_REPO_PROPERTY);
            }
        }
    }

    private void downloadPom(String groupId, String artifactId, String version, File destDir) throws TaskException {
        String url = "http://central.maven.org/maven2/";
        url += StringUtils.replace(groupId, ".", "/") + "/";
        url += artifactId + "/";
        url += version + "/";
        url += artifactId + "-" + version + ".pom";

        download(url, new File(destDir, "pom.xml"));
    }

    private File mkdir(File parent, String project) {
        File projectDir = new File(parent, project);
        projectDir.mkdir();
        return projectDir;
    }

    private File download(String url, File destination) throws TaskException {
        try {
            FileUtils.copyURLToFile(new URL(url), destination);
            return destination;
        } catch (Exception e) {
            LOG.error("error during downloading file from {} ", url, e);
            throw new TaskException("error during downloading file from " + url, e);
        }
    }

    public MavenSupport setUseSystemRepo(boolean useSystemRepo) {
        this.useSystemRepo = useSystemRepo;
        return this;
    }

    public boolean isUseSystemRepo() {
        return useSystemRepo;
    }
}
