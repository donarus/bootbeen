package cz.cuni.mff.d3s.been.taskapi.messaging.service.timestamp;

import cz.cuni.mff.d3s.been.taskapi.messaging.RequestForResponse;

public class GenerateTimestampRequest extends RequestForResponse {

    @Override
    public int timeoutSeconds() {
        return 5;
    }

}
