package cz.cuni.mff.d3s.been.taskapi.messaging.service.shared;

import cz.cuni.mff.d3s.been.taskapi.messaging.RequestForResponse;

public class GetSharedPropRequest extends RequestForResponse {
    private String key;

    public GetSharedPropRequest() {}

    public GetSharedPropRequest(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public int timeoutSeconds() {
        return 5;
    }

}
