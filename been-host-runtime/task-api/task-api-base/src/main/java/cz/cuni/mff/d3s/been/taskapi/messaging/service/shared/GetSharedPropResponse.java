package cz.cuni.mff.d3s.been.taskapi.messaging.service.shared;

import cz.cuni.mff.d3s.been.taskapi.messaging.Response;

public class GetSharedPropResponse extends Response {
    private String value;

    public GetSharedPropResponse() {
    }

    public GetSharedPropResponse(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
