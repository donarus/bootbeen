package cz.cuni.mff.d3s.been.taskapi.messaging.service.uuid;

import cz.cuni.mff.d3s.been.taskapi.messaging.RequestForResponse;

public class GenerateUuidRequest extends RequestForResponse {

    @Override
    public int timeoutSeconds() {
        return 5;
    }

}
