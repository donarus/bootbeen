package cz.cuni.mff.d3s.been.taskapi.logging.impl;

import cz.cuni.mff.d3s.been.model.log.LogLevel;

public interface LogLevelGetter {
    LogLevel getLogLevel();
}
