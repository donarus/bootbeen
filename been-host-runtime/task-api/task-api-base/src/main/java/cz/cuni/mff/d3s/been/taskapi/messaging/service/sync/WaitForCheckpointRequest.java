package cz.cuni.mff.d3s.been.taskapi.messaging.service.sync;

import cz.cuni.mff.d3s.been.taskapi.messaging.RequestForResponse;

public class WaitForCheckpointRequest extends RequestForResponse {
    private String checkpointName;

    public WaitForCheckpointRequest() {
    }

    public WaitForCheckpointRequest(String checkpointName) {
        this.checkpointName = checkpointName;
    }

    public String getCheckpointName() {
        return checkpointName;
    }

    public void setCheckpointName(String checkpointName) {
        this.checkpointName = checkpointName;
    }

    @Override
    public int timeoutSeconds() {
        return NO_TIMEOUT;
    }
}
