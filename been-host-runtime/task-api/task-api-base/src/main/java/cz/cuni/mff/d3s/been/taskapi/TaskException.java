package cz.cuni.mff.d3s.been.taskapi;

/**
 * This exception should be thrown in case something went wrong during task execution
 */
public class TaskException extends Exception {
    public TaskException(String message, Throwable cause) {
        super(message, cause);
    }

    public TaskException(String message) {
        super(message);
    }

    public TaskException(Throwable cause) {
        super(cause);
    }
}
