package cz.cuni.mff.d3s.been.taskapi.messaging;

import cz.cuni.mff.d3s.been.utils.json.JsonException;
import cz.cuni.mff.d3s.been.utils.json.JsonUtils;

public class Common {

    static Message deserializeMessage(String serializedRequest) throws MessagingException {
        try {
            return JsonUtils.read(serializedRequest, Message.class);
        } catch (JsonException e) {
            throw new MessagingException("Can't deserialize message", e);
        }
    }

    static String serializeMessage(Message message) throws MessagingException {
        try {
            return JsonUtils.writeToString(message);
        } catch (JsonException e) {
            throw new MessagingException("Can't serialize message.", e);
        }
    }

}
