package cz.cuni.mff.d3s.been.taskapi;

import cz.cuni.mff.d3s.been.model.benchmark.BenchmarkPlan;
import cz.cuni.mff.d3s.been.utils.json.JsonException;
import cz.cuni.mff.d3s.been.utils.json.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * This is the base class for implementing benchmark generators. Each real generator should extend this class and implement
 * generatePlan method. see {@link SuperTask}
 */
public abstract class BenchmarkPlanGenerator extends SuperTask {

    private static final Logger LOG = LoggerFactory.getLogger(BenchmarkPlanGenerator.class);

    private File generatedBenchmarkPlanLocation;


    /**
     * In this method should be implementation of generator logic.
     *
     * @param args real arguments from command line
     * @return generated benchmark plan with all task descriptors set
     * @throws TaskException in case something went wrong during generator execution
     */
    protected abstract BenchmarkPlan generatePlan(String[] args) throws TaskException;

    /**
     * This method is called by {@link TaskRunner}.
     * <p>
     * The generated benchmark plan is written to {@link BenchmarkPlanGenerator#generatedBenchmarkPlanLocation} which
     * has been already set by {@link BenchmarkPlanGenerator#setGeneratedBenchmarkPlanLocation} method
     *
     * @param args arguments from command line
     * @throws TaskException in case something went wrong during generator execution
     */
    public final void generate(String[] args) throws TaskException {
        // wrapped in abstract method in case we'll want to change some
        // default behavior of this method in future
        LOG.info("Starting benchmark plan generator with arguments: [{}]", String.join(", ", args));

        BenchmarkPlan plan = generatePlan(args);

        try {
            JsonUtils.writeToFile(plan, generatedBenchmarkPlanLocation);
        } catch (JsonException e) {
            throw new TaskException("Unable to store generated benchmark plan", e);
        }
    }

    public final void setGeneratedBenchmarkPlanLocation(File generatedBenchmarkPlanLocation) {
        this.generatedBenchmarkPlanLocation = generatedBenchmarkPlanLocation;
    }

}
