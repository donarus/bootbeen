package cz.cuni.mff.d3s.been.taskapi.messaging;

public abstract class Response implements Message {
    private String requestId;

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
}
