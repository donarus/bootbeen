package cz.cuni.mff.d3s.been.taskapi.messaging.service.result.model;

public final class ResultItem {
    private String key;
    private String taskId;
    private String taskContextId;
    private String benchmarkId;
    private Result result;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getTaskContextId() {
        return taskContextId;
    }

    public  void setTaskContextId(String taskContextId) {
        this.taskContextId = taskContextId;
    }

    public String getBenchmarkId() {
        return benchmarkId;
    }

    public  void setBenchmarkId(String benchmarkId) {
        this.benchmarkId = benchmarkId;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }
}
