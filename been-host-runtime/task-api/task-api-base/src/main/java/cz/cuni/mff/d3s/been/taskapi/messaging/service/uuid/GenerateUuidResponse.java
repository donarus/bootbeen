package cz.cuni.mff.d3s.been.taskapi.messaging.service.uuid;

import cz.cuni.mff.d3s.been.taskapi.messaging.Response;

public class GenerateUuidResponse extends Response {
    private String uuid;

    public GenerateUuidResponse() {
    }

    public GenerateUuidResponse(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
}
