package cz.cuni.mff.d3s.been.taskapi.messaging;

public abstract class ClientRequestHandler<Req extends Request> {
    public final void handle(Request request) {
        handleRequest((Req) request);
    }

    protected abstract void handleRequest(Req request);

    public abstract Class<Req> getRequestClass();
}
