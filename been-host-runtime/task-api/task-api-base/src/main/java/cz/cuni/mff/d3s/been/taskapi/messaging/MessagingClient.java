package cz.cuni.mff.d3s.been.taskapi.messaging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zeromq.ZMQ;
import org.zeromq.ZMQException;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import static cz.cuni.mff.d3s.been.taskapi.messaging.Common.deserializeMessage;
import static cz.cuni.mff.d3s.been.taskapi.messaging.Common.serializeMessage;
import static org.zeromq.ZMQ.*;

public class MessagingClient {
    private static final int SEND_RECEIVE_TIMEOUT = 5000;

    private static final Logger LOG = LoggerFactory.getLogger(MessagingClient.class);

    private final ZMQ.Context ctx;
    private final Socket sender;
    private final Socket receiver;
    private final Thread listener;

    private AtomicLong requestIdGenerator = new AtomicLong(new Date().getTime());

    private Map<Class<Request>, ClientRequestHandler> requestHandlers = new HashMap<>();

    private ConcurrentMap<String, Object> responseWaiters = new ConcurrentHashMap<>();
    private ConcurrentMap<String, Response> responseHolders = new ConcurrentHashMap<>();
    private volatile boolean terminated;

    public MessagingClient(String serverSenderAddr, String serverReceiverAddr) {
        this.ctx = ZMQ.context(1);

        this.sender = ctx.socket(PUSH);
        this.sender.setSendTimeOut(SEND_RECEIVE_TIMEOUT);
        this.sender.setReceiveTimeOut(SEND_RECEIVE_TIMEOUT);
        this.sender.connect(serverReceiverAddr);

        this.receiver = ctx.socket(PULL);
        this.receiver.setSendTimeOut(SEND_RECEIVE_TIMEOUT);
        this.receiver.setReceiveTimeOut(SEND_RECEIVE_TIMEOUT);
        this.receiver.connect(serverSenderAddr);

        this.listener = new Thread(this::listen); // listen for incoming messages
        listener.start();
    }


    public void terminate() {
        LOG.info("setting terminating to true");
        terminated = true;

        LOG.info("setting linger to 1");
        sender.setLinger(1);
        receiver.setLinger(1);

        LOG.info("terminating socket");
    }

    private void listen() {
        Thread.currentThread().setName("MessagingClient listener");
        while (!terminated) {
            try {
                String serializedMessage = receiver.recvStr();
                if (serializedMessage != null) {
                    process(serializedMessage);
                }
            } catch (Exception e) {
                if (!terminated) {
                    LOG.error("Exception occured in MessagingClient listener. Client will try to continue receive messages", e);
                }
            }
        }
        receiver.close();
        LOG.info("Messaging Client terminated");
    }

    private void process(String serializedMessage) {
        Message message;
        try {
            message = deserializeMessage(serializedMessage);
        } catch (MessagingException e) {
            LOG.error("Cannot deserialize message [{}]", serializedMessage, e);
            return;
        }

        try {
            handleMessage(message);
        } catch (RuntimeException e) {
            LOG.error("Exception occured during handling message [{}]", serializedMessage, e);
        }
    }

    public void send(Request request) throws MessagingException {
        try {
            sender.send(serializeMessage(request));
        } catch (ZMQException e) {
            throw new MessagingException(e);
        }
    }

    public <T extends Response> T sendAndWaitForResponse(RequestForResponse requestForResponse) throws MessagingException, InterruptedException {
        final String id = Long.toHexString(requestIdGenerator.getAndIncrement());
        requestForResponse.setId(id);
        Object mutex = new Object();
        responseWaiters.put(id, mutex);
        try {
            MutexWaiter waiter = new MutexWaiter(mutex);
            waiter.start();

            try {
                send(requestForResponse);
            } catch (MessagingException me) {
                waiter.interrupt();
                throw me;
            }

            // not implemented due to lack of time
            try {
                waiter.join(requestForResponse.hasTimeout() ? TimeUnit.SECONDS.toMillis(requestForResponse.timeoutSeconds()) : 0);
                if (waiter.isAlive()) {
                    throw new MessagingException("Response waiter for request '" + requestForResponse.getId() + "' of type '" + requestForResponse.getClass().getName() + "' timeouted");
                }
            } catch (InterruptedException e) {
                waiter.interrupt();
                throw e;
            }

            if (waiter.failed()) {
                throw waiter.getException();
            }

            return (T) responseHolders.get(id);
        } finally {
            responseWaiters.remove(id);
            responseHolders.remove(id);
        }

    }

    private void handleMessage(Message message) {
        if (Request.class.isAssignableFrom(message.getClass())) {
            handleRequest((Request) message);
        } else if (Response.class.isAssignableFrom(message.getClass())) {
            handleResponse((Response) message);
        } else {
            LOG.error("Unknown message type [{}].", message.getClass());
        }
    }


    private void handleRequest(Request request) {
        ClientRequestHandler requestHandler = requestHandlers.get(request.getClass());
        if (requestHandler != null) {
            requestHandler.handle(request);
        } else {
            LOG.error("Request handler for [{}] not registered.", request.getClass());
        }
    }


    private void handleResponse(Response response) {
        Object mutex = responseWaiters.get(response.getRequestId());
        if (mutex == null) {
            LOG.error("Reciever response for non existing request with id [{}].", response.getRequestId());
            return;
        }

        responseHolders.put(response.getRequestId(), response);
        synchronized (mutex) {
            mutex.notify();
        }
    }

    public MessagingClient addRequestHandler(ClientRequestHandler handler) {
        requestHandlers.put(handler.getRequestClass(), handler);
        return this;
    }

    public boolean isTerminated() {
        return terminated;
    }

    private static final class MutexWaiter extends Thread {

        private final Object mutex;

        private volatile InterruptedException exception;

        private MutexWaiter(Object mutex) {
            this.mutex = mutex;
        }

        @Override
        public void run() {
            try {
                synchronized (mutex) {
                    mutex.wait();
                }
            } catch (InterruptedException e) {
                exception = e;
            }
        }

        public InterruptedException getException() {
            return exception;
        }

        public boolean failed() {
            return exception != null;
        }
    }
}
