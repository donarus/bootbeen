package cz.cuni.mff.d3s.been.taskapi.logging;

import cz.cuni.mff.d3s.been.model.log.LogLevel;
import cz.cuni.mff.d3s.been.model.log.LogMessage;
import cz.cuni.mff.d3s.been.taskapi.logging.impl.TaskLogProcessor;
import cz.cuni.mff.d3s.been.taskapi.logging.impl.TaskLoggerFactory;
import cz.cuni.mff.d3s.been.taskapi.messaging.MessagingClient;
import cz.cuni.mff.d3s.been.taskapi.messaging.MessagingException;
import cz.cuni.mff.d3s.been.taskapi.messaging.service.log.StoreLogRequest;

/**
 * Simple proxy which resends log message to zeromq socket to host runtime which stores them
 */
public class LogProxy implements TaskLogProcessor {
    private final MessagingClient messagingClient;
    private final LogLevel logLevel;

    /**
     * @param messagingClient messaging client used to resend messages to host runtime
     * @param logLevel        log level which should be handled
     */
    public LogProxy(MessagingClient messagingClient, LogLevel logLevel) {
        this.messagingClient = messagingClient;
        this.logLevel = logLevel;
    }

    /**
     * register this proxy to logger. Must be called after the messaging client has been instantiated and connected
     *
     * @return return itself
     */
    public LogProxy register() {
        TaskLoggerFactory.setLogLevel(logLevel);
        TaskLoggerFactory.setLogSender(this);
        return this;
    }

    private void log(LogMessage logMessage) throws MessagingException {
        if (!messagingClient.isTerminated()) {
            messagingClient.send(new StoreLogRequest(logMessage));
        } else {
            System.out.println(logMessage.toDownloadableString());
        }
    }

    @Override
    public void process(LogMessage logMessage) {
        try {
            log(logMessage);
        } catch (MessagingException e) {
            // Logging doesn't work, so re-log does not make sense :D
            System.err.println("Unexpected exception caused that log message [" + logMessage.toDownloadableString() + "] " +
                    "couldn't be sent and persisted to been");
            e.printStackTrace(System.err);
        }
    }
}