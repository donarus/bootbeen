package cz.cuni.mff.d3s.been.taskapi.logging.impl;

import cz.cuni.mff.d3s.been.model.log.LogLevel;
import cz.cuni.mff.d3s.been.model.log.LogMessage;
import org.slf4j.ILoggerFactory;
import org.slf4j.Logger;

import java.util.LinkedList;
import java.util.Queue;

public final class TaskLoggerFactory implements ILoggerFactory {

    private static Queue<LogMessage> logMessageQueue;

    private static TaskLogProcessor taskLogProcessor;

    private static TaskLogProcessor resender;

    private static LogLevelGetterImpl logLevelGetter = new LogLevelGetterImpl();

    private static volatile boolean synchronizedWithBeen = false;

    private static final Object mutex = new Object();

    static {
        logMessageQueue = new LinkedList<>();
        taskLogProcessor = new TaskLogProcessor() {
            @Override
            public void process(LogMessage logMessage) {
                if (synchronizedWithBeen) {
                    resender.process(logMessage);
                } else {
                    synchronized (mutex) {
                        // nested "if" just to prevent synchronized block when been is already synchronized with task
                        if (!synchronizedWithBeen) {
                            logMessageQueue.add(logMessage);
                        } else {
                            resender.process(logMessage);
                        }
                    }
                }
            }
        };
    }

    public static void setLogLevel(LogLevel logLevel) {
        logLevelGetter.logLevel = logLevel;
    }

    @Override
    public Logger getLogger(String name) {
        return new TaskLogger(name, taskLogProcessor, logLevelGetter);
    }

    public static void setLogSender(TaskLogProcessor resender) {
        synchronized (mutex) {
            TaskLoggerFactory.resender = resender;
            while (!logMessageQueue.isEmpty()) {
                LogMessage msg = logMessageQueue.poll();
                if (msg.getLevel().ordinal() >= logLevelGetter.getLogLevel().ordinal()) {
                    resender.process(msg);
                } // else ignore ... uninteresting message
            }
            synchronizedWithBeen = true;
        }
    }

    public static final class LogLevelGetterImpl implements LogLevelGetter {
        private volatile LogLevel logLevel = LogLevel.TRACE; // highest by default .. change later in "setLogSender"

        @Override
        public LogLevel getLogLevel() {
            return logLevel;
        }
    }

}
