package cz.cuni.mff.d3s.been.taskapi;

/**
 * Task API constants
 */
public class TaskApiConstants {
    /**
     * Predefined system environment variables
     */
    public static final class SystemEnv {
        /**
         * Log level under which the task should run - TRACE, WARN, DEBUG, INFO, ERROR
         */
        public static final String TASK_LOG_LEVEL = "TASK_LOG_LEVEL";

        /**
         * task uuid
         */
        public static final String TASK_ID = "TASK_ID";

        /**
         * task context uuid for current task
         */
        public static final String TASK_CONTEXT_ID = "TASK_CONTEXT_ID";

        /**
         * benchmark uuid for current task
         */
        public static final String BENCHMARK_ID = "BENCHMARK_ID";

        /**
         * file system location of actual task descriptor in JSON format
         */
        public static final String CURRENT_TASK_DESCRIPTOR_LOCATION = "CURRENT_TASK_DESCRIPTOR_LOCATION";


        /**
         * file system location of actual bpk descriptor in JSON format
         */
        public static final String BPK_DESCRIPTOR_LOCATION = "BPK_DESCRIPTOR_LOCATION";

        /**
         * file system location of file where benchmark generator should store the generated benchmark plan in JSON format
         */
        public static final String GENERATED_BENCHMARK_PLAN_LOCATION = "GENERATED_BENCHMARK_PLAN_LOCATION";

        /**
         * directory where the BPK has been extracted
         */
        public static final String EXTRACTED_BPK_DIR = "EXTRACTED_BPK_DIR";

        /**
         * empty working directory for the task
         */
        public static final String TASK_WORKING_DIR = "TASK_WORKING_DIR";

        /**
         * empty temp directory for the task
         */
        public static final String TASK_TEMP_DIR = "TASK_TEMP_DIR";

        /**
         * jeroMQ address of HR sender to which the task should connect its receiver
         */
        public static final String COMM_SERVER_SENDER_ADDR = "COMM_SENDER_ADDR";

        /**
         * jeroMQ address of HR receiver to which the task should connect its sender
         */
        public static final String COMM_SERVER_RECEIVER_ADDR = "COMM_RECEIVER_ADDR";
    }
}
