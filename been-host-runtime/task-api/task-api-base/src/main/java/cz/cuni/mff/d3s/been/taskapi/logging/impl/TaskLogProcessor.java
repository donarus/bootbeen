package cz.cuni.mff.d3s.been.taskapi.logging.impl;

import cz.cuni.mff.d3s.been.model.log.LogMessage;

public interface TaskLogProcessor {
    void process(LogMessage logMessage);
}
