package cz.cuni.mff.d3s.been.taskapi.messaging;

public abstract class ServerRequestForResponseHandler<Req extends RequestForResponse> {
    public final Response handle(String taskId, String taskContextId, String benchmarkId, RequestForResponse request) {
        return handleRequest(taskId, taskContextId, benchmarkId, (Req) request);
    }

    protected abstract Response handleRequest(String taskId, String taskContextId, String benchmarkId, Req request);

    public abstract Class<Req> getRequestClass();

}
