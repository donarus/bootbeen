package cz.cuni.mff.d3s.been.taskapi.messaging.service.timestamp;

import cz.cuni.mff.d3s.been.taskapi.messaging.Response;

public class GenerateTimestampResponse extends Response {
    private long timestamp;

    public GenerateTimestampResponse() {
    }

    public GenerateTimestampResponse(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
