package cz.cuni.mff.d3s.been.taskapi;

import cz.cuni.mff.d3s.been.model.benchmark.BenchmarkPlan;
import cz.cuni.mff.d3s.been.model.bpk.Bpk;
import cz.cuni.mff.d3s.been.model.result.SerializedResult;
import cz.cuni.mff.d3s.been.model.td.TaskDescriptor;
import cz.cuni.mff.d3s.been.taskapi.messaging.MessagingClient;
import cz.cuni.mff.d3s.been.taskapi.messaging.MessagingException;
import cz.cuni.mff.d3s.been.taskapi.messaging.service.misc.GetActualBenchmarkPlanRequest;
import cz.cuni.mff.d3s.been.taskapi.messaging.service.misc.GetActualBenchmarkPlanResponse;
import cz.cuni.mff.d3s.been.taskapi.messaging.service.result.GetStoredResultResponse;
import cz.cuni.mff.d3s.been.taskapi.messaging.service.result.GetStoredResultsRequest;
import cz.cuni.mff.d3s.been.taskapi.messaging.service.result.StoreResultRequest;
import cz.cuni.mff.d3s.been.taskapi.messaging.service.result.model.Result;
import cz.cuni.mff.d3s.been.taskapi.messaging.service.shared.GetSharedPropRequest;
import cz.cuni.mff.d3s.been.taskapi.messaging.service.shared.GetSharedPropResponse;
import cz.cuni.mff.d3s.been.taskapi.messaging.service.shared.SetSharedPropRequest;
import cz.cuni.mff.d3s.been.taskapi.messaging.service.sync.*;
import cz.cuni.mff.d3s.been.taskapi.messaging.service.timestamp.GenerateTimestampRequest;
import cz.cuni.mff.d3s.been.taskapi.messaging.service.timestamp.GenerateTimestampResponse;
import cz.cuni.mff.d3s.been.taskapi.messaging.service.uuid.GenerateUuidRequest;
import cz.cuni.mff.d3s.been.taskapi.messaging.service.uuid.GenerateUuidResponse;
import cz.cuni.mff.d3s.been.utils.json.JsonException;
import cz.cuni.mff.d3s.been.utils.json.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public abstract class SuperTask implements ISuperTask {
    private static final Logger LOG = LoggerFactory.getLogger(SuperTask.class);
    private String taskId;
    private String taskContextId;
    private String benchmarkId;
    private Bpk bpkDescriptor;
    private TaskDescriptor taskDescriptor;
    private File extractedBpkDir;
    private File taskWorkingDir;
    private File taskTempDir;
    private MessagingClient messagingClient;

    @Override
    public final void storeResult(Result result) throws TaskException {
        SerializedResult serializedResult = new SerializedResult();
        serializedResult.setBenchmarkId(benchmarkId);
        serializedResult.setTaskContextId(taskContextId);
        serializedResult.setTaskId(taskId);
        try {
            serializedResult.setResultJson(JsonUtils.writeToString(result));
        } catch (JsonException e) {
            String msg = "Task was unable to serialize result";
            LOG.error(msg, e);
            throw new TaskException(msg, e);
        }

        try {
            messagingClient.send(new StoreResultRequest(serializedResult));
        } catch (MessagingException e) {
            String msg = "Task was unable to store result";
            LOG.error(msg, e);
            throw new TaskException(msg, e);
        }
    }

    @Override
    public final BenchmarkPlan getActualBenchmarkPlan() throws TaskException {
        try {
            GetActualBenchmarkPlanResponse response = messagingClient.sendAndWaitForResponse(new GetActualBenchmarkPlanRequest());
            return response.getBenchmarkPlan();
        } catch (MessagingException | InterruptedException e) {
            String msg = "Exception occured while during retrieval of actual benchmark plan for task ['" + taskId + "']";
            LOG.error(msg, e);
            throw new TaskException(msg, e);
        }
    }

    @Override
    public final List<Result> getResults(String taskId) throws TaskException {
        try {
            GetStoredResultResponse response = messagingClient.sendAndWaitForResponse(new GetStoredResultsRequest(taskId));
            List<SerializedResult> serializedResults = response.getResults();
            List<Result> results = new ArrayList<>();
            for (SerializedResult serializedResult : serializedResults) {
                results.add(JsonUtils.read(serializedResult.getResultJson(), Result.class));
            }
            return results;
        } catch (JsonException | MessagingException | InterruptedException e) {
            String msg = "Exception occured while during retrieval of results for task ['" + taskId + "']";
            LOG.error(msg, e);
            throw new TaskException(msg, e);
        }
    }

    @Override
    public final void waitForCheckpoint(String checkpointName) throws TaskException {
        LOG.info("Waiting for checkpoint: {}", checkpointName);
        try {
            WaitForCheckpointResponse response = messagingClient.sendAndWaitForResponse(new WaitForCheckpointRequest(checkpointName));
            LOG.info("Checkpoint {} reached", checkpointName);
        } catch (MessagingException | InterruptedException e) {
            throw new TaskException("Exception occured while waiting for checkpoint ['" + checkpointName + "']", e);
        }
    }


    @Override
    public final void checkpointReached(String checkpointName) throws TaskException {
        LOG.info("Informing that checkpoint {} has been reached", checkpointName);
        try {
            messagingClient.send(new CheckpointReachedRequest(checkpointName));
        } catch (MessagingException e) {
            throw new TaskException("Exception occured while informing that checkpoint ['" + checkpointName + "'] has been reached", e);
        }
    }

    @Override
    public final void waitForLatch(String latchName, int initialCount) throws TaskException {
        LOG.info("Waiting for checkpoint: {}", latchName);
        try {
            WaitForLatchResponse response = messagingClient.sendAndWaitForResponse(new WaitForLatchRequest(latchName, initialCount));
            LOG.info("Latch {} reached", latchName);
        } catch (MessagingException | InterruptedException e) {
            throw new TaskException("Exception occured while waiting for latch ['" + latchName + "']", e);
        }
    }

    @Override
    public final void latchCountDown(String latchName) throws TaskException {
        LOG.info("Decreasing latch {}", latchName);
        try {
            messagingClient.send(new LatchCountDownRequest(latchName));
        } catch (MessagingException e) {
            throw new TaskException("Exception occured while decreasing latch counter for latch ['" + latchName + "']", e);
        }
    }

    @Override
    public final String generateUuid() throws TaskException {
        try {
            GenerateUuidResponse response = messagingClient.sendAndWaitForResponse(new GenerateUuidRequest());
            return response.getUuid();
        } catch (MessagingException | InterruptedException e) {
            throw new TaskException("Exception occured while requesting new uuid");
        }
    }

    @Override
    public final long generateTimestamp() throws TaskException {
        try {
            GenerateTimestampResponse response = messagingClient.sendAndWaitForResponse(new GenerateTimestampRequest());
            return response.getTimestamp();
        } catch (MessagingException | InterruptedException e) {
            throw new TaskException("Exception occured while requesting timestamp");
        }
    }

    @Override
    public final String getSharedProperty(String key) throws TaskException {
        try {
            GetSharedPropResponse response = messagingClient.sendAndWaitForResponse(new GetSharedPropRequest(key));
            return response.getValue();
        } catch (MessagingException | InterruptedException e) {
            throw new TaskException("Exception occured while getting shared property ['" + key + "']");
        }
    }

    @Override
    public final void setSharedProperty(String key, String value) throws TaskException {
        try {
            messagingClient.send(new SetSharedPropRequest(key, value));
        } catch (MessagingException e) {
            throw new TaskException("Exception occured while setting shared property ['" + key + "'='" + value + "']");
        }
    }

    @Override
    public final String getTaskId() {
        return taskId;
    }

    public final void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    @Override
    public final String getTaskContextId() {
        return taskContextId;
    }

    public final void setTaskContextId(String taskContextId) {
        this.taskContextId = taskContextId;
    }

    @Override
    public final String getBenchmarkId() {
        return benchmarkId;
    }

    public final void setBenchmarkId(String benchmarkId) {
        this.benchmarkId = benchmarkId;
    }

    @Override
    public final Bpk getBpkDescriptor() {
        return bpkDescriptor;
    }

    public final void setBpkDescriptor(Bpk bpkDescriptor) {
        this.bpkDescriptor = bpkDescriptor;
    }

    @Override
    public final TaskDescriptor getTaskDescriptor() {
        return taskDescriptor;
    }

    public final void setTaskDescriptor(TaskDescriptor taskDescriptor) {
        this.taskDescriptor = taskDescriptor;
    }

    @Override
    public final File getExtractedBpkDir() {
        return extractedBpkDir;
    }

    public final void setExtractedBpkDir(File extractedBpkDir) {
        this.extractedBpkDir = extractedBpkDir;
    }

    public final void setTaskWorkingDir(File taskWorkingDir) {
        this.taskWorkingDir = taskWorkingDir;
    }

    @Override
    public final File getTaskWorkingDir() {
        return taskWorkingDir;
    }

    public final void setTaskTempDir(File taskTempDir) {
        this.taskTempDir = taskTempDir;
    }

    @Override
    public final File getTaskTempDir() {
        return taskTempDir;
    }

    public final void setMessagingClient(MessagingClient messagingClient) {
        this.messagingClient = messagingClient;
    }

    @Override
    public final boolean sleepQuietely(long time) {
        try {
            Thread.sleep(time);
            return true;
        } catch (InterruptedException e) {
            LOG.error("Interrupted while sleeping", e);
            // ignore
        }
        return false;
    }
}
