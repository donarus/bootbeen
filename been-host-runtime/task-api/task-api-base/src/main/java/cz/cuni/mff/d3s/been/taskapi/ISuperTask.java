package cz.cuni.mff.d3s.been.taskapi;

import cz.cuni.mff.d3s.been.model.benchmark.BenchmarkPlan;
import cz.cuni.mff.d3s.been.model.bpk.Bpk;
import cz.cuni.mff.d3s.been.model.td.TaskDescriptor;
import cz.cuni.mff.d3s.been.taskapi.messaging.service.result.model.Result;

import java.io.File;
import java.util.List;

public interface ISuperTask {

    /**
     * Store result from task
     * @param result result to be stored
     * @throws TaskException in case result couldn't be stored
     */
    void storeResult(Result result) throws TaskException;

    /**
     * Returns underlying benchmark plan
     *
     * @return benchmark plan owning this task
     * @throws TaskException in case benchmark plan couldn't be obtained
     */
    BenchmarkPlan getActualBenchmarkPlan() throws TaskException;

    /**
     * Retrieve results for given task
     *
     * @param taskId task uuid for which we need the results
     * @return list of results for given task
     * @throws TaskException in case results couldn't be obtained
     */
    List<Result> getResults(String taskId) throws TaskException;

    /**
     * Wait until checkpoint is reached in the current task context.
     * Returns immedeatelly if checkpoint has been already reached in the current task context.
     *
     * @param checkpointName name of the checkpoint
     * @throws TaskException in case something went wrong during waiting
     */
    void waitForCheckpoint(String checkpointName) throws TaskException;

    /**
     * Inform other tasks in the same context ID that the checkpoint has been reached.
     *
     * @param checkpointName name of the checkpoint
     * @throws TaskException if something went wrong during informing other tasks
     */
    void checkpointReached(String checkpointName) throws TaskException;

    /**
     * Sets the latch count and wait until it reaches zero.
     * In case the latch with given name has been already counted down requested times, returns immedeately.
     *
     * @param latchName    latch name
     * @param initialCount requested countdowns
     * @throws TaskException if something went wrong during waiting
     */
    void waitForLatch(String latchName, int initialCount) throws TaskException;

    /**
     * decrease latch
     *
     * @param latchName latch name
     * @throws TaskException if something went wrong during decreasing
     */
    void latchCountDown(String latchName) throws TaskException;

    /**
     * Generate random UUID. It should be guaranteed that the generator will never return the same UUID also in the
     * completely different setup.
     *
     * @return generated UUID
     * @throws TaskException if something went wrong during uuid generation
     */
    String generateUuid() throws TaskException;


    /**
     * Get cluster-wide timestamp
     *
     * @return cluster-wide timestamp
     * @throws TaskException if something went wrong during timestamp retrieval
     */
    long generateTimestamp() throws TaskException;

    /**
     * Get context-wide shared property.
     *
     * @param key key
     * @return property value
     * @throws TaskException if something went wrong during retrieval
     */
    String getSharedProperty(String key) throws TaskException;

    /**
     * Set context-wide shared property. Rewrite silently if exists.
     *
     * @param key   key
     * @param value value
     * @throws TaskException if something went wrong during setting the property
     */
    void setSharedProperty(String key, String value) throws TaskException;

    /**
     * return current task ID
     *
     * @return task uuid
     */
    String getTaskId();

    /**
     * return current tassk context ID
     *
     * @return task context uuid
     */
    String getTaskContextId();

    /**
     * return current benchmark ID
     *
     * @return benchmark uuid
     */
    String getBenchmarkId();

    /**
     * return current bpk descriptor (which contains all task descriptor templates)
     *
     * @return bpk descriptor
     */
    Bpk getBpkDescriptor();

    /**
     * return current task descriptor (not only template as is specified in BPK descriptor template, but real task descriptor)
     *
     * @return task descriptor
     */
    TaskDescriptor getTaskDescriptor();

    /**
     * return folder where task has been extracted
     *
     * @return extracted directory
     */
    File getExtractedBpkDir();

    /**
     * return task working directory folder
     *
     * @return working directory folder
     */
    File getTaskWorkingDir();

    /**
     * return temporaty folder
     *
     * @return temporary folder
     */
    File getTaskTempDir();

    /**
     * sleep quitely for given time in millis
     *
     * @param time in millis
     * @return true if were not interrupted
     */
    boolean sleepQuietely(long time);
}
