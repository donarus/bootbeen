package cz.cuni.mff.d3s.been.taskapi.messaging.service.sync;

import cz.cuni.mff.d3s.been.taskapi.messaging.Request;

public class LatchCountDownRequest implements Request {
    private String latchName;

    public LatchCountDownRequest() {
    }

    public LatchCountDownRequest(String latchName) {
        this.latchName = latchName;
    }

    public String getLatchName() {
        return latchName;
    }

    public void setLatchName(String latchName) {
        this.latchName = latchName;
    }
}
