package cz.cuni.mff.d3s.been.taskapi.messaging.service.misc;

import cz.cuni.mff.d3s.been.model.benchmark.BenchmarkPlan;
import cz.cuni.mff.d3s.been.taskapi.messaging.Response;

public class GetActualBenchmarkPlanResponse extends Response {
    private BenchmarkPlan benchmarkPlan;

    public GetActualBenchmarkPlanResponse() {
    }

    public GetActualBenchmarkPlanResponse(BenchmarkPlan benchmarkPlan) {
        this.benchmarkPlan = benchmarkPlan;
    }

    public BenchmarkPlan getBenchmarkPlan() {
        return benchmarkPlan;
    }

    public void setBenchmarkPlan(BenchmarkPlan benchmarkPlan) {
        this.benchmarkPlan = benchmarkPlan;
    }
}
