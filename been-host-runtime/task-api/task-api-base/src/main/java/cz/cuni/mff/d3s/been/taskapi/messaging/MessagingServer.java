package cz.cuni.mff.d3s.been.taskapi.messaging;

import cz.cuni.mff.d3s.been.utils.json.JsonException;
import cz.cuni.mff.d3s.been.utils.json.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zeromq.ZContext;
import org.zeromq.ZFrame;
import org.zeromq.ZMQ;
import org.zeromq.ZMsg;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static cz.cuni.mff.d3s.been.taskapi.messaging.Common.deserializeMessage;

public class MessagingServer {
    private static final int SEND_RECEIVE_TIMEOUT = 5000;

    private static final Logger LOG = LoggerFactory.getLogger(MessagingServer.class);

    private final String taskId;
    private final String taskContextId;
    private final String benchmarkId;

    private final ZMQ.Context ctx;
    private final ZMQ.Socket sender;
    private final ZMQ.Socket receiver;

    private volatile boolean terminated;

    private Map<Class<Request>, ServerRequestHandler> requestHandlers = new HashMap<>();
    private Map<Class<RequestForResponse>, ServerRequestForResponseHandler> requestForResponseHandlers = new HashMap<>();

    private String senderAddress;
    private String receiverAddress;

    private Thread listener;

    private ExecutorService executor = Executors.newCachedThreadPool();

    public MessagingServer(String taskId, String taskContextId, String benchmarkId) {
        this.taskId = taskId;
        this.taskContextId = taskContextId;
        this.benchmarkId = benchmarkId;

        this.ctx = ZMQ.context(1);
        this.sender = ctx.socket(ZMQ.PUSH);
        this.sender.setSendTimeOut(SEND_RECEIVE_TIMEOUT);
        this.sender.setReceiveTimeOut(SEND_RECEIVE_TIMEOUT);

        this.receiver = ctx.socket(ZMQ.PULL);
        this.receiver.setSendTimeOut(SEND_RECEIVE_TIMEOUT);
        this.receiver.setReceiveTimeOut(SEND_RECEIVE_TIMEOUT);
    }

    public void terminate() {
        terminated = true;
        sender.setLinger(1);
        receiver.setLinger(1);
        executor.shutdownNow();
        sender.close();
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ctx.term();
    }

    public MessagingServer start() {
        int senderPort = sender.bindToRandomPort("tcp://*");
        int receiverPort = receiver.bindToRandomPort("tcp://*");
        this.senderAddress = "tcp://*:" + senderPort;
        this.receiverAddress = "tcp://*:" + receiverPort;

        this.listener = new Thread(this::listen);
        listener.start();
        return this;
    }

    public void listen() {
        while (!terminated) {
            try {
                String serializedMessage = receiver.recvStr();
                if (serializedMessage != null) {
                    process(serializedMessage);
                }
            } catch (Exception e) {
                if (!terminated) {
                    LOG.error("Exception occured in MessagingServer. Server will try to continue", e);
                }
            }
        }
        receiver.close();
        LOG.info("Messaging Server terminated");
    }

    private void process(String serializedRequest) {
        Request request;
        try {
            request = (Request) deserializeMessage(serializedRequest); // no responses from task for now
        } catch (MessagingException e) {
            LOG.error("Cannot process request [{}]", serializedRequest, e);
            return;
        }

        try {
            handleRequest(request);
        } catch (RuntimeException e) {
            LOG.error("Exception occured during handling request [{}]", serializedRequest, e);
        }
    }

    private void handleRequest(Request request) {
        if (RequestForResponse.class.isAssignableFrom(request.getClass())) {
            handleRequestForResponse((RequestForResponse) request);
        } else {
            handleNormalRequest(request);
        }
    }

    private void handleNormalRequest(Request request) {
        ServerRequestHandler requestHandler = requestHandlers.get(request.getClass());
        if (requestHandler != null) {
            Runnable runnable = () -> requestHandler.handle(taskId, taskContextId, benchmarkId, request);
            executor.submit(runnable);
        } else {
            LOG.error("Request handler for [{}] not registered.", request.getClass());
        }
    }

    private void handleRequestForResponse(RequestForResponse request) {
        ServerRequestForResponseHandler requestHandler = requestForResponseHandlers.get(request.getClass());
        if (requestHandler != null) {
            Runnable runnable = () -> {
                Response response = requestHandler.handle(taskId, taskContextId, benchmarkId, request);
                response.setRequestId(request.getId());
                try {
                    new ZFrame(JsonUtils.writeToString(response)).sendAndDestroy(sender);
                } catch (JsonException e) {
                    LOG.error("Unable to serialize response for request [{}]", response.getRequestId());
                }
            };
            executor.submit(runnable);
        } else {
            LOG.error("Request handler for [{}] not registered.", request.getClass());
        }
    }

    public MessagingServer addRequestHandler(ServerRequestHandler handler) {
        requestHandlers.put(handler.getRequestClass(), handler);
        return this;
    }

    public MessagingServer addRequestForResponseHandler(ServerRequestForResponseHandler handler) {
        requestForResponseHandlers.put(handler.getRequestClass(), handler);
        return this;
    }

    public String getSenderAddress() {
        return senderAddress;
    }

    public String getReceiverAddress() {
        return receiverAddress;
    }
}
