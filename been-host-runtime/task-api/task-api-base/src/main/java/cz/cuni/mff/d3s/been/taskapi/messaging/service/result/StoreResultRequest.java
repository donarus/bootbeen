package cz.cuni.mff.d3s.been.taskapi.messaging.service.result;

import cz.cuni.mff.d3s.been.model.result.SerializedResult;
import cz.cuni.mff.d3s.been.taskapi.messaging.Request;

public class StoreResultRequest implements Request {
    private SerializedResult serializedResult;

    public StoreResultRequest() {
    }

    public StoreResultRequest(SerializedResult serializedResult) {
        this.serializedResult = serializedResult;
    }

    public SerializedResult getResult() {
        return serializedResult;
    }

    public void setResult(SerializedResult serializedResult) {
        this.serializedResult = serializedResult;
    }
}
