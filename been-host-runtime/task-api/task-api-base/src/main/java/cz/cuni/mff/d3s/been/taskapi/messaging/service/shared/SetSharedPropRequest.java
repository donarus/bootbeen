package cz.cuni.mff.d3s.been.taskapi.messaging.service.shared;

import cz.cuni.mff.d3s.been.taskapi.messaging.Request;
import cz.cuni.mff.d3s.been.taskapi.messaging.RequestForResponse;

public class SetSharedPropRequest implements Request {
    private String key;
    private String value;

    public SetSharedPropRequest() {}

    public SetSharedPropRequest(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
