package cz.cuni.mff.d3s.been.taskapi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the base class for implementing tasks. Each real task should extend this class and implement
 * start method. see {@link SuperTask}
 */
public abstract class Task extends SuperTask {

    private static final Logger LOG = LoggerFactory.getLogger(cz.cuni.mff.d3s.been.model.task.Task.class);

    /**
     * This method is called by {@link TaskRunner}
     *
     * @param args arguments from command line
     * @throws TaskException in case something went wrong during task execution
     */
    public final void start(String[] args) throws TaskException {
        // wrapped in abstract method in case we'll want to change some
        // default behavior of this method in future
        LOG.info("Starting task with arguments: [{}]", String.join(", ", args));
        startTask(args);
    }

    /**
     * In this method should be implementation of task\s logic.
     *
     * @param args real arguments from command line
     * @throws TaskException in case something went wrong during task execution
     */
    protected abstract void startTask(String[] args) throws TaskException;

}
