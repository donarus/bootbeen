package cz.cuni.mff.d3s.been.taskapi;

/**
 * Wrapper for task taskId,taskContextId,benchmarkId
 */
public class TaskIdentifiers {
    private final String taskId;
    private final String taskContextId;
    private final String benchmarkId;

    public TaskIdentifiers(String taskId, String taskContextId, String benchmarkId) {
        this.taskId = taskId;
        this.taskContextId = taskContextId;
        this.benchmarkId = benchmarkId;
    }

    public String getTaskId() {
        return taskId;
    }

    public String getTaskContextId() {
        return taskContextId;
    }

    public String getBenchmarkId() {
        return benchmarkId;
    }
}
