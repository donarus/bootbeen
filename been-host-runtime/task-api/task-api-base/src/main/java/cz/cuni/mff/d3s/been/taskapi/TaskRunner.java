package cz.cuni.mff.d3s.been.taskapi;

import cz.cuni.mff.d3s.been.model.bpk.Bpk;
import cz.cuni.mff.d3s.been.model.log.LogLevel;
import cz.cuni.mff.d3s.been.model.td.JavaTaskDescriptor;
import cz.cuni.mff.d3s.been.taskapi.logging.LogProxy;
import cz.cuni.mff.d3s.been.taskapi.messaging.MessagingClient;
import cz.cuni.mff.d3s.been.utils.json.JsonException;
import cz.cuni.mff.d3s.been.utils.json.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import static cz.cuni.mff.d3s.been.taskapi.TaskApiConstants.SystemEnv.*;

public class TaskRunner {
    private static final Logger LOG = LoggerFactory.getLogger(TaskRunner.class);
    private final MessagingClient messagingClient;

    private final Class<?> taskClass;
    private final String[] taskArgs;
    private final Bpk bpkDescriptor;
    private final JavaTaskDescriptor currentTaskDescriptor;
    private final String taskId;
    private final String taskContextId;
    private final String benchmarkId;
    private final File extractedBpkDir;
    private final File taskWorkingDir;
    private final File taskTempDir;
    private final File generatedBenchmarkPlanLocation;

    public static void main(String[] args) {
        try {
            new TaskRunner(args).runTask();
            System.exit(0);
        } catch (Throwable t) {
            System.exit(1);
        }
    }

    public TaskRunner(String[] args) {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            Thread.currentThread().setName("Shutdown Hook Thread");
            shutdown();
        }));

        LOG.info("TaskRunner is initializing new Java Task [{}]", System.getenv(TASK_ID));
        List<String> argsList = Arrays.asList(args);
        try {
            this.taskClass = getTaskClass(parseTaskClassName(argsList));
            this.taskArgs = parseTaskArgs(argsList);
            this.currentTaskDescriptor = loadTaskDescriptor(new File(System.getenv(CURRENT_TASK_DESCRIPTOR_LOCATION)));
            this.bpkDescriptor = loadBpkDescriptor(new File(System.getenv(BPK_DESCRIPTOR_LOCATION)));
        } catch (TaskException e) {
            // no need to fix anything, but terminate app
            LOG.error("Unable to start task", e);
            System.exit(1);
            throw new AssertionError("Should not never happen!! Just to make compiler happy and allow final fields.");
        }

        this.generatedBenchmarkPlanLocation = new File(System.getenv(GENERATED_BENCHMARK_PLAN_LOCATION));

        this.extractedBpkDir = new File(EXTRACTED_BPK_DIR);
        this.taskWorkingDir = new File(TASK_WORKING_DIR);
        this.taskTempDir = new File(TASK_TEMP_DIR);

        this.taskId = System.getenv(TASK_ID);
        this.taskContextId = System.getenv(TASK_CONTEXT_ID);
        this.benchmarkId = System.getenv(BENCHMARK_ID);

        this.messagingClient = new MessagingClient(
                System.getenv(COMM_SERVER_SENDER_ADDR),
                System.getenv(COMM_SERVER_RECEIVER_ADDR)
        );

        LogLevel logLevel = LogLevel.valueOf(System.getenv(TASK_LOG_LEVEL));

        new LogProxy(messagingClient, logLevel).register();
    }

    private synchronized void shutdown() {
        LOG.info("Shutting down task [{}]", taskId);

        try {
            if (messagingClient != null) {
                messagingClient.terminate();
            }
        } catch (Exception e) {
            try {
                LOG.error("Unable to terminate messaging client for task [{}]", taskId);
            } catch (Throwable t) {
                // ignore
            }
        }
    }

    private void runTask() throws TaskException {
        try {
            if (BenchmarkPlanGenerator.class.isAssignableFrom(taskClass)) {
                LOG.info("Task [{}] is benchmark generator, starting up ...", taskId);
                runBenchmarkPlanGenerator(taskClass);
            } else if (Task.class.isAssignableFrom(taskClass)) {
                LOG.info("Task [{}] is classic task, starting up ...", taskId);
                runTask(taskClass);
            } else {
                String msg = "Invalid task type " + taskClass.getName();
                LOG.error(msg);
                throw new TaskException(msg);
            }
        } catch (TaskException e) {
            LOG.error("Unable to run task", e);
            throw e;
        } catch (Throwable t) {
            LOG.error("Unable to run task", t);
            throw t;
        }
    }

    private void runTask(Class<?> clazz) throws TaskException {
        Task task = instantiate(clazz);
        setupTask(task);
        task.start(taskArgs);
    }

    private void runBenchmarkPlanGenerator(Class<?> clazz) throws TaskException {
        BenchmarkPlanGenerator generator = instantiate(clazz);
        setupGenerator(generator);
        generator.generate(taskArgs);
    }

    private void setupTask(Task task) {
        setupSuperTask(task);
    }

    private void setupGenerator(BenchmarkPlanGenerator generator) {
        setupSuperTask(generator);
        generator.setGeneratedBenchmarkPlanLocation(generatedBenchmarkPlanLocation);
    }

    private void setupSuperTask(SuperTask task) {
        task.setTaskId(taskId);
        task.setTaskContextId(taskContextId);
        task.setBenchmarkId(benchmarkId);
        task.setBpkDescriptor(bpkDescriptor);
        task.setExtractedBpkDir(extractedBpkDir);
        task.setTaskWorkingDir(taskWorkingDir);
        task.setTaskTempDir(taskTempDir);
        task.setTaskDescriptor(currentTaskDescriptor);
        task.setMessagingClient(messagingClient);
    }

    private JavaTaskDescriptor loadTaskDescriptor(File taskDescriptorLocation) throws TaskException {
        try {
            return JsonUtils.read(taskDescriptorLocation, JavaTaskDescriptor.class);
        } catch (JsonException e) {
            String msg = "Can't load task descriptor";
            LOG.error(msg, e);
            throw new TaskException(msg, e);
        }
    }

    private Bpk loadBpkDescriptor(File bpkDescriptorLocation) throws TaskException {
        try {
            return JsonUtils.read(bpkDescriptorLocation, Bpk.class);
        } catch (JsonException e) {
            String msg = "Can't load bpk descriptor";
            LOG.error(msg, e);
            throw new TaskException(msg, e);
        }
    }

    private <T> T instantiate(Class<?> clazz) throws TaskException {
        try {
            return (T) clazz.newInstance();
        } catch (IllegalAccessException | InstantiationException e) {
            String msg = "Cannot create new task instance of class [" + clazz.getName() + "]";
            LOG.error(msg, e);
            throw new TaskException(msg, e);
        }
    }

    private Class<?> getTaskClass(String taskClassName) throws TaskException {
        try {
            return Class.forName(taskClassName);
        } catch (ClassNotFoundException e) {
            String msg = "Class [" + taskClassName + "] is not on classpath";
            LOG.error(msg, e);
            throw new TaskException(msg, e);
        }
    }

    private String[] parseTaskArgs(List<String> argsList) throws TaskException {
        if (argsList.size() == 0) {
            String msg = "Too few arguments, at least single argument specifying " +
                    "task class must be passed to " + TaskRunner.class.getSimpleName() + ".";
            LOG.error(msg);
            throw new TaskException(msg);
        }
        return argsList.subList(1, argsList.size()).toArray(new String[0]);
    }

    private String parseTaskClassName(List<String> argsList) {
        return argsList.get(0);
    }
}
