package cz.cuni.mff.d3s.been.taskapi.messaging.service.result;

import cz.cuni.mff.d3s.been.taskapi.messaging.RequestForResponse;

public class GetStoredResultsRequest extends RequestForResponse {
    private String taskId;

    public GetStoredResultsRequest() {
    }

    public GetStoredResultsRequest(String taskId) {
        this.taskId = taskId;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    @Override
    public int timeoutSeconds() {
        return 5;
    }
}
