package cz.cuni.mff.d3s.been.taskapi.messaging.service.log;

import cz.cuni.mff.d3s.been.model.log.LogMessage;
import cz.cuni.mff.d3s.been.taskapi.messaging.Request;

public class StoreLogRequest implements Request {
    private LogMessage logMessage;

    public StoreLogRequest(){}

    public StoreLogRequest(LogMessage logMessage) {
        this.logMessage = logMessage;
    }

    public LogMessage getLogMessage() {
        return logMessage;
    }

    public void setLogMessage(LogMessage logMessage) {
        this.logMessage = logMessage;
    }
}
