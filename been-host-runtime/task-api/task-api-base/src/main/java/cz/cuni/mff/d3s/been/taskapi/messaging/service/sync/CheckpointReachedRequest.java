package cz.cuni.mff.d3s.been.taskapi.messaging.service.sync;

import cz.cuni.mff.d3s.been.taskapi.messaging.Request;

public class CheckpointReachedRequest implements Request {
    private String checkpointName;

    public CheckpointReachedRequest() {
    }

    public CheckpointReachedRequest(String checkpointName) {
        this.checkpointName = checkpointName;
    }

    public String getCheckpointName() {
        return checkpointName;
    }

    public void setCheckpointName(String checkpointName) {
        this.checkpointName = checkpointName;
    }
}
