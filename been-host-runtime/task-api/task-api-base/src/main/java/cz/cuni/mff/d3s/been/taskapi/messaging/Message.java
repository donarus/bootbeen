package cz.cuni.mff.d3s.been.taskapi.messaging;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.io.Serializable;

/**
 * Base class for messages sent over jeromq socket between task and running host runtime.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public interface Message extends Serializable {
}
