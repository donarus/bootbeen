package cz.cuni.mff.d3s.been.taskapi.messaging.service.misc;

import cz.cuni.mff.d3s.been.taskapi.messaging.RequestForResponse;

public class GetActualBenchmarkPlanRequest extends RequestForResponse {

    @Override
    public int timeoutSeconds() {
        return 5;
    }

}
