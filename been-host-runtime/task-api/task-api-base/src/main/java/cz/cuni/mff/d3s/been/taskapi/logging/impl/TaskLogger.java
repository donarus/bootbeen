package cz.cuni.mff.d3s.been.taskapi.logging.impl;

import cz.cuni.mff.d3s.been.model.log.LogLevel;
import cz.cuni.mff.d3s.been.model.log.LogMessage;
import org.slf4j.helpers.FormattingTuple;
import org.slf4j.helpers.MarkerIgnoringBase;
import org.slf4j.helpers.MessageFormatter;

/**
 * Logger implementation for a BEEN Task.
 * <p>
 * Logs are redirected to {@link TaskLogProcessor}.
 */
final class TaskLogger extends MarkerIgnoringBase {

    private final TaskLogProcessor taskLogProcessor;

    private String name;

    private final LogLevelGetter logLevelGetter;

    public TaskLogger(String name, TaskLogProcessor taskLogProcessor, LogLevelGetter logLevelGetter) {
        this.taskLogProcessor = taskLogProcessor;
        this.name = name;
        this.logLevelGetter = logLevelGetter;
    }

    /**
     * Are {@code trace} messages currently enabled?
     */
    @Override
    public boolean isTraceEnabled() {
        return isLevelEnabled(LogLevel.TRACE);
    }

    /**
     * A simple implementation which logs messages of level TRACE according to the
     * format outlined above.
     */
    @Override
    public void trace(String msg) {
        if (isTraceEnabled()) {
            log(LogLevel.TRACE, msg, null);
        }
    }

    /**
     * Perform single parameter substitution before logging the message of level
     * TRACE according to the format outlined above.
     */
    @Override
    public void trace(String format, Object param1) {
        formatAndLog(LogLevel.TRACE, format, param1, null);
    }

    /**
     * Perform double parameter substitution before logging the message of level
     * TRACE according to the format outlined above.
     */
    @Override
    public void trace(String format, Object param1, Object param2) {
        formatAndLog(LogLevel.TRACE, format, param1, param2);
    }

    /**
     * Perform double parameter substitution before logging the message of level
     * TRACE according to the format outlined above.
     */
    @Override
    public void trace(String format, Object... argArray) {
        formatAndLog(LogLevel.TRACE, format, argArray);
    }

    /**
     * Log a message of level TRACE, including an exception.
     */
    @Override
    public void trace(String msg, Throwable t) {
        if (isTraceEnabled()) {
            log(LogLevel.TRACE, msg, t);
        }
    }

    /**
     * Are {@code debug} messages currently enabled?
     */
    @Override
    public boolean isDebugEnabled() {
        return isLevelEnabled(LogLevel.DEBUG);
    }

    /**
     * A simple implementation which logs messages of level DEBUG according to the
     * format outlined above.
     */
    @Override
    public void debug(String msg) {
        if (isDebugEnabled()) {
            log(LogLevel.DEBUG, msg, null);
        }
    }

    /**
     * Perform single parameter substitution before logging the message of level
     * DEBUG according to the format outlined above.
     */
    @Override
    public void debug(String format, Object param1) {
        formatAndLog(LogLevel.DEBUG, format, param1, null);
    }

    /**
     * Perform double parameter substitution before logging the message of level
     * DEBUG according to the format outlined above.
     */
    @Override
    public void debug(String format, Object param1, Object param2) {
        formatAndLog(LogLevel.DEBUG, format, param1, param2);
    }

    /**
     * Perform double parameter substitution before logging the message of level
     * DEBUG according to the format outlined above.
     */
    @Override
    public void debug(String format, Object... argArray) {
        formatAndLog(LogLevel.DEBUG, format, argArray);
    }

    /**
     * Log a message of level DEBUG, including an exception.
     */
    @Override
    public void debug(String msg, Throwable t) {
        if (isDebugEnabled()) {
            log(LogLevel.DEBUG, msg, t);
        }
    }

    /**
     * Are {@code info} messages currently enabled?
     */
    @Override
    public boolean isInfoEnabled() {
        return isLevelEnabled(LogLevel.INFO);
    }

    /**
     * A simple implementation which logs messages of level INFO according to the
     * format outlined above.
     */
    @Override
    public void info(String msg) {
        if (isInfoEnabled()) {
            log(LogLevel.INFO, msg, null);
        }
    }

    /**
     * Perform single parameter substitution before logging the message of level
     * INFO according to the format outlined above.
     */
    @Override
    public void info(String format, Object arg) {
        formatAndLog(LogLevel.INFO, format, arg, null);
    }

    /**
     * Perform double parameter substitution before logging the message of level
     * INFO according to the format outlined above.
     */
    @Override
    public void info(String format, Object arg1, Object arg2) {
        formatAndLog(LogLevel.INFO, format, arg1, arg2);
    }

    /**
     * Perform double parameter substitution before logging the message of level
     * INFO according to the format outlined above.
     */
    @Override
    public void info(String format, Object... argArray) {
        formatAndLog(LogLevel.INFO, format, argArray);
    }

    /**
     * Log a message of level INFO, including an exception.
     */
    @Override
    public void info(String msg, Throwable t) {
        if (isInfoEnabled()) {
            log(LogLevel.INFO, msg, t);
        }
    }

    /**
     * Are {@code warn} messages currently enabled?
     */
    @Override
    public boolean isWarnEnabled() {
        return isLevelEnabled(LogLevel.WARN);
    }

    /**
     * A simple implementation which always logs messages of level WARN according
     * to the format outlined above.
     */
    @Override
    public void warn(String msg) {
        if (isWarnEnabled()) {
            log(LogLevel.WARN, msg, null);
        }
    }

    /**
     * Perform single parameter substitution before logging the message of level
     * WARN according to the format outlined above.
     */
    @Override
    public void warn(String format, Object arg) {
        formatAndLog(LogLevel.WARN, format, arg, null);
    }

    /**
     * Perform double parameter substitution before logging the message of level
     * WARN according to the format outlined above.
     */
    @Override
    public void warn(String format, Object arg1, Object arg2) {
        formatAndLog(LogLevel.WARN, format, arg1, arg2);
    }

    /**
     * Perform double parameter substitution before logging the message of level
     * WARN according to the format outlined above.
     */
    @Override
    public void warn(String format, Object... argArray) {
        formatAndLog(LogLevel.WARN, format, argArray);
    }

    /**
     * Log a message of level WARN, including an exception.
     */
    @Override
    public void warn(String msg, Throwable t) {
        if (isWarnEnabled()) {
            log(LogLevel.WARN, msg, t);
        }
    }

    /**
     * Are {@code error} messages currently enabled?
     */
    @Override
    public boolean isErrorEnabled() {
        return isLevelEnabled(LogLevel.ERROR);
    }

    /**
     * A simple implementation which always logs messages of level ERROR according
     * to the format outlined above.
     */
    @Override
    public void error(String msg) {
        if (isErrorEnabled()) {
            log(LogLevel.ERROR, msg, null);
        }
    }

    /**
     * Perform single parameter substitution before logging the message of level
     * ERROR according to the format outlined above.
     */
    @Override
    public void error(String format, Object arg) {
        formatAndLog(LogLevel.ERROR, format, arg, null);
    }

    /**
     * Perform double parameter substitution before logging the message of level
     * ERROR according to the format outlined above.
     */
    @Override
    public void error(String format, Object arg1, Object arg2) {
        formatAndLog(LogLevel.ERROR, format, arg1, arg2);
    }

    /**
     * Perform double parameter substitution before logging the message of level
     * ERROR according to the format outlined above.
     */
    @Override
    public void error(String format, Object... argArray) {
        formatAndLog(LogLevel.ERROR, format, argArray);
    }

    /**
     * Log a message of level ERROR, including an exception.
     */
    @Override
    public void error(String msg, Throwable t) {
        if (isErrorEnabled()) {
            log(LogLevel.ERROR, msg, t);
        }
    }

    protected boolean isLevelEnabled(LogLevel logLevel) {
        // log level are numerically ordered so can use simple numeric
        // comparison
        return (logLevel.ordinal() >= this.logLevelGetter.getLogLevel().ordinal());
    }

    private void formatAndLog(LogLevel level, String format, Object... arguments) {
        if (isLevelEnabled(level)) {
            FormattingTuple tp = MessageFormatter.arrayFormat(format, arguments);
            log(level, tp.getMessage(), tp.getThrowable());
        }
    }

    /**
     * Logs messages.
     *
     * @param level   log level
     * @param message log message
     * @param t       throwable when logging exceptions
     */
    private void log(LogLevel level, String message, Throwable t) {
        taskLogProcessor.process(new LogMessage(name, level, message).withThreadName().withThrowable(t));
    }

}
