package cz.cuni.mff.d3s.been.taskapi.messaging;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.io.Serializable;

/**
 * Resuest from task to running HR. This request expects some response)
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public abstract class RequestForResponse implements Request {

    public final int NO_TIMEOUT = 0;

    private String id;

    /**
     * gets the id of the request used to pair with response
     *
     * @return request id
     */
    public final String getId() {
        return id;
    }

    /**
     * sets the id of the request used to pair with response
     *
     * @param id request idø
     */
    public final void setId(String id) {
        this.id = id;
    }

    /**
     * Timeout to get the response. 0 means NO_TIMEOUT
     */
    public abstract int timeoutSeconds();

    /**
     * Returns true if there is a timeout set on the request
     * @return
     */
    public boolean hasTimeout () {
        return timeoutSeconds() > NO_TIMEOUT;
    }
}
