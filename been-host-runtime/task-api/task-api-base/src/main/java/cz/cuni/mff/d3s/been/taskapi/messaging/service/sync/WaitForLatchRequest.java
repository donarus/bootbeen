package cz.cuni.mff.d3s.been.taskapi.messaging.service.sync;

import cz.cuni.mff.d3s.been.taskapi.messaging.RequestForResponse;

public class WaitForLatchRequest extends RequestForResponse {
    private String latchName;
    private int initialCount;

    public WaitForLatchRequest() {
    }

    public WaitForLatchRequest(String latchName, int initialCount) {
        this.latchName = latchName;
        this.initialCount = initialCount;
    }

    public String getLatchName() {
        return latchName;
    }

    public void setLatchName(String latchName) {
        this.latchName = latchName;
    }

    public int getInitialCount() {
        return initialCount;
    }

    public void setInitialCount(int initialCount) {
        this.initialCount = initialCount;
    }

    @Override
    public int timeoutSeconds() {
        return NO_TIMEOUT;
    }
}
