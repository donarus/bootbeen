package cz.cuni.mff.d3s.been.taskapi.messaging.service.result;

import cz.cuni.mff.d3s.been.model.result.SerializedResult;
import cz.cuni.mff.d3s.been.taskapi.messaging.Response;

import java.util.List;

public class GetStoredResultResponse extends Response {
    private List<SerializedResult> results;

    public GetStoredResultResponse() {

    }

    public GetStoredResultResponse(List<SerializedResult> results) {
        this.results = results;
    }

    public List<SerializedResult> getResults() {
        return results;
    }

    public void setResults(List<SerializedResult> results) {
        this.results = results;
    }
}
