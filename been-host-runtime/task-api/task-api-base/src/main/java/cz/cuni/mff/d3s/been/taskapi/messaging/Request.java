package cz.cuni.mff.d3s.been.taskapi.messaging;

/**
 * Base request sent from task to HR (this request doesn't expect any response)
 */
public interface Request extends Message {

}
