package cz.cuni.mff.d3s.been.taskapi.messaging;

public abstract class ServerRequestHandler<Req extends Request> {
    public final void handle(String taskId, String taskContextId, String benchmarkId, Request request) {
        handleRequest(taskId, taskContextId, benchmarkId, (Req) request);
    }

    protected abstract void handleRequest(String taskId, String taskContextId, String benchmarkId, Req request);

    public abstract Class<Req> getRequestClass();

}
