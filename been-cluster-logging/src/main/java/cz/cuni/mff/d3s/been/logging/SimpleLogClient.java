package cz.cuni.mff.d3s.been.logging;

import cz.cuni.mff.d3s.been.api.clients.LogStorageClient;
import cz.cuni.mff.d3s.been.api.cluster.data.MultiMaps;
import cz.cuni.mff.d3s.been.api.cluster.data.structures.ClusterMultiMap;
import cz.cuni.mff.d3s.been.model.log.LogMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

import static java.util.Comparator.comparingLong;

@Service
public class SimpleLogClient implements LogStorageClient {

    @Autowired
    private MultiMaps multiMaps;

    private ClusterMultiMap<String, LogMessage> logs; // key is task uuid

    @Override
    public void log(String taskUuid, LogMessage logMsg) {
        logs.put(taskUuid, logMsg);
    }

    @Override
    public List<LogMessage> getLogsForTask(String taskUuid) {
        List<LogMessage> messages = new ArrayList<>(logs.get(taskUuid));
        messages.sort(comparingLong(LogMessage::getTimestamp));
        return messages;
    }

    @PostConstruct
    private void setUp() {
        this.logs = multiMaps.getMap("been::task_logs");
    }
}
