package cz.cuni.mff.d3s.been.storage;

import cz.cuni.mff.d3s.been.api.clients.ResultsStorageClient;
import cz.cuni.mff.d3s.been.api.cluster.ClusterUuidGenerator;
import cz.cuni.mff.d3s.been.api.cluster.data.Maps;
import cz.cuni.mff.d3s.been.api.cluster.data.structures.ClusterMap;
import cz.cuni.mff.d3s.been.model.result.SerializedResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class HazelcastResultsStorageClient implements ResultsStorageClient {

    @Autowired
    private ClusterUuidGenerator uuidGenerator;

    @Autowired
    private Maps maps;

    private ClusterMap<String, SerializedResult> resultsMap;

    @Override
    public String storeResult(SerializedResult result) {
        String key = uuidGenerator.generate();
        result.setKey(key);
        resultsMap.put(key, result);
        return key;
    }

    @Override
    public List<SerializedResult> getResultsForTask(String taskId) {
        return filter(notNullEq(taskId, (r) -> r.getTaskId()));
    }

    private List<SerializedResult> filter(ClusterMap.ClusterMapPredicate<String, SerializedResult> predicate) {
        return resultsMap.filter(predicate).stream().collect(toList());
    }

    private ClusterMap.ClusterMapPredicate<String, SerializedResult> notNullEq(Object req, PropertyExtractor extractor) {
        return (key, result) -> {
            Object value = extractor.extract(result);
            return value != null && value.equals(req);
        };
    }

    @PostConstruct
    private void setup() {
        this.resultsMap = maps.getMap("been::hazelcast_result_storage::results_map");
    }

    private interface PropertyExtractor extends Serializable {
        Object extract(SerializedResult result);
    }

}
