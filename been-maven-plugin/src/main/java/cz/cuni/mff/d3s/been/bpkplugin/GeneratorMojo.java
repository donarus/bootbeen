package cz.cuni.mff.d3s.been.bpkplugin;

import cz.cuni.mff.d3s.been.bpkplugin.zip.FileToArchive;
import cz.cuni.mff.d3s.been.bpkplugin.zip.ItemToArchive;
import cz.cuni.mff.d3s.been.bpkplugin.zip.StringToArchive;
import cz.cuni.mff.d3s.been.bpkplugin.zip.ZipUtil;
import cz.cuni.mff.d3s.been.model.bpk.Bpk;
import cz.cuni.mff.d3s.been.model.bpk.BpkId;
import cz.cuni.mff.d3s.been.model.td.JavaArtifact;
import cz.cuni.mff.d3s.been.model.td.JavaTaskDescriptor;
import cz.cuni.mff.d3s.been.model.td.TaskDescriptor;
import cz.cuni.mff.d3s.been.utils.bpk.BpkException;
import cz.cuni.mff.d3s.been.utils.bpk.BpkSerializer;
import cz.cuni.mff.d3s.been.utils.td.TaskDescriptorException;
import cz.cuni.mff.d3s.been.utils.td.TaskDescriptorSerializer;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.*;
import org.apache.maven.project.MavenProject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

@Mojo(name = "buildpackage", defaultPhase = LifecyclePhase.PACKAGE, requiresDependencyCollection = ResolutionScope.RUNTIME, threadSafe = true)
@Execute(phase = LifecyclePhase.COMPILE)
public class GeneratorMojo extends AbstractMojo {

    @Parameter(defaultValue = "${project.build.directory}/${project.build.finalName}.bpk")
    File resultBpk;

    @Parameter(defaultValue = "${project.build.directory}/${project.build.finalName}.jar")
    File jarFile;

    @Parameter
    String description;

    @Parameter(alias = "taskDescriptors")
    List<String> taskDescriptorTemplates = new ArrayList<>();

    @Parameter(defaultValue = "${project}", required = true, readonly = true)
    MavenProject project;

    @Parameter(defaultValue = "${localRepository}", required = true, readonly = true)
    ArtifactRepository localRepository;

    public void execute() throws MojoExecutionException {

        if (!jarFile.exists() || jarFile.isDirectory()) {
            throw new MojoExecutionException("Jar file '" + jarFile.getAbsolutePath() + "' does not exist or is directory...");
        }

        Map<JavaArtifact, File> javaTaskDependencies = collectJavaTaskDependencies();
        List<JavaArtifact> javaTaskDependencyArtifacts = new ArrayList<>(javaTaskDependencies.keySet());
        List<TaskDescriptor> taskDescriptors = createTaskDescriptors();
        addLibrariesToJavaDescriptors(taskDescriptors, javaTaskDependencyArtifacts);
        Bpk bpk = createBpkDescriptor(taskDescriptors);
        List<ItemToArchive> itemsToArchive = prepareItemsToArchive(javaTaskDependencies, bpk);
        createArchive(itemsToArchive);
    }

    private void createArchive(List<ItemToArchive> itemsToArchive) throws MojoExecutionException {
        try {
            ZipUtil.createZip(itemsToArchive, resultBpk);
        } catch (IOException e) {
            throw new MojoExecutionException("Unable to create bpk archive", e);
        }
    }

    private List<ItemToArchive> prepareItemsToArchive(Map<JavaArtifact, File> javaTaskDependencies, Bpk bpk) throws MojoExecutionException {
        List<ItemToArchive> itemsToArchive = new ArrayList<>();
        addJavaTaskDependencies(javaTaskDependencies, itemsToArchive);
        addBpkDescriptor(bpk, itemsToArchive);
        return itemsToArchive;
    }

    private void addBpkDescriptor(Bpk bpk, List<ItemToArchive> items) throws MojoExecutionException {
        try {
            String pathInZip = "config.json";
            getLog().info("  Adding to BPK: " + pathInZip);
            items.add(new StringToArchive(pathInZip, BpkSerializer.serializeToString(bpk)));
        } catch (BpkException e) {
            throw new MojoExecutionException("Unable to copy bpk descriptor to libraries", e);
        }
    }

    private void addJavaTaskDependencies(Map<JavaArtifact, File> javaTaskDependencies, List<ItemToArchive> items) throws MojoExecutionException {
        for (Map.Entry<JavaArtifact, File> entry : javaTaskDependencies.entrySet()) {
            File file = entry.getValue();
            System.out.println(file.getAbsolutePath());
            String pathInZip = entry.getKey().getPathInBpk();
            getLog().info("  Adding to BPK: " + pathInZip);
            items.add(new FileToArchive(pathInZip, file));
        }
    }

    private void addLibrariesToJavaDescriptors(List<TaskDescriptor> taskDescriptors, List<JavaArtifact> libraries) {
        taskDescriptors.stream()
                .filter(t -> t instanceof JavaTaskDescriptor)
                .map(t -> (JavaTaskDescriptor) t)
                .forEach(t -> t.getJavaConfig().setDependencies(libraries));
    }

    private Map<JavaArtifact, File> collectJavaTaskDependencies() {
        List<Artifact> resolvedArtifacts = project.getArtifacts().stream().map(localRepository::find).collect(toList());
        Map<JavaArtifact, File> dependencies = resolvedArtifacts.stream().collect(toMap(this::toJavaArtifact, Artifact::getFile));
        dependencies.put(toJavaArtifact(project.getArtifact()), jarFile);

        return dependencies;
    }

    private JavaArtifact toJavaArtifact(Artifact a) {
        return new JavaArtifact(a.getGroupId(), a.getArtifactId(), a.getVersion());
    }

    private List<TaskDescriptor> createTaskDescriptors() throws MojoExecutionException {
        List<TaskDescriptor> taskDescriptorTemplates = new ArrayList<>();
        for (String tdTemplate : this.taskDescriptorTemplates) {
            getLog().info("  Parsing task descriptor template: " + tdTemplate);
            try {
                taskDescriptorTemplates.add(TaskDescriptorSerializer.deserialize(new File(project.getBasedir(), tdTemplate)));
            } catch (TaskDescriptorException e) {
                throw new MojoExecutionException("Unable to deserialize task descriptor template", e);
            }
        }
        return taskDescriptorTemplates;
    }

    private Bpk createBpkDescriptor(List<TaskDescriptor> taskDescriptorTemplates) {
        BpkId bpkId = new BpkId(project.getGroupId(), project.getArtifactId(), project.getVersion());
        return new Bpk(bpkId, taskDescriptorTemplates, description);
    }
}
