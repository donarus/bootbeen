package cz.cuni.mff.d3s.been.core.detectors;

import cz.cuni.mff.d3s.been.model.node.OperatingSystemFamily;
import cz.cuni.mff.d3s.been.model.node.SystemArchitecture;
import cz.cuni.mff.d3s.been.model.node.SystemEndianness;
import cz.cuni.mff.d3s.been.model.node.SystemInfo;

import java.nio.ByteOrder;

import static org.apache.commons.lang3.SystemUtils.*;

class SystemInfoCreator {

    SystemInfo create() {
        SystemInfo systemInfo = new SystemInfo();

        // Q:
        //    HOW TO FIND THE OS BIT TYPE [64/32] ?
        // A:
        //        System.getProperty("os.arch");
        //    This should be available on all platforms, see the Java System Properties Tutorial for more information.
        //    But 64 bit Windows platforms will lie to the JVM if it is a 32 bit JVM. Actually 64 bit Windows will lie
        //    to any 32 bit process about the environment to help old32 bit programs work properly on a 64 bit OS. Read
        //    the MSDN article about WOW64 for more information.
        //
        //    As a result of WOW64, a 32 bit JVM calling System.getProperty("os.arch") will return "x86". If you want to
        //    get the real architecture of the underlying OS on Windows, use the following logic:
        //
        //        String arch = System.getenv("PROCESSOR_ARCHITECTURE");
        //        String wow64Arch = System.getenv("PROCESSOR_ARCHITEW6432");
        //        String realArch = arch.endsWith("64") || wow64Arch != null && wow64Arch.endsWith("64") ? "64" : "32";


        // set architecture (which will be updated later, in case of windows OS)
        systemInfo.setSystemArchitecture(System.getProperty("os.arch").contains("64") ? SystemArchitecture.X64 : SystemArchitecture.X86);

        // set OS family
        if (IS_OS_FREE_BSD || IS_OS_NET_BSD || IS_OS_OPEN_BSD) {
            systemInfo.setOperatingSystemFamily(OperatingSystemFamily.BSD);
        } else if (IS_OS_WINDOWS) {
            systemInfo.setOperatingSystemFamily(OperatingSystemFamily.WINDOWS);
            // windows OS, so we need to re-detect architecture the correct windows-like way
            String arch = System.getenv("PROCESSOR_ARCHITECTURE");
            String wow64Arch = System.getenv("PROCESSOR_ARCHITEW6432");
            systemInfo.setSystemArchitecture(arch != null && arch.endsWith("64") || wow64Arch != null && wow64Arch.endsWith("64") ? SystemArchitecture.X64 : SystemArchitecture.X86);
        } else if (IS_OS_MAC_OSX) {
            systemInfo.setOperatingSystemFamily(OperatingSystemFamily.MAC_OSX);
        } else if (IS_OS_MAC) {
            systemInfo.setOperatingSystemFamily(OperatingSystemFamily.MAC);
        } else if (IS_OS_LINUX) {
            systemInfo.setOperatingSystemFamily(OperatingSystemFamily.LINUX);
        } else {
            systemInfo.setOperatingSystemFamily(OperatingSystemFamily.UNKNOWN);
        }

        // detect and set endianness
        systemInfo.setSystemEndianness(ByteOrder.nativeOrder().equals(ByteOrder.BIG_ENDIAN) ? SystemEndianness.BIG_ENDIAN : SystemEndianness.LITTLE_ENDIAN);

        // detect and set cpu and memory related info
        //FIXME/TODO
        oshi.SystemInfo si = new oshi.SystemInfo();
        systemInfo.setSystemMemory(si.getHardware().getMemory().getTotal());
        systemInfo.setNumberOfPhysicalCores(si.getHardware().getProcessor().getPhysicalProcessorCount());
        systemInfo.setNumberOfLogicalCores(si.getHardware().getProcessor().getLogicalProcessorCount());

        return systemInfo;
    }

}
