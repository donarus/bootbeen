package cz.cuni.mff.d3s.been.core.detectors;

import cz.cuni.mff.d3s.been.model.node.SystemInfo;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DetectorsConfiguration {

    @Bean
    public SystemInfo systemInfo() {
        return new SystemInfoCreator().create();
    }
}
