package cz.cuni.mff.d3s.been.core.hazelcast.clusterwide;

import com.hazelcast.core.*;
import cz.cuni.mff.d3s.been.api.cluster.ClusterExecutorException;
import cz.cuni.mff.d3s.been.api.cluster.ClusterExecutorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static cz.cuni.mff.d3s.been.core.hazelcast.Constants.SCHEDULED_EXECUTOR_NAME;
import static cz.cuni.mff.d3s.been.core.hazelcast.Constants.SCHEDULED_TASKS_MAP_NAME;

@Service
public class ClusterExecutorServiceHC implements ClusterExecutorService {

    @Autowired
    private HazelcastInstance hazelcastInstance;

    @Override
    public <T> T submitOnNode(CommandWithResponse<T> callable, String nodeUuid) throws ClusterExecutorException {
        Member member = findHazelcastMember(nodeUuid);

        if (member == null) {
            throw new ClusterExecutorException("Node '" + nodeUuid + "' is not part of the cluster.");
        }

        Future<T> future = getExcutorService().submitToMember(callable, member);

        try {
            return future.get();
        } catch (InterruptedException | ExecutionException e) {
            throw new ClusterExecutorException("Task failed during execution on node '" + nodeUuid + "'", e);
        }

    }

    private IExecutorService getExcutorService() {
        return hazelcastInstance.getExecutorService("ClusterExecutor");
    }

    @Override
    public void executeOnNode(Command runnable, String nodeUuid) throws ClusterExecutorException {
        Member member = findHazelcastMember(nodeUuid);

        if (member == null) {
            throw new ClusterExecutorException("Node '" + nodeUuid + "' is not part of the cluster.");
        }

        getExcutorService().executeOnMember(runnable, member);
    }

    @Override
    public void scheduleAnywhereOnCluster(String name, Command command, long initialDelay, long period) {
        IMap<String, String> scheduledTasks = getScheduledTasksNameMap();
        try {
            scheduledTasks.lock(name);
            if (!scheduledTasks.containsKey(name)) {
                hazelcastInstance
                        .getScheduledExecutorService(SCHEDULED_EXECUTOR_NAME)
                        .scheduleAtFixedRate(command, initialDelay, period, TimeUnit.SECONDS);

                scheduledTasks.put(name, name);
            }
        } finally {
            scheduledTasks.unlock(name);
        }
    }

    private Member findHazelcastMember(String uuid) {
        return hazelcastInstance.getCluster().getMembers().stream()
                .filter(member -> member.getUuid().equals(uuid)).findFirst().orElse(null);
    }

    private IMap getScheduledTasksNameMap() {
        return hazelcastInstance.getMap(SCHEDULED_TASKS_MAP_NAME);
    }
}
