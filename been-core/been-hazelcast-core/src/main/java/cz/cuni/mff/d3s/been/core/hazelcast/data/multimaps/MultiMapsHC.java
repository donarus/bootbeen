package cz.cuni.mff.d3s.been.core.hazelcast.data.multimaps;

import com.hazelcast.core.HazelcastInstance;
import cz.cuni.mff.d3s.been.api.cluster.data.MultiMaps;
import cz.cuni.mff.d3s.been.api.cluster.data.structures.ClusterMultiMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MultiMapsHC implements MultiMaps {

    @Autowired
    private HazelcastInstance hazelcastInstance;

    @Override
    public <K, V> ClusterMultiMap<K, V> getMap(String mapName) {
        return new ClusterMultiMapHC<>(hazelcastInstance.getMultiMap(mapName));
    }

    @Override
    public void destroyMultiMap(String multiMapName) {
        hazelcastInstance.getMultiMap(multiMapName).destroy();
    }
}
