package cz.cuni.mff.d3s.been.core.hazelcast.data.multimaps;

import com.hazelcast.core.MultiMap;
import cz.cuni.mff.d3s.been.api.cluster.data.structures.ClusterMultiMap;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public class ClusterMultiMapHC<K, V> implements ClusterMultiMap<K, V> {

    private MultiMap<K, V> multiMap;

    public ClusterMultiMapHC(MultiMap<K, V> multiMap) {
        this.multiMap = multiMap;
    }

    @Override
    public boolean put(K key, V value) {
        return multiMap.put(key, value);
    }

    @Override
    public Collection<V> get(K key) {
        return multiMap.get(key);
    }

    @Override
    public boolean remove(Object key, Object value) {
        return multiMap.remove(key, value);
    }

    @Override
    public Collection<V> remove(Object key) {
        return multiMap.remove(key);
    }

    @Override
    public Set<K> keySet() {
        return multiMap.keySet();
    }

    @Override
    public Collection<V> values() {
        return multiMap.values();
    }

    @Override
    public Set<Map.Entry<K, V>> entrySet() {
        return multiMap.entrySet();
    }

    @Override
    public boolean containsKey(K key) {
        return multiMap.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return multiMap.containsValue(value);
    }

    @Override
    public boolean containsEntry(K key, V value) {
        return multiMap.containsEntry(key, value);
    }

    @Override
    public int size() {
        return multiMap.size();
    }

    @Override
    public void clear() {
        multiMap.clear();
    }

    @Override
    public int valueCount(K key) {
        return multiMap.valueCount(key);
    }
}
