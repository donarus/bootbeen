package cz.cuni.mff.d3s.been.core.hazelcast.data.maps;

import com.hazelcast.core.HazelcastInstance;
import cz.cuni.mff.d3s.been.api.cluster.data.Maps;
import cz.cuni.mff.d3s.been.api.cluster.data.structures.ClusterMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MapsHC implements Maps {

    @Autowired
    private HazelcastInstance hazelcastInstance;

    @Override
    public <K, V> ClusterMap<K, V> getMap(String mapName) {
        return new ClusterMapHC<K, V>(hazelcastInstance.getMap(mapName));
    }

    @Override
    public void destroyMap(String mapName) {
        hazelcastInstance.getMap(mapName).destroy();
    }
}
