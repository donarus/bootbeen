package cz.cuni.mff.d3s.been.core.hazelcast.data.lists;

import com.hazelcast.core.*;
import cz.cuni.mff.d3s.been.api.cluster.data.Lists;
import cz.cuni.mff.d3s.been.api.cluster.data.structures.ClusterList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ListsHC implements Lists {

    @Autowired
    private HazelcastInstance hazelcastInstance;

    @Override
    public <T> ClusterList<T> getList(String listName) {
        return new ClusterListHC<T>(hazelcastInstance.getList(listName));
    }

    @Override
    public void destroyList(String listName) {
        hazelcastInstance.getList(listName).destroy();
    }

}
