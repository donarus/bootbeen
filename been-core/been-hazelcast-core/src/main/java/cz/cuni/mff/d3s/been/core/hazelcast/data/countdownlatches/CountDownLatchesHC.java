package cz.cuni.mff.d3s.been.core.hazelcast.data.countdownlatches;

import com.hazelcast.core.HazelcastInstance;
import cz.cuni.mff.d3s.been.api.cluster.data.CountDownLatches;
import cz.cuni.mff.d3s.been.api.cluster.data.structures.ClusterCountDownLatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CountDownLatchesHC implements CountDownLatches {

    @Autowired
    private HazelcastInstance hazelcastInstance;

    @Override
    public ClusterCountDownLatch getCountDownLatch(String name) {
        return new ClusterCountDownLatchHC(hazelcastInstance.getCountDownLatch(name));
    }

    @Override
    public void destroyCountDownLatch(String name) {
        hazelcastInstance.getCountDownLatch(name).destroy();
    }

}