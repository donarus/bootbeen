package cz.cuni.mff.d3s.been.core.hazelcast;

public class Constants {

    public static final String SERVICES_MAP_NAME = "::been_internal:services_map";

    public static final String SCHEDULED_TASKS_MAP_NAME = "::been_internal:scheduler_tasknames";

    public static final String SCHEDULED_EXECUTOR_NAME = "::been_internal:scheduler_executor";

    public static final String UUID_GENERATOR_NAME = "::been_internal:uuid_generator";

}
