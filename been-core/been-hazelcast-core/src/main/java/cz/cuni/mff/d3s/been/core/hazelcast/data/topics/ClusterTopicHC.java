package cz.cuni.mff.d3s.been.core.hazelcast.data.topics;

import com.hazelcast.core.ITopic;
import cz.cuni.mff.d3s.been.api.cluster.data.structures.ClusterTopic;

public class ClusterTopicHC<T> implements ClusterTopic<T> {
    private ITopic<Object> topic;

    public ClusterTopicHC(ITopic<Object> topic) {
        this.topic = topic;
    }

    @Override
    public void publish(T message) {
        topic.publish(message);
    }

    @Override
    public void subscribe(ClusterTopicSubscription<T> subscription) {
        topic.addMessageListener(message -> subscription.onMessage((T) message.getMessageObject()));
    }
}
