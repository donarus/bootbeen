package cz.cuni.mff.d3s.been.core.hazelcast.data.countdownlatches;

import com.hazelcast.core.ICountDownLatch;
import cz.cuni.mff.d3s.been.api.cluster.data.structures.ClusterCountDownLatch;

import java.util.concurrent.TimeUnit;

public class ClusterCountDownLatchHC implements ClusterCountDownLatch {

    private final ICountDownLatch countDownLatch;

    public ClusterCountDownLatchHC(ICountDownLatch countDownLatch) {
        this.countDownLatch = countDownLatch;
    }

    @Override
    public boolean await(long timeout, TimeUnit unit) throws InterruptedException {
        return countDownLatch.await(timeout, unit);
    }

    @Override
    public void countDown() {
        countDownLatch.countDown();
    }

    @Override
    public int getCount() {
        return countDownLatch.getCount();
    }

    @Override
    public boolean trySetCount(int count) {
        return countDownLatch.trySetCount(count);
    }
}
