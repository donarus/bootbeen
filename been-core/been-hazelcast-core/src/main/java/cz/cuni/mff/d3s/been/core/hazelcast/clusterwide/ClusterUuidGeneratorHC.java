package cz.cuni.mff.d3s.been.core.hazelcast.clusterwide;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IAtomicReference;
import com.hazelcast.core.IdGenerator;
import cz.cuni.mff.d3s.been.api.cluster.ClusterUuidGenerator;
import cz.cuni.mff.d3s.been.core.hazelcast.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.UUID;

@Service
public class ClusterUuidGeneratorHC implements ClusterUuidGenerator {

    @Autowired
    private HazelcastInstance hazelcastInstance;

    private IdGenerator idGenerator;

    @Override
    public String generate() {
        String uuid = UUID.randomUUID().toString();
        String hcuid = String.format("%08X", idGenerator.newId());
        return uuid + "-" + hcuid;
    }

    @Override
    public long generateLong() {
        return idGenerator.newId();
    }

    @PostConstruct
    private void assignIdGenerator() {
        this.idGenerator = hazelcastInstance.getIdGenerator(Constants.UUID_GENERATOR_NAME);
        IAtomicReference<Boolean> initializedRef = hazelcastInstance.getAtomicReference("been_internal::generatorInitialized");
        boolean needsInitialization = initializedRef.compareAndSet(false, true);
        if(needsInitialization) {
            idGenerator.init(hazelcastInstance.getCluster().getClusterTime());
        }
    }

}
