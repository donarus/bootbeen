package cz.cuni.mff.d3s.been.core.hazelcast.data.queues;

import com.hazelcast.core.HazelcastInstance;
import cz.cuni.mff.d3s.been.api.cluster.data.Queues;
import cz.cuni.mff.d3s.been.api.cluster.data.structures.ClusterQueue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class QueuesHC implements Queues {

    @Autowired
    private HazelcastInstance hazelcastInstance;

    @Override
    public <T> ClusterQueue<T> getQueue(String queueName) {
        return new ClusterQueueHC<T>(hazelcastInstance.getQueue(queueName));
    }

    @Override
    public void destroyQueue(String queueName) {
        hazelcastInstance.getQueue(queueName).destroy();
    }
}
