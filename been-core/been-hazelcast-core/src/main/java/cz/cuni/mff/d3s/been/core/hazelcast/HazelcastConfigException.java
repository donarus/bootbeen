package cz.cuni.mff.d3s.been.core.hazelcast;

public class HazelcastConfigException extends RuntimeException {
    public HazelcastConfigException(String message, Throwable cause) {
        super(message, cause);
        System.exit(1);
    }

    public HazelcastConfigException(String message) {
        super(message);
        System.exit(1);
    }
}
