package cz.cuni.mff.d3s.been.core.hazelcast;

import com.hazelcast.cluster.ClusterState;
import com.hazelcast.config.Config;
import com.hazelcast.config.XmlConfigBuilder;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import cz.cuni.mff.d3s.been.core.hazelcast.model.NodeInfoHC;
import cz.cuni.mff.d3s.been.model.node.NodeInfo;
import cz.cuni.mff.d3s.been.model.node.SystemInfo;
import cz.cuni.mff.d3s.been.utils.BeenProperties;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.io.IOException;
import java.net.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

@Configuration
public class HazelcastCoreConfiguration {

    public static final Logger LOG = LoggerFactory.getLogger(HazelcastCoreConfiguration.class);

    private static final String HAZELCAST_XMLCONFIG = "hazelcast.xmlconfig";

    @Autowired
    private BeenProperties beenProperties;

    @Bean
    HazelcastInstance hazelcastInstance() {
        String hazelcastConfigFilePath = beenProperties.getProperty(HAZELCAST_XMLCONFIG);
        Config config;

        if (hazelcastConfigFilePath != null) {
            URL hazelcastConfigUrl;
            try {
                hazelcastConfigUrl = new URL(hazelcastConfigFilePath);
                LOG.debug("Detected that [" + hazelcastConfigFilePath + "] is URL");
            } catch (MalformedURLException e) {
                // ignore and try file

                File hazelcastConfigFile = new File(hazelcastConfigFilePath);
                if (!hazelcastConfigFile.exists()) {
                    String message = "['" + hazelcastConfigFilePath + "'] configured as ['" + HAZELCAST_XMLCONFIG + "'] in been configuration " +
                            "is not valid URL and also is not valid location of any file on local filesystem.";
                    LOG.error(message);
                    throw new HazelcastConfigException(message);
                }
                try {
                    hazelcastConfigUrl = hazelcastConfigFile.toURI().toURL();
                } catch (MalformedURLException e1) {
                    throw new HazelcastConfigException("Unexpected exception while creating URL from existing file", e);
                }
            }

            try {
                config = new XmlConfigBuilder(hazelcastConfigUrl).build();
            } catch (IOException e) {
                String message = "Provided Hazelcast configuration ['" + hazelcastConfigFilePath + "'] configured as ['" + HAZELCAST_XMLCONFIG + "'] " +
                        "in been configuration is not invalid.";
                LOG.error(message);
                throw new HazelcastConfigException(message, e);
            }

        } else {
            config = new Config();
            config.getNetworkConfig()
                    .getJoin()
                    .getMulticastConfig()
                    .setLoopbackModeEnabled(true)
                    .setEnabled(true);
            config.setProperty("hazelcast.logging.type", "slf4j");
        }

        return Hazelcast.newHazelcastInstance(config);
    }

    @Bean
    NodeInfo nodeInfo(HazelcastInstance hazelcastInstance, SystemInfo systemInfo) {
        String nodeUuid = hazelcastInstance.getCluster().getLocalMember().getUuid();
        String localMemberUuid = hazelcastInstance.getCluster().getLocalMember().getUuid();
        final NodeInfoHC nodeInfo = new NodeInfoHC(nodeUuid, getIpAddresses(), systemInfo, localMemberUuid);
        nodeInfo.setHostname(determineHostname());
        return nodeInfo;
    }

    private String determineHostname() {
        String host = null;
        // try InetAddress.LocalHost first;
        try {
            host = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            // InetAddress.getLocalHost().getHostName() may not work in certain environments.
        }

        if (StringUtils.isEmpty(host)) {
            host = System.getenv("COMPUTERNAME");
        }

        if (StringUtils.isEmpty(host)) {
            host = System.getenv("HOSTNAME");
        }

        return host;
    }

    private String[] getIpAddresses() throws RuntimeException {
        List<String> addresses = new ArrayList<>();
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress addr = enumIpAddr.nextElement();
                    String hostAddress = addr.getHostAddress();

                    if (hostAddress.contains(":")) {
                        // skip ... current version support just ipv4 by default. support is not planned for now
                    } else {
                        addresses.add(addr.getHostAddress());
                    }
                }
            }
            return addresses.toArray(new String[0]);
        } catch (SocketException e) {
            throw new RuntimeException("Cannot list IP addresses", e);
        }
    }

}
