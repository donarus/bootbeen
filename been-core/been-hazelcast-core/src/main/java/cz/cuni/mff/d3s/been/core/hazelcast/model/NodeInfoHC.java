package cz.cuni.mff.d3s.been.core.hazelcast.model;

import cz.cuni.mff.d3s.been.model.node.NodeInfo;
import cz.cuni.mff.d3s.been.model.node.SystemInfo;

import java.util.Date;

public class NodeInfoHC extends NodeInfo {

    private final String hazelcastMemberId;

    public NodeInfoHC(String uuid, String[] ipAddresses, SystemInfo systemInfo, String hazelcastMemberId) {
        super(uuid, ipAddresses, systemInfo, new Date());
        this.hazelcastMemberId = hazelcastMemberId;
    }

    public String getHazelcastMemberId() {
        return hazelcastMemberId;
    }

}
