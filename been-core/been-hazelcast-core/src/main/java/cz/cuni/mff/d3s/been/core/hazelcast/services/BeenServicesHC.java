package cz.cuni.mff.d3s.been.core.hazelcast.services;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.query.Predicate;
import cz.cuni.mff.d3s.been.api.common.BeenServices;
import cz.cuni.mff.d3s.been.core.hazelcast.Constants;
import cz.cuni.mff.d3s.been.model.service.ServiceInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BeenServicesHC implements BeenServices {

    @Autowired
    private HazelcastInstance hazelcastInstance;

    private IMap<String, ServiceInfo> servicesMap;

    @Override
    public List<ServiceInfo> list() {
        return new ArrayList<>(servicesMap.values());
    }

    @Override
    public List<ServiceInfo> listByNodeUuid(String nodeUUID) {
        return new ArrayList<>(servicesMap.values(
                (Predicate<String, ServiceInfo>) entry -> nodeUUID.equals(entry.getValue().getNodeUUID())
        ));
    }

    @Override
    public <T extends ServiceInfo> List<T> listByNameAndVendor(String name, String vendor) {
        return servicesMap.values(
                (Predicate<String, ServiceInfo>) entry ->
                        name.equals(entry.getValue().getName())
                                && vendor.equals(entry.getValue().getVendor()))
                .stream().map(t -> (T) t).collect(Collectors.toList());
    }

    @Override
    public <T extends ServiceInfo> T findByServiceUuid(String uuid) {
        return (T) servicesMap.get(uuid);
    }

    @Override
    public void register(ServiceInfo serviceInfo) {
        servicesMap.put(serviceInfo.getServiceUUID(), serviceInfo);
    }

    @Override
    public void removeByNodeUuid(String nodeUuid) {
        servicesMap.removeAll(entry -> entry.getValue().getNodeUUID().equals(nodeUuid));
    }

    @PostConstruct
    private void assignServicesMap() {
        this.servicesMap = hazelcastInstance.getMap(Constants.SERVICES_MAP_NAME);
    }

}
