package cz.cuni.mff.d3s.been.core.hazelcast.data.locks;

import com.hazelcast.core.HazelcastInstance;
import cz.cuni.mff.d3s.been.api.cluster.data.Locks;
import cz.cuni.mff.d3s.been.api.cluster.data.structures.ClusterLock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LocksHC implements Locks {

    @Autowired
    private HazelcastInstance hazelcastInstance;

    @Override
    public ClusterLock getLock(String name) {
        return new ClusterLockHC(hazelcastInstance.getLock(name));
    }

    @Override
    public void destroyLock(String name) {
        hazelcastInstance.getLock(name).destroy();
    }

}
