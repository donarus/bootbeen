package cz.cuni.mff.d3s.been.core.hazelcast.data.maps;

import com.hazelcast.core.IMap;
import com.hazelcast.projection.Projection;
import com.hazelcast.query.Predicate;
import cz.cuni.mff.d3s.been.api.cluster.data.structures.ClusterMap;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class ClusterMapHC<K, V> implements ClusterMap<K, V> {

    private IMap<K, V> map;

    ClusterMapHC(IMap map) {
        this.map = map;
    }

    @Override
    public void lock(K key) {
        map.lock(key);
    }

    @Override
    public boolean tryLock(K key) {
        return map.tryLock(key);
    }

    @Override
    public boolean tryLock(K key, long time, TimeUnit timeUnit) throws InterruptedException {
        return map.tryLock(key, time, timeUnit);
    }

    @Override
    public void unlock(K key) {
        map.unlock(key);
    }

    @Override
    public List<V> filter(ClusterMapPredicate<K, V> predicate) {
        return new ArrayList<>(map.values(hcPredicate(predicate)));
    }

    @Override
    public List<V> listByKeys(Collection<K> keys) {
        return new ArrayList<V>(map.getAll(new HashSet<>(keys)).values());
    }

    @Override
    public <T> List<T> map(ClusterMapValueMapper<V, T> mapper) {
        return new ArrayList<>(map.project(hcProjection(mapper)));
    }

    @Override
    public <T> T getAndMap(String key, ClusterMapValueMapper<V, T> mapper) {
        Collection<T> values = map.project(hcProjection(mapper), hcPredicate((k, v) -> k.equals(key)));
        if (values.isEmpty()) {
            return null;
        } else {
            return values.iterator().next();
        }
    }

    @Override
    public <R> List<R> filterAndMap(ClusterMapPredicate<K, V> predicate, ClusterMapValueMapper<V, R> mapper) {
        return new ArrayList<R>(map.project(hcProjection(mapper), hcPredicate(predicate)));
    }

    @Override
    public void removeAll(ClusterMapPredicate<K, V> predicate) {
        map.removeAll(hcPredicate(predicate));
    }

    @Override
    public int size() {
        return map.size();
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    public boolean containsKey(Object key) {
        return map.containsKey(key);
    }

    @Override
    public boolean containsValue(Object value) {
        return map.containsKey(value);
    }

    @Override
    public V get(Object key) {
            return map.get(key);
    }

    @Override
    public V put(K key, V value) {
        return map.put(key, value);
    }

    @Override
    public V remove(Object key) {
        return map.remove(key);
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        map.putAll(m);
    }

    @Override
    public void clear() {
        map.clear();
    }

    @Override
    public Set<K> keySet() {
        return map.keySet();
    }

    @Override
    public Collection<V> values() {
        return map.values();
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        return map.entrySet();
    }


    private static <T, K, V> Projection<Entry<K, V>, T> hcProjection(final ClusterMapValueMapper<V, T> mapper) {
        return new Projection<Entry<K, V>, T>() {
            @Override
            public T transform(Entry<K, V> input) {
                return mapper.map(input.getValue());
            }
        };
    }

    private static <K, V> Predicate<K, V> hcPredicate(ClusterMapPredicate<K, V> predicate) {
        return (Predicate<K, V>) mapEntry -> predicate.apply(mapEntry.getKey(), mapEntry.getValue());
    }
}
