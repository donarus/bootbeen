package cz.cuni.mff.d3s.been.core.hazelcast.data.locks;

import com.hazelcast.core.ILock;
import cz.cuni.mff.d3s.been.api.cluster.data.structures.ClusterLock;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;

public class ClusterLockHC implements ClusterLock {

    private ILock lock;

    public ClusterLockHC(ILock lock) {
        this.lock = lock;
    }

    @Override
    public void lock() {
        lock.lock();
    }

    @Override
    public void lockInterruptibly() throws InterruptedException {
        lock.lockInterruptibly();
    }

    @Override
    public boolean tryLock() {
        return lock.tryLock();
    }

    @Override
    public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
        return lock.tryLock(time, unit);
    }

    @Override
    public void unlock() {
        lock.unlock();
    }

    @Override
    public Condition newCondition() {
        return lock.newCondition(lock.getName() + "_condition");
    }

}
