package cz.cuni.mff.d3s.been.core.hazelcast.debug;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hazelcast.core.*;
import cz.cuni.mff.d3s.been.api.cluster.ClusterDebugInfoGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Service
public class HazelcastClusterDebugInfoGenerator implements ClusterDebugInfoGenerator {

    @Autowired
    private HazelcastInstance hazelcastInstance;

    @Override
    public String generate() throws RuntimeException {
        Map debugInfo = new HashMap();

        Map maps = new HashMap();
        Map lists = new HashMap();
        Map queues = new HashMap();

        debugInfo.put("maps", maps);
        debugInfo.put("lists", lists);
        debugInfo.put("queues", queues);

        for (DistributedObject obj : hazelcastInstance.getDistributedObjects()) {
            if (obj instanceof IMap) {
                maps.put(obj.getName(), new HashMap<>((IMap) obj));
            } else if (obj instanceof IList) {
                lists.put(obj.getName(), new ArrayList<>((IList) obj));
            } else if (obj instanceof IQueue) {
                queues.put(obj.getName(), new ArrayDeque<>((IQueue) obj));
            }
        }

        try {
            return new ObjectMapper().writer().writeValueAsString(debugInfo);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

    }
}
