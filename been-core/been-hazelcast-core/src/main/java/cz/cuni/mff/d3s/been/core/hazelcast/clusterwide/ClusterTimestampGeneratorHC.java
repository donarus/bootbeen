package cz.cuni.mff.d3s.been.core.hazelcast.clusterwide;

import com.hazelcast.core.HazelcastInstance;
import cz.cuni.mff.d3s.been.api.cluster.ClusterTimestampGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClusterTimestampGeneratorHC implements ClusterTimestampGenerator {
    @Autowired
    private HazelcastInstance hazelcastInstance;

    @Override
    public long generate() {
        return hazelcastInstance.getCluster().getClusterTime();
    }
}
