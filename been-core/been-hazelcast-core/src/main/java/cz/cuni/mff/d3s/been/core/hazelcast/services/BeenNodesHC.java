package cz.cuni.mff.d3s.been.core.hazelcast.services;

import com.hazelcast.core.*;
import com.hazelcast.map.listener.EntryAddedListener;
import com.hazelcast.map.listener.EntryRemovedListener;
import com.hazelcast.map.listener.EntryUpdatedListener;
import com.hazelcast.query.Predicate;
import cz.cuni.mff.d3s.been.api.cluster.ClusterUuidGenerator;
import cz.cuni.mff.d3s.been.api.common.BeenNodes;
import cz.cuni.mff.d3s.been.core.hazelcast.model.NodeInfoHC;
import cz.cuni.mff.d3s.been.model.node.NodeInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;

@Service
@Lazy(false) // ensure post-construct registration
public class BeenNodesHC implements BeenNodes {

    public static final Logger LOG = LoggerFactory.getLogger(BeenNodesHC.class);

    private static final String NODES_MAP_NAME = "been_internal::nodes_map";

    @Autowired
    private HazelcastInstance hazelcastInstance;

    @Autowired
    private NodeInfo localNodeInfo;

    @Autowired
    private ClusterUuidGenerator uuidGenerator;

    @Override
    public List<NodeInfo> list() {
        return new ArrayList<>(getNodesMap().values());
    }

    @Override
    public NodeInfo getByUuid(String nodeUUID) {
        return getNodesMap().get(nodeUUID);
    }

    @Override
    public void register(NodeInfo nodeInfo) {
        getNodesMap().put(nodeInfo.getUuid(), nodeInfo);
    }

    public Map<String, NodeListener> nodeRemovedListeners = Collections.synchronizedMap(new HashMap<>());
    public Map<String, NodeListener> nodeAddedListeners = Collections.synchronizedMap(new HashMap<>());
    public Map<String, NodeListener> nodeUpdatedListeners = Collections.synchronizedMap(new HashMap<>());

    @Override
    public String addNodeRemovedListener(NodeListener listener) {
        String uuid = uuidGenerator.generate();
        nodeRemovedListeners.put(uuid, listener);
        return uuid;
    }

    @Override
    public void removeNodeRemovedListener(String listenerUuid) {
        nodeRemovedListeners.remove(listenerUuid);
    }


    @Override
    public String addNodeAddedListener(NodeListener listener) {
        String uuid = uuidGenerator.generate();
        nodeAddedListeners.put(uuid, listener);
        return uuid;
    }

    @Override
    public void removeNodeAddedListener(String listenerUuid) {
        nodeAddedListeners.remove(listenerUuid);
    }


    @Override
    public String addNodeUpdatedListener(NodeListener listener) {
        String uuid = uuidGenerator.generate();
        nodeUpdatedListeners.put(uuid, listener);
        return uuid;
    }

    @Override
    public void removeNodeUpdatedListener(String listenerUuid) {
        nodeUpdatedListeners.remove(listenerUuid);
    }

    private IMap<String, NodeInfo> getNodesMap() {
        return hazelcastInstance.getMap(NODES_MAP_NAME);
    }

    @Override
    public List<NodeInfo> listByUuids(Collection<String> nodeUuids) {
        return new ArrayList<>(getNodesMap().getAll(new HashSet<>(nodeUuids)).values());
    }

    @PostConstruct
    public void registerInfo() {
        LOG.debug("Regitering node ['" + localNodeInfo.getUuid() + "']");
        register(localNodeInfo);

        getNodesMap().addEntryListener((EntryRemovedListener<String, NodeInfo>) event
                -> nodeRemovedListeners.values().forEach(l -> l.handle(event.getOldValue())), true);

        getNodesMap().addEntryListener((EntryAddedListener<String, NodeInfo>) event
                -> nodeAddedListeners.values().forEach(l -> l.handle(event.getValue())), true);

        getNodesMap().addEntryListener((EntryUpdatedListener<String, NodeInfo>) event
                -> nodeUpdatedListeners.values().forEach(l -> l.handle(event.getValue())), true);

        hazelcastInstance.getCluster().addMembershipListener(new MembershipListener() {
            @Override
            public void memberRemoved(MembershipEvent membershipEvent) {
                String memberUuid = membershipEvent.getMember().getUuid();
                Set<String> keys = getNodesMap().keySet((Predicate<String, NodeInfo>) entry
                        -> ((NodeInfoHC) entry.getValue()).getHazelcastMemberId().equals(memberUuid));
                for (String key : keys) {
                    getNodesMap().remove(key);
                }
            }

            @Override
            public void memberAdded(MembershipEvent membershipEvent) {
                // do nothing
            }

            @Override
            public void memberAttributeChanged(MemberAttributeEvent memberAttributeEvent) {
                // do nothing
            }
        });
    }

}
