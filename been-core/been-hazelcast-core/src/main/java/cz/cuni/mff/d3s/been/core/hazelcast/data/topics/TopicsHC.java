package cz.cuni.mff.d3s.been.core.hazelcast.data.topics;

import com.hazelcast.core.HazelcastInstance;
import cz.cuni.mff.d3s.been.api.cluster.data.Topics;
import cz.cuni.mff.d3s.been.api.cluster.data.structures.ClusterTopic;
import org.springframework.beans.factory.annotation.Autowired;

public class TopicsHC implements Topics {

    @Autowired
    private HazelcastInstance hazelcastInstance;

    @Override
    public <T> ClusterTopic<T> getTopic(String topicName) {
        return new ClusterTopicHC<T>(hazelcastInstance.getTopic(topicName));
    }

    @Override
    public void destroyTopic(String topicName) {
        hazelcastInstance.getTopic(topicName).destroy();
    }
}
