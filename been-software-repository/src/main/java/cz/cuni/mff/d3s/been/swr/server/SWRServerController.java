package cz.cuni.mff.d3s.been.swr.server;


import cz.cuni.mff.d3s.been.model.bpk.Bpk;
import cz.cuni.mff.d3s.been.model.bpk.BpkId;
import cz.cuni.mff.d3s.been.utils.bpk.BpkException;
import cz.cuni.mff.d3s.been.utils.bpk.BpkLoader;
import cz.cuni.mff.d3s.been.swr.Constants;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.*;

@RestController
@Profile("swr-server")
@RequestMapping(value = Constants.SWR_BASE_URL_MAPPING)
public final class SWRServerController {
    private static final Logger LOG = LoggerFactory.getLogger(SWRServerController.class);

    @Autowired
    private SWRFileSystemBasedStorage storage;

    @RequestMapping(value = Constants.SWR_LIST_URL_MAPPING, method = RequestMethod.GET)
    public ResponseEntity<List<BpkId>> list() {
        return ResponseEntity.ok(storage.listArtifacts());
    }

    @RequestMapping(value = Constants.SWR_GET_BPK_DESCRIPTOR, method = RequestMethod.GET)
    public ResponseEntity<Bpk> getBpkDescriptor(@RequestParam("groupId") String groupId,
                                                @RequestParam("artifactId") String artifactId,
                                                @RequestParam("version") String version,
                                                HttpServletResponse response) {
        response.setCharacterEncoding("UTF-8");
        File artifact = storage.getArtifact(new BpkId(groupId, artifactId, version));
        if (artifact == null) {
            return ResponseEntity.status(NOT_FOUND).body(null);
        }

        try {
            return ResponseEntity.ok(BpkLoader.load(artifact));
        } catch (BpkException e) {
            LOG.error("Task descriptor for {}:{}:{} cannot be read", groupId, artifactId, version, e);
            return ResponseEntity.status(INTERNAL_SERVER_ERROR).body(null);
        }
    }

    @RequestMapping(value = Constants.SWR_GET_URL_MAPPING, method = RequestMethod.GET)
    public ResponseEntity<FileSystemResource> get(@RequestParam("groupId") String groupId,
                                                  @RequestParam("artifactId") String artifactId,
                                                  @RequestParam("version") String version,
                                                  HttpServletResponse httpResponse) throws IOException {

        ResponseBuilder<FileSystemResource> response = new ResponseBuilder<>();
        BpkId bpkId = new BpkId(groupId, artifactId, version);
        File bpkFile = response.getIfNoError((r) -> loadArtifact(bpkId, response));
        response.body = new FileSystemResource(bpkFile);
        String fileName = URLEncoder.encode(bpkFile.getName(), "UTF-8");
        response.addHeader("Content-disposition", "attachment; filename=" + fileName);
        /* RFC2046 says "The "octet-stream" subtype is used to indicate that a body contains arbitrary binary data"
         and "The recommended action for an implementation that receives an "application/octet-stream" entity is to
         simply offer to put the data in a file[...]" */
        response.contentType = APPLICATION_OCTET_STREAM;
        return response.build();

    }

    @RequestMapping(value = Constants.SWR_DELETE_URL_MAPPING, method = RequestMethod.DELETE)
    public ResponseEntity<Boolean> delete(@RequestParam("groupId") String groupId,
                                          @RequestParam("artifactId") String artifactId,
                                          @RequestParam("version") String version) {
        ResponseBuilder<Boolean> response = new ResponseBuilder<>();
        BpkId bpkId = new BpkId(groupId, artifactId, version);
        response.doIfNoError((r) -> deleteArtifact(bpkId, response));
        response.doIfNoError((r) -> r.body = true);
        return response.build();
    }

    @RequestMapping(value = Constants.SWR_ADD_URL_MAPPING, method = RequestMethod.PUT)
    public ResponseEntity<Boolean> add(@RequestParam("bpk") MultipartFile file) {
        ResponseBuilder<Boolean> response = new ResponseBuilder<>();
        File receivedFile = response.getIfNoError((r) -> receiveFile(file, r));
        BpkId bpkId = response.getIfNoError((r) -> readBpkId(receivedFile, r));
        response.doIfNoError((r) -> storeArtifact(receivedFile, bpkId, r));
        response.doIfNoError((r) -> r.body = true);
        return response.build();
    }

    private void deleteArtifact(BpkId bpkId, ResponseBuilder response) {
        boolean deleted = storage.deleteArtifact(bpkId);
        if (!deleted) {
            response.statusCode = INTERNAL_SERVER_ERROR;
            response.errorMessage = "Server was unable to delete artifact";
        }
    }

    private void storeArtifact(File receivedFile, BpkId bpkId, ResponseBuilder response) {
        boolean stored = storage.storeArtifact(bpkId, receivedFile);
        if (!stored) {
            response.statusCode = INTERNAL_SERVER_ERROR;
            response.errorMessage = "Server was unable to store artifact";
        }
    }

    private File loadArtifact(BpkId bpkId, ResponseBuilder response) {
        File f = storage.getArtifact(bpkId);
        if (f == null || !f.exists()) {
            response.statusCode = NOT_FOUND;
            response.errorMessage = "Artifact " + bpkId.toString() + " was not found in repository";
            return null;
        } else {
            return f;
        }
    }

    private BpkId readBpkId(File bpkFile, ResponseBuilder response) {
        try {
            return BpkLoader.load(bpkFile).getBpkId();
        } catch (BpkException e) {
            String msg = "Unable to store uploaded file.";
            LOG.error(msg, e);
            response.errorMessage = msg + " " + e.getMessage();
            response.statusCode = UNPROCESSABLE_ENTITY;
        }
        return null;
    }

    private File receiveFile(MultipartFile file, ResponseBuilder response) {
        try {
            File f = File.createTempFile("upload_", "_tmp");
            FileUtils.copyInputStreamToFile(file.getInputStream(), f);
            return f;
        } catch (IOException e) {
            String msg = "Unable to store uploaded file.";
            LOG.error(msg, e);
            response.errorMessage = msg + " " + e.getMessage();
            response.statusCode = UNPROCESSABLE_ENTITY;
        }
        return null;
    }

    private static class ResponseBuilder<T> {
        T body;
        String errorMessage;
        HttpStatus statusCode = HttpStatus.OK;
        MediaType contentType = MediaType.APPLICATION_JSON;
        private HttpHeaders headers = new HttpHeaders();

        ResponseEntity build() {
            if (isInError()) {
                return ResponseEntity.status(statusCode).body(errorMessage);
            } else if (body == null) {
                return ResponseEntity.noContent().headers(headers).build();
            } else {
                return ResponseEntity.ok().contentType(contentType).headers(headers).body(body);
            }
        }

        void addHeader(String headerName, String headerValue) {
            headers.add(headerName, headerValue);
        }

        private boolean isWithoutError() {
            return statusCode == OK;
        }

        private boolean isInError() {
            return !isWithoutError();
        }

        private void doIfNoError(VoidResponseModifier supplier) {
            supplier.doAndModify(this);
        }

        private <U> U getIfNoError(GetResponseModifier<U> supplier) {
            return supplier.getAndModify(this);
        }
    }

    private interface VoidResponseModifier {
        void doAndModify(ResponseBuilder responseBuilder);
    }

    private interface GetResponseModifier<T> {
        T getAndModify(ResponseBuilder responseBuilder);
    }
}
