package cz.cuni.mff.d3s.been.swr.client;

import org.slf4j.Logger;
import org.springframework.http.HttpStatus;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import static org.slf4j.LoggerFactory.getLogger;

final class SWRFileDeleter {

    private static final Logger LOG = getLogger(SWRFileDeleter.class);

    private final String url;

    SWRFileDeleter(String url) {
        this.url = url;
    }

    boolean delete() {
        HttpURLConnection con = null;

        try {
            URL url = new URL(this.url);
            con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("DELETE");
            con.connect();

            HttpStatus status = HttpStatus.valueOf(con.getResponseCode());
            if (status.is2xxSuccessful()) {
                return true;
            }
        } catch (ProtocolException e) {
            LOG.error("Unknown protocol for url '{}'", url, e);
        } catch (MalformedURLException e) {
            LOG.error("Url '{}' is not valid", url, e);
        } catch (IOException e) {
            LOG.error("Cannot GET file from url '{}'", url, e);
        } finally {
            con.disconnect();
        }
        return false;
    }
}
