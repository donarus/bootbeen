package cz.cuni.mff.d3s.been.swr.server;

import cz.cuni.mff.d3s.been.model.service.ServiceInfo;
import cz.cuni.mff.d3s.been.swr.Constants;

public final class SWRServiceInfo extends ServiceInfo {

    private String[] hosts;
    private Integer port;
    private Boolean https;

    public SWRServiceInfo(String nodeUuid, String uuid) {
        super(Constants.SERVICE_NAME, Constants.SERVICE_VENDOR, Constants.SERVICE_VERSION, nodeUuid, uuid);
    }

    @Override
    public String getDescription() {
        return Constants.SERVICE_DESCRIPTION;
    }

    public String[] getHosts() {
        return hosts;
    }

    public void setHosts(String[] hosts) {
        this.hosts = hosts;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public Boolean isHttps() {
        return https;
    }

    public void setHttps(Boolean https) {
        this.https = https;
    }
}
