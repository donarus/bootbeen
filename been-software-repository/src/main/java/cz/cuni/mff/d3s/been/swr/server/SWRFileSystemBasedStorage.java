package cz.cuni.mff.d3s.been.swr.server;

import cz.cuni.mff.d3s.been.utils.BeenProperties;
import cz.cuni.mff.d3s.been.model.bpk.BpkId;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
@Profile("swr-server")
public final class SWRFileSystemBasedStorage {

    private static final Logger LOG = LoggerFactory.getLogger(SWRFileSystemBasedStorage.class);

    private final File home;

    private File repository;

    private File deletedDir;

    public SWRFileSystemBasedStorage(BeenProperties properties) {
        String swrHome = properties.getProperty("been.fsbswr.home", new File("fsbswr_home").getAbsolutePath());
        this.home = new File(swrHome);
        initialize();
    }

    private void initialize() {
        try {
            initializeFileStructure();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean deleteArtifact(BpkId bpkId) {
        File artifact = getArtifactFile(bpkId);
        if (!artifact.exists()) {
            return true;
        }
        return artifact.delete();
    }

    private File getArtifactFile(BpkId bpkId) {
        File groupIdDir = new File(repository, bpkId.getGroupId());
        File artifactIdDir = new File(groupIdDir, bpkId.getArtifactId());
        File versionDir = new File(artifactIdDir, bpkId.getVersion());
        File artifact = new File(versionDir, bpkId.getArtifactId() + "-" + bpkId.getVersion() + ".bpk");
        return artifact;
    }

    public File getArtifact(BpkId bpkId) {
        File artifact = getArtifactFile(bpkId);
        return artifact.exists() ? artifact : null;
    }

    public List<BpkId> listArtifacts() {
        List<BpkId> artifacts = new ArrayList<>();

        Collection<File> files = FileUtils.listFiles(
                repository,
                new String[]{"bpk"},
                true
        );

        Path repositoryPath = repository.toPath();
        for (File file : files) {
            Path p = repositoryPath.relativize(file.toPath());
            if (p.getNameCount() == 4) {
                String groupId = p.getName(0).toString();
                String artifactId = p.getName(1).toString();
                String version = p.getName(2).toString();
                String bpkName = p.getName(3).toString();
                String expectedBpkName = artifactId + "-" + version + ".bpk";
                if (expectedBpkName.equals(bpkName)) {
                    artifacts.add(new BpkId(groupId, artifactId, version));
                }
            } // else ignore (not a BPK)
        }

        LOG.info("SW Repository artifacts successfully loaded ({} artifacts available)", artifacts.size());
        if (LOG.isDebugEnabled()) {
            for (BpkId bpkId : artifacts) {
                LOG.debug("{} available in repository", bpkId.toString());
            }
        }
        return artifacts;
    }

    public boolean storeArtifact(BpkId bpkId, File newArtifact) {
        File artifactFile = getArtifactFile(bpkId);
        File artifactParentDir = artifactFile.getParentFile();
        artifactParentDir.mkdirs();
        if (newArtifact.renameTo(artifactFile)) {
            return true;
        }
        return false;
    }

    private void initializeFileStructure() throws IOException {
        if (!home.exists()) {
            home.mkdirs();
        }

        repository = new File(home, "repository");

        if (!repository.exists()) {
            repository.mkdirs();
        }

        deletedDir = new File(home, "deletedDir");
        FileUtils.deleteDirectory(deletedDir);
        deletedDir.mkdirs();


        LOG.info("SW Repository directory structure initialized in '{}'", home.getAbsolutePath());
    }
}