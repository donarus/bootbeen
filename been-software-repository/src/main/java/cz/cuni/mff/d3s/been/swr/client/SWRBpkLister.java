package cz.cuni.mff.d3s.been.swr.client;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cuni.mff.d3s.been.model.bpk.BpkId;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.springframework.http.HttpStatus;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.List;

import static org.slf4j.LoggerFactory.getLogger;

final class SWRBpkLister {

    private static final Logger LOG = getLogger(SWRBpkLister.class);

    private final String url;

    SWRBpkLister(String url) {
        this.url = url;
    }

    List<BpkId> list() {
        String body = null;
        try {
            body = IOUtils.toString(new URL(url), "UTF-8");
        } catch (IOException e) {
            LOG.error("Unable to read content from url {} to string", url, e);
            return null;
        }

        ObjectMapper mapper = new ObjectMapper();
        JavaType type = mapper.getTypeFactory().constructCollectionType(List.class, BpkId.class);
        try {
            return mapper.readValue(body, type);
        } catch (IOException e) {
            LOG.error("Unable to map result to list of bpks", e);
        }
        return null;
    }
}
