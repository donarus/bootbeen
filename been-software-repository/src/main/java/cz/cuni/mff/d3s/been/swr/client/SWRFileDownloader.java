package cz.cuni.mff.d3s.been.swr.client;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import static java.io.File.createTempFile;
import static org.slf4j.LoggerFactory.getLogger;

final class SWRFileDownloader {

    private static final Logger LOG = getLogger(SWRFileDownloader.class);

    private final String url;

    SWRFileDownloader(String url) {
        this.url = url;
    }

    File download() {
        File f = null;
        try {
            f = createTempFile("swrDlFile_", "_temp");
        } catch (IOException e) {
            LOG.error("Url '{}' is not valid", url, e);
        }

        try {
            FileUtils.copyURLToFile(new URL(url), f);
            return f;
        } catch (IOException e) {
            LOG.error("Cannot GET file from url '{}'", url, e);
        }

        return null;
    }
}
