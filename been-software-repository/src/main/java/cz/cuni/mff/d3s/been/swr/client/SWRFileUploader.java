package cz.cuni.mff.d3s.been.swr.client;

import cz.cuni.mff.d3s.been.api.clients.SoftwareRepositoryException;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.springframework.http.HttpStatus;

import java.io.File;
import java.io.IOException;

import static org.slf4j.LoggerFactory.getLogger;

final class SWRFileUploader {

    private static final Logger LOG = getLogger(SWRFileUploader.class);

    private final String url;

    SWRFileUploader(String url) {
        this.url = url;
    }

    void upload(File bpk) throws SoftwareRepositoryException {
        MultipartEntity reqEntity = new MultipartEntity();
        reqEntity.addPart("bpk", new FileBody(bpk));

        HttpPut put = new HttpPut(url);
        put.setEntity(reqEntity);

        try {
            HttpResponse response = new DefaultHttpClient().execute(put);
            HttpStatus status = HttpStatus.valueOf(response.getStatusLine().getStatusCode());
            if (!status.is2xxSuccessful()) {
                throw new SoftwareRepositoryException("Unable to upload file, reason: " + parseReasonMessage(response));
            }
        } catch (IOException e) {
            throw new SoftwareRepositoryException("Cannot PUT bpk to url '" + url + "'", e);
        }
    }

    private String parseReasonMessage(HttpResponse response) {
        String message = "UNKNOWN";
        try {
            message = IOUtils.toString(response.getEntity().getContent(), "UTf8");
        } catch (IOException e) {
            LOG.error("Unknown error when detecting failure reason of bpk upload", e);
        }
        return message;
    }
}
