package cz.cuni.mff.d3s.been.swr.client;

import cz.cuni.mff.d3s.been.api.clients.SoftwareRepositoryClient;
import cz.cuni.mff.d3s.been.api.clients.SoftwareRepositoryException;
import cz.cuni.mff.d3s.been.api.common.BeenServices;
import cz.cuni.mff.d3s.been.model.bpk.Bpk;
import cz.cuni.mff.d3s.been.model.bpk.BpkId;
import cz.cuni.mff.d3s.been.model.service.ServiceInfo;
import cz.cuni.mff.d3s.been.model.td.TaskDescriptor;
import cz.cuni.mff.d3s.been.swr.server.SWRServiceInfo;
import cz.cuni.mff.d3s.been.swr.Urls;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static cz.cuni.mff.d3s.been.swr.Constants.*;
import static java.util.stream.Collectors.toList;
import static org.slf4j.LoggerFactory.getLogger;

@Service
public final class SWRClient implements SoftwareRepositoryClient {

    private static final Logger LOG = getLogger(SWRClient.class);

    @Autowired
    private BeenServices beenServices;

    @Override
    public List<BpkId> list() throws SoftwareRepositoryException {
        return collectOnAll((baseUrl) -> new SWRBpkLister(Urls.getListURL(baseUrl)).list()).stream().distinct().collect(toList());
    }

    @Override
    public File get(BpkId bpkId) throws SoftwareRepositoryException {
        return doToFirstSuccess((baseUrl) -> new SWRFileDownloader(Urls.getGetURL(baseUrl, bpkId)).download());
    }

    @Override
    public void delete(BpkId bpkId) throws SoftwareRepositoryException {
        doOnAll((baseUrl) -> new SWRFileDeleter(Urls.getDeleteURL(baseUrl, bpkId)).delete());
    }

    @Override
    public void upload(File bpk) throws SoftwareRepositoryException {
        doOnFirst(baseUrl -> {
            new SWRFileUploader(Urls.getUploadURL(baseUrl)).upload(bpk);
            return true;
        });
    }

    @Override
    public TaskDescriptor getTaskDescriptor(BpkId bpkId, String taskDescriptorName) throws SoftwareRepositoryException {
        List<TaskDescriptor> taskDescriptors = listTaskDescriptors(bpkId);
        if (taskDescriptors == null) {
            return null;
        }
        return taskDescriptors.stream().filter(td -> td.getName().equals(taskDescriptorName))
                .findFirst().orElse(null);
    }

    @Override
    public Bpk getBpkDescriptor(BpkId bpkId) throws SoftwareRepositoryException {
        return doToFirstSuccess((baseUrl) -> new SWRBpkDescriptorReceiver(Urls.getGetBpkDescriptorUrl(baseUrl, bpkId)).receive());
    }

    @Override
    public List<TaskDescriptor> listTaskDescriptors(BpkId bpkId) throws SoftwareRepositoryException {
        Bpk bpk = getBpkDescriptor(bpkId);
        if (bpk == null) {
            return null;
        }
        return bpk.getTaskDescriptorTemplates();
    }


    private <T> T doToFirstSuccess(HostActionPerformer<T> performer) throws SoftwareRepositoryException {
        for (SWRServiceInfo swrService : getServerServiceInfos()) {
            for (String host : swrService.getHosts()) {
                String baseUrl = Urls.getSwrBaseUrlMapping(host, swrService.getPort(), swrService.isHttps());
                T result = performer.perform(baseUrl);
                if (result != null) {
                    return result;
                }
            }
        }
        return null;
    }

    private void doOnFirst(HostActionPerformer<Boolean> performer) throws SoftwareRepositoryException {
        for (SWRServiceInfo swrService : getServerServiceInfos()) {
            for (String host : swrService.getHosts()) {
                String baseUrl = Urls.getSwrBaseUrlMapping(host, swrService.getPort(), swrService.isHttps());
                if (performer.perform(baseUrl)) {
                    return;
                }
            }
        }
    }

    private void doOnAll(HostActionPerformer<Boolean> performer) throws SoftwareRepositoryException {
        for (SWRServiceInfo swrService : getServerServiceInfos()) {
            for (String host : swrService.getHosts()) {
                String baseUrl = Urls.getSwrBaseUrlMapping(host, swrService.getPort(), swrService.isHttps());
                if (performer.perform(baseUrl)) {
                    continue;
                }
            }
        }
    }

    private <T> List<T> collectOnAll(HostActionPerformer<Collection<T>> performer) throws SoftwareRepositoryException {
        List<T> items = new ArrayList<T>();
        for (SWRServiceInfo swrService : getServerServiceInfos()) {
            for (String host : swrService.getHosts()) {
                String baseUrl = Urls.getSwrBaseUrlMapping(host, swrService.getPort(), swrService.isHttps());
                Collection<T> chunk = performer.perform(baseUrl);
                if (chunk != null) {
                    items.addAll(chunk);
                    break;
                }
            }
        }
        return items;
    }

    private interface HostActionPerformer<T> {
        T perform(String baseUrl) throws SoftwareRepositoryException;
    }

    private List<SWRServiceInfo> getServerServiceInfos() {
        List<ServiceInfo> serviceInfos = this.beenServices.listByNameAndVendor(SERVICE_NAME, SERVICE_VENDOR);
        return serviceInfos.stream().map(si -> (SWRServiceInfo) si).collect(toList());
    }

}
