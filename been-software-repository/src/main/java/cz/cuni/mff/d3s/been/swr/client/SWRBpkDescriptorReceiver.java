package cz.cuni.mff.d3s.been.swr.client;

import cz.cuni.mff.d3s.been.model.bpk.Bpk;
import cz.cuni.mff.d3s.been.utils.bpk.BpkException;
import cz.cuni.mff.d3s.been.utils.bpk.BpkSerializer;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.slf4j.Logger;
import org.springframework.http.HttpStatus;

import java.io.IOException;

import static org.slf4j.LoggerFactory.getLogger;

final class SWRBpkDescriptorReceiver {

    private static final Logger LOG = getLogger(SWRBpkDescriptorReceiver.class);

    private final String url;

    SWRBpkDescriptorReceiver(String url) {
        this.url = url;
    }

    Bpk receive() {
        HttpGet get = new HttpGet(url);

        try {
            HttpResponse response = new DefaultHttpClient().execute(get);
            HttpStatus status = HttpStatus.valueOf(response.getStatusLine().getStatusCode());
            if (status.is2xxSuccessful()) {
                String body = IOUtils.toString(response.getEntity().getContent(), "UTF-8");
                try {
                    return BpkSerializer.deserialize(body);
                } catch (BpkException e) {
                    LOG.error("Cannot deserialize BPK received from url '{}'", url, e);
                }
            } else {
                LOG.error("Cannot GET file from url '{}', status code: {}, message: ", url, status.value(), status.getReasonPhrase());
            }
        } catch (IOException e) {
            LOG.error("Cannot GET file from url '{}'", url, e);
        }
        return null;
    }
}
