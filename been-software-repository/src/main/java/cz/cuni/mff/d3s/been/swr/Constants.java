package cz.cuni.mff.d3s.been.swr;

public final class Constants {
    public static final String SWR_BASE_URL_MAPPING = "/internal/swr-server";
    public static final String SWR_LIST_URL_MAPPING = "/bpk/list";
    public static final String SWR_GET_URL_MAPPING = "/bpk/get";
    public static final String SWR_DELETE_URL_MAPPING = "/bpk/delete";
    public static final String SWR_ADD_URL_MAPPING = "/bpk/add";
    public static final String SWR_GET_BPK_DESCRIPTOR = "/bpk/descriptor";

    public static final String SERVICE_NAME = "software repository";
    public static final String SERVICE_VENDOR = "cz.cuni.mff.d3s.been";
    public static final String SERVICE_VERSION = "FSBSWR 1.0";
    public static final String SERVICE_DESCRIPTION = "Filesystem based software repository with rest-api.";

}
