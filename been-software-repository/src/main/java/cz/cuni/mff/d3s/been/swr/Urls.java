package cz.cuni.mff.d3s.been.swr;

import cz.cuni.mff.d3s.been.model.bpk.BpkId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import static java.lang.String.format;

public class Urls {

    private static final Logger LOG = LoggerFactory.getLogger(Constants.class);

    public static String getSwrBaseUrlMapping(String host, Integer port, boolean https) {
        String url = "";
        url += https ? "https://" : "http://";
        url += host + ":" + port;
        url += Constants.SWR_BASE_URL_MAPPING;
        return url;
    }

    public static String getListURL(String baseUrl) {
        return combineUrl(baseUrl, Constants.SWR_LIST_URL_MAPPING);
    }

    public static String getGetURL(String baseUrl, BpkId bpkId) {
        return combineUrl(baseUrl, Constants.SWR_GET_URL_MAPPING) + bpkIdUrlPart(bpkId);
    }

    public static String getUploadURL(String baseUrl) {
        return combineUrl(baseUrl, Constants.SWR_ADD_URL_MAPPING);
    }

    public static String getGetBpkDescriptorUrl(String baseUrl, BpkId bpkId) {
        return combineUrl(baseUrl, Constants.SWR_GET_BPK_DESCRIPTOR) + bpkIdUrlPart(bpkId);
    }

    public static String getDeleteURL(String baseUrl, BpkId bpkId) {
        return combineUrl(baseUrl, Constants.SWR_DELETE_URL_MAPPING) + bpkIdUrlPart(bpkId);
    }

    private static String bpkIdUrlPart(BpkId bpkId) {
        return format("?groupId=%s&artifactId=%s&version=%s", enc(bpkId.getGroupId()), enc(bpkId.getArtifactId()), enc(bpkId.getVersion()));
    }

    private static String combineUrl(String baseUrl, String actionPart) {
        return baseUrl + actionPart;
    }

    private static String enc(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            LOG.error("Software repository can't work correctly when UTF-8 encoding is not supported.", e);
        }
        return null;
    }
}
