package cz.cuni.mff.d3s.been.swr.server;

import cz.cuni.mff.d3s.been.model.node.NodeInfo;
import cz.cuni.mff.d3s.been.api.common.BeenServices;
import cz.cuni.mff.d3s.been.api.cluster.ClusterUuidGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.EmbeddedServletContainerInitializedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ForkJoinPool;

@Service
@Profile("swr-server")
public class SWRServiceInfoRegistrator implements ApplicationListener<EmbeddedServletContainerInitializedEvent> {

    @Autowired
    private BeenServices beenServices;

    @Autowired
    private ClusterUuidGenerator uuidGenerator;

    @Autowired
    private NodeInfo nodeInfo;

    @Override
    public void onApplicationEvent(EmbeddedServletContainerInitializedEvent event) {
        int port = event.getEmbeddedServletContainer().getPort();
        SWRServiceInfo serviceInfo = new SWRServiceInfo(nodeInfo.getUuid(), uuidGenerator.generate());
        serviceInfo.setHttps(false); // TODO https currently not supported
        serviceInfo.setPort(port);
        serviceInfo.setHosts(nodeInfo.getIpAddresses());
        beenServices.register(serviceInfo);
    }
}
