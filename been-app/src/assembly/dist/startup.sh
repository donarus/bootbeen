#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

BEEN_APP=(`cd $DIR && ls been-app-*.jar | grep been-app | grep jar`)

JAR_PATH=$DIR/$BEEN_APP

(cd $DIR && java -jar $JAR_PATH --config config/config.properties)



