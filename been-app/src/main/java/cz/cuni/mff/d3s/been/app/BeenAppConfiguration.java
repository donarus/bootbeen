package cz.cuni.mff.d3s.been.app;

import cz.cuni.mff.d3s.been.utils.BeenProperties;
import cz.cuni.mff.d3s.been.utils.thread.ExecutorPools;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.annotation.PostConstruct;
import java.io.FileInputStream;
import java.io.IOException;

@Configuration
@EnableAutoConfiguration
@EnableAsync
@EnableScheduling
@ComponentScan("cz.cuni.mff.d3s.been")
public class BeenAppConfiguration {

    private static final Logger LOG = LoggerFactory.getLogger(BeenAppConfiguration.class);

    @Bean
    public BeenProperties beenProperties(@Value("${config:}") String config) {
        BeenProperties props = new BeenProperties();
        if (config != null && !config.isEmpty()) {
            try {
                props.load(new FileInputStream(config));
            } catch (IOException e) {
                LOG.error("Configuration file {} cannot be opened for reading. Configuration will not be loaded and defaults will be used.", config, e);
            }
        }
        return props;
    }

    @Bean
    public EmbeddedServletContainerCustomizer containerCustomizer(BeenProperties properties) {
        return (container -> {
            container.setPort(Integer.valueOf((String) properties.getOrDefault("been.port", "8080")));
        });
    }

    @Autowired
    private BeenProperties beenProperties;

    @PostConstruct
    public void configure() {
        int scheduledExecutorCorePoolSize = Integer.valueOf((String) beenProperties.getOrDefault("been.common.scheduledExecutorCorePoolSize", "1"));
        ExecutorPools.setScheduledExecutorCorePoolSize(scheduledExecutorCorePoolSize);
    }
}
