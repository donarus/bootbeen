package cz.cuni.mff.d3s.been.app;

import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

@SpringBootApplication
public class Bootstrap {
    private static final Logger LOG;

    private static final Options options;
    private static final String BEEN_BANNER = "\n\n" +
            "    _                 _   _____                 \n" +
            "   | |               | | | ___ \\                \n" +
            "   | |__   ___   ___ | |_| |_/ / ___  ___ _ __  \n" +
            "   | '_ \\ / _ \\ / _ \\| __| ___ \\/ _ \\/ _ \\ '_ \\ \n" +
            "   | |_) | (_) | (_) | |_| |_/ /  __/  __/ | | |\n" +
            "   |_.__/ \\___/ \\___/ \\__\\____/ \\___|\\___|_| |_|\n" +
            "                                             \n";

    static {
        LOG = LoggerFactory.getLogger(Bootstrap.class);

        options = new Options();

        Option config = Option.builder("c")
                .longOpt("config")
                .hasArg()
                .desc("path to config file (Relative to current working directory or absolute)")
                .build();

        options.addOption(config);
    }

    public static void main(String[] args) throws ParseException {
        CommandLine line = new DefaultParser().parse(options, args);

        if (!line.hasOption("config")) {
            LOG.info("Configuration not provided, BootBEEN will try to use default values where possible.");
        }


        Properties props = parseProperties(line.getOptionValue("config"));

        SpringApplication app = new SpringApplication(BeenAppConfiguration.class);
        app.setBanner((environment, sourceClass, out) -> LOG.info(BEEN_BANNER));
        app.setAdditionalProfiles(getProfiles(props));
        List<String> modifiedArgs = new ArrayList<>();
        if (line.hasOption("config")) {
            modifiedArgs.add("--config=" + line.getOptionValue("config"));
        }
        app.run(modifiedArgs.toArray(new String[0]));
        LOG.info("\n-----------------\n\nhurreyyy!!! BootBEEN is up and running!!!\n\n-----------------");
    }

    private static String[] getProfiles(Properties props) {
        String[] profiles = props.getProperty("been.profiles", "been-rest,swr-server,hr-server").split(",");
        LOG.info("Activating profiles: {}", Arrays.toString(profiles));
        return profiles;
    }

    private static Properties parseProperties(String path) {
        Properties props = new Properties();

        if (path != null) {
            File propsFile = new File(path);
            if (!propsFile.exists()) {
                LOG.error("Specified configuration file: {} does not exists. Exiting.", path);
                System.exit(8);
            }

            try {
                props.load(new FileInputStream(propsFile));
            } catch (IOException e) {
                LOG.error("Error occured while loading configuration file: {}. Exiting.", path, e);
                System.exit(8);
            }
        }
        return props;
    }

}
