package cz.cuni.mff.d3s.been.app;

import cz.cuni.mff.d3s.been.api.clients.HostRuntimeClient;
import cz.cuni.mff.d3s.been.api.clients.SoftwareRepositoryClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class ServiceChecker {

    @Autowired
    List<SoftwareRepositoryClient> swrClients;

    @Autowired
    List<HostRuntimeClient> hrClients;

    @PostConstruct
    void check() {
        if (swrClients.isEmpty()) {
            throw new RuntimeException("Missing software repository client.");
        } else if (swrClients.size() > 1) {
            throw new RuntimeException("Multiple software repository clients detected.");
        }

        if (hrClients.isEmpty()) {
            throw new RuntimeException("Missing host runtime client.");
        } else if (swrClients.size() > 1) {
            throw new RuntimeException("Multiple host runtime clients detected.");
        }
    }

}
