package cz.cuni.mff.d3s.been.api.cluster.data;

import cz.cuni.mff.d3s.been.api.cluster.data.structures.ClusterTopic;

/**
 * This service provides methods for obtaining and
 * destroing cluster wide {@link ClusterTopic} instances.
 */
public interface Topics {

    /**
     * Obtain cluster-wide topic with given name
     *
     * @param <T>  instance type of items in the topic
     * @param name name of the topic to obtain
     * @return new or existing topic with given name
     */
    <T> ClusterTopic<T> getTopic(String name);

    /**
     * destroy {@link ClusterTopic} instance with
     * given name cluster-wide
     *
     * @param name name of the topic to detroy
     */
    void destroyTopic(String name);
}
