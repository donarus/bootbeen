package cz.cuni.mff.d3s.been.api.cluster.data.structures;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;


/**
 * Concurrent, distributed implementation of {@link Map}
 * <p>
 * Instances of this class should not be implemented as a service but they
 * should be instantiated and obtained through
 * {@link cz.cuni.mff.d3s.been.api.cluster.data.Maps}.
 * </p>
 *
 * @param <K> key type of entries in this map
 * @param <V> value type of entries in this mal
 */
public interface ClusterMap<K, V> extends Map<K, V> {

    /**
     * Acquires the lock for the specified key.
     * <p>If the lock is not available, then
     * the current thread becomes disabled for thread scheduling
     * purposes and lies dormant until the lock has been acquired.
     * </p>
     * <p>
     * You get a lock whether the value is present in the map or not. Other
     * threads (possibly on other systems) would block on their invoke of
     * <code>lock()</code> until the non-existent key is unlocked. If the lock
     * holder introduces the key to the map, the <code>put()</code> operation
     * is not blocked. If a thread not holding a lock on the non-existent key
     * tries to introduce the key while a lock exists on the non-existent key,
     * the <code>put()</code> operation blocks until it is unlocked.
     * </p>
     * Locks are re-entrant (if locked N times must be unlocked N times).
     *
     * @param key key to lock.
     * @throws NullPointerException if the specified key is null.
     */
    void lock(K key);

    /**
     * Tries to acquire the lock for the specified key.
     * <p>If the lock is not available then the current thread
     * doesn't wait and returns false immediately.
     *
     * @param key key to lock.
     * @return true if lock has been acquired, false otherwise
     * @throws NullPointerException if the specified key is null.
     */
    boolean tryLock(K key);

    /**
     * Tries to acquire the lock for the specified key.
     * <p>If the lock is not available, then
     * the current thread becomes disabled for thread scheduling
     * purposes and lies dormant until one of two things happens:
     * <ul>
     * <li>the lock is acquired by the current thread, or
     * <li>the specified waiting time elapses.
     * </ul>
     *
     * @param key      key to lock in this map.
     * @param time     maximum time to wait for the lock.
     * @param timeUnit time unit of the <tt>time</tt> argument.
     * @return <tt>true</tt> if the lock was acquired and <tt>false</tt>
     * if the waiting time elapsed before the lock was acquired.
     * @throws NullPointerException if the specified key is null.
     * @throws InterruptedException if function has been interrupted during waiting for lock acquire.
     */
    boolean tryLock(K key, long time, TimeUnit timeUnit) throws InterruptedException;

    /**
     * Releases the lock for the specified key. It never blocks and
     * returns immediately.
     *
     * @param key locked key in this map
     * @throws NullPointerException if the specified key is null.
     */
    void unlock(K key);

    /**
     * Filter and return values corresponding given predicate.
     *
     * @param predicate predicate  used for filtering
     * @return filtered values
     */
    List<V> filter(ClusterMapPredicate<K, V> predicate);

    /**
     * Return list of items with given keys.
     * @param keys keys
     * @return filtered collection
     */
    List<V> listByKeys(Collection<K> keys);

    /**
     * Map all values from type V to type T using given mapper and return in new list.
     *
     * @param mapper mapper used to map values vrom V to T
     * @param <T>    result type of mapped values
     * @return mapped values
     */
    <T> List<T> map(ClusterMapValueMapper<V, T> mapper);

    /**
     * Map value with given key to new type and return result
     *
     * @param key    key of mapped value
     * @param mapper mapper used to map value vrom V to T
     * @param <T>    result type of mapped value
     * @return mapped value for given key
     */
    <T> T getAndMap(String key, ClusterMapValueMapper<V, T> mapper);

    /**
     * Filter values by predicate and map values using mapper and return as new list.
     *
     * @param predicate predicate  used for filtering
     * @param mapper    mapper used to map values vrom V to T
     * @param <T>       result type of mapped values
     * @return mapped values
     */
    <T> List<T> filterAndMap(ClusterMapPredicate<K, V> predicate, ClusterMapValueMapper<V, T> mapper);

    /**
     * Remove all values fulfilling a given predicate
     *
     * @param predicate predicate  used for selecting items to filter out filtering
     */
    void removeAll(ClusterMapPredicate<K, V> predicate);

    /**
     * Predicate used for filtering in {@link ClusterMap}
     *
     * @param <K> key class of cluster map entry on which this predicate will be used
     * @param <V> value class of cluster map entry on which this predicate will be used
     */
    interface ClusterMapPredicate<K, V> extends Serializable {
        /**
         * Apply this predicate on given key and value.
         *
         * @param key   key
         * @param value value
         * @return true if given key and value pass requirements specified by this predicate
         */
        boolean apply(K key, V value);
    }

    /**
     * Mapper used for mapping values from one type to another in {@link ClusterMap}
     *
     * @param <V> source value type
     * @param <T> target value type
     */
    interface ClusterMapValueMapper<V, T> extends Serializable {
        /**
         * vake value of class V and remap it to value of class T
         *
         * @param value value to be remapped
         * @return remapped value
         */
        T map(V value);
    }
}
