package cz.cuni.mff.d3s.been.api.cluster;

/**
 * Cluster-wide timestamp generator.
 */
public interface ClusterTimestampGenerator {
    /**
     * Generate cluster timestamp. If invoked at the exactly same time on two different cluster nodes, the return value
     * should be identical.
     *
     * @return generated timestamp in milliseconds
     */
    long generate();
}
