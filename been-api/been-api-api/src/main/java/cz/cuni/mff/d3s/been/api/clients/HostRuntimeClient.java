package cz.cuni.mff.d3s.been.api.clients;

import cz.cuni.mff.d3s.been.model.benchmark.BenchmarkPlan;
import cz.cuni.mff.d3s.been.model.bpk.BpkId;
import cz.cuni.mff.d3s.been.model.benchmark.Benchmark;
import cz.cuni.mff.d3s.been.model.task.Task;
import cz.cuni.mff.d3s.been.model.task.TaskState;
import cz.cuni.mff.d3s.been.model.td.TaskDescriptor;

import java.util.List;

public interface HostRuntimeClient {

    void runTask(BpkId bpkId, TaskDescriptor taskDescriptor);

    void runBenchmark(BenchmarkPlan benchmarkPlan);

    void killTask(Task task);

    void killTask(String taskUuid);

    /**
     * Retrieves all tasks currently stored in the cluster.
     * Each item in returned collection is deep copy of the
     * original item, so modification of such item does not
     * resolve in any modification in underlying backing
     * structure.
     *
     * @return all tasks in cluster
     */
    List<Task> listTasks();

    /**
     * Retrieves all benchmarks currently stored in the cluster.
     * Each item in returned collection is deep copy of the
     * original item, so modification of such item does not
     * resolve in any modification in underlying backing
     * structure. Benchmark plan can be huge, so method can be
     * called with parameter includePlan set to FALSE, which
     * can result in less cluster overhead.
     *
     * @param includePlan whenever returned benchmark items
     *                    should contain their benchmark plan
     *                    or not
     * @return all benchmarks in cluster
     */
    List<Benchmark> listBenchmarks(boolean includePlan);

    /**
     * Retrieves all tasks which are/were running on node with given nodeUuid.
     * Each item in returned collection is deep copy of the
     * original item, so modification of such item does not
     * resolve in any modification in underlying backing
     * structure.
     *
     * @param nodeUuid id of node on which the task is/was running
     * @return tasks running on node with given id
     */
    List<Task> listTasksByNodeUuid(String nodeUuid);

    /**
     * Retrieves task with given ID.
     * Task returned by this method is deep copy of the
     * original item, so modification of such item does not
     * resolve in any modification in underlying backing
     * structure.
     *
     * @param taskUuid id of task to be retrieved, corresponding to {@link Task#taskUuid}
     * @return task with given id or null if it doesn't exists
     */
    Task getTaskByUuid(String taskUuid);

    Benchmark getBenchmarkByUuid(String benchmarkUuid);

    List<Benchmark> listBenchmarks();

    void killBenchmark(String benchmarkUuid);

    void killTaskContext(String taskContextId);
}
