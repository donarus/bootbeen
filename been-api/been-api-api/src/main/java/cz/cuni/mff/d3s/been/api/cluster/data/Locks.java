package cz.cuni.mff.d3s.been.api.cluster.data;

import cz.cuni.mff.d3s.been.api.cluster.data.structures.ClusterLock;

/**
 * This service provides methods for obtaining and
 * destroing cluster wide {@link ClusterLock} instances.
 */
public interface Locks {

    /**
     * Obtain cluster-wide lock with given name
     *
     * @param name name of the lock to obtain
     * @return new or existing lock with given name
     */
    ClusterLock getLock(String name);

    /**
     * destroy {@link ClusterLock} instance with
     * given name cluster-wide
     *
     * @param name name of the lock to detroy
     */
    void destroyLock(String name);

}
