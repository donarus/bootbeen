package cz.cuni.mff.d3s.been.api.cluster.data.structures;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

/**
 * A map whose keys can be associated with multiple values.
 * <p>
 * Instances of this class should not be implemented as a service but they
 * should be instantiated and obtained through
 * {@link cz.cuni.mff.d3s.been.api.cluster.data.MultiMaps}.
 * </p>
 *
 * @param <K> key type of entries in this map
 * @param <V> value type of entries in this map
 */
public interface ClusterMultiMap<K, V> {

    /**
     * Stores a key-value pair in the multimap. If there is already
     * some collection of values stored under the given key, the new
     * value is added to this collection.
     *
     * @param key   key
     * @param value value
     * @return true if value bas been inserter under specified key
     */
    boolean put(K key, V value);

    /**
     * Retrieves collection of values stored under the given key
     *
     * @param key key
     * @return collection of values for given key
     */
    Collection<V> get(K key);

    /**
     * Remove entry with given key and calue from the map
     *
     * @param key   key
     * @param value value
     * @return true if the size of the multimap changed () after the remove operation, false otherwise.
     */
    boolean remove(Object key, Object value);

    /**
     * remove entry with given key
     *
     * @param key key
     * @return collection of values which were stored under the removed key
     */
    Collection<V> remove(Object key);

    /**
     * return all keys for
     *
     * @return all keys
     */
    Set<K> keySet();

    /**
     * return all values over all entries
     *
     * @return all values
     */
    Collection<V> values();

    /**
     * Return all stored entries
     *
     * @return all entries
     */
    Set<Map.Entry<K, V>> entrySet();

    /**
     * Returns whether the multimap contains an entry with given key.
     *
     * @param key key to be checked
     * @return true if the multimap contains the key
     */
    boolean containsKey(K key);

    /**
     * Returns whether the multimap contains an entry under arbirtary key with the value.
     *
     * @param value the value whose existence is checked.
     * @return true if the multimap contains an entry with the value, false otherwise.
     */
    boolean containsValue(Object value);

    /**
     * check it the multimap contains given value under the given key
     *
     * @param key   key
     * @param value value
     * @return true if multimap contains the key-value pair, false otherwise
     */
    boolean containsEntry(K key, V value);

    /**
     * Returns the number of all entries in the multimap
     *
     * @return number of entries
     */
    int size();

    /**
     * Remove all entries from the multimap
     */
    void clear();

    /**
     * Returns the number of entries for given key in the multimap
     *
     * @param key key
     * @return number of entries
     */
    int valueCount(K key);
}
