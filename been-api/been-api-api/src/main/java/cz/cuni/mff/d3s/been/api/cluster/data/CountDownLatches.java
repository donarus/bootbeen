package cz.cuni.mff.d3s.been.api.cluster.data;

import cz.cuni.mff.d3s.been.api.cluster.data.structures.ClusterCountDownLatch;

/**
 * This service provides methods for obtaining and
 * destroing cluster wide {@link ClusterCountDownLatch} instances.
 */
public interface CountDownLatches {

    /**
     * Obtain cluster-wide count down latch with given name
     *
     * @param name name of the latch to obtain
     * @return new or existing latch with given name
     */
    ClusterCountDownLatch getCountDownLatch(String name);

    /**
     * destroy {@link ClusterCountDownLatch} instance with
     * given name cluster-wide
     *
     * @param name name of the latch to detroy
     */
    void destroyCountDownLatch(String name);
}
