/**
 * The package contains definition of services for obtaining and working with cluster-wide data structures.
 *
 * @see cz.cuni.mff.d3s.been.api.cluster.data.structures.ClusterCountDownLatch
 * @see cz.cuni.mff.d3s.been.api.cluster.data.structures.ClusterList
 * @see cz.cuni.mff.d3s.been.api.cluster.data.structures.ClusterLock
 * @see cz.cuni.mff.d3s.been.api.cluster.data.structures.ClusterMap
 * @see cz.cuni.mff.d3s.been.api.cluster.data.structures.ClusterMultiMap
 * @see cz.cuni.mff.d3s.been.api.cluster.data.structures.ClusterQueue
 * @see cz.cuni.mff.d3s.been.api.cluster.data.structures.ClusterTopic
 */

package cz.cuni.mff.d3s.been.api.cluster.data;