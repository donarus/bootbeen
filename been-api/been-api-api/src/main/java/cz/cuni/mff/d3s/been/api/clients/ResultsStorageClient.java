package cz.cuni.mff.d3s.been.api.clients;

import cz.cuni.mff.d3s.been.model.result.SerializedResult;

import java.util.List;

public interface ResultsStorageClient {

    String storeResult(SerializedResult result);

    List<SerializedResult> getResultsForTask(String taskId);

}
