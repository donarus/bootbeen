package cz.cuni.mff.d3s.been.api.cluster.data.structures;

import java.util.concurrent.locks.Lock;

/**
 * Cluster-wide implementation of {@link Lock}.
 * <p>
 * A lock is a tool for controlling access to a shared resource by
 * multiple threads. Commonly, a lock provides exclusive access to a
 * shared resource: only one thread at a time can acquire the lock and
 * all access to the shared resource requires that the lock be
 * acquired first.
 * </p>
 * <p>
 * Instances of this class should not be implemented as a service but they
 * should be instantiated and obtained through
 * {@link cz.cuni.mff.d3s.been.api.cluster.data.Locks}.
 * </p>
 */
public interface ClusterLock extends Lock {
}
