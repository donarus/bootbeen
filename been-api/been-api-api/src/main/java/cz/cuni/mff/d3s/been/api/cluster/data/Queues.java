package cz.cuni.mff.d3s.been.api.cluster.data;

import cz.cuni.mff.d3s.been.api.cluster.data.structures.ClusterQueue;

/**
 * This service provides methods for obtaining and
 * destroing cluster wide {@link ClusterQueue} instances.
 */
public interface Queues {

    /**
     * Obtain cluster-wide queue with given name
     *
     * @param <T>  instance type of items in the queue
     * @param name name of the queue to obtain
     * @return new or existing queue with given name
     */
    <T> ClusterQueue<T> getQueue(String name);

    /**
     * destroy {@link ClusterQueue} instance with
     * given name cluster-wide
     *
     * @param name name of the queue to detroy
     */
    void destroyQueue(String name);
}
