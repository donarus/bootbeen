package cz.cuni.mff.d3s.been.api.cluster.data;

import cz.cuni.mff.d3s.been.api.cluster.data.structures.ClusterMap;

/**
 * This service provides methods for obtaining and
 * destroing cluster wide {@link ClusterMap} instances.
 */
public interface Maps {

    /**
     * Obtain cluster-wide map with given name
     *
     * @param <K>  key type of items in the map
     * @param <V>  value type of items in the map
     * @param name name of the map to obtain
     * @return new or existing map with given name
     */
    <K, V> ClusterMap<K, V> getMap(String name);

    /**
     * destroy {@link ClusterMap} instance with
     * given name cluster-wide
     *
     * @param name name of the map to detroy
     */
    void destroyMap(String name);
}
