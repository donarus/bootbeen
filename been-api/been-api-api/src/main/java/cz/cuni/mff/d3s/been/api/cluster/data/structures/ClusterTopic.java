package cz.cuni.mff.d3s.been.api.cluster.data.structures;

/**
 * A cluster-wide implementation of topic. Callers are able to publish messages or subscribe for them.
 * <p>
 * Instances of this class should not be implemented as a service but they
 * should be instantiated and obtained through
 * {@link cz.cuni.mff.d3s.been.api.cluster.data.Topics}.
 * </p>
 *
 * @param <T> type of messages sent through this topic
 */
public interface ClusterTopic<T> {

    /**
     * Publich message to the topic.
     *
     * @param message message
     */
    void publish(T message);

    /**
     * Subscribe for messages published to this topic.
     *
     * @param subscription message handler
     */
    void subscribe(ClusterTopicSubscription<T> subscription);

    /**
     * Message handler for {@link ClusterTopic}
     *
     * @param <T> type of the message corresponding to type of
     *            messages sent to the destination topic
     */
    interface ClusterTopicSubscription<T> {

        /**
         * Handler called when the message is received.
         *
         * @param message message
         */
        void onMessage(T message);
    }
}
