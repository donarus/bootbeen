package cz.cuni.mff.d3s.been.api.common;

import cz.cuni.mff.d3s.been.model.service.ServiceInfo;

import java.util.List;

/**
 * Utility service giving us methods to gain information about services running in the cluster and registering them.
 */
public interface BeenServices {

    /**
     * List information about all services running in the cluster
     *
     * @return list of information about services
     */
    List<ServiceInfo> list();


    /**
     * Find  all services running on node with given UUID.
     *
     * @param nodeUUID uuid of the node searched services should running on
     * @return info about services running on this node or empty ilst if no one has been found
     */
    List<ServiceInfo> listByNodeUuid(String nodeUUID);

    /**
     * Find registered service by given name and vendor
     *
     * @param name   name of the searched service
     * @param vendor vendor of the searched service
     * @param <T>    type of the searched service
     * @return info about services or empty list if no aone has been found
     */
    <T extends ServiceInfo> List<T> listByNameAndVendor(String name, String vendor);


    /**
     * Find registered service by given uuid
     *
     * @param uuid of the searched service
     * @param <T>  type of the searched service
     * @return info about the service or null if not found
     */
    <T extends ServiceInfo> T findByServiceUuid(String uuid);

    /**
     * Register new service to the cluster
     *
     * @param serviceInfo service to be registered
     */
    void register(ServiceInfo serviceInfo);

    /**
     * Remove service from the cluster
     *
     * @param uuid uuid of the service to be removed
     */
    void removeByNodeUuid(String uuid);
}
