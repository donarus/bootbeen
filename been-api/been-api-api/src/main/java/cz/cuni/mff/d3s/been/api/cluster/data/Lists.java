package cz.cuni.mff.d3s.been.api.cluster.data;

import cz.cuni.mff.d3s.been.api.cluster.data.structures.ClusterList;

/**
 * This service provides methods for obtaining and
 * destroing cluster wide {@link ClusterList} instances.
 */
public interface Lists {

    /**
     * Obtain cluster-wide list with given name
     *
     * @param <T> instance type of items in the list
     * @param name name of the list to obtain
     * @return new or existing list with given name
     */
    <T> ClusterList<T> getList(String name);

    /**
     * destroy {@link ClusterList} instance with
     * given name cluster-wide
     *
     * @param name name of the list to detroy
     */
    void destroyList(String name);
}
