package cz.cuni.mff.d3s.been.api.clients;

import cz.cuni.mff.d3s.been.model.log.LogMessage;

import java.util.List;

/**
 * This service is intended to store and retrieve task logs.
 */
public interface LogStorageClient {

    /**
     * Store log message for task.
     *
     * @param taskUuid id of task
     * @param logMsg   log message to be stored
     */
    void log(String taskUuid, LogMessage logMsg);

    /**
     * Retreive logs stored for given task
     *
     * @param taskUuid id of task
     * @return log messages for task with given id
     */
    List<LogMessage> getLogsForTask(String taskUuid);
}
