package cz.cuni.mff.d3s.been.api.cluster;


import java.io.Serializable;
import java.util.concurrent.Callable;

/**
 * Custom type of cluster-wide executor service providing methods
 * for execution of {@link Command} or {@link CommandWithResponse}
 * on some specific node and for safe scheduling {@link Command} commands
 * clusterwide
 */
public interface ClusterExecutorService {

    /**
     * submit command on the node with given uuid
     *
     * @param command  command to be executed
     * @param nodeUuid uuid of the target node
     * @param <T>      type of the returned result
     * @return result of execution on target node
     * @throws ClusterExecutorException when execution fails from some reason
     */
    <T> T submitOnNode(CommandWithResponse<T> command, String nodeUuid) throws ClusterExecutorException;

    /**
     * execute command on the node with given uuid
     *
     * @param command  command to be executed
     * @param nodeUuid uuid of the target node
     * @throws ClusterExecutorException when execution fails from some reason
     */
    void executeOnNode(Command command, String nodeUuid) throws ClusterExecutorException;

    /**
     * Schedule command with given name to run periodically with given delay and period on some random node.
     * The command is always executed just on single node at the same time. It is not guaranteed on which node the command executes.
     * Once the command is scheduled, it CAN NOT be rescheduled, ie. once some command is scheduled under the given name
     * using this method, all othed calls for scheduling some command with the same name all over the cluster will results in
     * NO-OP
     *
     * @param name name of the command
     * @param command command
     * @param initialDelay initial delay before first execution in milliseconds
     * @param period schedule period in milliseconds
     */
    void scheduleAnywhereOnCluster(String name, Command command, long initialDelay, long period);

    /**
     * Base command which should be executed on some cluster node using {@link ClusterExecutorService}.
     * <p>
     * This command MUST BE serializable - so also all instances passed and used in {@link Command#run()} method body
     * must be serializable. If you intend to use for example some node-specific bean inside the cluster, look for example
     * for class cz.cuni.mff.d3s.been.springsupport.SpringContextHolder, which is part of 'been-spring-support' module.
     * Then you can do the following SpringContextHolder.bean(MyService.class).methodOnMyServiceBean(...).
     * </p>
     */
    interface Command extends Runnable, Serializable {
    }

    /**
     * Base command with result which should be executed on some cluster node using {@link ClusterExecutorService}.
     * <p>
     * This command MUST BE serializable - so also all instances passed and used in {@link Command#run()} method body
     * must be serializable. If you intend to use for example some node-specific bean inside the cluster, look for example
     * for class cz.cuni.mff.d3s.been.springsupport.SpringContextHolder, which is part of 'been-spring-support' module.
     * Then you can do the following SpringContextHolder.bean(MyService.class).methodOnMyServiceBean(...).
     * </p>
     * type of the result
     */
    interface CommandWithResponse<T> extends Callable<T>, Serializable {
    }
}
