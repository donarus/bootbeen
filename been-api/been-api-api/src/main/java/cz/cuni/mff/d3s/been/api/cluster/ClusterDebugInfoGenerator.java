package cz.cuni.mff.d3s.been.api.cluster;

/**
 * The purpose of this interface is to generate
 * debug info about underlying cluster.
 * <p>
 * It is not strictly defined how the debug info should look
 * like but it is recommended to generate at least something parsable
 * like JSON or XML.
 * <p>
 * This debug info can be used for example by REST api.
 */
public interface ClusterDebugInfoGenerator {

    /**
     * The result string must be an non-empty string but
     * how it should look like is not defined. The returned message
     * should be used for debugging purposes only and no one should
     * rely on format of this message.
     *
     * @return generated debug info string
     */
    String generate();

}
