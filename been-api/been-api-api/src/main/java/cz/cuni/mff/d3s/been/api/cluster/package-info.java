/**
 * This package wraps definitions for base cluster-wide services and cluster-wide objects.
 * The recommended approach ti implement running cluster is to create new maven module and implement
 * all services
 */
package cz.cuni.mff.d3s.been.api.cluster;