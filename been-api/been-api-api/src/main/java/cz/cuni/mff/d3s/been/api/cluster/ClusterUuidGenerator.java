package cz.cuni.mff.d3s.been.api.cluster;

/**
 * Cluster-wide UUID generator.
 */
public interface ClusterUuidGenerator {

    /**
     * Generate new uuid. Implementor should quarantee that there will not
     * be a collision also between restarts of the whole cluster
     *
     * @return cluster-wide uuid
     */
    String generate();

    /**
     * Generate new cluster-wide unique long. Implementor should quarantee that there will not
     * be a collision also between restarts of the whole cluster
     *
     * @return cluster-wide uuid
     */
    long generateLong();
}
