package cz.cuni.mff.d3s.been.api.clients;

public class SoftwareRepositoryException extends Exception {

    public SoftwareRepositoryException(String message, Throwable cause) {
        super(message, cause);
    }

    public SoftwareRepositoryException(String message) {
        super(message);
    }

    public SoftwareRepositoryException(Throwable cause) {
        super(cause);
    }

}
