package cz.cuni.mff.d3s.been.api.common;

import cz.cuni.mff.d3s.been.model.node.NodeInfo;

import java.util.Collection;
import java.util.List;


/**
 * This class should help the user to access connected BeenNodes.
 */
public interface BeenNodes {

    /**
     * List nodes connected to the cluster
     *
     * @return list of information about connected nodes
     */
    List<NodeInfo> list();

    /**
     * Get information about concrete node
     *
     * @param uuid uuid of the node about chich we want to get some information
     * @return information about the node
     */
    NodeInfo getByUuid(String uuid);

    /**
     * Collect information about nodes with given UUIDs
     *
     * @param taskUuids uuids of nodes we want to get information about
     * @return collected list of informatiob about nodes
     */
    List<NodeInfo> listByUuids(Collection<String> taskUuids);

    /**
     * Inform about registering new node to the cluster
     *
     * @param nodeInfo node which have been registered
     */
    void register(NodeInfo nodeInfo);

    /**
     * add listener listening for node connection events
     *
     * @param listener listener
     * @return listened identifier
     */
    String addNodeAddedListener(NodeListener listener);

    /**
     * removes listener with given identifier
     *
     * @param listenerUuid identifier
     */
    void removeNodeAddedListener(String listenerUuid);

    /**
     * add listener listening for node node update events
     *
     * @param listener listener
     * @return listened identifier
     */
    String addNodeUpdatedListener(NodeListener listener);

    /**
     * removes listener with given identifier
     *
     * @param listenerUuid identifier
     */

    void removeNodeUpdatedListener(String listenerUuid);

    /**
     * add listener listening for node disconnected events
     *
     * @param listener listener
     * @return listened identifier
     */
    String addNodeRemovedListener(NodeListener listener);

    /**
     * removes listener with given identifier
     *
     * @param listenerUuid identifier
     */
    void removeNodeRemovedListener(String listenerUuid);

    /**
     * Interface for node vent listeners
     */
    interface NodeListener {
        void handle(NodeInfo nodeInfo);
    }

}
