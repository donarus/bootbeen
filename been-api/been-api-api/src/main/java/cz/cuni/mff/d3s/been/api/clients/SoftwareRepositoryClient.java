package cz.cuni.mff.d3s.been.api.clients;

import cz.cuni.mff.d3s.been.model.bpk.Bpk;
import cz.cuni.mff.d3s.been.model.bpk.BpkId;
import cz.cuni.mff.d3s.been.model.td.TaskDescriptor;

import java.io.File;
import java.util.List;

public interface SoftwareRepositoryClient {
    File get(BpkId bpkId) throws SoftwareRepositoryException;

    List<BpkId> list() throws SoftwareRepositoryException;

    void delete(BpkId bpkId) throws SoftwareRepositoryException;

    void upload(File bpk) throws SoftwareRepositoryException;

    TaskDescriptor getTaskDescriptor(BpkId bpkId, String taskDescriptorName) throws SoftwareRepositoryException;

    Bpk getBpkDescriptor(BpkId bpkId) throws SoftwareRepositoryException;

    List<TaskDescriptor> listTaskDescriptors(BpkId bpkId) throws SoftwareRepositoryException;
}
