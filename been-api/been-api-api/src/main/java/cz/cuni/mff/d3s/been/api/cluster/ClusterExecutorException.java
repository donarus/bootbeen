package cz.cuni.mff.d3s.been.api.cluster;


/**
 * The exception should is thrown in reasonable cases from {@link ClusterExecutorService}
 */
public class ClusterExecutorException extends Exception {
    public ClusterExecutorException(String message, Throwable cause) {
        super(message, cause);
    }

    public ClusterExecutorException(Throwable cause) {
        super(cause);
    }

    public ClusterExecutorException(String message) {
        super(message);
    }
}
