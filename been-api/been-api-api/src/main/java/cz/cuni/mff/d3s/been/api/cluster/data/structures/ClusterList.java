package cz.cuni.mff.d3s.been.api.cluster.data.structures;

import java.util.List;

/**
 * Concurrent, distributed implementation of {@link List}
 * <p>
 * Instances of this class should not be implemented as a service but they
 * should be instantiated and obtained through
 * {@link cz.cuni.mff.d3s.been.api.cluster.data.Lists}.
 * </p>
 *
 * @param <T> type of iems in this list
 */
public interface ClusterList<T> extends List<T> {

    /**
     * Adds an event listener for this list. Listener will be notified
     * for all list add/remove events.
     *
     * @param listener the event listener
     * @return returns the assigned listener id.
     */
    String addListener(ClusterListEventListener listener);

    /**
     * Removes the specified item listener.
     * Returns silently if the specified listener was not added before.
     *
     * @param listenerHandle assigned listener id
     */
    void removeListener(String listenerHandle);

    /**
     * Event listener for {@link ClusterList}
     *
     * @param <T> item type
     */
    interface ClusterListEventListener<T> {
        /**
         * Invoked when an item is added.
         *
         * @param item the added item
         */
        void itemAdded(T item);

        /**
         * Invoked when an item is removed.
         *
         * @param item the removed item.
         */
        void itemRemoved(T item);
    }

}
