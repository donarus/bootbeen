package cz.cuni.mff.d3s.been.api.cluster.data;

import cz.cuni.mff.d3s.been.api.cluster.data.structures.ClusterMultiMap;

/**
 * This service provides methods for obtaining and
 * destroing cluster wide {@link ClusterMultiMap} instances.
 */
public interface MultiMaps {

    /**
     * Obtain cluster-wide multimap with given name
     *
     * @param <K>  key type of items in the multimap
     * @param <V>  value type of items in the multimap
     * @param name name of the multimap to obtain
     * @return new or existing multimap with given name
     */
    <K, V> ClusterMultiMap<K, V> getMap(String name);

    /**
     * destroy {@link ClusterMultiMap} instance with
     * given name cluster-wide
     *
     * @param name name of the multimap to detroy
     */
    void destroyMultiMap(String name);
}
