package cz.cuni.mff.d3s.been.api.cluster.data.structures;

import java.util.concurrent.TimeUnit;

/**
 * A cluster-wide synchronization aid that allows one or more threads running
 * on one or more cluster members to wait until a set of operations
 * being performed in other threads on some node completes.
 * <p>
 * Instances of this class should not be implemented as a service but they
 * should be instantiated and obtained through
 * {@link cz.cuni.mff.d3s.been.api.cluster.data.CountDownLatches}.
 * </p>
 */
public interface ClusterCountDownLatch {

    /**
     * Causes the current thread to wait until the latch has counted down to
     * zero, unless the thread is {@linkplain Thread#interrupt interrupted}.
     *
     * @param timeout timeout
     * @param unit time unit
     * @return true when latch has been reached in given timeout, false when timeouted
     */
    boolean await(long timeout, TimeUnit unit) throws InterruptedException;

    /**
     * Decrements the count of the latch, releasing all waiting threads if
     * the count reaches zero.
     *
     * <p>If the current count is greater than zero then it is decremented.
     * If the new count is zero then all waiting threads are re-enabled for
     * thread scheduling purposes.
     *
     * <p>If the current count equals zero then nothing happens.
     */
    void countDown();

    /**
     * Returns the current value of latch counter
     * @return the current count
     */
    int getCount();

    /**
     * Sets the count to the given value if the current count is zero.
     * <p>
     * If count is not zero, then this method does nothing and returns {@code false}.
     *
     * @param count the number of times {@link #countDown} must be invoked
     *              before threads can pass through {@link #await}
     * @return {@code true} if the new count was set, {@code false} if the current
     * count is not zero
     * @throws IllegalArgumentException if {@code count} is negative
     */
    boolean trySetCount(int count);
}
