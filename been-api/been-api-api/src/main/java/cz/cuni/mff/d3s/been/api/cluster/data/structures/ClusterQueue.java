package cz.cuni.mff.d3s.been.api.cluster.data.structures;

import java.util.concurrent.BlockingQueue;

/**
 * A cluster-wide implementation of {@link BlockingQueue}
 * <p>
 * Instances of this class should not be implemented as a service but they
 * should be instantiated and obtained through
 * {@link cz.cuni.mff.d3s.been.api.cluster.data.Queues}.
 * </p>
 *
 * @param <T> type of elements stored in queue
 */
public interface ClusterQueue<T> extends BlockingQueue<T> {

}
