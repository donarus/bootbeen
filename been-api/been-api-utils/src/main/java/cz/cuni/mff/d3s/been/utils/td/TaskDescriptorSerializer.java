package cz.cuni.mff.d3s.been.utils.td;

import cz.cuni.mff.d3s.been.model.td.TaskDescriptor;
import cz.cuni.mff.d3s.been.utils.json.JsonException;
import cz.cuni.mff.d3s.been.utils.json.JsonUtils;

import java.io.File;
import java.io.InputStream;

public class TaskDescriptorSerializer {

    public static String serializeToString(TaskDescriptor taskDescriptor) throws TaskDescriptorException {
        return trySerialize(taskDescriptor, JsonUtils::writeToString);
    }

    public static TaskDescriptor deserialize(String json) throws TaskDescriptorException {
        return tryDeserialize(() -> JsonUtils.read(json, TaskDescriptor.class));
    }

    public static TaskDescriptor deserialize(File file) throws TaskDescriptorException {
        return tryDeserialize(() -> JsonUtils.read(file, TaskDescriptor.class));
    }

    public static TaskDescriptor deserialize(InputStream is) throws TaskDescriptorException {
        return tryDeserialize(() -> JsonUtils.read(is, TaskDescriptor.class));
    }

    private static TaskDescriptor tryDeserialize(Deserializer deserializer) throws TaskDescriptorException {
        try {
            return deserializer.deserialize();
        } catch (JsonException e) {
            throw new TaskDescriptorException("Cannot deserialize task descriptor!", e);
        }
    }

    private static <T> T trySerialize(TaskDescriptor td, Serializer<T> serializer) throws TaskDescriptorException {
        try {
            return serializer.serialize(td);
        } catch (JsonException e) {
            throw new TaskDescriptorException("Cannot serialize task descriptor!", e);
        }
    }

    private interface Deserializer {
        TaskDescriptor deserialize() throws JsonException;
    }

    private interface Serializer<T> {
        T serialize(TaskDescriptor taskDescriptor) throws JsonException;
    }
}
