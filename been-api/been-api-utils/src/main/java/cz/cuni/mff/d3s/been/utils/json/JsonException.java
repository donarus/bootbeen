package cz.cuni.mff.d3s.been.utils.json;

/**
 * The purpose of this ixception is to be thrown in case something went wrong during serialization/deserialization
 * it {@link JsonUtils}
 */
public class JsonException extends Exception {
    public JsonException(String message, Throwable cause) {
        super(message, cause);
    }
}
