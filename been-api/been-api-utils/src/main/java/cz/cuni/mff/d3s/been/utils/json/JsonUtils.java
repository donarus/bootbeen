package cz.cuni.mff.d3s.been.utils.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;


/**
 * Utility class with methods used to serialize to and deserialize from JSON objects
 */
public class JsonUtils {

    /**
     * Deserialize JSON object stored in file.
     *
     * @param fileWithJson file where the JSON object is serialized
     * @param clazz        class to which the object should be deserialized
     * @param <T>          type to which the object should be deserialized
     * @return deserialized object
     * @throws JsonException when object cannot be deserialized from some reason
     */
    public static <T> T read(File fileWithJson, Class<T> clazz) throws JsonException {
        try {
            return getMapper().readerFor(clazz).readValue(fileWithJson);
        } catch (IOException e) {
            throw new JsonException(createFailedDeserializationExceptionMessage(clazz), e);
        }
    }

    /**
     * Deserialize JSON object stored in input stream.
     *
     * @param jsonInputStream input stream with serialized JSON object
     * @param clazz           class to which the object should be deserialized
     * @param <T>             type to which the object should be deserialized
     * @return deserialized object
     * @throws JsonException when object cannot be deserialized from some reason
     */
    public static <T> T read(InputStream jsonInputStream, Class<T> clazz) throws JsonException {
        try {
            return getMapper().readerFor(clazz).readValue(jsonInputStream);
        } catch (IOException e) {
            throw new JsonException(createFailedDeserializationExceptionMessage(clazz), e);
        }
    }


    /**
     * Deserialize JSON object stored in string.
     *
     * @param jsonString string with serialized JSON
     * @param clazz      class to which the object should be deserialized
     * @param <T>        type to which the object should be deserialized
     * @return deserialized object
     * @throws JsonException when object cannot be deserialized from some reason
     */
    public static <T> T read(String jsonString, Class<T> clazz) throws JsonException {
        try {
            return getMapper().readerFor(clazz).readValue(jsonString);
        } catch (IOException e) {
            throw new JsonException(createFailedDeserializationExceptionMessage(clazz), e);
        }
    }

    /**
     * Serialize given object to json
     *
     * @param <T>    type of object to be serialized
     * @param object object to be serialized
     * @throws JsonException when object cannot be serialized from some reason
     * @see JsonUtils#writeToPrettyString
     */
    public static <T extends Serializable> String writeToString(T object) throws JsonException {
        try {
            ObjectMapper mapper = getMapper();
            return mapper.writerFor(object.getClass()).writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new JsonException(createFailedSerializationExceptionMessage(object.getClass()), e);
        }
    }

    /**
     * Serialize given object to given stream
     *
     * @param object object to be serialized
     * @param out    stream to which we want to serialize
     * @param <T>    type of serialized item
     * @throws JsonException in case serialization failed from some reason
     */
    public static <T extends Serializable> void writeToStream(T object, OutputStream out) throws JsonException {
        try {
            ObjectMapper mapper = getMapper();
            mapper.writerFor(object.getClass()).writeValue(out, object);
        } catch (JsonProcessingException e) {
            throw new JsonException(createFailedSerializationExceptionMessage(object.getClass()), e);
        } catch (IOException e) {
            throw new JsonException("Can't write serialized data to stream", e);
        }
    }

    /**
     * Serialize given object to given stream in pretty format
     *
     * @param object object to be serialized
     * @param out    stream to which we want to serialize
     * @param <T>    type of serialized item
     * @throws JsonException in case serialization failed from some reason
     */
    public static <T extends Serializable> void writeToStreamPretty(T object, OutputStream out) throws JsonException {
        try {
            ObjectMapper mapper = getMapper();
            mapper.writerFor(object.getClass()).withDefaultPrettyPrinter().writeValue(out, object);
        } catch (JsonProcessingException e) {
            throw new JsonException(createFailedSerializationExceptionMessage(object.getClass()), e);
        } catch (IOException e) {
            throw new JsonException("Can't write serialized data to stream", e);
        }
    }

    /**
     * Serialize given object to JSON.
     * Similar to {@link JsonUtils#writeToString(Serializable)} but the result is formatted
     *
     * @param <T>    type of object to be serialized
     * @param object object to be serialized
     * @throws JsonException when object cannot be serialized from some reason
     */
    public static <T extends Serializable> String writeToPrettyString(T object) throws JsonException {
        try {
            ObjectMapper mapper = getMapper();
            return mapper.writerFor(object.getClass()).withDefaultPrettyPrinter().writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new JsonException(createFailedSerializationExceptionMessage(object.getClass()), e);
        }
    }

    /**
     * Serialize given object to JSON and write this json to given file.
     *
     * @param <T>    type of object to be serialized
     * @param object object to be serialized
     * @param file   file to whih the serialized object will be written
     * @throws JsonException when object cannot be serialized from some reason
     */
    public static <T> void writeToFile(T object, File file) throws JsonException {
        try {
            ObjectMapper mapper = getMapper();
            mapper.writerFor(object.getClass()).writeValue(file, object);
        } catch (JsonProcessingException e) {
            throw new JsonException(createFailedSerializationExceptionMessage(object.getClass()), e);
        } catch (IOException e) {
            throw new JsonException(createFailedSerializationExceptionMessage(object.getClass()), e);
        }
    }

    private static ObjectMapper getMapper() {
        return new ObjectMapper();
    }

    private static <T> String createFailedDeserializationExceptionMessage(Class<T> clazz) {
        return "Unable to deserialize object of class " + clazz.getName() + ".";
    }

    private static <T> String createFailedSerializationExceptionMessage(Class<T> clazz) {
        return "Unable to serialize object of class " + clazz.getName() + ".";
    }

}
