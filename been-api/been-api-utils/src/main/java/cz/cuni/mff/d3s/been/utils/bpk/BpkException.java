package cz.cuni.mff.d3s.been.utils.bpk;

import cz.cuni.mff.d3s.been.model.bpk.Bpk;

/**
 * Wraps all exceptions which can happen while manipulating a {@link Bpk}.
 */
public class BpkException extends Exception {

    /**
     * Constructs a new exception with the specified detail message.
     *
     * @param message the detail message
     */
    public BpkException(String message) {
        super(message);
    }

    /**
     * Constructs a new exception with the specified detail message and cause.
     *
     * @param message the detail message
     * @param cause   cause of the exception
     */
    public BpkException(String message, Throwable cause) {
        super(message, cause);
    }

}
