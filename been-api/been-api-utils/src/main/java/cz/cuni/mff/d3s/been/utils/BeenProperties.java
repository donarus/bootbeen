package cz.cuni.mff.d3s.been.utils;

import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

public final class BeenProperties extends Properties {

    private Set<String> usedKeys = new HashSet<>();

    public BeenProperties(Properties properties) {
        super(properties);
    }

    public BeenProperties() {
        super();
    }

    @Override
    public synchronized Object get(Object key) {
        usedKeys.add((String)key);
        return super.get(key);
    }

    @Override
    public synchronized Object getOrDefault(Object key, Object defaultValue) {
        usedKeys.add((String)key);
        return super.getOrDefault(key, defaultValue);
    }

    @Override
    public String getProperty(String key) {
        usedKeys.add((String)key);
        return super.getProperty(key);
    }

    @Override
    public String getProperty(String key, String defaultValue) {
        usedKeys.add((String)key);
        return super.getProperty(key, defaultValue);
    }

    // fixme remove after development
    public void printUsedKeys() {
        System.out.println(String.join(", ", usedKeys));
    };
}
