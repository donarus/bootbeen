package cz.cuni.mff.d3s.been.utils.td;

public class TaskDescriptorException extends Exception {
    public TaskDescriptorException(String message, Throwable cause) {
        super(message, cause);
    }
}
