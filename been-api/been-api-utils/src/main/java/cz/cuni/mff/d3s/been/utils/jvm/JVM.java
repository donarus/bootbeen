package cz.cuni.mff.d3s.been.utils.jvm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;

/**
 * Utility class helping us to determine is JVM is shutting down
 */
public class JVM {
    private static final Logger LOG = LoggerFactory.getLogger(JVM.class);

    /**
     * Tries to determine that JVM is shutting down. It is not guaranteed that this methow will work on all
     * platforms/operating systems. In case shutdown cannot be detected, always returns false
     *
     * @return if JVM is shutting down, false otherwise or if it cannot be detected
     */
    public static boolean isShuttingDown() {
        try {
            Field running = Class.forName("java.lang.Shutdown").getDeclaredField("RUNNING");
            Field state = Class.forName("java.lang.Shutdown").getDeclaredField("state");
            running.setAccessible(true);
            state.setAccessible(true);
            return state.getInt(null) > running.getInt(null);
        } catch (Exception e) {
            LOG.warn("Can't detect if JVM is shutting down.", e);
            return false;
        }
    }
}
