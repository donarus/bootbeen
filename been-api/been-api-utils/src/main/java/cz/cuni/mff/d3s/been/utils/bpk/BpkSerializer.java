package cz.cuni.mff.d3s.been.utils.bpk;

import cz.cuni.mff.d3s.been.model.bpk.Bpk;
import cz.cuni.mff.d3s.been.utils.json.JsonException;
import cz.cuni.mff.d3s.been.utils.json.JsonUtils;

import java.io.File;
import java.io.InputStream;

public class BpkSerializer {

    public static String serializeToString(Bpk bpk) throws BpkException {
        return trySerialize(bpk, JsonUtils::writeToString);
    }

    public static Bpk deserialize(String json) throws BpkException {
        return tryDeserialize(() -> JsonUtils.read(json, Bpk.class));
    }

    public static Bpk deserialize(File file) throws BpkException {
        return tryDeserialize(() -> JsonUtils.read(file, Bpk.class));
    }

    public static Bpk deserialize(InputStream is) throws BpkException {
        return tryDeserialize(() -> JsonUtils.read(is, Bpk.class));
    }

    private static Bpk tryDeserialize(BpkSerializer.Deserializer deserializer) throws BpkException {
        try {
            return deserializer.deserialize();
        } catch (JsonException e) {
            throw new BpkException("Cannot deserialize bpk descriptor!", e);
        }
    }

    private static <T> T trySerialize(Bpk bpk, BpkSerializer.Serializer<T> serializer) throws BpkException {
        try {
            return serializer.serialize(bpk);
        } catch (JsonException e) {
            throw new BpkException("Cannot serialize bpk descriptor!", e);
        }
    }

    private interface Deserializer {
        Bpk deserialize() throws JsonException;
    }

    private interface Serializer<T> {
        T serialize(Bpk bpk) throws JsonException;
    }
}
