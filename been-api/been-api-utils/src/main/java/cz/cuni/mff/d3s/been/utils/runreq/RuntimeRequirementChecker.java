package cz.cuni.mff.d3s.been.utils.runreq;

import cz.cuni.mff.d3s.been.model.node.SystemInfo;
import cz.cuni.mff.d3s.been.model.td.JavaTaskDescriptor;
import cz.cuni.mff.d3s.been.model.td.TaskDescriptor;
import cz.cuni.mff.d3s.been.model.td.TaskRuntimeRequirement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class RuntimeRequirementChecker {

    private static final Logger LOG = LoggerFactory.getLogger(RuntimeRequirementChecker.class);

    private Map<TaskRuntimeRequirement.Operator, ConditionEvaluator> conditionEvaluators = new HashMap<>();
    private Map<String, SystemInfoValueProvider> systemInfoValueProviders = new HashMap<>();

    {
        conditionEvaluators.put(TaskRuntimeRequirement.Operator.EQ, (systemValue, requestedValue) -> systemValue.compareTo(requestedValue) == 0);
        conditionEvaluators.put(TaskRuntimeRequirement.Operator.NE, (systemValue, requestedValue) -> systemValue.compareTo(requestedValue) != 0);
        conditionEvaluators.put(TaskRuntimeRequirement.Operator.LE, (systemValue, requestedValue) -> systemValue.compareTo(requestedValue) <= 0);
        conditionEvaluators.put(TaskRuntimeRequirement.Operator.LT, (systemValue, requestedValue) -> systemValue.compareTo(requestedValue) < 0);
        conditionEvaluators.put(TaskRuntimeRequirement.Operator.GE, (systemValue, requestedValue) -> systemValue.compareTo(requestedValue) >= 0);
        conditionEvaluators.put(TaskRuntimeRequirement.Operator.GT, (systemValue, requestedValue) -> systemValue.compareTo(requestedValue) > 0);

        /* ------------------- */

        systemInfoValueProviders.put(TaskRuntimeRequirement.LOGICAL_CORES, SystemInfo::getNumberOfLogicalCores);
        systemInfoValueProviders.put(TaskRuntimeRequirement.PHYSICAL_CORES, SystemInfo::getNumberOfPhysicalCores);
        systemInfoValueProviders.put(TaskRuntimeRequirement.OPERATING_SYSTEM_FAMILY, SystemInfo::getOperatingSystemFamily);
        systemInfoValueProviders.put(TaskRuntimeRequirement.OPERATING_SYSTEM_NAME, SystemInfo::getOperatingSystemName);
        systemInfoValueProviders.put(TaskRuntimeRequirement.SYSTEM_ARCHITECTURE, SystemInfo::getSystemArchitecture);
        systemInfoValueProviders.put(TaskRuntimeRequirement.SYSTEM_ENDIANNESS, SystemInfo::getSystemEndianness);
        systemInfoValueProviders.put(TaskRuntimeRequirement.SYSTEM_MEMORY, SystemInfo::getSystemMemory);
    }

    private SystemInfo systemInfo;

    public RuntimeRequirementChecker(SystemInfo systemInfo) {
        this.systemInfo = systemInfo;
    }

    public boolean check(List<TaskRuntimeRequirement> requirements) {
        if (requirements == null || requirements.isEmpty()) {
            return true;
        }

        boolean passed = true;
        for (TaskRuntimeRequirement requirement : requirements) {
            LOG.debug("Applying requirement: [{}].", requirement);
            if (evaluateRequirement(systemInfo, requirement)) {
                LOG.debug("Requirement passed: [{}].", requirement);
            } else {
                passed = false;
                LOG.debug("Requirement not passed: [{}].", requirement);
            }
        }
        return passed;
    }

    private boolean evaluateRequirement(SystemInfo systemInfo, TaskRuntimeRequirement requirement) {
        SystemInfoValueProvider systemInfoValueProvider = systemInfoValueProviders.get(requirement.getProperty());
        if (systemInfoValueProvider == null) {
            LOG.warn("Unknown tye of requirement[{}]. Requirement will be skipped. [{}].", requirement.getClass(), requirement);
            return true;
        }

        Comparable systemValue = systemInfoValueProvider.provide(systemInfo);
        if (systemValue instanceof Number) {
            systemValue = new BigDecimal(systemValue.toString());
        }

        Comparable requiredValue = (Comparable) requirement.getValue();
        if (requiredValue instanceof Number) {
            requiredValue = new BigDecimal(requiredValue.toString());
        }

        if (systemValue instanceof Enum) {
            Class aClass = systemValue.getClass();
            requiredValue = Enum.valueOf(aClass, (String) requiredValue);
        }

        ConditionEvaluator evaluator = conditionEvaluators.get(requirement.getOperator());
        return evaluator.evaluate(systemValue, requiredValue);
    }


    private interface ConditionEvaluator<T extends Comparable> {
        boolean evaluate(T systemValue, T requestedValue);
    }

    private interface SystemInfoValueProvider {
        Comparable provide(SystemInfo systemInfo);
    }
}
