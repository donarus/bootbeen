package cz.cuni.mff.d3s.been.utils.thread;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;

public class ExecutorPools {

    private static final ScheduledThreadPoolExecutor scheduledExecutorService = new ScheduledThreadPoolExecutor(1);

    public static ScheduledExecutorService scheduledExecutor() {
        return scheduledExecutorService;
    }

    public static void setScheduledExecutorCorePoolSize(int newCorePoolSize) {
        scheduledExecutorService.setCorePoolSize(newCorePoolSize);
    }
}
