package cz.cuni.mff.d3s.been.utils.bpk;

import cz.cuni.mff.d3s.been.model.bpk.Bpk;
import cz.cuni.mff.d3s.been.utils.json.JsonException;
import cz.cuni.mff.d3s.been.utils.json.JsonUtils;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Utility class for reading {@link Bpk}.
 */
public class BpkLoader {

    /**
     * Name of the file with configuration information in packages.
     */
    private static final String CONFIG_FILE = "config.json";

    /**
     * Reads the BPK configuration from the specified file.
     *
     * @param bpkFileOrDir the file to parse
     * @return parsed BPK configuration
     * @throws BpkException when the input is invalid or an I/O error occurs
     */
    public static Bpk load(File bpkFileOrDir) throws BpkException {
        if (bpkFileOrDir.isDirectory()) {
            return loadFromUnpackedBpk(bpkFileOrDir);
        } else {
            return loadFromPackedBpk(bpkFileOrDir);
        }
    }

    private static Bpk loadFromUnpackedBpk(File unpackedBpk) throws BpkException {
        File config = new File(unpackedBpk, CONFIG_FILE);

        if (!config.exists()) {
            throw new BpkException("File '" + config.getAbsolutePath() + "' was not found.");
        } else if (config.isDirectory()) {
            throw new BpkException("File '" + config.getAbsolutePath() + "' is directory.");
        }


        try (InputStream is = new FileInputStream(config)) {
            return parseBpkConfigFile(is);
        } catch (IOException e) {
            throw new BpkException("Cannot read " + CONFIG_FILE + " in extracted bpk archive");
        }
    }

    private static Bpk loadFromPackedBpk(File bpkFile) throws BpkException {
        try (ZipFile zipFile = new ZipFile(bpkFile)) {
            return load(zipFile);
        } catch (IOException e) {
            throw new BpkException("Bpk file is not valid zip file.", e);
        }
    }

    private static Bpk load(ZipFile zipFile) throws BpkException {
        ZipArchiveEntry configEntry = zipFile.getEntry(CONFIG_FILE);

        if (configEntry == null) {
            throw new BpkException("Bpk file does not contain " + CONFIG_FILE + " descriptor file");
        }

        try (InputStream is = zipFile.getInputStream(configEntry)) {
            return parseBpkConfigFile(is);
        } catch (IOException e) {
            throw new BpkException("Cannot read " + CONFIG_FILE + " in input bpk archive");
        }
    }

    private static Bpk parseBpkConfigFile(InputStream is) throws BpkException {
        try {
            return JsonUtils.read(is, Bpk.class);
        } catch (JsonException e) {
            throw new BpkException("Cannot parse BPK config!", e);
        }
    }

}
