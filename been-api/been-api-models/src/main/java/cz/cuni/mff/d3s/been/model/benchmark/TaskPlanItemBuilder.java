package cz.cuni.mff.d3s.been.model.benchmark;

import cz.cuni.mff.d3s.been.model.td.TaskDescriptor;

public class TaskPlanItemBuilder {
    private TaskPlanItem currentPlan = new TaskPlanItem();

    private TaskPlanItemBuilder(String contextName) {
        currentPlan.setName(contextName);
    }

    public static TaskPlanItemBuilder planNew(String name) {
        return new TaskPlanItemBuilder(name);
    }

    public TaskPlanItemBuilder withDescription(String description) {
        currentPlan.setDescription(description);
        return this;
    }

    public TaskPlanItemBuilder withTaskDescriptor(TaskDescriptor taskDescriptor) {
        currentPlan.setTaskDescriptor(taskDescriptor);
        return this;
    }

    public TaskPlanItem build() {
        return currentPlan;
    }
}
