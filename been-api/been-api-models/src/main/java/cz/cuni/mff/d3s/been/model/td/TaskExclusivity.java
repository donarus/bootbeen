package cz.cuni.mff.d3s.been.model.td;

/**
 * Policy telling which tasks can run together on the same node.
 */
public enum TaskExclusivity {
    /**
     * Only single task from the same task context can run on the host runtime at the same time
     */
    CONTEXT_EXCLUSIVE,

    /**
     * Only single task can run on the host runtime at the same time
     */
    EXCLUSIVE,

    /**
     * Multiple tasks can run on the host runtime at the same time, no matter if they are from the same task context
     */
    NON_EXCLUSIVE
}
