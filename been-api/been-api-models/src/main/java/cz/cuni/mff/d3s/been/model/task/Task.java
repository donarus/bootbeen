package cz.cuni.mff.d3s.been.model.task;

import cz.cuni.mff.d3s.been.model.bpk.BpkId;
import cz.cuni.mff.d3s.been.model.td.TaskDescriptor;
import cz.cuni.mff.d3s.been.model.td.TaskType;

import java.io.File;
import java.io.Serializable;
import java.util.Date;

public class Task implements Serializable {

    private TaskType taskType;

    /**
     * Cluster-wide UUID of the task.
     */
    private String taskUuid;

    /**
     * CLuster-wide UUID of the task context this task belongs to
     */
    private String taskContextUuid;

    /**
     * Cluster-wide UUID of benchmark this task belongs to
     */
    private String benchmarkUuid;

    /**
     * Bpk identifiers of BPK package this task came from
     */
    private BpkId bpkId;

    /**
     * Actual state of the task.
     */
    private TaskState taskState;

    /**
     * Cluster-wide UUID of the node this task is running on
     */
    private String nodeUuid;

    /**
     * Full path to the wirking and home directory of this task on the owner node
     */
    private File taskDirectory;

    /**
     * Exit code after the task finished
     */
    private int taskExitCode;

    /**
     * time of task start
     */
    private Date taskStartTime;

    /**
     * time of task end
     */
    private Date taskEndTime;

    /**
     * the task descriptor corresponding to this task
     */
    private TaskDescriptor taskDescriptor;

    public TaskState getTaskState() {
        return taskState;
    }

    public void setTaskState(TaskState taskState) {
        this.taskState = taskState;
    }

    public String getTaskUuid() {
        return taskUuid;
    }

    public void setTaskUuid(String taskUuid) {
        this.taskUuid = taskUuid;
    }

    public BpkId getBpkId() {
        return bpkId;
    }

    public void setBpkId(BpkId bpkId) {
        this.bpkId = bpkId;
    }

    public String getNodeUuid() {
        return nodeUuid;
    }

    public void setNodeUuid(String nodeUuid) {
        this.nodeUuid = nodeUuid;
    }

    public File getTaskDirectory() {
        return taskDirectory;
    }

    public void setTaskDirectory(File taskDirectory) {
        this.taskDirectory = taskDirectory;
    }

    public int getTaskExitCode() {
        return taskExitCode;
    }

    public void setTaskExitCode(int taskExitCode) {
        this.taskExitCode = taskExitCode;
    }

    public Date getTaskStartTime() {
        return taskStartTime;
    }

    public void setTaskStartTime(Date taskStartTime) {
        this.taskStartTime = taskStartTime;
    }

    public Date getTaskEndTime() {
        return taskEndTime;
    }

    public void setTaskEndTime(Date taskEndTime) {
        this.taskEndTime = taskEndTime;
    }

    public TaskDescriptor getTaskDescriptor() {
        return taskDescriptor;
    }

    public void setTaskDescriptor(TaskDescriptor taskDescriptor) {
        this.taskDescriptor = taskDescriptor;
    }

    public String getTaskContextUuid() {
        return taskContextUuid;
    }

    public void setTaskContextUuid(String taskContextUuid) {
        this.taskContextUuid = taskContextUuid;
    }

    public String getBenchmarkUuid() {
        return benchmarkUuid;
    }

    public void setBenchmarkUuid(String benchmarkUuid) {
        this.benchmarkUuid = benchmarkUuid;
    }

    public TaskType getTaskType() {
        return taskType;
    }

    public void setTaskType(TaskType taskType) {
        this.taskType = taskType;
    }
}
