package cz.cuni.mff.d3s.been.model.benchmark;

import java.io.Serializable;

public abstract class PlanItem implements Serializable {
    private String name;
    private String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private PlanItemState state = PlanItemState.PLANNED;
    public PlanItemState getState() {
        return state;
    }

    public void setState(PlanItemState state) {
        this.state = state;
    }


}
