package cz.cuni.mff.d3s.been.model.td;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static cz.cuni.mff.d3s.been.model.td.TaskRuntimeRequirement.Operator.*;
import static java.util.Arrays.asList;

public final class TaskRuntimeRequirement implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(TaskRuntimeRequirement.class);

    public static final String SYSTEM_MEMORY = "SYSTEM_MEMORY";
    public static final String SYSTEM_ENDIANNESS = "SYSTEM_ENDIANNESS";
    public static final String SYSTEM_ARCHITECTURE = "SYSTEM_ARCHITECTURE";
    public static final String PHYSICAL_CORES = "PHYSICAL_CORES";
    public static final String LOGICAL_CORES = "LOGICAL_CORES";
    public static final String OPERATING_SYSTEM_FAMILY = "OPERATING_SYSTEM_FAMILY";
    public static final String OPERATING_SYSTEM_NAME = "OPERATING_SYSTEM_NAME";

    private static final Map<String, List<Operator>> ALLOWED_OPERATORS = new HashMap<>();

    static {
        ALLOWED_OPERATORS.put(SYSTEM_MEMORY, asList(EQ, NE, GT, GE, LT, LE));
        ALLOWED_OPERATORS.put(SYSTEM_ENDIANNESS, asList(EQ, NE));
        ALLOWED_OPERATORS.put(SYSTEM_ARCHITECTURE, asList(EQ, NE));
        ALLOWED_OPERATORS.put(PHYSICAL_CORES, asList(EQ, NE, GT, GE, LT, LE));
        ALLOWED_OPERATORS.put(LOGICAL_CORES, asList(EQ, NE, GT, GE, LT, LE));
        ALLOWED_OPERATORS.put(OPERATING_SYSTEM_FAMILY, asList(EQ, NE));
        ALLOWED_OPERATORS.put(OPERATING_SYSTEM_NAME, asList(EQ, NE));
    }

    public enum Operator {
        EQ, NE, GT, GE, LT, LE
    }

    /**
     * Property which should be checked. One of SYSTEM_MEMORY, SYSTEM_ENDIANNESS, SYSTEM_ARCHITECTURE,
     * PHYSICAL_CORES, LOGICAL_CORES, OPERATING_SYSTEM_FAMILY, OPERATING_SYSTEM_NAME
     */
    private String property;

    /**
     * Operator used to compare the value of the property with given value on the concrete node
     */
    private Operator operator;

    /**
     * This value will be checked against the value of the property on this node
     */
    private Object value;

    public TaskRuntimeRequirement(String property, Operator operator, Object value) {
        this.property = property;
        this.operator = operator;
        this.value = value;
    }

    public TaskRuntimeRequirement() {
    }

    /**
     * Validate if the correct operator was used with the given property
     *
     * @return true if the operator is allowed, false otherwise
     */
    public boolean validate() {
        if (!ALLOWED_OPERATORS.keySet().contains(property)) {
            LOG.info("Used non-default property '{}' for runtime requirement. Skipping check of allowed operators.", property);
        } else {
            if (!ALLOWED_OPERATORS.get(property).contains(operator)) {
                LOG.error("Operator '{}' is not supported for property '{}'. It can be misused.", operator, property);
                return false;
            }
        }
        return true;
    }

    public Operator getOperator() {
        return operator;
    }

    public void setOperator(Operator operator) {
        this.operator = operator;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    @Override
    public String toString() {
        return "TaskRuntimeRequirement={" +
                "property=" + property + "," +
                "operator=" + operator + "," +
                "value=" + value +
                "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof TaskRuntimeRequirement)) return false;

        TaskRuntimeRequirement that = (TaskRuntimeRequirement) o;

        return new EqualsBuilder()
                .append(getProperty(), that.getProperty())
                .append(getOperator(), that.getOperator())
                .append(getValue(), that.getValue())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getProperty())
                .append(getOperator())
                .append(getValue())
                .toHashCode();
    }
}
