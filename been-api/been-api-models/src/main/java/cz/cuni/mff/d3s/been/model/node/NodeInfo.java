package cz.cuni.mff.d3s.been.model.node;

import java.io.Serializable;
import java.util.Date;

/**
 * This class contains information about running node.
 */
public class NodeInfo implements Serializable {
    /**
     * Cluster-wide UUID of the node
     */
    private String uuid;


    /**
     * List of all IP addresses the node has
     */
    private String[] ipAddresses;

    /**
     * Hostname defined for the node
     */
    private String hostname;

    /**
     * Some basic system info about the node
     */
    private SystemInfo systemInfo;

    /**
     * The time this node started up
     */
    private Date startupTime;

    public NodeInfo(String uuid, String[] ipAddresses, SystemInfo systemInfo, Date startupTime) {
        this.uuid = uuid;
        this.ipAddresses = ipAddresses;
        this.systemInfo = systemInfo;
        this.startupTime = startupTime;
    }

    public String getUuid() {
        return uuid;
    }

    public String[] getIpAddresses() {
        return ipAddresses;
    }

    public SystemInfo getSystemInfo() {
        return systemInfo;
    }

    public Date getStartupTime() {
        return startupTime;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }
}
