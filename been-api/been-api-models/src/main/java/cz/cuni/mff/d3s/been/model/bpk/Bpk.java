package cz.cuni.mff.d3s.been.model.bpk;

import cz.cuni.mff.d3s.been.model.td.TaskDescriptor;

import java.io.Serializable;
import java.util.List;

/**
 * Represents BPK package
 */
public class Bpk implements Serializable {
    /**
     * identifiers of BPK package (groupId, artifactId, version)
     */
    private BpkId bpkId;

    /**
     * Human readable description of the BPK package
     */
    private String description;

    /**
     * Contains set of defined task descriptors in this BPK package
     */
    private List<TaskDescriptor> taskDescriptorTemplates;

    public Bpk() {
    }

    public Bpk(BpkId bpkId) {
        this.bpkId = bpkId;
    }

    public Bpk(BpkId bpkId, List<TaskDescriptor> taskDescriptorTemplates) {
        this.bpkId = bpkId;
        this.taskDescriptorTemplates = taskDescriptorTemplates;
    }

    public Bpk(BpkId bpkId, List<TaskDescriptor> taskDescriptorTemplates, String description) {
        this.bpkId = bpkId;
        this.taskDescriptorTemplates = taskDescriptorTemplates;
        this.description = description;
    }

    public void setBpkId(BpkId bpkId) {
        this.bpkId = bpkId;
    }

    public BpkId getBpkId() {
        return bpkId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<TaskDescriptor> getTaskDescriptorTemplates() {
        return taskDescriptorTemplates;
    }

    public void setTaskDescriptorTemplates(List<TaskDescriptor> taskDescriptorTemplates) {
        this.taskDescriptorTemplates = taskDescriptorTemplates;
    }
}
