package cz.cuni.mff.d3s.been.model.benchmark;

public class TaskContextPlanBuilder {
    private TaskContextPlanItem currentPlan = new TaskContextPlanItem();

    private TaskContextPlanBuilder(String contextName) {
        currentPlan.setName(contextName);
    }

    public static TaskContextPlanBuilder planNew(String name) {
        return new TaskContextPlanBuilder(name);
    }

    public TaskContextPlanBuilder withDescription(String description) {
        currentPlan.setDescription(description);
        return this;
    }

    public TaskContextPlanBuilder addTask(TaskPlanItem taskPlan) {
        currentPlan.getTasksPlanItems().add(taskPlan);
        return this;
    }

    public TaskContextPlanBuilder addFinalizerTask(TaskPlanItem finalizer) {
        currentPlan.setFinalizerTaskPlanItem(finalizer);
        return this;
    }

    public TaskContextPlanItem build() {
        return currentPlan;
    }
}
