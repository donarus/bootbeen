package cz.cuni.mff.d3s.been.model.benchmark;


/**
 * Utility class helping build the benchmark plan
 */
public class BenchmarkPlanBuilder {

    private BenchmarkPlan currentPlan = new BenchmarkPlan();

    private BenchmarkPlanBuilder(String benchmarkName) {
        currentPlan.setName(benchmarkName);
    }

    public static BenchmarkPlanBuilder planNew(String name) {
        return new BenchmarkPlanBuilder(name);
    }

    public BenchmarkPlanBuilder withDescription(String description) {
        currentPlan.setDescription(description);
        return this;
    }

    public BenchmarkPlanBuilder addTaskContextPlan(TaskContextPlanItem taskContextPlan) {
        currentPlan.getTaskContextPlanItems().add(taskContextPlan);
        return this;
    }

    public BenchmarkPlanBuilder addFinalizerTask(TaskPlanItem finalizer) {
        currentPlan.setFinalizerTaskPlanItem(finalizer);
        return this;
    }

    public BenchmarkPlan build() {
        return currentPlan;
    }
}
