package cz.cuni.mff.d3s.been.model.node;

public enum OperatingSystemFamily {
    MAC,
    MAC_OSX,
    LINUX,
    WINDOWS,
    BSD,
    UNKNOWN
}