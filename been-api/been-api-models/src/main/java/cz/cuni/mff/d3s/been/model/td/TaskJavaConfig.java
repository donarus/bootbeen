package cz.cuni.mff.d3s.been.model.td;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.List;

/**
 * Specifies java-related task options
 */
public class TaskJavaConfig implements Serializable {

    /**
     * main class of the task extending cz.cuni.mff.d3s.been.taskapi.Task ot cz.cuni.mff.d3s.been.taskapi.BenchmarkPlanGenerator
     */
    private String mainClass;

    /**
     * options passed to VM when running task
     */

    private List<String> javaOptions;

    /**
     * java dependencies of the task
     */
    private List<JavaArtifact> dependencies; // dependencies paths in BPK

    /**
     * debug config - specifies liste/attach mode, host, port and suspendOnStart property
     */
    private TaskJavaDebugConfig javaDebugConfig;

    public String getMainClass() {
        return mainClass;
    }

    public void setMainClass(String mainClass) {
        this.mainClass = mainClass;
    }

    public List<String> getJavaOptions() {
        return javaOptions;
    }

    public void setJavaOptions(List<String> javaOptions) {
        this.javaOptions = javaOptions;
    }

    public TaskJavaDebugConfig getJavaDebugConfig() {
        return javaDebugConfig;
    }

    public void setJavaDebugConfig(TaskJavaDebugConfig javaDebugConfig) {
        this.javaDebugConfig = javaDebugConfig;
    }

    public List<JavaArtifact> getDependencies() {
        return dependencies;
    }

    public void setDependencies(List<JavaArtifact> dependencies) {
        this.dependencies = dependencies;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof TaskJavaConfig)) return false;

        TaskJavaConfig that = (TaskJavaConfig) o;

        return new EqualsBuilder()
                .append(getMainClass(), that.getMainClass())
                .append(getJavaOptions(), that.getJavaOptions())
                .append(getDependencies(), that.getDependencies())
                .append(getJavaDebugConfig(), that.getJavaDebugConfig())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getMainClass())
                .append(getJavaOptions())
                .append(getDependencies())
                .append(getJavaDebugConfig())
                .toHashCode();
    }
}
