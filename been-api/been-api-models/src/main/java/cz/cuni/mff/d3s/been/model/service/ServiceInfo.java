package cz.cuni.mff.d3s.been.model.service;

import java.io.Serializable;

/**
 * This class represents the base info about
 * services running at some node.
 */
public class ServiceInfo implements Serializable {

    /**
     * Name of the service
     */
    protected final String name;

    /**
     * The name of the vendor for this service
     */
    protected final String vendor;

    /**
     * The version of this service
     */
    protected final String version;

    /**
     * Cluster-wide UUID of the node the service is running on
     */
    protected final String nodeUuid;

    /**
     * Cluster-wide UUID of the running service
     */
    protected final String uuid;

    public ServiceInfo(String name, String vendor, String version, String nodeUuid, String uuid) {
        this.name = name;
        this.vendor = vendor;
        this.version = version;
        this.nodeUuid = nodeUuid;
        this.uuid = uuid;
    }

    public String getName() {
        return name;
    }

    public String getVendor() {
        return vendor;
    }

    public String getVersion() {
        return version;
    }

    public String getNodeUUID() {
        return nodeUuid;
    }

    public String getDescription() {
        return null;
    }

    public String getServiceUUID() {
        return uuid;
    }

}
