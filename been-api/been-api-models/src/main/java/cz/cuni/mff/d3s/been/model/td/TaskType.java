package cz.cuni.mff.d3s.been.model.td;

public enum TaskType {
    /**
     * The task of this type should generate benchmark plan
     */
    BENCHMARK_GENERATOR,

    /**
     * Normal and finalizer tasks
     */
    REGULAR_TASK
}
