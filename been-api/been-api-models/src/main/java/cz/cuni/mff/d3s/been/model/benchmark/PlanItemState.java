package cz.cuni.mff.d3s.been.model.benchmark;

public enum PlanItemState {
    PLANNED,
    RUNNING,
    FAILED,
    FINISHED
}
