package cz.cuni.mff.d3s.been.model.td;

import java.io.Serializable;

public class NativeTaskDescriptor extends TaskDescriptor implements Serializable {

    private String executableFileName;

    public String getExecutableFileName() {
        return executableFileName;
    }

    public void setExecutableFileName(String executableFileName) {
        this.executableFileName = executableFileName;
    }
}
