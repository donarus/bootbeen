package cz.cuni.mff.d3s.been.model.node;

public enum SystemEndianness {
    BIG_ENDIAN,
    LITTLE_ENDIAN
}
