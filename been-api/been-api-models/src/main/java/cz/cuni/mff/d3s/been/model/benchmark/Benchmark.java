package cz.cuni.mff.d3s.been.model.benchmark;

import cz.cuni.mff.d3s.been.model.bpk.BpkId;

import java.io.Serializable;
import java.util.Date;

public class Benchmark implements Serializable {

    /**
     * Bpk identifiers of the BPK package this benchmark came from
     */
    private BpkId bpkId;

    /**
     * Cluster-wide UUIF of the benchmark
     */
    private String benchmarkUuid;

    /**
     * Current benchmark execution plan
     */
    private BenchmarkPlan benchmarkPlan;

    /**
     * Current state of the benchmark
     */
    private BenchmarkState benchmarkState;

    /**
     * Name of the benchmark, human readable
     */
    private String benchmarkName;

    /**
     * ClusterWide UUID of task which generated the benchmark plan
     */
    private String generatorTaskUuid;

    /**
     * Time this benchmark started
     */
    private Date start;

    /**
     * Time this benchmark ended
     */
    private Date end;

    public String getBenchmarkUuid() {
        return benchmarkUuid;
    }

    public void setBenchmarkUuid(String benchmarkUuid) {
        this.benchmarkUuid = benchmarkUuid;
    }

    public BenchmarkPlan getBenchmarkPlan() {
        return benchmarkPlan;
    }

    public void setBenchmarkPlan(BenchmarkPlan benchmarkPlan) {
        this.benchmarkPlan = benchmarkPlan;
    }

    public BpkId getBpkId() {
        return bpkId;
    }

    public void setBpkId(BpkId bpkId) {
        this.bpkId = bpkId;
    }

    public BenchmarkState getBenchmarkState() {
        return benchmarkState;
    }

    public void setBenchmarkState(BenchmarkState benchmarkState) {
        this.benchmarkState = benchmarkState;
    }

    public Benchmark lightVersion() {
        Benchmark light = new Benchmark();
        light.benchmarkName = benchmarkName;
        light.benchmarkState = benchmarkState;
        light.benchmarkUuid = benchmarkUuid;
        light.generatorTaskUuid = generatorTaskUuid;
        light.bpkId = bpkId;
        light.start = start;
        light.end = end;
        return light;
    }

    public String getBenchmarkName() {
        return benchmarkName;
    }

    public void setBenchmarkName(String benchmarkName) {
        this.benchmarkName = benchmarkName;
    }

    public void setGeneratorTaskUuid(String generatorTaskUuid) {
        this.generatorTaskUuid = generatorTaskUuid;
    }

    public String getGeneratorTaskUuid() {
        return generatorTaskUuid;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }
}
