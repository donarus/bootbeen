package cz.cuni.mff.d3s.been.model.log;

/**
 * BEEN log level numbering
 */
public enum LogLevel {
    /**
     * TRACE log level
     */
    TRACE,

    /**
     * DEBUG log level
     */
    DEBUG,

    /**
     * INFO log level
     */
    INFO,

    /**
     * WARN log level
     */
    WARN,

    /**
     * ERROR log level
     */
    ERROR
}
