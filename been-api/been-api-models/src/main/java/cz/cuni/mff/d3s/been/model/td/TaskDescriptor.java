package cz.cuni.mff.d3s.been.model.td;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import cz.cuni.mff.d3s.been.model.log.LogLevel;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static cz.cuni.mff.d3s.been.model.td.TaskType.REGULAR_TASK;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public abstract class TaskDescriptor implements Serializable {
    /**
     * This value set in setTimeout disables timeour. Used as default value for timeout set in the task descriptor.
     */
    public static final int NO_TIMEOUT = -1;

    /**
     * Type of the task .. benchmark generator or regular task
     */
    private TaskType type = REGULAR_TASK;

    /**
     * no timeout by default
     */
    private long timeoutSeconds = NO_TIMEOUT;

    /**
     * name of the underlying task
     */
    private String name;

    /**
     * log level used by loger in running task
     */
    private LogLevel logLevel = LogLevel.INFO;

    /**
     * human readable description of the underlying task
     */
    private String description = "";

    /**
     * set properties are used as environmental properties in running task
     */
    private Map<String, String> properties = new HashMap<>();

    /**
     * requirements for host runtime the task should run on
     */
    private List<TaskRuntimeRequirement> runtimeRequirements = new ArrayList<>();

    /**
     * task exclusivity mode .. EXCLUSIVE = only one task per node, CONTEXT_EXCLUSITE = only one task within same
     * task context per node, NON_EXCLUSIVE = any number of tasks on the same node (default).
     */
    private TaskExclusivity exclusivity = TaskExclusivity.NON_EXCLUSIVE;

    /**
     * Arguments passed on command line to given task
     */
    private List<String> commandLineArguments = new ArrayList<>();

    public TaskDescriptor() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TaskExclusivity getExclusivity() {
        return exclusivity;
    }

    public void setExclusivity(TaskExclusivity exclusivity) {
        this.exclusivity = exclusivity;
    }

    public List<String> getCommandLineArguments() {
        return commandLineArguments;
    }

    public void setCommandLineArguments(List<String> commandLineArguments) {
        this.commandLineArguments = commandLineArguments;
    }

    public List<TaskRuntimeRequirement> getRuntimeRequirements() {
        return runtimeRequirements;
    }

    public void setRuntimeRequirements(List<TaskRuntimeRequirement> runtimeRequirements) {
        this.runtimeRequirements = runtimeRequirements;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof TaskDescriptor)) return false;

        TaskDescriptor that = (TaskDescriptor) o;

        return new EqualsBuilder()
                .append(getType(), that.getType())
                .append(getName(), that.getName())
                .append(getDescription(), that.getDescription())
                .append(getProperties(), that.getProperties())
                .append(getRuntimeRequirements(), that.getRuntimeRequirements())
                .append(getTimeoutSeconds(), that.getTimeoutSeconds())
                .append(getExclusivity(), that.getExclusivity())
                .append(getCommandLineArguments(), that.getCommandLineArguments())
                .append(getLogLevel(), that.getLogLevel())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getType())
                .append(getName())
                .append(getDescription())
                .append(getProperties())
                .append(getRuntimeRequirements())
                .append(getTimeoutSeconds())
                .append(getExclusivity())
                .append(getCommandLineArguments())
                .append(getLogLevel())
                .toHashCode();
    }

    public Map<String, String> getProperties() {
        return properties;
    }

    public void setProperties(Map<String, String> properties) {
        this.properties = properties;
    }

    public TaskType getType() {
        return type;
    }

    public void setType(TaskType type) {
        this.type = type;
    }

    public long getTimeoutSeconds() {
        return timeoutSeconds;
    }

    public void setTimeoutSeconds(long timeoutSeconds) {
        this.timeoutSeconds = timeoutSeconds;
    }

    public LogLevel getLogLevel() {
        return logLevel;
    }

    public void setLogLevel(LogLevel logLevel) {
        this.logLevel = logLevel;
    }

}
