package cz.cuni.mff.d3s.been.model.benchmark;


import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class BenchmarkPlan extends PlanItem implements Serializable {
    private String benchmarkUuid;
    private List<TaskContextPlanItem> taskContextPlanItems = new ArrayList<>();
    private TaskPlanItem finalizerTaskPlanItem;

    public List<TaskContextPlanItem> getTaskContextPlanItems() {
        return taskContextPlanItems;
    }

    public void setTaskContextPlanItems(List<TaskContextPlanItem> taskContextPlanItems) {
        this.taskContextPlanItems = taskContextPlanItems;
    }

    public String getBenchmarkUuid() {
        return benchmarkUuid;
    }

    public void setBenchmarkUuid(String benchmarkUuid) {
        this.benchmarkUuid = benchmarkUuid;
    }

    @Override
    public String toString() {
        String output = String.format(" - BenchmarkPlanItem [name: %s, state: %s, benchmarkId: %s]", getName(), getState().toString(), benchmarkUuid);
        for (TaskContextPlanItem taskContextPlan : taskContextPlanItems) {
            String[] lines = taskContextPlan.toString().split("\\r?\\n");
            for (String line : lines) {
                output += "\n  " + line;
            }
        }
        return output;
    }

    public TaskPlanItem getFinalizerTaskPlanItem() {
        return finalizerTaskPlanItem;
    }

    public void setFinalizerTaskPlanItem(TaskPlanItem finalizerTaskPlanItem) {
        this.finalizerTaskPlanItem = finalizerTaskPlanItem;
    }

    @JsonIgnore
    public List<TaskPlanItem> runningTaskPlans() {
        List<TaskPlanItem> plans = new ArrayList<>();
        if (finalizerTaskPlanItem != null
                && (finalizerTaskPlanItem.getState() == PlanItemState.RUNNING || finalizerTaskPlanItem.getState() == PlanItemState.PLANNED)) {
            plans.add(finalizerTaskPlanItem);
        }

        if (taskContextPlanItems != null) {
            for (TaskContextPlanItem item : taskContextPlanItems) {
                plans.addAll(item.runningTaskPlans());
            }
        }

        return plans;
    }

    @JsonIgnore
    public List<TaskContextPlanItem> runningTaskContextPlans() {
        List<TaskContextPlanItem> plans = new ArrayList<>();
        for (TaskContextPlanItem taskContextPlanItem : taskContextPlanItems) {
            if (taskContextPlanItem.getState() == PlanItemState.RUNNING) {
                plans.add(taskContextPlanItem);
            }
        }
        return plans;
    }

    public List<String> getRegularTaskIdsForContext(String taskContextId) {
        if (taskContextPlanItems != null) {
            for (TaskContextPlanItem taskContextPlanItem : taskContextPlanItems) {
                if (taskContextId.equals(taskContextPlanItem.getTaskContextUuid())) {
                    return taskContextPlanItem.getTasksPlanItems().stream().map(ti -> ti.getTaskUuid()).collect(Collectors.toList());
                }
            }
        }
        return null;
    }
}
