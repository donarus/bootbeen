package cz.cuni.mff.d3s.been.model.node;

public enum SystemArchitecture {
    X86,
    X64
}