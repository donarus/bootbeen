package cz.cuni.mff.d3s.been.model.td;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.File;
import java.io.Serializable;

/**
 * Identifies java dependency artifact with groupId, artifactId, version.
 */
public class JavaArtifact implements Serializable {
    private String groupId;
    private String artifactId;
    private String version;

    public JavaArtifact() {
    }

    public JavaArtifact(String groupId, String artifactId, String version) {
        this.groupId = groupId;
        this.artifactId = artifactId;
        this.version = version;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getArtifactId() {
        return artifactId;
    }

    public void setArtifactId(String artifactId) {
        this.artifactId = artifactId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * Constructs the path to the dependency in the BPK archive
     * @return
     */
    @JsonIgnore
    public String getPathInBpk() {
        String sep = File.separator;
        return "lib" + sep + groupId + sep + artifactId + sep + version + sep + artifactId + "-" + version + ".jar";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof JavaArtifact)) return false;

        JavaArtifact that = (JavaArtifact) o;

        return new EqualsBuilder()
                .append(getGroupId(), that.getGroupId())
                .append(getArtifactId(), that.getArtifactId())
                .append(getVersion(), that.getVersion())
                .append(getPathInBpk(), that.getPathInBpk())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getGroupId())
                .append(getArtifactId())
                .append(getVersion())
                .append(getPathInBpk())
                .toHashCode();
    }

    @Override
    public String toString() {
        return groupId + ":" + artifactId + ":" + version;
    }
}
