package cz.cuni.mff.d3s.been.model.benchmark;

public enum BenchmarkState {
    BEING_GENERATED, REQUESTED, RUNNING, FAILED, FINISHED, KILLED
}
