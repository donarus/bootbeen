package cz.cuni.mff.d3s.been.model.result;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

import java.io.Serializable;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public final class SerializedResult implements Serializable {
    /**
     * Cluster-wide UUID of the task this result belongs to
     */
    private String taskId;

    /**
     * Cluster-wide UUID of the owning task context of the task this result belongs to
     */
    private String taskContextId;

    /**
     * Cluster-wide UUID of the owning benchmark of the task this result belongs to
     */
    private String benchmarkId;

    /**
     * Result serialized to JSON format
     */
    private String resultJson;

    /**
     * The key used as a key to store the result
     */
    private String key;

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public String getTaskContextId() {
        return taskContextId;
    }

    public void setTaskContextId(String taskContextId) {
        this.taskContextId = taskContextId;
    }

    public String getBenchmarkId() {
        return benchmarkId;
    }

    public void setBenchmarkId(String benchmarkId) {
        this.benchmarkId = benchmarkId;
    }

    public String getResultJson() {
        return resultJson;
    }

    public void setResultJson(String resultJson) {
        this.resultJson = resultJson;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

}
