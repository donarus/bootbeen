package cz.cuni.mff.d3s.been.model.td;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

public class TaskJavaDebugConfig implements Serializable {
    private TaskJavaDebugMode mode;
    private String host;
    private int port;
    private boolean suspendOnStart;

    public TaskJavaDebugMode getMode() {
        return mode;
    }

    public void setMode(TaskJavaDebugMode mode) {
        this.mode = mode;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public boolean isSuspendOnStart() {
        return suspendOnStart;
    }

    public void setSuspendOnStart(boolean suspendOnStart) {
        this.suspendOnStart = suspendOnStart;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof TaskJavaDebugConfig)) return false;

        TaskJavaDebugConfig that = (TaskJavaDebugConfig) o;

        return new EqualsBuilder()
                .append(isSuspendOnStart(), that.isSuspendOnStart())
                .append(getMode(), that.getMode())
                .append(getHost(), that.getHost())
                .append(getPort(), that.getPort())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getMode())
                .append(getHost())
                .append(getPort())
                .append(isSuspendOnStart())
                .toHashCode();
    }
}
