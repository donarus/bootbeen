package cz.cuni.mff.d3s.been.model.node;

import java.io.Serializable;

public class SystemInfo implements Serializable {

    private long systemMemory;
    private int numberOfPhysicalCores;
    private int numberOfLogicalCores;
    private OperatingSystemFamily operatingSystemFamily;
    private String operatingSystemName;
    private SystemArchitecture systemArchitecture;
    private SystemEndianness systemEndianness;

    public long getSystemMemory() {
        return systemMemory;
    }

    public void setSystemMemory(long systemMemory) {
        this.systemMemory = systemMemory;
    }

    public int getNumberOfPhysicalCores() {
        return numberOfPhysicalCores;
    }

    public void setNumberOfPhysicalCores(int numberOfPhysicalCores) {
        this.numberOfPhysicalCores = numberOfPhysicalCores;
    }

    public int getNumberOfLogicalCores() {
        return numberOfLogicalCores;
    }

    public void setNumberOfLogicalCores(int numberOfLogicalCores) {
        this.numberOfLogicalCores = numberOfLogicalCores;
    }

    public OperatingSystemFamily getOperatingSystemFamily() {
        return operatingSystemFamily;
    }

    public void setOperatingSystemFamily(OperatingSystemFamily operatingSystemFamily) {
        this.operatingSystemFamily = operatingSystemFamily;
    }

    public SystemArchitecture getSystemArchitecture() {
        return systemArchitecture;
    }

    public void setSystemArchitecture(SystemArchitecture systemArchitecture) {
        this.systemArchitecture = systemArchitecture;
    }

    public SystemEndianness getSystemEndianness() {
        return systemEndianness;
    }

    public void setSystemEndianness(SystemEndianness systemEndianness) {
        this.systemEndianness = systemEndianness;
    }

    public String getOperatingSystemName() {
        return operatingSystemName;
    }

    public void setOperatingSystemName(String operatingSystemName) {
        this.operatingSystemName = operatingSystemName;
    }
}
