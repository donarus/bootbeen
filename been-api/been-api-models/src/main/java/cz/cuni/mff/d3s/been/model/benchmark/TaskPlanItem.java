package cz.cuni.mff.d3s.been.model.benchmark;

import cz.cuni.mff.d3s.been.model.td.TaskDescriptor;

import java.io.Serializable;

public class TaskPlanItem extends PlanItem implements Serializable {
    private String taskUuid;
    private TaskDescriptor taskDescriptor;
    private String benchmarkUuid;
    private String taskContextUuid;

    public void setTaskDescriptor(TaskDescriptor taskDescriptor) {
        this.taskDescriptor = taskDescriptor;
    }

    public TaskDescriptor getTaskDescriptor() {
        return taskDescriptor;
    }

    public String getTaskContextUuid() {
        return taskContextUuid;
    }

    public void setTaskContextUuid(String taskContextUuid) {
        this.taskContextUuid = taskContextUuid;
    }

    public void setBenchmarkUuid(String benchmarkUuid) {
        this.benchmarkUuid = benchmarkUuid;
    }

    public String getBenchmarkUuid() {
        return benchmarkUuid;
    }

    @Override
    public String toString() {
        return String.format(" - TaskPlanItem [name: %s, state: %s, taskId: %s]", getName(), getState().toString(), taskUuid);
    }

    public void setTaskUuid(String taskUuid) {
        this.taskUuid = taskUuid;
    }

    public String getTaskUuid() {
        return taskUuid;
    }
}
