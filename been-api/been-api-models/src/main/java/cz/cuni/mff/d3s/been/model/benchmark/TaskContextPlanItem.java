package cz.cuni.mff.d3s.been.model.benchmark;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TaskContextPlanItem extends PlanItem implements Serializable {
    private String taskContextUuid;
    private List<TaskPlanItem> tasks = new ArrayList<>();
    private TaskPlanItem finalizerTaskPlanItem;

    public List<TaskPlanItem> getTasksPlanItems() {
        return tasks;
    }

    public void setTaskPlanItems(List<TaskPlanItem> taskPlanItems) {
        this.tasks = taskPlanItems;
    }

    public void setFinalizerTaskPlanItem(TaskPlanItem finalizerTaskPlanItem) {
        this.finalizerTaskPlanItem = finalizerTaskPlanItem;
    }

    public TaskPlanItem getFinalizerTaskPlanItem() {
        return finalizerTaskPlanItem;
    }

    public String getTaskContextUuid() {
        return taskContextUuid;
    }

    public void setTaskContextUuid(String taskContextUuid) {
        this.taskContextUuid = taskContextUuid;
    }

    @Override
    public String toString() {
        String output = String.format(" - TaskContextPlanItem [name: %s, state: %s, taskContextId: %s]", getName(), getState().toString(), taskContextUuid);
        for (TaskPlanItem taskPlan : tasks) {
            String[] lines = taskPlan.toString().split("\\r?\\n");
            for (String line : lines) {
                output += "\n  " + line;
            }
        }
        if (finalizerTaskPlanItem != null) {
            output += "\n   *" + finalizerTaskPlanItem.toString();
        }
        return output;
    }

    @JsonIgnore
    public List<TaskPlanItem> runningTaskPlans() {
        List<TaskPlanItem> ids = new ArrayList<>();
        if (finalizerTaskPlanItem != null
                && (finalizerTaskPlanItem.getState() == PlanItemState.RUNNING || finalizerTaskPlanItem.getState() == PlanItemState.PLANNED)) {
            ids.add(finalizerTaskPlanItem);
        }

        if (tasks != null) {
            for (TaskPlanItem item : tasks) {
                if (item.getState() == PlanItemState.RUNNING || item.getState() == PlanItemState.PLANNED) {
                    ids.add(item);
                }
            }
        }

        return ids;
    }
}