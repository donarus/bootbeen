package cz.cuni.mff.d3s.been.model.td;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

public class JavaTaskDescriptor extends TaskDescriptor implements Serializable {

    /**
     * Task java options - mainClass, vm opts, dependencies, debug options ...
     */
    private TaskJavaConfig javaConfig;

    public TaskJavaConfig getJavaConfig() {
        return javaConfig;
    }

    public void setJavaConfig(TaskJavaConfig javaConfig) {
        this.javaConfig = javaConfig;
    }

    public JavaTaskDescriptor() {
        super();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof JavaTaskDescriptor)) return false;

        JavaTaskDescriptor that = (JavaTaskDescriptor) o;

        return new EqualsBuilder()
                .appendSuper(super.equals(o))
                .append(getJavaConfig(), that.getJavaConfig())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .appendSuper(super.hashCode())
                .append(getJavaConfig())
                .toHashCode();
    }

}
