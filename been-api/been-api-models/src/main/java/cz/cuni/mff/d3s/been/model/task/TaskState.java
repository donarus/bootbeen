package cz.cuni.mff.d3s.been.model.task;

public enum TaskState {
    REQUESTED, NOT_ENOUGH_RESOURCES, STARTING, RUNNING, FINISHED, FAILED, TIMEOUTED, KILLED
}
