package cz.cuni.mff.d3s.been.model.td;

public enum TaskJavaDebugMode {
    CONNECT,
    LISTEN
}
