package com.example.been;

import cz.cuni.mff.d3s.been.model.benchmark.*;
import cz.cuni.mff.d3s.been.model.td.JavaTaskDescriptor;
import cz.cuni.mff.d3s.been.model.td.TaskExclusivity;
import cz.cuni.mff.d3s.been.taskapi.BenchmarkPlanGenerator;
import cz.cuni.mff.d3s.been.taskapi.TaskException;
import cz.cuni.mff.d3s.been.utils.object.DeepCopy;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

public class BenchmarkGenerator extends BenchmarkPlanGenerator {
    public BenchmarkPlan generatePlan(String[] args) throws TaskException {
        try {
            List<String> versions = Arrays.asList(
                    "2.5.5", "2.5.4", "2.5.3", "2.5.2",
                    "2.5.1", "2.5", "2.4", "2.3", "2.2",
                    "2.1", "2.0.0"
            );
            BenchmarkPlanBuilder builder = BenchmarkPlanBuilder.planNew("spark-core benchmark");
            Map<String, String> props = getTaskDescriptor().getProperties();
            int nrOfClients = Integer.valueOf(props.getOrDefault("nrOfClients", "5"));
            for (String version : versions) {
                builder.addTaskContextPlan(createContextPlan(nrOfClients, version));
            }
            JavaTaskDescriptor finalizer = loadTemplate("benchmark finalizer");
            builder.addFinalizerTask(taskItem("benchfin", finalizer));
            return builder.build();
        } catch (Exception e) {
            throw new TaskException(e);
        }
    }

    private TaskContextPlanItem createContextPlan(int nrOfClients, String version)
            throws Exception {
        String exclusivity = getTaskDescriptor().getProperties().get("tasksExclusivity");

        JavaTaskDescriptor ctxFinTemplate = loadTemplate("context finalizer");
        JavaTaskDescriptor clientTemplate = loadTemplate("client task");
        JavaTaskDescriptor serverTemplate = loadTemplate("server task");

        if(exclusivity != null) {
            TaskExclusivity taskExclusivity = TaskExclusivity.valueOf(exclusivity);
            clientTemplate.setExclusivity(taskExclusivity);
            serverTemplate.setExclusivity(taskExclusivity);
        }

        TaskContextPlanBuilder tcBuilder = TaskContextPlanBuilder.planNew("tc_" + version);
        for (int i = 0; i < nrOfClients; i++) {
            JavaTaskDescriptor client = DeepCopy.copy(clientTemplate);
            tcBuilder.addTask(taskItem("client", client));
        }
        JavaTaskDescriptor server = DeepCopy.copy(serverTemplate);
        server.getProperties().put("version", version);
        server.getProperties().put("nrOfClients", "" + nrOfClients);
        tcBuilder.addTask(taskItem("server", server));
        JavaTaskDescriptor finalizer = DeepCopy.copy(ctxFinTemplate);
        finalizer.getProperties().put("version", version);
        tcBuilder.addFinalizerTask(taskItem("ctxfin", finalizer));
        return tcBuilder.build();
    }


    JavaTaskDescriptor loadTemplate(String name) {
        return (JavaTaskDescriptor) getBpkDescriptor()
                .getTaskDescriptorTemplates()
                .stream().filter(t -> t.getName().equals(name)).findFirst().get();
    }

    TaskPlanItem taskItem(String name, JavaTaskDescriptor desc) {
        return TaskPlanItemBuilder.planNew(name)
                .withTaskDescriptor(desc).build();
    }

}
