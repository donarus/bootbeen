package com.example.been;

import cz.cuni.mff.d3s.been.taskapi.messaging.service.result.model.Result;

import java.util.Map;

public class BenchmarkResult extends Result {
    private Map<String, AverageTimeResult> result;

    public BenchmarkResult() {

    }

    public BenchmarkResult(Map<String, AverageTimeResult> result) {
        this.result = result;
    }

    public Map<String, AverageTimeResult> getResult() {
        return result;
    }

    public void setResult(Map<String, AverageTimeResult> result) {
        this.result = result;
    }
}
