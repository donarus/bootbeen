package com.example.been;

import cz.cuni.mff.d3s.been.model.benchmark.BenchmarkPlan;
import cz.cuni.mff.d3s.been.taskapi.Task;
import cz.cuni.mff.d3s.been.taskapi.TaskException;
import cz.cuni.mff.d3s.been.taskapi.messaging.service.result.model.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class ContextFinalizerTask extends Task {
    private static final Logger LOG = LoggerFactory.getLogger(ContextFinalizerTask.class);

    @Override
    protected void startTask(String[] args) throws TaskException {
        BenchmarkPlan bp = getActualBenchmarkPlan();
        List<String> taskIds = bp.getRegularTaskIdsForContext(getTaskContextId());
        List<Double> averageTimeResults = new ArrayList<>();
        for (String taskId : taskIds) {
            List<Result> taskResults = getResults(taskId);
            for (Result taskResult : taskResults) {
                if (taskResult instanceof AverageTimeResult) {
                    LOG.info("TaskResult for task {} : {}", taskId, ((AverageTimeResult) taskResult).getAverage());
                    averageTimeResults.add(((AverageTimeResult) taskResult).getAverage());
                }
            }
        }
        final double average = averageTimeResults.stream().mapToDouble(x -> x).sum() / averageTimeResults.size();
        LOG.info("contextResult for context {} : {}", getTaskContextId(), average);
        storeResult(new AverageTimeResult(average));
        LOG.info("started context finalizing");
    }
}
