package com.example.been;

import cz.cuni.mff.d3s.been.taskapi.Task;
import cz.cuni.mff.d3s.been.taskapi.TaskException;
import cz.cuni.mff.d3s.been.taskapi.maven.MavenSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Spark;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Map;

import static java.lang.Integer.parseInt;

public class ServerTask extends Task {

    private static final Logger LOG = LoggerFactory.getLogger(ClientTask.class);

    @Override
    protected void startTask(String[] args) throws TaskException {
        try {
            Map<String, String> props = getTaskDescriptor().getProperties();
            String version = props.get("version");
            int nrOfClients = parseInt(props.get("nrOfClients"));
            String ip = InetAddress.getLocalHost().getHostAddress();
            for (File dep : sparkCoreDeps(version)) {
                Common.classLoadJar(dep);
            }
            Spark.setPort(4567);
            Spark.get("/getreq", (req, res) -> "Hello World");
            sleepQuietely(1000); // give server some time to warp-up
            setSharedProperty("serverUrl", "http://" + ip + ":4567/getreq");
            checkpointReached("SERVER_STARTED");
            waitForLatch("WARMED_UP", nrOfClients);
            checkpointReached("TEST_STARTED");
            LOG.debug("waiting for latch 'TEST_FINISHED'");
            waitForLatch("TEST_FINISHED", nrOfClients);
            LOG.debug("latch reached 'TEST_FINISHED'");
            Spark.stop();
        } catch (Exception e) {
            throw new TaskException(e);
        }
    }

    private List<File> sparkCoreDeps(String version) throws Exception {
        return MavenSupport.forTask(this).setUseSystemRepo(true).downloadDependencies("com.sparkjava", "spark-core", version);
    }

}
