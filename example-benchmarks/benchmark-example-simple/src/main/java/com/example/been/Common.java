package com.example.been;

import cz.cuni.mff.d3s.been.taskapi.TaskException;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;

public class Common {

    protected static final void classLoadJar(File file) throws TaskException {
        try {
            Method method = URLClassLoader.class.getDeclaredMethod("addURL", new Class[]{URL.class});
            method.setAccessible(true);
            method.invoke(ClassLoader.getSystemClassLoader(), file.toURI().toURL());
        } catch (Exception e) {
            throw new TaskException(String.format("Cannot load %s", file.toString()), e);
        }
    }

}
