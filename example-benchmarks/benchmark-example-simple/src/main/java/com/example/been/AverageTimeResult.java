package com.example.been;

import cz.cuni.mff.d3s.been.taskapi.messaging.service.result.model.Result;

public class AverageTimeResult extends Result {
    private double average;

    public AverageTimeResult() {}

    public AverageTimeResult(double average) {
        this.average = average;
    }

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }
}
