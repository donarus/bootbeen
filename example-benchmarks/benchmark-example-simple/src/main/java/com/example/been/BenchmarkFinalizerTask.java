package com.example.been;

import cz.cuni.mff.d3s.been.model.benchmark.BenchmarkPlan;
import cz.cuni.mff.d3s.been.model.benchmark.PlanItemState;
import cz.cuni.mff.d3s.been.model.benchmark.TaskContextPlanItem;
import cz.cuni.mff.d3s.been.taskapi.Task;
import cz.cuni.mff.d3s.been.taskapi.TaskException;
import cz.cuni.mff.d3s.been.taskapi.messaging.service.result.model.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BenchmarkFinalizerTask extends Task {
    private static final Logger LOG = LoggerFactory.getLogger(BenchmarkFinalizerTask.class);

    protected void startTask(String[] args) throws TaskException {
        BenchmarkPlan plan = getActualBenchmarkPlan();
        Map<String, AverageTimeResult> benchmarkResult = new HashMap<>();
        for (TaskContextPlanItem tcItem : plan.getTaskContextPlanItems()) {
            if (tcItem.getFinalizerTaskPlanItem() != null
                    && tcItem.getFinalizerTaskPlanItem().getTaskUuid() != null
                    && tcItem.getFinalizerTaskPlanItem().getState() == PlanItemState.FINISHED) {
                List<Result> taskResults = getResults(tcItem.getFinalizerTaskPlanItem().getTaskUuid());
                if (taskResults.size() != 1) {
                    LOG.warn("There should be only single result from the task context finalizer with id: " + tcItem.getTaskContextUuid());
                } else {
                    String version = tcItem.getFinalizerTaskPlanItem().getTaskDescriptor().getProperties().get("version");
                    AverageTimeResult contextresult = (AverageTimeResult) taskResults.get(0);
                    benchmarkResult.put(version, contextresult);
                }
            }
        }
        storeResult(new BenchmarkResult(benchmarkResult));
    }
}
