package com.example.been;

import cz.cuni.mff.d3s.been.taskapi.Task;
import cz.cuni.mff.d3s.been.taskapi.TaskException;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.DoubleSummaryStatistics;
import java.util.List;

public class ClientTask extends Task {

    private static final Logger LOG = LoggerFactory.getLogger(ClientTask.class);

    @Override
    protected void startTask(String[] args) throws TaskException {
        try {
            waitForCheckpoint("SERVER_STARTED");

            String url = getSharedProperty("serverUrl");

            LOG.info("url: " + url);

            for (int i = 0; i < 500; i++) {
                read(url);
            }
            latchCountDown("WARMED_UP");
            waitForCheckpoint("TEST_STARTED");
            List<Long> times = new ArrayList<>();
            for (int i = 0; i < 10000; i++) {
                times.add(read(url));
            }
            latchCountDown("TEST_FINISHED");
            DoubleSummaryStatistics dss = new DoubleSummaryStatistics();
            times.forEach(dss::accept);
            storeResult(new AverageTimeResult(dss.getAverage()));
        } catch (Exception e) {
            throw new TaskException(e);
        }
    }

    private long read(String url) throws IOException {
        long start = System.nanoTime();
        try (InputStream in = new URL(url).openStream();) {
            IOUtils.toString(in, "UTF8");
        }
        return System.nanoTime() - start;
    }

}
