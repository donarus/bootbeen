package cz.cuni.mff.d3s.been.rest.messages;

import cz.cuni.mff.d3s.been.rest.model.SimpleNodeInfo;

public class ClusterNodeAddedMessage implements AbstractMessage {
    private SimpleNodeInfo node;

    public ClusterNodeAddedMessage() {
    }

    public ClusterNodeAddedMessage(SimpleNodeInfo node) {
        this.node = node;
    }

    public SimpleNodeInfo getNode() {
        return node;
    }

    public void setNode(SimpleNodeInfo node) {
        this.node = node;
    }

    @Override
    public final String getType() {
        return "NODE_ADDED";
    }
}
