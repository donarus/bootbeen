package cz.cuni.mff.d3s.been.rest.controllers;

import cz.cuni.mff.d3s.been.rest.response.ResponseWithBody;
import cz.cuni.mff.d3s.been.rest.response.ResponseWithListBody;
import cz.cuni.mff.d3s.been.api.clients.SoftwareRepositoryClient;
import cz.cuni.mff.d3s.been.api.clients.SoftwareRepositoryException;
import cz.cuni.mff.d3s.been.model.bpk.Bpk;
import cz.cuni.mff.d3s.been.model.bpk.BpkId;
import cz.cuni.mff.d3s.been.model.td.TaskDescriptor;
import cz.cuni.mff.d3s.been.model.td.TaskType;
import cz.cuni.mff.d3s.been.utils.bpk.BpkException;
import cz.cuni.mff.d3s.been.utils.bpk.BpkLoader;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.stream.Collectors;

@RestController
@Profile("been-rest")
@RequestMapping(value = "/public/swr")
public final class SWRController {

    private static final Logger LOG = LoggerFactory.getLogger(SWRController.class);

    @Autowired
    private SoftwareRepositoryClient client;

    @CrossOrigin
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<ResponseWithListBody<BpkId>> list() {
        ResponseWithListBody<BpkId> response = new ResponseWithListBody<>();

        try {
            response.setData(client.list());
        } catch (SoftwareRepositoryException e) {
            response.addErrorMessage("Unable to list bpks");
            LOG.error("Unable to list bpks", e);
        }

        return response.toResponseEntity();
    }


    @CrossOrigin
    @RequestMapping(value = "/delete", method = RequestMethod.DELETE)
    public ResponseEntity<ResponseWithBody<Boolean>> delete(
            @RequestParam("groupId") String groupId,
            @RequestParam("artifactId") String artifactId,
            @RequestParam("version") String version
    ) {
        ResponseWithBody<Boolean> response = new ResponseWithBody<>();

        try {
            client.delete(new BpkId(groupId, artifactId, version));
            response.setData(true);
        } catch (SoftwareRepositoryException e) {
            response.setResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            response.addErrorMessage("Been API was unable to process 'delete BPK' operation.", e);
            LOG.error("Unable to delete bpk", e);
        }

        return response.toResponseEntity();
    }


    @CrossOrigin
    @RequestMapping(value = "/upload", method = {RequestMethod.POST, RequestMethod.PUT})
    public ResponseEntity<ResponseWithBody<BpkId>> upload(
            @RequestParam MultipartFile bpk
    ) {
        ResponseWithBody<BpkId> response = new ResponseWithBody<>();

        boolean ok = true;

        File fileFromUser = null;
        try {
            fileFromUser = File.createTempFile("uploaded_", bpk.getName());
        } catch (IOException e) {
            ok = false;
            String msg = "Unable to create temporary file as a storage for uploaded file.";
            LOG.error(msg, e);
            response.setResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            response.addErrorMessage(msg, e);
        }

        if (ok) {
            try {
                FileUtils.copyInputStreamToFile(bpk.getInputStream(), fileFromUser);
            } catch (IOException e) {
                ok = false;
                String msg = "Unable to copy file from user to temporary storage file.";
                LOG.error(msg, e);
                response.setResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR);
                response.addErrorMessage(msg, e);
            }
        }

        if (ok) {
            try {
                client.upload(fileFromUser);

                try {
                    response.setData(BpkLoader.load(fileFromUser).getBpkId());
                } catch (BpkException e) {
                    response.addErrorMessage("Unexpected exception while parsing Bpk file name", e);
                }

                response.addInfoMessage("Bpk successfully uploaded.");
            } catch (SoftwareRepositoryException e) {
                response.setResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR);
                response.addErrorMessage("Been API was unable to process 'upload BPK' operation. ", e);
                LOG.error("Unable to upload bpk to software repository.", e);
            }
        }

        fileFromUser.delete();
        return response.toResponseEntity();
    }

    @CrossOrigin
    @RequestMapping(value = "/getTaskDescriptor", method = RequestMethod.GET)
    public ResponseEntity<ResponseWithBody<TaskDescriptor>> getTaskDescriptor(
            @RequestParam("groupId") String groupId,
            @RequestParam("artifactId") String artifactId,
            @RequestParam("version") String version,
            @RequestParam(value = "taskDescriptorName", required = false, defaultValue = "default") String taskDescriptorName
    ) {
        ResponseWithBody<TaskDescriptor> response = new ResponseWithBody<>();

        BpkId bpkId = new BpkId(groupId, artifactId, version);
        TaskDescriptor td = null;
        try {
            td = client.getTaskDescriptor(bpkId, taskDescriptorName);
            response.setData(td);
        } catch (SoftwareRepositoryException e) {
            response.setResponseStatus(HttpStatus.NOT_FOUND);
            response.addErrorMessage("Task descriptor with id '" + bpkId.toString() + "' and name '" + taskDescriptorName + "' was not found", e);
            LOG.error("Unable to get task descriptor", e);
        }

        return response.toResponseEntity();
    }


    @CrossOrigin
    @RequestMapping(value = "/getBpkDescriptor", method = RequestMethod.GET)
    public ResponseEntity<ResponseWithBody<Bpk>> getBpkDescriptor(
            @RequestParam("groupId") String groupId,
            @RequestParam("artifactId") String artifactId,
            @RequestParam("version") String version) {
        ResponseWithBody<Bpk> response = new ResponseWithBody<>();

        BpkId bpkId = new BpkId(groupId, artifactId, version);
        Bpk bd = null;
        try {
            bd = client.getBpkDescriptor(bpkId);
            bd.setTaskDescriptorTemplates(bd.getTaskDescriptorTemplates().stream().filter(t -> t.getType() == TaskType.BENCHMARK_GENERATOR).collect(Collectors.toList()));
            response.setData(bd);
        } catch (SoftwareRepositoryException e) {
            LOG.error("unable to get bpkdescriptor", e);
            response.setResponseStatus(HttpStatus.NOT_FOUND);
            response.addErrorMessage("Bpk descriptor with id " + bpkId.toString() + " was not found", e);
        }

        return response.toResponseEntity();
    }

    @CrossOrigin
    @RequestMapping(value = "/get", method = RequestMethod.GET)
    public ResponseEntity get(@RequestParam("groupId") String groupId,
                              @RequestParam("artifactId") String artifactId,
                              @RequestParam("version") String version,
                              HttpServletResponse httpServletResponse) {
        ResponseWithBody<Boolean> errorResponse = new ResponseWithBody<>();

        boolean ok = true;

        BpkId bpkId = new BpkId(groupId, artifactId, version);
        File f = null;
        try {
            f = client.get(bpkId);
        } catch (SoftwareRepositoryException e) {
            e.printStackTrace();
        }
        if (f == null || !f.exists()) {
            ok = false;
            errorResponse.setResponseStatus(HttpStatus.NOT_FOUND);
            errorResponse.addErrorMessage("Bpk with id " + bpkId.toString() + " was not found");
        }

        if (ok) {
            try {
                IOUtils.copy(new FileInputStream(f), httpServletResponse.getOutputStream());
            } catch (IOException e) {
                ok = false;
                String msg = "Unable to send bpk file to user.";
                LOG.error(msg, e);
                errorResponse.setResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR);
                errorResponse.addErrorMessage(msg, e);
            }
        }

        if (ok) {
            // RFC2046 says "The "octet-stream" subtype is used to
            // indicate that a body contains arbitrary binary data"
            // and "The recommended action for an implementation that
            // receives an "application/octet-stream" entity is to
            // simply offer to put the data in a file[...]"
            try {
                String fileName = URLEncoder.encode(artifactId + "-" + version + ".bpk", "UTF-8");
                httpServletResponse.setContentType("application/octet-stream");
                httpServletResponse.setHeader("Content-disposition", "attachment; filename=" + fileName);
            } catch (UnsupportedEncodingException e) {
                ok = false;
                String msg = "Internal server error.";
                LOG.error(msg, e);
                errorResponse.setResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR);
                errorResponse.addErrorMessage(msg, e);
            }
        }

        if (ok) {
            return ResponseEntity.ok(null);
        } else {
            errorResponse.setData(false);
            return errorResponse.toResponseEntity();
        }
    }
}
