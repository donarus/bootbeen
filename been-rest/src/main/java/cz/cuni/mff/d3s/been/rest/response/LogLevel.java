package cz.cuni.mff.d3s.been.rest.response;

public enum LogLevel {
    INFO, WARN, ERROR
}
