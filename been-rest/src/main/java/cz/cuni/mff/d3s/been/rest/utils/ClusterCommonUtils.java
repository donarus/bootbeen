package cz.cuni.mff.d3s.been.rest.utils;

import cz.cuni.mff.d3s.been.rest.model.FullNodeInfo;
import cz.cuni.mff.d3s.been.rest.model.FullServiceInfo;
import cz.cuni.mff.d3s.been.rest.model.SimpleNodeInfo;
import cz.cuni.mff.d3s.been.rest.model.SimpleServiceInfo;
import cz.cuni.mff.d3s.been.api.common.BeenNodes;
import cz.cuni.mff.d3s.been.api.common.BeenServices;
import cz.cuni.mff.d3s.been.model.node.NodeInfo;
import cz.cuni.mff.d3s.been.model.service.ServiceInfo;
import cz.cuni.mff.d3s.been.utils.json.JsonException;
import cz.cuni.mff.d3s.been.utils.json.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

@Service("BeenApiClusterCommonUtils")
@Profile("been-rest")
public class ClusterCommonUtils {

    @Autowired
    private BeenNodes beenNodes;

    @Autowired
    private BeenServices beenServices;

    public List<SimpleNodeInfo> getSimpleNodeInfos() {
        return beenNodes.list().stream().map(this::getSimpleNodeInfo).collect(toList());
    }

    public SimpleNodeInfo getSimpleNodeInfo(NodeInfo nodeInfo) {
        Map<String, String> services = new HashMap<>();
        for (ServiceInfo serviceInfo : beenServices.listByNodeUuid(nodeInfo.getUuid())) {
            services.put(serviceInfo.getServiceUUID(), serviceInfo.getName());
        }
        return new SimpleNodeInfo(nodeInfo.getUuid(), nodeInfo.getStartupTime(), services);
    }

    public List<SimpleServiceInfo> getSimpleServiceInfos() {
        return beenServices.list().stream().map(ser -> {
            SimpleServiceInfo sSer = new SimpleServiceInfo(ser.getServiceUUID(), ser.getNodeUUID());
            sSer.setName(ser.getName());
            sSer.setVersion(ser.getVersion());
            sSer.setVendor(ser.getVendor());
            return sSer;
        }).collect(toList());
    }

    public FullNodeInfo getFullNodeInfo(NodeInfo nodeInfo) {
        SimpleNodeInfo simpleNodeInfo = getSimpleNodeInfo(nodeInfo);
        FullNodeInfo fullNodeInfo = new FullNodeInfo(simpleNodeInfo.getNodeUuid(), simpleNodeInfo.getStartupTime(),
                simpleNodeInfo.getServices(), asList(nodeInfo.getIpAddresses()), nodeInfo.getHostname());

        try {
            fullNodeInfo.setRawNode(JsonUtils.writeToPrettyString(nodeInfo));
        } catch (JsonException e) {
            //ignore
        }
        return fullNodeInfo;
    }

    public FullServiceInfo getFullServiceInfo(ServiceInfo serviceInfo) {
        FullServiceInfo fullServiceInfo = new FullServiceInfo(serviceInfo.getServiceUUID(), serviceInfo.getNodeUUID());
        fullServiceInfo.setName(serviceInfo.getName());
        fullServiceInfo.setVendor(serviceInfo.getVendor());
        fullServiceInfo.setVersion(serviceInfo.getVersion());
        fullServiceInfo.setDescription(serviceInfo.getDescription());
        try {
            fullServiceInfo.setRawServiceInfo(JsonUtils.writeToPrettyString(serviceInfo));
        } catch (JsonException e) {
            //ignore
        }
        return fullServiceInfo;
    }
}
