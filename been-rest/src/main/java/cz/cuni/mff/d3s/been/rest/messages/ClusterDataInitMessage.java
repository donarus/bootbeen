package cz.cuni.mff.d3s.been.rest.messages;

import cz.cuni.mff.d3s.been.rest.model.SimpleNodeInfo;
import cz.cuni.mff.d3s.been.rest.model.SimpleServiceInfo;

import java.util.List;

public class ClusterDataInitMessage implements AbstractMessage {

    private List<SimpleNodeInfo> nodes;
    private List<SimpleServiceInfo> services;

    public ClusterDataInitMessage() {
    }

    public ClusterDataInitMessage(List<SimpleNodeInfo> nodes, List<SimpleServiceInfo> services) {
        this.nodes = nodes;
        this.services = services;
    }

    @Override
    public String getType() {
        return "DATA_INIT";
    }

    public List<SimpleNodeInfo> getNodes() {
        return nodes;
    }

    public void setNodes(List<SimpleNodeInfo> nodes) {
        this.nodes = nodes;
    }

    public List<SimpleServiceInfo> getServices() {
        return services;
    }

    public void setServices(List<SimpleServiceInfo> services) {
        this.services = services;
    }
}
