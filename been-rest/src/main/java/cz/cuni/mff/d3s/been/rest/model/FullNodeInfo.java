package cz.cuni.mff.d3s.been.rest.model;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class FullNodeInfo extends SimpleNodeInfo {

    private List<String> ipAddresses;

    private String hostname;
    private String rawNode;

    public FullNodeInfo() {
    }

    public FullNodeInfo(String nodeUuid, Date startupTime, Map<String, String> services, List<String> ipAddresses, String hostname) {
        super(nodeUuid, startupTime, services);
        this.ipAddresses = ipAddresses;
        this.hostname = hostname;
    }

    public List<String> getIpAddresses() {
        return ipAddresses;
    }

    public void setIpAddresses(List<String> ipAddresses) {
        this.ipAddresses = ipAddresses;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public void setRawNode(String rawNode) {
        this.rawNode = rawNode;
    }

    public String getRawNode() {
        return rawNode;
    }
}
