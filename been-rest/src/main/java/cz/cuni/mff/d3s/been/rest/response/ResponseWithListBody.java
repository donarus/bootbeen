package cz.cuni.mff.d3s.been.rest.response;

import org.springframework.http.ResponseEntity;

import java.util.List;

public class ResponseWithListBody<T> extends AbstractResponse<ResponseWithListBody<T>> {
    private List<T> data;

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    @Override
    public ResponseEntity<ResponseWithListBody<T>> toResponseEntity() {
        return ResponseEntity.status(status).body(this);
    }
}
