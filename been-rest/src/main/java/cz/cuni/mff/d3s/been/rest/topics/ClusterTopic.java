package cz.cuni.mff.d3s.been.rest.topics;

import cz.cuni.mff.d3s.been.rest.BeenWSSupport;
import cz.cuni.mff.d3s.been.rest.TopicDriver;
import cz.cuni.mff.d3s.been.rest.messages.AbstractMessage;
import cz.cuni.mff.d3s.been.rest.messages.ClusterNodeAddedMessage;
import cz.cuni.mff.d3s.been.rest.messages.ClusterNodeRemovedMessage;
import cz.cuni.mff.d3s.been.api.common.BeenNodes;
import cz.cuni.mff.d3s.been.rest.utils.ClusterCommonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
@Profile("been-rest")
public class ClusterTopic {

    private static final String TOPIC_NAME = "/topic/cluster";

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @Autowired
    private BeenNodes beenNodes;

    @Autowired
    private ClusterCommonUtils clusterCommonUtils;

    @Autowired
    private BeenWSSupport wsSupport;

    @PostConstruct
    public synchronized void startTopic() {
        TopicDriver driver = createDriver();
        wsSupport.registerTopicDriver(TOPIC_NAME, driver);
    }

    private TopicDriver createDriver() {
        return new TopicDriver() {
            private volatile boolean isActive = false;
            private volatile String addedListenerId;
            private volatile String removedListenerId;

            @Override
            public synchronized void activate() {
                if (isActive()) {
                    return;
                }
                isActive = true;

                removedListenerId = beenNodes.addNodeRemovedListener(nodeInfo -> send(new ClusterNodeRemovedMessage(nodeInfo.getUuid())));
                addedListenerId = beenNodes.addNodeAddedListener(nodeInfo -> send(new ClusterNodeAddedMessage(clusterCommonUtils.getSimpleNodeInfo(nodeInfo))));
            }

            @Override
            public synchronized void deactivate() {
                if (!isActive()) {
                    return;
                }
                isActive = false;

                beenNodes.removeNodeAddedListener(addedListenerId);
                beenNodes.removeNodeRemovedListener(removedListenerId);

                addedListenerId = null;
                removedListenerId = null;
            }

            @Override
            public synchronized boolean isActive() {
                return isActive;
            }
        };
    }

    void send(AbstractMessage event) {
        messagingTemplate.convertAndSend(TOPIC_NAME, event);
    }

}
