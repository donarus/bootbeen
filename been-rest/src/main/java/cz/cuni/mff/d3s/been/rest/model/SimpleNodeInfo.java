package cz.cuni.mff.d3s.been.rest.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

public class SimpleNodeInfo implements Serializable {
    private String nodeUuid;
    private Date startupTime;
    private Map<String, String> services;

    public SimpleNodeInfo() {
    }

    public SimpleNodeInfo(String nodeUuid, Date startupTime, Map<String, String> services) {
        this.nodeUuid = nodeUuid;
        this.startupTime = startupTime;
        this.services = services;
    }

    public String getNodeUuid() {
        return nodeUuid;
    }

    public void setNodeUuid(String nodeUuid) {
        this.nodeUuid = nodeUuid;
    }

    public Date getStartupTime() {
        return startupTime;
    }

    public void setStartupTime(Date startupTime) {
        this.startupTime = startupTime;
    }

    public Map<String, String> getServices() {
        return services;
    }

    public void setServices(Map<String, String> services) {
        this.services = services;
    }
}
