package cz.cuni.mff.d3s.been.rest.model;

import java.io.Serializable;

public class SimpleServiceInfo implements Serializable {
    private String serviceUuid;
    private String nodeUuid;
    private String name;
    private String vendor;
    private String version;

    public SimpleServiceInfo() {
    }

    public SimpleServiceInfo(String serviceUuid, String nodeUuid) {
        this.serviceUuid = serviceUuid;
        this.nodeUuid = nodeUuid;
    }

    public String getServiceUuid() {
        return serviceUuid;
    }

    public void setServiceUuid(String serviceUuid) {
        this.serviceUuid = serviceUuid;
    }

    public String getNodeUuid() {
        return nodeUuid;
    }

    public void setNodeUuid(String nodeUuid) {
        this.nodeUuid = nodeUuid;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setVendor(String vendor) {
        this.vendor = vendor;
    }

    public String getVendor() {
        return vendor;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }
}
