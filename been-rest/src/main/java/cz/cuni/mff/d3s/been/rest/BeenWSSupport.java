package cz.cuni.mff.d3s.been.rest;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.messaging.AbstractSubProtocolEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import org.springframework.web.socket.messaging.SessionSubscribeEvent;
import org.springframework.web.socket.messaging.SessionUnsubscribeEvent;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Service
@Profile("been-rest")
public class BeenWSSupport implements ApplicationListener<AbstractSubProtocolEvent> {
    private static final Logger LOG = LoggerFactory.getLogger(BeenWSSupport.class);

    private static final String TOPIC_NAME_HEADER_KEY = "simpDestination";
    private static final String SESSION_ID_HEADER_KEY = "simpSessionId";
    private static final String SUBSCRIBTION_ID_HEADER_KEY = "simpSubscriptionId";

    private final TopicSubscriptions topicSubscriptions = new TopicSubscriptions();

    public boolean isTopicUsed(String topicName) {
        return topicSubscriptions.hasSubscriptions(topicName);
    }

    private Map<String, TopicDriver> topicDrivers = Collections.synchronizedMap(new HashMap<>());

    @Override
    public void onApplicationEvent(AbstractSubProtocolEvent rawEvent) {
        MessageHeaders headers = rawEvent.getMessage().getHeaders();
        String topicName = (String) headers.getOrDefault(TOPIC_NAME_HEADER_KEY, null);
        String subscriptionId = (String) headers.getOrDefault(SUBSCRIBTION_ID_HEADER_KEY, null);
        String sessionId = (String) headers.getOrDefault(SESSION_ID_HEADER_KEY, null);

        if (rawEvent instanceof SessionSubscribeEvent) {
            topicSubscriptions.registerSubscription(topicName, sessionId, subscriptionId);
            LOG.debug("Session [{}] subscribed to topic [{}] over websocket. Actual subscriptions: {}", sessionId, topicName, topicSubscriptions);
            TopicDriver driver = topicDrivers.get(topicName);
            if (driver != null && !driver.isActive()) {
                driver.activate();
            }
        } else if (rawEvent instanceof SessionUnsubscribeEvent) {
            topicSubscriptions.unregisterSubscription(sessionId, subscriptionId);
            LOG.debug("Session [{}] unsubscribed from topic [{}] over websocket. Actual subscriptions: {}", sessionId, topicName, topicSubscriptions);
            TopicDriver driver = topicDrivers.get(topicName);
            if (driver != null && driver.isActive()) {
                driver.deactivate();
            }
        } else if (rawEvent instanceof SessionDisconnectEvent) {
            topicSubscriptions.unregisterSession(sessionId);
            LOG.debug("Session [{}] disconnected. Actual subscriptions: {}", sessionId, topicSubscriptions);
            Set<String> liveTopicNames = topicSubscriptions.liveTopicNames();
            for (Map.Entry<String, TopicDriver> entry : topicDrivers.entrySet()) {
                String name = entry.getKey();
                TopicDriver driver = entry.getValue();
                if (!liveTopicNames.contains(name) && driver.isActive()) {
                    driver.deactivate();
                }
            }
        }
    }

    public void registerTopicDriver(String topicName, TopicDriver topicDriver) {
        topicDrivers.put(topicName, topicDriver);
    }

    private static final class TopicSubscriptions {
        private final Map<String, Map<String, String>> topicSubscriptions = new ConcurrentHashMap<>();

        public boolean hasSubscriptions(String topicName) {
            return !topicSubscriptions.getOrDefault(topicName, Collections.emptyMap()).isEmpty();
        }

        public synchronized void registerSubscription(String topicName, String sessionId, String subscriptionId) {
            topicSubscriptions.computeIfAbsent(topicName, (k) -> new HashMap<>()).put(sessionId, subscriptionId);
        }

        public synchronized void unregisterSubscription(String sessionId, String subscriptionId) {
            for (Map<String, String> topic : topicSubscriptions.values()) {
                topic.remove(sessionId, subscriptionId);
            }
            cleanup();
        }

        public synchronized void unregisterSession(String sessionId) {
            for (Map<String, String> topic : topicSubscriptions.values()) {
                topic.remove(sessionId);
            }
            cleanup();
        }

        public synchronized Set<String> liveTopicNames() {
            return topicSubscriptions.keySet();
        }

        private synchronized void cleanup() {
            for (String key : topicSubscriptions.keySet()) {
                if (topicSubscriptions.get(key).isEmpty()) {
                    topicSubscriptions.remove(key);
                }
            }
        }

        @Override
        public String toString() {
            return ToStringBuilder.reflectionToString(this);
        }
    }
}
