package cz.cuni.mff.d3s.been.rest;

public interface TopicDriver {
    void activate();

    void deactivate();

    boolean isActive();
}
