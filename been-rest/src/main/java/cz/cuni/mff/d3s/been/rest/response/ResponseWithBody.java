package cz.cuni.mff.d3s.been.rest.response;

import org.springframework.http.ResponseEntity;

public class ResponseWithBody<T> extends AbstractResponse<ResponseWithBody<T>> {

    private T data;

    public ResponseWithBody() {
    }

    public ResponseWithBody(T data) {
        setData(data);
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public ResponseEntity<ResponseWithBody<T>> toResponseEntity() {
        return ResponseEntity.status(status).body(this);
    }
}
