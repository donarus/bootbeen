package cz.cuni.mff.d3s.been.rest.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cuni.mff.d3s.been.api.clients.HostRuntimeClient;
import cz.cuni.mff.d3s.been.api.clients.LogStorageClient;
import cz.cuni.mff.d3s.been.api.clients.ResultsStorageClient;
import cz.cuni.mff.d3s.been.model.benchmark.Benchmark;
import cz.cuni.mff.d3s.been.model.benchmark.TaskContextPlanItem;
import cz.cuni.mff.d3s.been.model.benchmark.TaskPlanItem;
import cz.cuni.mff.d3s.been.model.bpk.BpkId;
import cz.cuni.mff.d3s.been.model.log.LogMessage;
import cz.cuni.mff.d3s.been.model.result.SerializedResult;
import cz.cuni.mff.d3s.been.model.task.Task;
import cz.cuni.mff.d3s.been.model.td.TaskDescriptor;
import cz.cuni.mff.d3s.been.rest.model.SimpleBenchmarkInfo;
import cz.cuni.mff.d3s.been.rest.model.SimpleTaskContextInfo;
import cz.cuni.mff.d3s.been.rest.model.SimpleTaskInfo;
import cz.cuni.mff.d3s.been.rest.response.ResponseWithBody;
import cz.cuni.mff.d3s.been.rest.response.ResponseWithListBody;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@Profile("been-rest")
@RequestMapping(value = "/public/tasks")
public final class TasksController {

    @Autowired
    private HostRuntimeClient hostRuntimeClient;

    @Autowired
    private LogStorageClient logClient;

    @Autowired
    private ResultsStorageClient resultsStorageClient;

    @CrossOrigin
    @RequestMapping(value = "/run", method = RequestMethod.POST)
    public ResponseEntity<ResponseWithBody<Boolean>> runTask(@RequestBody RunTaskRequest runTaskRequest) {
        ResponseWithBody<Boolean> response = new ResponseWithBody<Boolean>();

        hostRuntimeClient.runTask(runTaskRequest.getBpkId(), runTaskRequest.getTaskDescriptor());
        response.setData(true);
        response.addInfoMessage("New benchmark has been initiated");

        return response.toResponseEntity();
    }

    @CrossOrigin
    @RequestMapping(value = "/killBenchmark", method = RequestMethod.DELETE)
    public ResponseEntity<ResponseWithBody<Boolean>> killBenchmark(@RequestParam("benchmarkId") String benchmarkUuid) throws IOException {
        ResponseWithBody<Boolean> response = new ResponseWithBody<Boolean>();

        hostRuntimeClient.killBenchmark(benchmarkUuid);
        response.setData(true);

        return response.toResponseEntity();
    }

    @CrossOrigin
    @RequestMapping(value = "/killTaskContext", method = RequestMethod.DELETE)
    public ResponseEntity<ResponseWithBody<Boolean>> killTaskContext(@RequestParam("taskContextId") String taskContextId) throws IOException {
        ResponseWithBody<Boolean> response = new ResponseWithBody<Boolean>();

        hostRuntimeClient.killTaskContext(taskContextId);
        response.setData(true);

        return response.toResponseEntity();
    }


    @CrossOrigin
    @RequestMapping(value = "/kill", method = RequestMethod.DELETE)
    public ResponseEntity<ResponseWithBody<Boolean>> killTask(@RequestParam("taskId") String taskUuid) throws IOException {
        ResponseWithBody<Boolean> response = new ResponseWithBody<Boolean>();

        hostRuntimeClient.killTask(taskUuid);
        response.setData(true);

        return response.toResponseEntity();
    }

    @CrossOrigin
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ResponseEntity<ResponseWithListBody<Task>> list() throws IOException {
        ResponseWithListBody<Task> response = new ResponseWithListBody<>();

        response.setData(hostRuntimeClient.listTasks());

        return response.toResponseEntity();
    }

    @CrossOrigin
    @RequestMapping(value = "/listBenchmarks", method = RequestMethod.GET)
    public ResponseEntity<ResponseWithListBody<SimpleBenchmarkInfo>> listBenchmarks() throws IOException {
        ResponseWithListBody<SimpleBenchmarkInfo> response = new ResponseWithListBody<>();

        List<Benchmark> benchmarks = hostRuntimeClient.listBenchmarks(false);
        List<SimpleBenchmarkInfo> simpleBenchmarkInfos = benchmarks.stream().map(b ->
                new SimpleBenchmarkInfo(b.getBenchmarkUuid(), b.getBenchmarkName(), b.getBpkId(), b.getBenchmarkState(), b.getStart(), b.getEnd())
        ).collect(Collectors.toList());
        response.setData(simpleBenchmarkInfos);

        return response.toResponseEntity();
    }

    @CrossOrigin
    @RequestMapping(value = "/getBenchmarkDetail", method = RequestMethod.GET)
    public ResponseEntity<ResponseWithBody<SimpleBenchmarkInfo>> listTaskContexts(@RequestParam("benchmarkId") String benchmarkUuid) throws IOException {
        ResponseWithBody<SimpleBenchmarkInfo> response = new ResponseWithBody<>();

        Benchmark benchmark = hostRuntimeClient.getBenchmarkByUuid(benchmarkUuid);
        SimpleBenchmarkInfo info = new SimpleBenchmarkInfo(benchmark.getBenchmarkUuid(), benchmark.getBenchmarkName(),
                benchmark.getBpkId(), benchmark.getBenchmarkState(), benchmark.getStart(), benchmark.getEnd());
        List<SimpleTaskContextInfo> taskContexts = new ArrayList<>();
        if (benchmark.getBenchmarkPlan() != null && benchmark.getBenchmarkPlan().getTaskContextPlanItems() != null) {
            for (TaskContextPlanItem planItem : benchmark.getBenchmarkPlan().getTaskContextPlanItems()) {
                SimpleTaskContextInfo contextInfo = new SimpleTaskContextInfo(planItem.getTaskContextUuid(), planItem.getName(), planItem.getState());
                List<SimpleTaskInfo> tasks = new ArrayList<>();
                for (TaskPlanItem taskPlanItem : planItem.getTasksPlanItems()) {
                    Task task = null;
                    if (taskPlanItem.getTaskUuid() != null) {
                        task = hostRuntimeClient.getTaskByUuid(taskPlanItem.getTaskUuid());
                    }
                    SimpleTaskInfo taskInfo = new SimpleTaskInfo(taskPlanItem.getTaskUuid(), taskPlanItem.getName(),
                            (task != null ? task.getTaskState() : null), (task != null ? task.getTaskStartTime() : null), (task != null ? task.getTaskEndTime() : null));
                    tasks.add(taskInfo);
                }
                if (planItem.getFinalizerTaskPlanItem() != null) {
                    TaskPlanItem finalizerPlan = planItem.getFinalizerTaskPlanItem();
                    Task task = null;
                    if (finalizerPlan.getTaskUuid() != null) {
                        task = hostRuntimeClient.getTaskByUuid(finalizerPlan.getTaskUuid());
                    }
                    SimpleTaskInfo finalizer = new SimpleTaskInfo(finalizerPlan.getTaskUuid(), finalizerPlan.getName(),
                            (task != null ? task.getTaskState() : null), (task != null ? task.getTaskStartTime() : null), (task != null ? task.getTaskEndTime() : null));
                    contextInfo.setFinalizerTask(finalizer);
                }
                contextInfo.setTasks(tasks);
                taskContexts.add(contextInfo);
            }
            info.setTaskContexts(taskContexts);
        }
        if (benchmark.getBenchmarkPlan() != null && benchmark.getBenchmarkPlan().getFinalizerTaskPlanItem() != null) {
            TaskPlanItem finalizerPlan = benchmark.getBenchmarkPlan().getFinalizerTaskPlanItem();
            Task task = null;
            if (finalizerPlan.getTaskUuid() != null) {
                task = hostRuntimeClient.getTaskByUuid(finalizerPlan.getTaskUuid());
            }
            SimpleTaskInfo finalizer = new SimpleTaskInfo(finalizerPlan.getTaskUuid(), finalizerPlan.getName(),
                    (task != null ? task.getTaskState() : null), (task != null ? task.getTaskStartTime() : null), (task != null ? task.getTaskEndTime() : null));
            info.setFinalizerTask(finalizer);
        }

        Task generatorTask = hostRuntimeClient.getTaskByUuid(benchmark.getGeneratorTaskUuid());

        SimpleTaskInfo generator = new SimpleTaskInfo(benchmark.getGeneratorTaskUuid(),
                generatorTask.getTaskDescriptor().getName(), generatorTask.getTaskState(), generatorTask.getTaskStartTime(), generatorTask.getTaskEndTime());
        info.setGeneratorTask(generator);

        response.setData(info);

        return response.toResponseEntity();
    }

    @CrossOrigin
    @RequestMapping(value = "/detail", method = RequestMethod.GET)
    public ResponseEntity<ResponseWithBody<Task>> detail(@RequestParam("taskId") String taskUuid) throws IOException {
        ResponseWithBody<Task> response = new ResponseWithBody<>();

        Task task = hostRuntimeClient.getTaskByUuid(taskUuid);
        response.setData(task);

        return response.toResponseEntity();
    }

    @CrossOrigin
    @RequestMapping(value = "/logs", method = RequestMethod.GET)
    public ResponseEntity<ResponseWithListBody<LogMessage>> logs(@RequestParam("taskId") String taskUuid) throws IOException {
        ResponseWithListBody<LogMessage> response = new ResponseWithListBody<>();


        List<LogMessage> logs = logClient.getLogsForTask(taskUuid);
        response.setData(logs);

        return response.toResponseEntity();
    }

    @CrossOrigin
    @RequestMapping(value = "/getResults", method = RequestMethod.GET)
    public ResponseEntity getResults(@RequestParam("taskId") String taskId, HttpServletResponse httpServletResponse) {
        ResponseWithBody<Boolean> errorResponse = new ResponseWithBody<>();

        boolean ok = true;

        List<SerializedResult> results = resultsStorageClient.getResultsForTask(taskId);
        String[] rawResults = results.stream().map(r -> r.getResultJson()).toArray(s -> new String[s]);

        if (ok) {
            try {
                String res = "[" + String.join(", ", rawResults) + "]";
                ObjectMapper objectMapper = new ObjectMapper();
                Object json = objectMapper.readValue(res, Object.class);
                String formatted = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);

                IOUtils.write(formatted, httpServletResponse.getOutputStream(), "UTF-8");
            } catch (IOException e) {
                ok = false;
                String msg = "Unable to send results file to user.";
                errorResponse.setResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR);
                errorResponse.addErrorMessage(msg, e);
            }
        }

        if (ok) {
            // RFC2046 says "The "octet-stream" subtype is used to
            // indicate that a body contains arbitrary binary data"
            // and "The recommended action for an implementation that
            // receives an "application/octet-stream" entity is to
            // simply offer to put the data in a file[...]"
            try {
                String fileName = URLEncoder.encode(taskId + ".json", "UTF-8");
                httpServletResponse.setContentType("application/octet-stream");
                httpServletResponse.setHeader("Content-disposition", "attachment; filename=" + fileName);
            } catch (UnsupportedEncodingException e) {
                ok = false;
                String msg = "Internal server error.";
                errorResponse.setResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR);
                errorResponse.addErrorMessage(msg, e);
            }
        }

        if (ok) {
            return ResponseEntity.ok(null);
        } else {
            errorResponse.setData(false);
            return errorResponse.toResponseEntity();
        }
    }


    public static class RunTaskRequest {
        private BpkId bpkId;
        private TaskDescriptor taskDescriptor;

        public BpkId getBpkId() {
            return bpkId;
        }

        public void setBpkId(BpkId bpkId) {
            this.bpkId = bpkId;
        }

        public TaskDescriptor getTaskDescriptor() {
            return taskDescriptor;
        }

        public void setTaskDescriptor(TaskDescriptor taskDescriptor) {
            this.taskDescriptor = taskDescriptor;
        }
    }

}
