package cz.cuni.mff.d3s.been.rest.messages;

import cz.cuni.mff.d3s.been.rest.model.FullServiceInfo;

public class ClusterServiceDetailMessage implements AbstractMessage {

    private FullServiceInfo service;

    public ClusterServiceDetailMessage() {
    }

    public ClusterServiceDetailMessage(FullServiceInfo service) {
        this.service = service;
    }

    @Override
    public String getType() {
        return "SERVICE_DETAIL";
    }

    public FullServiceInfo getService() {
        return service;
    }

    public void setService(FullServiceInfo service) {
        this.service = service;
    }
}
