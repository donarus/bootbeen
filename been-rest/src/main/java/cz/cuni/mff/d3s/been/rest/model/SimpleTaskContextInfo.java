package cz.cuni.mff.d3s.been.rest.model;

import cz.cuni.mff.d3s.been.model.benchmark.PlanItemState;

import java.io.Serializable;
import java.util.List;

public class SimpleTaskContextInfo implements Serializable {
    private String taskContextUuid;
    private String taskContextName;
    private PlanItemState state;
    private List<SimpleTaskInfo> tasks;
    private SimpleTaskInfo finalizerTask;

    public SimpleTaskContextInfo() {
    }

    public SimpleTaskContextInfo(String taskContextUuid, String taskContextName, PlanItemState state) {
        this.taskContextUuid = taskContextUuid;
        this.taskContextName = taskContextName;
        this.state = state;
    }

    public String getTaskContextUuid() {
        return taskContextUuid;
    }

    public void setTaskContextUuid(String taskContextUuid) {
        this.taskContextUuid = taskContextUuid;
    }

    public String getTaskContextName() {
        return taskContextName;
    }

    public void setTaskContextName(String taskContextName) {
        this.taskContextName = taskContextName;
    }

    public PlanItemState getState() {
        return state;
    }

    public void setState(PlanItemState state) {
        this.state = state;
    }

    public List<SimpleTaskInfo> getTasks() {
        return tasks;
    }

    public void setTasks(List<SimpleTaskInfo> tasks) {
        this.tasks = tasks;
    }

    public SimpleTaskInfo getFinalizerTask() {
        return finalizerTask;
    }

    public void setFinalizerTask(SimpleTaskInfo finalizerTask) {
        this.finalizerTask = finalizerTask;
    }
}
