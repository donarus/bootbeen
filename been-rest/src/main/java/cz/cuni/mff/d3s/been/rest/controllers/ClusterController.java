package cz.cuni.mff.d3s.been.rest.controllers;

import cz.cuni.mff.d3s.been.rest.messages.ClusterNodeDetailMessage;
import cz.cuni.mff.d3s.been.rest.messages.ClusterServiceDetailMessage;
import cz.cuni.mff.d3s.been.api.common.BeenNodes;
import cz.cuni.mff.d3s.been.api.common.BeenServices;
import cz.cuni.mff.d3s.been.rest.utils.ClusterCommonUtils;
import cz.cuni.mff.d3s.been.rest.messages.ClusterDataInitMessage;
import cz.cuni.mff.d3s.been.rest.model.SimpleNodeInfo;
import cz.cuni.mff.d3s.been.rest.model.SimpleServiceInfo;
import cz.cuni.mff.d3s.been.rest.response.ResponseWithBody;
import cz.cuni.mff.d3s.been.model.node.NodeInfo;
import cz.cuni.mff.d3s.been.model.service.ServiceInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@Profile("been-rest")
@RequestMapping(value = "/public/cluster")
public class ClusterController {

    @Autowired
    private ClusterCommonUtils clusterCommonUtils;

    @Autowired
    private BeenNodes beenNodes;

    @Autowired
    private BeenServices beenServices;

    @CrossOrigin
    @RequestMapping(value = "/data", method = RequestMethod.GET)
    public ResponseEntity<ResponseWithBody<ClusterDataInitMessage>> listNodes() {
        ResponseWithBody<ClusterDataInitMessage> response = new ResponseWithBody<>();
        List<SimpleNodeInfo> nodes = clusterCommonUtils.getSimpleNodeInfos();
        List<SimpleServiceInfo> services = clusterCommonUtils.getSimpleServiceInfos();
        ClusterDataInitMessage data = new ClusterDataInitMessage(nodes, services);
        response.setData(data);
        return response.toResponseEntity();
    }

    @CrossOrigin
    @RequestMapping(value = "/nodeDetail", method = RequestMethod.GET)
    public ResponseEntity<ResponseWithBody<ClusterNodeDetailMessage>> nodeDetail(@RequestParam("nodeUuid") String nodeUuid) {
        ResponseWithBody<ClusterNodeDetailMessage> response = new ResponseWithBody<>();
        NodeInfo nodeInfo = beenNodes.getByUuid(nodeUuid);
        if (nodeInfo != null) {
            response.setData(new ClusterNodeDetailMessage(clusterCommonUtils.getFullNodeInfo(nodeInfo)));
        } else {
            response.addErrorMessage("Node [" + nodeUuid + "] was not found.");
        }
        return response.toResponseEntity();
    }

    @CrossOrigin
    @RequestMapping(value = "/serviceDetail", method = RequestMethod.GET)
    public ResponseEntity<ResponseWithBody<ClusterServiceDetailMessage>> serviceDetail(@RequestParam("serviceUuid") String serviceUuid) {
        ResponseWithBody<ClusterServiceDetailMessage> response = new ResponseWithBody<>();
        final ServiceInfo service = beenServices.findByServiceUuid(serviceUuid);
        if (service != null) {
            response.setData(new ClusterServiceDetailMessage(clusterCommonUtils.getFullServiceInfo(service)));
        } else {
            response.addErrorMessage("Service [" + serviceUuid + "] was not found.");
        }
        return response.toResponseEntity();
    }


}
