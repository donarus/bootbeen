package cz.cuni.mff.d3s.been.rest.messages;

public class ClusterNodeRemovedMessage implements AbstractMessage {
    private String nodeUuid;

    public ClusterNodeRemovedMessage() {
    }

    public ClusterNodeRemovedMessage(String nodeUuid) {
        this.nodeUuid = nodeUuid;
    }

    public String getNodeUuid() {
        return nodeUuid;
    }

    public void setNodeUuid(String nodeUuid) {
        this.nodeUuid = nodeUuid;
    }

    @Override
    public final String getType() {
        return "NODE_REMOVED";
    }
}
