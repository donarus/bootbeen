package cz.cuni.mff.d3s.been.rest.model;

public class SimpleRuntimeInfo {
    public String runtimeId;
    public String hostname;
    public String port;
    public String type;
    public String system;
    public String hardwareInfo;
    public String exclusive;
    public String tasks;
    public String cpuLoad;
    public String freeMemory;
    public String state;
}
