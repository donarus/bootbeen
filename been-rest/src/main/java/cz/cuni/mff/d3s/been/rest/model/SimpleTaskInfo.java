package cz.cuni.mff.d3s.been.rest.model;

import cz.cuni.mff.d3s.been.model.task.TaskState;

import java.io.Serializable;
import java.util.Date;

public class SimpleTaskInfo implements Serializable {
    private String taskUuid;
    private String taskName;
    private TaskState taskState;
    private Date start;
    private Date end;

    public SimpleTaskInfo() {
    }

    public SimpleTaskInfo(String taskUuid, String taskName, TaskState taskState, Date start, Date end) {
        this.taskUuid = taskUuid;
        this.taskName = taskName;
        this.taskState = taskState;
        this.start = start;
        this.end = end;
    }

    public String getTaskUuid() {
        return taskUuid;
    }

    public void setTaskUuid(String taskUuid) {
        this.taskUuid = taskUuid;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public TaskState getTaskState() {
        return taskState;
    }

    public void setTaskState(TaskState taskState) {
        this.taskState = taskState;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }
}
