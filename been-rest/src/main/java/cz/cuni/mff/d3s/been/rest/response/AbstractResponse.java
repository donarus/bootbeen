package cz.cuni.mff.d3s.been.rest.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public abstract class AbstractResponse<T> {
    protected HttpStatus status = HttpStatus.OK;

    private List<LogMessage> messages = new ArrayList<>();

    public List<LogMessage> getMessages() {
        return messages;
    }

    public void addInfoMessage(String message) {
        addMessage(message, LogLevel.INFO);
    }

    public void addWarnMessage(String message) {
        addMessage(message, LogLevel.WARN);
    }

    public void addErrorMessage(String message) {
        addMessage(message, LogLevel.ERROR);
    }

    public void addErrorMessage(String message, Throwable t) {
        addMessage(message + " " + t.getMessage(), LogLevel.ERROR);
    }

    public void addMessage(String message, LogLevel logLevel) {
        LogMessage responseMessage = new LogMessage();
        responseMessage.setTimestamp(new Date().getTime());
        responseMessage.setMessage(message);
        responseMessage.setLogLevel(logLevel);
        this.messages.add(responseMessage);
    }

    @JsonIgnore
    public void setResponseStatus(HttpStatus status) {
        this.status = status;
    }

    @JsonIgnore
    public abstract ResponseEntity<T> toResponseEntity();
}
