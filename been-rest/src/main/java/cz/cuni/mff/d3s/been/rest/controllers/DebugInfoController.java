package cz.cuni.mff.d3s.been.rest.controllers;

import cz.cuni.mff.d3s.been.api.cluster.ClusterDebugInfoGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Profile("been-rest")
@RequestMapping(value = "/public/debug")
public class DebugInfoController {

    @Autowired
    private ClusterDebugInfoGenerator clusterDebugInfoGenerator;

    @CrossOrigin
    @RequestMapping(value = "/clusterDebugInfo", method = RequestMethod.GET)
    public ResponseEntity<String> list() {
        return ResponseEntity.ok(clusterDebugInfoGenerator.generate());
    }

}
