package cz.cuni.mff.d3s.been.rest.model;

import cz.cuni.mff.d3s.been.model.bpk.BpkId;
import cz.cuni.mff.d3s.been.model.benchmark.BenchmarkState;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class SimpleBenchmarkInfo implements Serializable {
    private String benchmarkUuid;
    private String benchmarkName;
    private BpkId bpkId;
    private BenchmarkState state;
    private List<SimpleTaskContextInfo> taskContexts;
    private SimpleTaskInfo finalizerTask;
    private SimpleTaskInfo generatorTask;
    private Date start;
    private Date end;

    public SimpleBenchmarkInfo() {
    }

    public SimpleBenchmarkInfo(String benchmarkUuid, String benchmarkName, BpkId bpkId, BenchmarkState benchmarkState, Date start, Date end) {
        this.benchmarkUuid = benchmarkUuid;
        this.benchmarkName = benchmarkName;
        this.bpkId = bpkId;
        this.state = benchmarkState;
        this.start = start;
        this.end = end;
    }

    public String getBenchmarkUuid() {
        return benchmarkUuid;
    }

    public void setBenchmarkUuid(String benchmarkUuid) {
        this.benchmarkUuid = benchmarkUuid;
    }

    public String getBenchmarkName() {
        return benchmarkName;
    }

    public void setBenchmarkName(String benchmarkName) {
        this.benchmarkName = benchmarkName;
    }

    public BpkId getBpkId() {
        return bpkId;
    }

    public void setBpkId(BpkId bpkId) {
        this.bpkId = bpkId;
    }

    public BenchmarkState getState() {
        return state;
    }

    public void setState(BenchmarkState state) {
        this.state = state;
    }

    public List<SimpleTaskContextInfo> getTaskContexts() {
        return taskContexts;
    }

    public void setTaskContexts(List<SimpleTaskContextInfo> taskContexts) {
        this.taskContexts = taskContexts;
    }

    public SimpleTaskInfo getFinalizerTask() {
        return finalizerTask;
    }

    public void setFinalizerTask(SimpleTaskInfo finalizerTask) {
        this.finalizerTask = finalizerTask;
    }

    public SimpleTaskInfo getGeneratorTask() {
        return generatorTask;
    }

    public void setGeneratorTask(SimpleTaskInfo generatorTask) {
        this.generatorTask = generatorTask;
    }

    public Date getStart() {
        return start;
    }

    public void setStart(Date start) {
        this.start = start;
    }

    public Date getEnd() {
        return end;
    }

    public void setEnd(Date end) {
        this.end = end;
    }
}
