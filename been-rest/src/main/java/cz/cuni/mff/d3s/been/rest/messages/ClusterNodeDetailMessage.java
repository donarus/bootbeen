package cz.cuni.mff.d3s.been.rest.messages;

import cz.cuni.mff.d3s.been.rest.model.FullNodeInfo;

public class ClusterNodeDetailMessage implements AbstractMessage {

    private FullNodeInfo node;

    public ClusterNodeDetailMessage() {
    }

    public ClusterNodeDetailMessage(FullNodeInfo node) {
        this.node = node;
    }

    @Override
    public String getType() {
        return "NODE_DETAIL";
    }

    public FullNodeInfo getNode() {
        return node;
    }

    public void setNode(FullNodeInfo node) {
        this.node = node;
    }
}
