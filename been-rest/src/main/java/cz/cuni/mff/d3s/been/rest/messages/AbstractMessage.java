package cz.cuni.mff.d3s.been.rest.messages;

public interface AbstractMessage {
    String getType();
}
