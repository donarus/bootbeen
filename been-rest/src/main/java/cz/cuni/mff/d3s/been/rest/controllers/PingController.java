package cz.cuni.mff.d3s.been.rest.controllers;

import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Profile("been-rest")
@RequestMapping(value = "/public/global")
public final class PingController {

    @CrossOrigin
    @RequestMapping(value = "/ping", method = RequestMethod.GET)
    public ResponseEntity<String> list() {
        return ResponseEntity.ok("PING to running Been successful!");
    }

}
