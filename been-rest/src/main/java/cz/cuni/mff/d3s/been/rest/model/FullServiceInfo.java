package cz.cuni.mff.d3s.been.rest.model;

public class FullServiceInfo extends SimpleServiceInfo {

    private String description;

    private String rawServiceInfo;

    public FullServiceInfo() {
        super();
    }

    public FullServiceInfo(String serviceUuid, String nodeUuid) {
        super(serviceUuid, nodeUuid);
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public String getRawServiceInfo() {
        return rawServiceInfo;
    }

    public void setRawServiceInfo(String rawServiceInfo) {
        this.rawServiceInfo = rawServiceInfo;
    }
}
