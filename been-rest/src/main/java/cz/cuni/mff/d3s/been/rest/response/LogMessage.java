package cz.cuni.mff.d3s.been.rest.response;

import java.io.Serializable;

public class LogMessage implements Serializable {
    private long timestamp;
    private String message;
    private LogLevel logLevel;

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setLogLevel(LogLevel logLevel) {
        this.logLevel = logLevel;
    }

    public LogLevel getLogLevel() {
        return logLevel;
    }
}
