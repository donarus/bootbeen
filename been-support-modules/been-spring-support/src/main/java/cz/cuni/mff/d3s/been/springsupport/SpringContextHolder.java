package cz.cuni.mff.d3s.been.springsupport;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

@Service
public class SpringContextHolder implements ApplicationContextAware {
    private static ApplicationContext context;

    @Override
    public void setApplicationContext(ApplicationContext ac) throws BeansException {
        context = ac;
    }

    public static ApplicationContext applicationContext() {
        return context;
    }

    public static <T> T bean(Class<T> clazz) {
        return context.getBean(clazz);
    }
}